Analytics tools
=== 

This is a cloudformation definition of EC2 instance with a docker container for backend tools 
used by analytics team


Installation 
----

- Fill `params.json` template file with needed parameter values for environment configuration: 
```json
[
  {
    "ParameterKey" : "securityGroups" ,
    "ParameterValue" : "sg-xxxxx,sg-yyyyy,sg-..."
  },
  {
    "ParameterKey" : "keyName",
    "ParameterValue" : "xxxxx"
  },
  {
    "ParameterKey" : "subnetId",
    "ParameterValue" : "subnet-xxxxx"
  },
  ...
]
```
- Put `params.json` file into accessible S3 bucket

- See `deploy.sh` for aws command line needed to deploy the stack. 

New EC2 instance will be created with docker composer file located at `/opt/analytics-tools.yml`



Portainer
--
Administration ui for docker (http://server:9000)

Allows launching, monitoring and connection to docker containers.
```bash
docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer
```

Redash
--
Administration and dashboarding of DB connections (http://server:80)

Depends on postgres and redis services and requires:
- Manual admin account creation on first run
- Manual database connection definition


Jupyter Hub
--

Jupyter notebook runner (http://server:8000)

Requires user accounts to be created inside the container:
- Execute `adduser XXX` command inside the container for each team member
- Execute `pip install notebook`

The following file mapping is applied for user homes: EC2 path `/opt/users/jupyter` - Container path `/home`


RStudio
--

RStudio server (http://server:8787)

Requires user accounts to be created inside the container:
- Execute `adduser XXX` command inside the container for each team member

Folder mapping: `/opt/users/rstudio` : `/home`


Nginx
--

Proxy to organise hosted applications by subdomains

- Log in into container console and use this command line for edit the config: 
```bash
apt-get update && apt-get install nano && nano /etc/nginx/conf.d/default.conf
```

- Put these contents: 

```nginx
map $http_upgrade $connection_upgrade {
    default upgrade;
    '' close;
}

upstream redash {
  server redash:5000;
}

upstream jupyter {
  server analytics.aba.solutions:8000;
}

upstream rstudio {
  server analytics.aba.solutions:8787;
}

upstream portainer {
  server analytics.aba.solutions:9000;
}

server {
  listen   80 default;
  gzip on;
  gzip_types *;
  gzip_proxied any;
  server_name redash.analytics.aba.solutions;

  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;

    proxy_pass       http://redash;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
  }
}

server {
  listen   80;
  server_name jupyter.analytics.aba.solutions;
  gzip on;
  gzip_types *;
  gzip_proxied any;

  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;

    proxy_pass       http://jupyter;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
  }
}

server {
  listen   80;
  server_name rstudio.analytics.aba.solutions;
  gzip on;
  gzip_types *;
  gzip_proxied any;

  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;

    proxy_pass       http://rstudio;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
  }
}

server {
  listen   80;
  server_name portainer.analytics.aba.solutions;
  gzip on;
  gzip_types *;
  gzip_proxied any;

  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;

    proxy_pass       http://portainer;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
  }
}
```
