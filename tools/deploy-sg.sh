#!/usr/bin/env bash

aws s3 cp ./templates/ s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/tools/templates --recursive

aws cloudformation create-stack --stack-name analyticsToolsGroup \
  --region eu-west-1 \
  --template-url https://s3-eu-west-1.amazonaws.com/infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/tools/templates/sgroup.yml \
  --parameters https://s3-eu-west-1.amazonaws.com/infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/tools/params.json \
  --capabilities CAPABILITY_NAMED_IAM
