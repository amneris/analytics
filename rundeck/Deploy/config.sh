#!/usr/bin/env bash

echo 'Environment [dev | prod]:'
read environment
export ENV_ANALYTICS_DEPLOY_ENVIRONMENT=$environment

echo 'Redshfit Cluster (no port):'
read cluster
export ENV_ANALYTICS_DEPLOY_CLUSTER=$cluster

echo 'S3 Bucket:'
read bucket
export ENV_ANALYTICS_DEPLOY_BUCKET=$bucket