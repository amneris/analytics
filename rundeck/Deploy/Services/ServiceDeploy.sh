#!/usr/bin/env bash

ServiceName=$( echo "$1");

echo 'Recreate' $ServiceName
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/$ServiceName/;
sh restart-service.sh

echo 'Remember to activate' $ServiceName

echo 'Service Created.'