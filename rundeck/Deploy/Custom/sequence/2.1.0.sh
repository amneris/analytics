#!/usr/bin/env bash


# Before release
# --------------
#
# * Stop SQLQueryService
# * Stop RedshiftLoaderService
# * Stop RedshiftDataTransformerService
# * Stop RedshiftReportingService
# * Stop ChartIO stored table refreshes


cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/Redshift/flyway/;
echo 'Remove obsolete schema staging_monetization'
flyway -schemas=staging_monetization clean

echo 'Recreate staging_payments - migration only up to the file 2.1.0.4!'
flyway -schemas=staging_payments   clean
flyway -schemas=staging_payments   -locations=filesystem:./staging_payments     -target=2.1.0.4  migrate

echo 'Recreate Business Logic'
flyway -schemas=business_logic    clean
flyway -schemas=business_logic    -locations=filesystem:./business_logic      migrate

echo 'Recreate fct_user_lifecycle, fct_campaign_performance; truncate fct_finance'
flyway -schemas=core               -locations=filesystem:./core                 migrate

echo 'Recreate Reporting Logic *after* changes to Core layer'
flyway -schemas=reporting_logic    clean
flyway -schemas=reporting_logic    -locations=filesystem:./reporting_logic      migrate

echo 'Recreate a couple of reporting tables'
flyway -schemas=reporting          -locations=filesystem:./reporting            migrate

echo 'Please continue with the release file 2.1.0b after manually loading the payments history'
