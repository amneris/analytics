#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/TaskRunnerService/; sh deploy.sh
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/DataPipelineFactory/src/; sh build.sh
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/DataPipelineFactory/; sh deploy.sh
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/RedshiftDataTransformerService/; sh restart-service.sh
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/RedshiftLoaderService/; sh restart-service.sh
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/RedshiftMaintenanceService/; sh restart-service.sh
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/RedshiftReportingService/; sh restart-service.sh
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/SQLQueryService/; sh restart-service.sh