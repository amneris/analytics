#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/Redshift/flyway/;

echo 'Cleaning schemas..'
flyway -schemas=business_logic,logic,playground,reporting_logic  clean;

echo 'Migrating schemas..'
flyway -schemas=business_logic     -locations=filesystem:./business_logic     migrate;
flyway -schemas=logic              -locations=filesystem:./logic              migrate;
flyway -schemas=reporting_logic    -locations=filesystem:./reporting_logic    migrate;
flyway -schemas=playground         -locations=filesystem:./playground         migrate;

flyway -schemas=core               -locations=filesystem:./core               migrate;
flyway -schemas=events             -locations=filesystem:./events             migrate;
flyway -schemas=reporting          -locations=filesystem:./reporting          migrate;
flyway -schemas=revenue_model      -locations=filesystem:./revenue_model      migrate;

flyway -schemas=staging_activity   -locations=filesystem:./staging_activity   migrate;
flyway -schemas=staging_countries  -locations=filesystem:./staging_countries  migrate;
flyway -schemas=staging_fees       -locations=filesystem:./staging_fees       migrate;
flyway -schemas=staging_installs   -locations=filesystem:./staging_installs   migrate;
flyway -schemas=staging_investment -locations=filesystem:./staging_investment migrate;
flyway -schemas=staging_partners   -locations=filesystem:./staging_partners   migrate;
flyway -schemas=staging_payments   -locations=filesystem:./staging_payments   migrate;
flyway -schemas=staging_users      -locations=filesystem:./staging_users      migrate;
flyway -schemas=transactional      -locations=filesystem:./transactional      migrate;
