#!/usr/bin/env bash

echo 'Deploying CloudFormation Stack for PushAPI..'
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/StreamDataDeliveryService/PushAPI
sh update-stack.sh

echo 'Deploying lambda code..'
sh deploy-sourcecode.sh

echo 'Success'
