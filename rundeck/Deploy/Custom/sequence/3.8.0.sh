#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/NotificationService/;
echo 'Update SNS'
sh deploy.sh;
sh update-stack-topics.sh;
echo 'SNS Updating, check CloudFormation Dashboard'

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/DataPipelineFactory/; sh deploy.sh


