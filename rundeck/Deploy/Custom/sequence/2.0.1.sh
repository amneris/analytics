#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/S3/buckets/data.analytics.aba/;
echo 'Upload Static Data'
sh upload_static.sh;
echo 'Static Data Uploaded'

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/Redshift/flyway/;
echo 'Delete old Reporting layer, and temporary layer, too'
flyway -schemas=reporting,reporting_new clean
echo 'Create New Reporting layer'
flyway -schemas=reporting        -locations=filesystem:./reporting                 migrate

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/SNS/;
echo 'Update SNS'
sh deploy.sh;
sh update-stack-topics.sh;
echo 'SNS Updating, check CloudFormation Dashboard'

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/DataPipelineFactory/;
echo 'Update DataPipelineFactory Code'
sh deploy.sh
echo 'Update Completed'

echo 'Finished the release'
