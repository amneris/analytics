#!/usr/bin/env bash

echo 'Deploying CloudFormation Stack for Analytics Tools Security group.'
cd ${ENV_ANALYTICS_DEPLOY_PATH}/tools
sh deploy-sg.sh

echo 'Success'
