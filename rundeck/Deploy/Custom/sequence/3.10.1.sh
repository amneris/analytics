#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/Redshift/flyway/;

flyway -schemas=layer_reporting    clean
flyway -schemas=layer_aggregation  clean
flyway -schemas=layer_logic        clean
