#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/IAM/; sh create-stack-firehose-delivery-role.sh
aws cloudformation wait stack-create-complete --stack-name analytics-firehose-delivery-role

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/StreamDataDeliveryService/DeliveryStreams/; sh deploy-stack.sh










