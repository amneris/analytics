#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/NotificationService/;
sh deploy.sh;
sh update-stack-topics.sh;
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/DataPipelineFactory/src; sh build.sh
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/DataPipelineFactory/; sh deploy.sh


