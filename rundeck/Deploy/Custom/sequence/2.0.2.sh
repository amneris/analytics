#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/Redshift/flyway/;
echo 'Reload static pay_supplier data'
flyway -schemas=staging_payments    -locations=filesystem:./staging_payments      migrate

echo 'Finished the release'
