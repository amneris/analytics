#!/usr/bin/env bash


cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/PaymentService/
sh deploy-stack.sh
aws cloudformation wait stack-create-complete --stack-name analytics-PaymentService
sh deploy-sourcecode.sh

