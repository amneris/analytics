#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/APIQueryService/; sh deploy-stack.sh
aws cloudformation wait stack-create-complete --stack-name analytics-APIQueryService

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/APIQueryService/; sh deploy-sourcecode.sh

