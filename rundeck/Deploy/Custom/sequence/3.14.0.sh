#!/usr/bin/env bash

echo 'Deploying CloudFormation Stack for Analytics Tools..'
cd ${ENV_ANALYTICS_DEPLOY_PATH}/tools
sh deploy.sh

echo 'Success'
