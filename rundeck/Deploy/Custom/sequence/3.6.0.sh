#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/IAM/; sh update-stack-lambda-execution-role.sh

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/StreamDataDeliveryService/DeliveryStreams/; sh deploy-stack.sh
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/StreamDataDeliveryService/PushAPI/; sh deploy-stack.sh;
aws cloudformation wait stack-create-complete --stack-name analytics-APIPushService


cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/StreamDataDeliveryService/PushAPI/; sh deploy-sourcecode.sh;
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/StreamDataDeliveryService/; sh restart-service.sh






