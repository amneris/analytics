#!/usr/bin/env bash


cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/NotificationService/; sh deploy.sh; sh update-stack-topics.sh;
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/IAM/; sh update-stack-lambda-execution-role.sh

aws cloudformation wait stack-update-complete --stack-name analytics-SNS-monitorization-topics

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/EventDeliveryService/
sh deploy-stack.sh
sh deploy-sourcecode.sh

aws cloudformation wait stack-create-complete --stack-name analytics-EventDeliveryService

Deploy for the Fanout function
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/EventDeliveryService/src/Fanout
sh fanout deploy --function analytics-EventDeliveryServiceFanout --exec-role analytics-lambda-execution-role
sh fanout hook --function analytics-EventDeliveryServiceFanout --source-type kinesis --source analytics-KinesisBackbone


cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/PaymentService/
sh deploy-stack.sh
aws cloudformation wait stack-create-complete --stack-name analytics-PaymentService
sh deploy-sourcecode.sh

