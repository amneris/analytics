#!/usr/bin/env bash

#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/IAM; sh update-stack-lambda-execution-role.sh
aws cloudformation wait stack-update-complete --stack-name analytics-lambda-execution-role

sh ${ENV_ANALYTICS_DEPLOY_PATH}/aws/APIQueryService/deploy-stack.sh
aws cloudformation wait stack-create-complete --stack-name analytics-APIQueryService
sh ${ENV_ANALYTICS_DEPLOY_PATH}/aws/APIQueryService/deploy-sourcecode.sh
