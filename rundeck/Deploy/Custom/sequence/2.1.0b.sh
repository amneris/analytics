#!/usr/bin/env bash

# Load payment history
# --------------------
#
# * Download the current payments file from S3
# * Replace the current payments file in S3 by uploading the payment history file to S3 folder
# * Recreate and run RedshiftLoaderService
# * Stop RedshiftLoaderService


cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/Redshift/flyway/;
echo 'Copy all acquisition history to the staging table to process it again'
flyway -schemas=staging_users      -locations=filesystem:./staging_users        migrate

echo 'Copy all installs history to the staging table to process it again'
flyway -schemas=staging_installs   -locations=filesystem:./staging_installs     migrate

echo 'Copy all investment history to the staging table to process it again'
flyway -schemas=staging_investment -locations=filesystem:./staging_investment   migrate

echo 'Copy all data to consolidated_payments'
flyway -schemas=staging_payments   -locations=filesystem:./staging_payments     migrate


# Recreate services
# -----------------
#
# * Recreate SQLQueryService
# * Recreate RedshiftLoaderService
# * Recreate RedshiftDataTransformerService
#
# * Start services in the right order and schedule
# * Let the services run until there's fresh payments data in the facts
#
# * Recreate RedshiftReportingService
# * Verify data in the reporting tables
# * Start ChartIO stored table refreshes again


echo 'Finished the release'
