#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/Redshift/flyway/;

DoClean=$( echo "$1");
SchemaName=$( echo "$2");
schema=$( echo "$2" | tr '[:upper:]'  '[:lower:]' );

if test $DoClean = "Yes";
    then echo "Clean "$SchemaName" Schema";
    if test $SchemaName = "Core";
        then echo "---- For security reasons, is not possible to clean CORE schema ----";
        else
            flyway -schemas=$schema clean;
    fi
fi

echo "Migrate "$SchemaName" Schema";
flyway -schemas=$schema        -locations=filesystem:./$schema                 migrate;

echo "Success. Migration Finished";