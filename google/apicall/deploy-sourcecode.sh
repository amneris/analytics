#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/google/apicall/src/APICall
rm -f APICall.zip

cd ${ENV_ANALYTICS_DEPLOY_PATH}/google/apicall/src/APICall
zip -r ${ENV_ANALYTICS_DEPLOY_PATH}/google/apicall/build/APICall.zip *.py *.yaml

cd ${ENV_ANALYTICS_DEPLOY_PATH}/google/apicall/src/APICall/lib

zip -ur ${ENV_ANALYTICS_DEPLOY_PATH}/google/apicall/build/APICall.zip *



aws s3 cp ${ENV_ANALYTICS_DEPLOY_PATH}/google/apicall/build \
s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/GoogleAPICall/build --recursive


aws lambda update-function-code --function-name GoogleAPICall \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key GoogleAPICall/build/APICall.zip
