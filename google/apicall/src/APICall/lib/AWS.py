import boto3
import json
from botocore.exceptions import ClientError
import logging
from collections import namedtuple

CLOUDWATCH = boto3.client('events')
LAMBDA = boto3.client('lambda')
DYNAMODB = boto3.resource('dynamodb')
S3 = boto3.resource('s3')
STS = boto3.client('sts')
SQS = boto3.resource('sqs')
SNS = boto3.client('sns')


def get_config(service_name):
    """Fetches specified service configuration from DynamoDB.

    Append the Account ID to the config so we have it readily available."""

    try:
        config = DYNAMODB.Table('AnalyticsConfig').get_item(Key={'id': service_name})['Item']
        config['AccountId'] = STS.get_caller_identity().get('Account')

        return _json2obj(config)

    except ClientError as e:
        logging.error(e)
        raise


def _json2obj(data):
    """Convert json to namedtuple."""
    return json.loads(json.dumps(data), object_hook=lambda d: namedtuple('Object', d.keys())(*d.values()))


def put_object_S3(content,s3key):
    """Function to keep the downloaded file in the correct S3 bucket."""
    s3bucket = get_config('S3Buckets')

    try:
        object = S3.Object(s3bucket.data[5:], s3key)
        object.put(Body=content)
        logging.info('File {0}/{1} Uploaded to S3'.format(s3bucket.data[5:], s3key))

    except ClientError as e:
        logging.error(e)
        raise


def public_to_SNS(message,attribute,arn_topic,conf):
    """Function to publish a message in SNS given the body, attribute and the correct arn_topic."""
    try:
        response = SNS.publish(
            # TargetArn is the service connected to the correct Lambda
            TargetArn='arn:aws:sns:eu-west-1:{0}:{1}'.format(conf.AccountId,arn_topic),
            Message=message,
            MessageAttributes=attribute)

        return response

    except ClientError as e:
        logging.error(e)
        raise

