-- Sales, even sales which have been refunded are inserted.
-- Refunds have to be taken into account in one of two ways in the Reporting layer:
--
-- 1) Cohorted Revenue = "Sale" type - "Refund" type (using the acquisition date for both)
-- 2) Actual Revenue = "Sale" type - "Refund" type (using the date of sales & date of refund)

START TRANSACTION;

-- Delete sale records for the date range currently in the staging table
DELETE FROM core.fct_campaign_performance
WHERE date_of_activity >= (SELECT MIN(DATE(dateendtransaction))
                           FROM staging_payments.payments)
      AND class_type = 'Sale';

-- Insert the sales currently in the staging table - even the sales that have been refunded
INSERT INTO core.fct_campaign_performance (
  date_of_activity,
  campaign_id,
  country_id,
  class_type,
  class_subtype,
  date_diff,
  range_id,
  quantity,
  amount_gross_eur,
  amount_net_eur
)
  SELECT
    sales.date_of_activity,
    --Campaign and country decided by acquisition
    COALESCE(campaign_id, -1)                         AS campaign_id,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE COALESCE(user_country_id, -1)
    END                                               AS country_id,
    sale_type                                         AS class_type,
    sale_subtype                                      AS class_subtype,
    COALESCE(DATEDIFF(day,
                      acquisition.date_of_activity,
                      sales.date_of_activity), -9999) AS date_diff,
    COALESCE(range_id, -1)                            AS range_id,
    --Each record is one sale
    COUNT(*)                                          AS quantity,
    SUM(amount_gross_eur)                             AS amount_gross_eur,
    SUM(amount_net_eur)                               AS amount_net_eur
  FROM business_logic.sales
    LEFT JOIN business_logic.acquisition USING (user_id)
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
    LEFT JOIN core.dim_range ON DATEDIFF(day,
                                         acquisition.date_of_activity,
                                         sales.date_of_activity) BETWEEN exclusive_range_low AND range_high
    -- Insert the sales currently in the staging table only
    INNER JOIN staging_payments.payments ON payments.id = sale_id
  WHERE sales.date_of_activity IS NOT NULL
  GROUP BY 1, 2, 3, 4, 5, 6, 7
  ORDER BY 1, 4, 5, 7, 6, 2, 3;

END TRANSACTION;
