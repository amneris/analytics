-- Free trials are sales with zero amount
--
-- Free trials are already included in the normal Sales. We add them to be able to report on them separately.
-- Don't ever add Sales and Free Trials together because you would be double counting some of the quantities.
START TRANSACTION;

-- Delete free trial records for the date range currently in the staging table
DELETE FROM core.fct_campaign_performance
WHERE date_of_activity >= (SELECT MIN(DATE(dateendtransaction))
                           FROM staging_payments.payments)
      AND class_type = 'Free Trial';

-- Insert the free trials currently in the staging table
INSERT INTO core.fct_campaign_performance (
  date_of_activity,
  campaign_id,
  country_id,
  class_type,
  class_subtype,
  date_diff,
  range_id,
  quantity,
  amount_gross_eur,
  amount_net_eur
)
  SELECT
    sales.date_of_activity,
    --Campaign and country decided by acquisition
    COALESCE(campaign_id, -1)                         AS campaign_id,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE COALESCE(user_country_id, -1)
    END                                               AS country_id,
    'Free Trial'                                      AS class_type,
    sale_subtype                                      AS class_subtype,
    COALESCE(DATEDIFF(day,
                      acquisition.date_of_activity,
                      sales.date_of_activity), -9999) AS date_diff,
    COALESCE(range_id, -1)                            AS range_id,
    --Each record is one free trial
    COUNT(*)                                          AS quantity,
    SUM(amount_gross_eur)                             AS amount_gross_eur,
    SUM(amount_net_eur)                               AS amount_net_eur
  FROM business_logic.sales
    LEFT JOIN business_logic.acquisition USING (user_id)
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
    LEFT JOIN core.dim_range ON DATEDIFF(day,
                                         acquisition.date_of_activity,
                                         sales.date_of_activity) BETWEEN exclusive_range_low AND range_high
    -- Insert the free trials currently in the staging table only
    INNER JOIN staging_payments.payments ON payments.id = sale_id
  WHERE sales.date_of_activity IS NOT NULL
        -- This is the key condition for free trials, and the only one that separates them from normal Sales
        AND amount_gross_eur BETWEEN 0 AND 0.01
  GROUP BY 1, 2, 3, 4, 5, 6, 7
  ORDER BY 1, 4, 5, 7, 6, 2, 3;

END TRANSACTION;
