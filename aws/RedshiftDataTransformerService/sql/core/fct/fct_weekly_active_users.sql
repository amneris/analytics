INSERT INTO core.fct_weekly_active_users(
  id,
  insert_timestamp,
  week_of_activity,
  device,
  country_id,
  user_type,
  wau,
  sessions
  )
    SELECT
        uuid(),
        current_timestamp,
        *
    FROM (
      SELECT DISTINCT
        week_of_activity,
        device,
        country_id,
        user_type,
        wau,
        sessions
      FROM staging_activity.weekly_active_users
    )
    WHERE
        week_of_activity < date_trunc('week', CURRENT_DATE - INTERVAL '3 hours')
        AND week_of_activity > ( SELECT COALESCE (max(week_of_activity), '0000-01-01') FROM core.fct_weekly_active_users);

-- In order to select the correct rows to append in core, we select to boundaries:
-- Top: week_of_activity less than current_date - 3 hours (correct delays with cooladata)
-- Botom: week_of_activity more than last known date in core

-- ANALYZE core.fct_weekly_active_users;





