-- Insert refunds
--
-- Actual Revenue = "Sale" type - "Refund" type

START TRANSACTION;

-- Delete refund records that are currently in the staging table
DELETE FROM core.fct_finance
WHERE EXISTS(
          SELECT *
          FROM staging_payments.payments
          WHERE id = payment_id
      )
      AND class_type = 'Refund';

-- Insert refunds currently in the staging table
INSERT INTO core.fct_finance (
  date_of_activity,
  payment_id,
  country_id,
  payment_gateway_id,
  is_amex,
  class_type,
  class_subtype,
  amount_gross_eur,
  amount_net_eur,
  discount_eur,
  tax_eur,
  fee_eur,
  currency_code,
  amount_gross_original,
  amount_net_original,
  discount_original,
  tax_original,
  fee_original
)
  SELECT
    refunds.date_of_activity,
    refund_id                         AS payment_id,
    refunds.country_id,
    refunds.payment_gateway_id,
    refunds.is_amex,
    'Refund'                          AS class_type,
    -- Subtype decided by the original sale - when a link between sale and refund exists in business_logic.sales
    COALESCE(sale_subtype, 'Unknown') AS class_subtype,
    refunds.amount_gross_eur,
    refunds.amount_net_eur,
    0                                 AS discount_eur,
    refunds.tax_eur,
    refunds.fee_eur,
    refunds.currency_code,
    refunds.amount_gross_original,
    refunds.amount_net_original,
    0                                 AS discount_original,
    refunds.tax_original,
    refunds.fee_original
  FROM business_logic.refunds
    -- Join to business_logic.sales to figure out classification subtype (First/Next)
    LEFT JOIN business_logic.sales USING (refund_id)
    -- The refund is currently in the staging table
    INNER JOIN staging_payments.payments ON payments.id = refund_id
  WHERE refunds.date_of_activity IS NOT NULL
  ORDER BY 1, 6, 7, 3, 4, 13, 5;

END TRANSACTION;
