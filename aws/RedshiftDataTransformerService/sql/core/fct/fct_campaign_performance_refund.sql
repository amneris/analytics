-- Refunds are added using the refund date, and date_diff between the acquisition and refund.

START TRANSACTION;

-- Delete refund records for the date range currently in the staging table
DELETE FROM core.fct_campaign_performance
WHERE date_of_activity >= (SELECT MIN(DATE(dateendtransaction))
                           FROM staging_payments.payments)
      AND class_type = 'Refund';

-- Insert refunds currently in the staging table
INSERT INTO core.fct_campaign_performance (
  date_of_activity,
  campaign_id,
  country_id,
  class_type,
  class_subtype,
  date_diff,
  range_id,
  quantity,
  amount_gross_eur,
  amount_net_eur
)
  SELECT
    refunds.date_of_activity                            AS date_of_activity,
    -- Campaign and country decided by acquisition
    COALESCE(campaign_id, -1)                           AS campaign_id,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE COALESCE(user_country_id, -1)
    END                                                 AS country_id,
    'Refund'                                            AS class_type,
    -- Subtype decided by the original sale - when a link between sale and refund exists in business_logic.sales
    COALESCE(sale_subtype, 'Unknown')                   AS class_subtype,
    COALESCE(DATEDIFF(day,
                      acquisition.date_of_activity,
                      refunds.date_of_activity), -9999) AS date_diff,
    COALESCE(range_id, -1)                              AS range_id,
    -- Each record is one refund
    COUNT(*)                                            AS quantity,
    SUM(refunds.amount_gross_eur)                       AS amount_gross_eur,
    SUM(refunds.amount_net_eur)                         AS amount_net_eur
  FROM business_logic.refunds
    LEFT JOIN business_logic.acquisition USING (user_id)
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
    LEFT JOIN core.dim_range ON DATEDIFF(day,
                                         acquisition.date_of_activity,
                                         refunds.date_of_activity) BETWEEN exclusive_range_low AND range_high
    -- Join to business_logic.sales to figure out classification subtype (First/Next)
    LEFT JOIN business_logic.sales USING (refund_id)
    -- The refund is currently in the staging table
    INNER JOIN staging_payments.payments ON payments.id = refund_id
  WHERE refunds.date_of_activity IS NOT NULL
  GROUP BY 1, 2, 3, 5, 6, 7
  ORDER BY 1, 4, 5, 7, 6, 2, 3;

END TRANSACTION;
