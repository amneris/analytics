-- Insert all sales that haven't been refunded as "Clean Sale".
-- Clean sales are comparable to ("Sale" type - "Sale Adjustment" type) in fct_campaign_performance.
--
-- For clean sales we can define the Last sale, and we can detect product Upgrades and Downgrades.
-- "Clean Sale" type can't be used for Actual Revenue reports, for that use ("Sale" type - "Refund" type) instead.

START TRANSACTION;

-- Delete all clean sale records for users that have a sale or refund in the staging table
DELETE FROM core.fct_user_lifecycle
WHERE class_type = 'Clean Sale'
      AND EXISTS(
          SELECT *
          FROM staging_payments.payments
          WHERE status IN (30, 40)
                AND userid = user_id
      );

-- Insert all clean sale records for users whose sale or refund is currently in the staging table
INSERT INTO core.fct_user_lifecycle (
  date_of_activity,
  user_id,
  campaign_id,
  country_id,
  product_id,
  payment_gateway_id,
  class_type,
  class_subtype,
  product_change_type,
  begin_date,
  date_of_acquisition,
  acquisition_country_id,
  amount_gross_eur,
  amount_net_eur,
  discount_eur,
  days_out,
  days_to_cancel
)
  SELECT
    clean_sales.date_of_activity,
    user_id,
    -- Campaign decided by acquisition
    COALESCE(campaign_id, -1) AS campaign_id,
    -- Payment country
    country_id,
    product_id,
    payment_gateway_id,
    'Clean Sale'              AS class_type,
    -- Number of the clean sale: 1 = First. 2, 3, 4, 5 and so on = Next.
    clean_sale_number         AS class_subtype,
    product_change_type,
    begin_date,
    COALESCE(acquisition.date_of_activity, '1900-01-01' :: DATE)
                              AS date_of_acquisition,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE COALESCE(acquisition.user_country_id, -1)
    END                       AS acquisition_country_id,
    amount_gross_eur,
    amount_net_eur,
    discount_gross_eur        AS discount_eur,
    days_out,
    0                         AS days_to_cancel
  FROM business_logic.clean_sales
    LEFT JOIN business_logic.acquisition USING (user_id)
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
    -- Users having sales or refunds in the staging table
    INNER JOIN (
                 SELECT DISTINCT userid AS user_id
                 FROM staging_payments.payments
                 WHERE status IN (30, 40)
               ) AS payments USING (user_id)
  WHERE clean_sales.date_of_activity IS NOT NULL
        AND clean_sales.begin_date IS NOT NULL
  ORDER BY 2, 7, 1, 8, 3, 4, 6, 5;

END TRANSACTION;
