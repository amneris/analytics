-- Sale Adjustment type is used to adjust sales so that we have clean sales.
-- Sale Adjustments are completely different from Refunds, and shouldn't be confused.

-- Sale Adjustments are added using the date and date_diff of the original sale.
-- Amount is simply the negative value of the original sale - not calculated based on the refund record.
-- Only refunds that can be matched with sales are considered (only business_logic.sales is used).
-- “Sale” type - “Sale Adjustment” type = “Clean Sale” type in fct_user_lifecycle

START TRANSACTION;

-- Delete sale adjustments corresponding to the refunds that are in the staging table
DELETE FROM core.fct_campaign_performance
WHERE class_type = 'Sale Adjustment';

-- Insert sale adjustments corresponding to the refunds that are in the staging table
INSERT INTO core.fct_campaign_performance (
  date_of_activity,
  campaign_id,
  country_id,
  class_type,
  class_subtype,
  date_diff,
  range_id,
  quantity,
  amount_gross_eur,
  amount_net_eur
)
  SELECT
    -- Date of the original sale
    sales.date_of_activity                            AS date_of_activity,
    -- Campaign and country decided by acquisition
    COALESCE(campaign_id, -1)                         AS campaign_id,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE COALESCE(user_country_id, -1)
    END                                               AS country_id,
    'Sale Adjustment'                                 AS class_type,
    -- Subtype decided by the original sale
    sale_subtype                                      AS class_subtype,
    -- date_diff of the original sale
    COALESCE(DATEDIFF(day,
                      acquisition.date_of_activity,
                      sales.date_of_activity), -9999) AS date_diff,
    COALESCE(range_id, -1)                            AS range_id,
    -- Each record is one sale adjustment
    COUNT(*)                                          AS quantity,
    SUM(amount_gross_eur)                             AS amount_gross_eur,
    SUM(amount_net_eur)                               AS amount_net_eur
  FROM business_logic.sales
    LEFT JOIN business_logic.acquisition USING (user_id)
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
    LEFT JOIN core.dim_range ON DATEDIFF(day,
                                         acquisition.date_of_activity,
                                         sales.date_of_activity) BETWEEN exclusive_range_low AND range_high
  WHERE refunded
        AND sales.date_of_activity IS NOT NULL
        AND sales.refund_date IS NOT NULL
  GROUP BY 1, 2, 3, 5, 6, 7
  ORDER BY 1, 4, 5, 7, 6, 2, 3;

END TRANSACTION;
