START TRANSACTION;

-- Delete acquisition records for the date range currently in the staging table
DELETE FROM core.fct_user_lifecycle
WHERE date_of_activity >= (SELECT MIN(DATE(entrydate))
                           FROM staging_users.users)
      AND class_type = 'Acquisition';

-- Insert the acquisitions currently in the staging table
INSERT INTO core.fct_user_lifecycle (
  date_of_activity,
  user_id,
  campaign_id,
  country_id,
  product_id,
  payment_gateway_id,
  class_type,
  class_subtype,
  product_change_type,
  begin_date,
  date_of_acquisition,
  acquisition_country_id,
  amount_gross_eur,
  amount_net_eur,
  discount_eur,
  days_out,
  days_to_cancel
)
  SELECT
    COALESCE(date_of_activity, '1900-01-01' :: DATE) AS date_of_activity,
    user_id,
    COALESCE(campaign_id, -1)                        AS campaign_id,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE user_country_id
    END                                              AS country_id,
    0                                                AS product_id,
    0                                                AS payment_gateway_id,
    'Acquisition'                                    AS class_type,
    'Acquisition'                                    AS class_subtype,
    'Not applicable'                                 AS product_change_type,
    COALESCE(date_of_activity, '1900-01-01' :: DATE) AS begin_date,
    COALESCE(date_of_activity, '1900-01-01' :: DATE) AS date_of_acquisition,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE user_country_id
    END                                              AS acquisition_country_id,
    0                                                AS amount_gross_eur,
    0                                                AS amount_net_eur,
    0                                                AS discount_eur,
    0                                                AS days_out,
    0                                                AS days_to_cancel
  FROM business_logic.acquisition
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
    -- Insert acquisitions currently in the staging table only
    INNER JOIN staging_users.users ON users.id = user_id
  ORDER BY 2, 7, 1, 8, 3, 4, 6, 5;

END TRANSACTION;
