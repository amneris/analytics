START TRANSACTION;

-- DELETE FROM INVESTMENT
DELETE FROM core.fct_campaign_performance
WHERE date_of_activity >= (SELECT MIN(date_of_activity)
                           FROM business_logic.investment)
      AND class_type = 'Investment';

-- APPEND INVESTMENT
INSERT INTO core.fct_campaign_performance (
  date_of_activity,
  campaign_id,
  country_id,
  class_type,
  class_subtype,
  date_diff,
  range_id,
  quantity,
  amount_gross_eur,
  amount_net_eur
)
  SELECT
    date_of_activity,
    COALESCE(campaign_id, -1)      AS campaign_id,
    investment.campaign_country_id AS country_id,
    'Investment'                   AS class_type,
    'Investment'                   AS class_subtype,
    0                              AS date_diff,
    0                              AS range_id,
    --For investments we insert one as quantity although it doesn't really mean much, and depends on grain
    1                              AS quantity,
    SUM(investment)                AS amount_gross_eur,
    SUM(investment)                AS amount_net_eur
  FROM business_logic.investment
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
  WHERE investment IS NOT NULL
  GROUP BY 1, 2, 3
  HAVING SUM(investment) > 0
  ORDER BY 1, 4, 5, 7, 6, 2, 3;

END TRANSACTION;
