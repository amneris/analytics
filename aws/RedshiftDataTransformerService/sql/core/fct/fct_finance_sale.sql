-- Insert all sales, even sales which have been refunded, as "Sale".
-- These records are comparable to "Sale" in fct_campaign_performance ans fct_user_lifecycle.
--
-- Actual Revenue = "Sale" type - "Refund" type

START TRANSACTION;

-- Delete sale records that are currently in the staging table
DELETE FROM core.fct_finance
WHERE EXISTS(
          SELECT *
          FROM staging_payments.payments
          WHERE id = payment_id
      )
      AND class_type = 'Sale';

-- Insert sales currently in the staging table
INSERT INTO core.fct_finance (
  date_of_activity,
  payment_id,
  country_id,
  payment_gateway_id,
  is_amex,
  class_type,
  class_subtype,
  amount_gross_eur,
  amount_net_eur,
  discount_eur,
  tax_eur,
  fee_eur,
  currency_code,
  amount_gross_original,
  amount_net_original,
  discount_original,
  tax_original,
  fee_original
)
  SELECT
    sales.date_of_activity,
    sale_id                 AS payment_id,
    country_id,
    payment_gateway_id,
    is_amex,
    sale_type               AS class_type,
    sale_subtype            AS class_subtype,
    amount_gross_eur,
    amount_net_eur,
    discount_gross_eur      AS discount_eur,
    tax_eur,
    fee_eur,
    currency_code,
    amount_gross_original,
    amount_net_original,
    discount_gross_original AS discount_original,
    tax_original,
    fee_original
  FROM business_logic.sales
    -- The sale is currently in the staging table
    INNER JOIN staging_payments.payments ON payments.id = sale_id
  WHERE sales.date_of_activity IS NOT NULL
  ORDER BY 1, 6, 7, 3, 4, 13, 5;

END TRANSACTION;
