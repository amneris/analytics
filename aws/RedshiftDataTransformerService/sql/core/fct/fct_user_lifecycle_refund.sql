START TRANSACTION;

-- Delete refund records for the date range currently in the staging table
DELETE FROM core.fct_user_lifecycle
WHERE date_of_activity >= (SELECT MIN(DATE(dateendtransaction))
                           FROM staging_payments.payments)
      AND class_type = 'Refund';

-- Insert refunds currently in the staging table
INSERT INTO core.fct_user_lifecycle (
  date_of_activity,
  user_id,
  campaign_id,
  country_id,
  product_id,
  payment_gateway_id,
  class_type,
  class_subtype,
  product_change_type,
  begin_date,
  date_of_acquisition,
  acquisition_country_id,
  amount_gross_eur,
  amount_net_eur,
  discount_eur,
  days_out,
  days_to_cancel
)
  SELECT
    refunds.date_of_activity,
    refunds.user_id,
    -- Campaign decided by acquisition
    COALESCE(campaign_id, -1)         AS campaign_id,
    -- Payment country
    refunds.country_id,
    refunds.product_id,
    refunds.payment_gateway_id,
    'Refund'                          AS class_type,
    -- Subtype decided by the original sale - when a link between sale and refund exists in business_logic.sales
    COALESCE(sale_subtype, 'Unknown') AS class_subtype,
    'Not applicable'                  AS product_change_type,
    refunds.date_of_activity          AS begin_date,
    COALESCE(acquisition.date_of_activity, '1900-01-01' :: DATE)
                                      AS date_of_acquisition,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE COALESCE(acquisition.user_country_id, -1)
    END                               AS acquisition_country_id,
    refunds.amount_gross_eur,
    refunds.amount_net_eur,
    0                                 AS discount_eur,
    0                                 AS days_out,
    0                                 AS days_to_cancel
  FROM business_logic.refunds
    LEFT JOIN business_logic.acquisition USING (user_id)
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
    -- Join to business_logic.sales to figure out classification subtype (First/Next)
    LEFT JOIN business_logic.sales USING (refund_id)
    -- The refund is currently in the staging table
    INNER JOIN staging_payments.payments ON payments.id = refund_id
  WHERE refunds.date_of_activity IS NOT NULL
  ORDER BY 2, 7, 1, 8, 3, 4, 6, 5;

END TRANSACTION;
