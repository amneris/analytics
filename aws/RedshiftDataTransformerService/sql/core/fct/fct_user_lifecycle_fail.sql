START TRANSACTION;

-- Delete fail records for the date range currently in the staging table
DELETE FROM core.fct_user_lifecycle
WHERE date_of_activity >= (SELECT MIN(DATE(dateendtransaction))
                           FROM staging_payments.payments)
      AND class_type = 'Fail';

-- Insert fails currently in the staging table
INSERT INTO core.fct_user_lifecycle (
  date_of_activity,
  user_id,
  campaign_id,
  country_id,
  product_id,
  payment_gateway_id,
  class_type,
  class_subtype,
  product_change_type,
  begin_date,
  date_of_acquisition,
  acquisition_country_id,
  amount_gross_eur,
  amount_net_eur,
  discount_eur,
  days_out,
  days_to_cancel
)
  SELECT
    fails.date_of_activity,
    user_id,
    -- Campaign decided by acquisition
    COALESCE(campaign_id, -1) AS campaign_id,
    -- Payment country
    country_id,
    product_id,
    payment_gateway_id,
    'Fail'                    AS class_type,
    -- Fail can only happen for next (recurring) sales
    'Next'                    AS class_subtype,
    'Not applicable'          AS product_change_type,
    fails.date_of_activity    AS begin_date,
    COALESCE(acquisition.date_of_activity, '1900-01-01' :: DATE)
                              AS date_of_acquisition,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE COALESCE(acquisition.user_country_id, -1)
    END                       AS acquisition_country_id,
    amount_gross_eur,
    amount_net_eur,
    discount_gross_eur        AS discount_eur,
    0                         AS days_out,
    0                         AS days_to_cancel
  FROM business_logic.fails
    LEFT JOIN business_logic.acquisition USING (user_id)
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
    -- The fail is currently in the staging table
    INNER JOIN staging_payments.payments ON payments.id = fail_id
  WHERE fails.date_of_activity IS NOT NULL
  ORDER BY 2, 7, 1, 8, 3, 4, 6, 5;

END TRANSACTION;
