START TRANSACTION;

-- First delete data that corresponds to the date ranges in the staging area.
-- Note that we're looking at the individual date ranges for each activity type.
-- Add a new delete statement for any additional activity types here.

-- Delete UNITS events
DELETE FROM core.fct_premium_activity
WHERE date_of_activity >= (SELECT MIN(date_of_activity)
                           FROM staging_activity.daily_premium_activity
                           WHERE class_type = 'Units'
)
      AND class_type = 'Units';

-- Delete VIDEOS events
DELETE FROM core.fct_premium_activity
WHERE date_of_activity >= (SELECT MIN(date_of_activity)
                           FROM staging_activity.daily_premium_activity
                           WHERE class_type = 'Videos'
)
      AND class_type = 'Videos';

-- Delete STUDY events
DELETE FROM core.fct_premium_activity
WHERE date_of_activity >= (SELECT MIN(date_of_activity)
                           FROM staging_activity.daily_premium_activity
                           WHERE class_type = 'Study'
)
      AND class_type = 'Study';

-- Insert premium activity currently in the staging table
INSERT INTO core.fct_premium_activity (

  date_of_activity,
  user_id,
  device,
  class_type,

  campaign_id,
  country_id,
  date_of_acquisition,
  date_of_conversion,
  conversion_product_id,

  current_product_id,
  sale_number,

  quantity

)
  SELECT
    date_of_activity,
    user_id,
    device,
    class_type,

    campaign_id,
    country_id,
    date_of_acquisition,
    date_of_conversion,
    conversion_product_id,

    current_product_id,
    sale_number,

    quantity


  FROM business_logic.fct_premium_activity;

END TRANSACTION;

ANALYZE core.fct_premium_activity;
