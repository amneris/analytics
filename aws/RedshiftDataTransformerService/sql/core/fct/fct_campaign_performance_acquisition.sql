START TRANSACTION;

-- Delete acquisitions for the date range currently in the staging table
DELETE FROM core.fct_campaign_performance
WHERE date_of_activity >= (SELECT MIN(DATE(entrydate))
                           FROM staging_users.users
)
      AND class_type = 'Acquisition';

-- Insert acquisitions currently in the staging table
INSERT INTO core.fct_campaign_performance (
  date_of_activity,
  campaign_id,
  country_id,
  class_type,
  class_subtype,
  date_diff,
  range_id,
  quantity,
  amount_gross_eur,
  amount_net_eur
)
  SELECT
    COALESCE(date_of_activity, '1900-01-01' :: DATE) AS date_of_activity,
    COALESCE(campaign_id, -1)                        AS campaign_id,
    CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
      THEN campaign_country_id
    ELSE user_country_id
    END                                              AS country_id,
    'Acquisition'                                    AS class_type,
    'Acquisition'                                    AS class_subtype,
    0                                                AS date_diff,
    0                                                AS range_id,
    --One record in the users table is one acquisition
    COUNT(*)                                         AS quantity,
    0                                                AS amount_gross_eur,
    0                                                AS amount_net_eur
  FROM business_logic.acquisition
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
    -- Insert acquisitions currently in the staging table only
    INNER JOIN staging_users.users ON users.id = user_id
  GROUP BY 1, 2, 3
  ORDER BY 1, 4, 5, 7, 6, 2, 3;

END TRANSACTION;
