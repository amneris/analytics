START TRANSACTION;

-- DELETE FROM iOS INSTALLS
DELETE FROM core.fct_campaign_performance
WHERE date_of_activity >= (SELECT MIN(date_of_activity)
                           FROM business_logic.installs
                           WHERE source_table = 'staging_installs.installs_ios')
      AND class_type = 'Installs'
      AND class_subtype = 'iOS';

-- INSERT iOS INSTALLS
INSERT INTO core.fct_campaign_performance (
  date_of_activity,
  campaign_id,
  country_id,
  class_type,
  class_subtype,
  date_diff,
  range_id,
  quantity,
  amount_gross_eur,
  amount_net_eur
)
  SELECT
    date_of_activity,
    COALESCE(campaign_id, -1) AS campaign_id,
    CASE WHEN installs.campaign_country_id NOT IN (-1, 0)
      THEN installs.campaign_country_id
    ELSE installs_country_id
    END                       AS country_id,
    'Installs'                AS class_type,
    'iOS'                     AS class_subtype,
    0                         AS date_diff,
    0                         AS range_id,
    SUM(installs)             AS quantity,
    0                         AS amount_gross_eur,
    0                         AS amount_net_eur
  FROM business_logic.installs
    LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)
  WHERE source_table = 'staging_installs.installs_ios'
  GROUP BY 1, 2, 3
  ORDER BY 1, 4, 5, 7, 6, 2, 3;

END TRANSACTION;
