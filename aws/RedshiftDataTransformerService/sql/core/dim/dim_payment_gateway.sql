START TRANSACTION;

UPDATE core.dim_payment_gateway
SET payment_gateway = name,
  last_changed_date = current_timestamp
FROM staging_payments.pay_suppliers
WHERE payment_gateway_id = id
      AND payment_gateway != name
      AND payment_gateway_id NOT IN (-1, 0);


INSERT INTO core.dim_payment_gateway (
  payment_gateway_id,
  payment_gateway,
  channel_id,
  last_changed_date,
  created_date
)
  SELECT
    id         AS payment_gateway_id,
    name       AS payment_gateway,
    CASE id
    -- Android-PlayStore --> Android
    WHEN 12
      THEN 4
    -- iOs-AppStore --> iOS
    WHEN 9
      THEN 3
    -- Anything else --> Web
    ELSE 2
    END               AS channel_id,
    current_timestamp AS last_changed_date,
    current_timestamp AS created_date
  FROM staging_payments.pay_suppliers
  WHERE NOT EXISTS(
      SELECT *
      FROM core.dim_payment_gateway
      WHERE payment_gateway_id = id
  );

END TRANSACTION;
