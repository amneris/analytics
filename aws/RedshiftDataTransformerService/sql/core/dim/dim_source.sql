START TRANSACTION;

UPDATE core.dim_source
SET source          = src.source,
  last_changed_date = SYSDATE
FROM (
       SELECT
         id       AS source_id,
         nametype AS source
       FROM staging_partners.sources_list
     ) AS src
WHERE dim_source.source_id = src.source_id
      AND (dim_source.source != src.source)
      AND dim_source.source_id NOT IN (-1, 0);


INSERT INTO core.dim_source (
  source_id,
  source,
  last_changed_date,
  created_date
)
  SELECT
    id       AS source_id,
    nametype AS source,
    SYSDATE  AS last_changed_date,
    SYSDATE  AS created_date
  FROM staging_partners.sources_list
  WHERE NOT EXISTS(
      SELECT *
      FROM core.dim_source
      WHERE source_id = id
  );

END TRANSACTION;
