START TRANSACTION;

--Look up network for acquisition and install records using the investment records
--Most of the times a partner_id only has one parent network and we can use it
UPDATE core.dim_campaign
SET
  network                   = src.network,
  last_changed_date         = current_timestamp,
  last_changed_source_table = src.source_table
FROM (
       SELECT
         partner_id,
         MAX(network)      AS network,
         MAX(source_table) AS source_table
       --Look up network from Investment records only
       FROM business_logic.investment AS src
       --Discard incorrect or undefined partners
       WHERE partner_id NOT IN (-1, 0)
       GROUP BY
         partner_id
       --Only consider partners which resolve to a single network
       HAVING COUNT(DISTINCT network) = 1
     ) AS src
WHERE dim_campaign.partner_id = src.partner_id
      AND dim_campaign.network != src.network
      AND dim_campaign.channel_id NOT IN (-1, 0);

END TRANSACTION;
