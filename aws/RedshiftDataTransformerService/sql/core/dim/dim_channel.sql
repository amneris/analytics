START TRANSACTION;

UPDATE core.dim_channel
SET channel         = namechannel,
  last_changed_date = current_timestamp
FROM staging_partners.aba_partners_channels
WHERE channel_id = idchannel
      AND channel != namechannel
      AND channel_id NOT IN (-1, 0);


INSERT INTO core.dim_channel (
  channel_id,
  channel,
  last_changed_date,
  created_date
)
  SELECT
    idchannel         AS channel_id,
    namechannel       AS channel,
    current_timestamp AS last_changed_date,
    current_timestamp AS created_date
  FROM staging_partners.aba_partners_channels
  WHERE NOT EXISTS(
      SELECT *
      FROM core.dim_channel
      WHERE channel_id = idchannel
  );

END TRANSACTION;
