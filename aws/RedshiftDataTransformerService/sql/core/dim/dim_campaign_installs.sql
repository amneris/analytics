START TRANSACTION;

UPDATE core.dim_campaign
SET
  channel_id                = src.channel_id,
  campaign_country_id       = src.campaign_country_id,
  last_changed_date         = current_timestamp,
  last_changed_source_table = src.source_table
FROM (
       SELECT
         campaign,
         partner_id,
         source_id,
         MIN(channel_id)          AS channel_id,
         MIN(campaign_country_id) AS campaign_country_id,
         MAX(source_table)        AS source_table
       FROM business_logic.installs AS src
       GROUP BY
         campaign,
         partner_id,
         source_id
     ) AS src
WHERE dim_campaign.campaign = src.campaign
      AND dim_campaign.partner_id = src.partner_id
      AND dim_campaign.source_id = src.source_id
      AND (dim_campaign.channel_id != src.channel_id
           OR dim_campaign.campaign_country_id != src.campaign_country_id)
      AND dim_campaign.channel_id NOT IN (-1, 0);

END TRANSACTION;


START TRANSACTION;

INSERT INTO core.dim_campaign (
  campaign,
  partner_id,
  source_id,
  network,
  channel_id,
  campaign_country_id,
  last_changed_date,
  created_date,
  last_changed_source_table,
  created_source_table
)
  SELECT
    campaign,
    partner_id,
    source_id,
    'Unknown',
    MIN(channel_id)          AS channel_id,
    MIN(campaign_country_id) AS campaign_country_id,
    current_timestamp,
    current_timestamp,
    MAX(source_table)        AS last_changed_source_table,
    MAX(source_table)        AS created_source_table
  FROM business_logic.installs AS src
  WHERE NOT EXISTS(
      SELECT *
      FROM core.dim_campaign
      WHERE campaign = src.campaign
            AND partner_id = src.partner_id
            AND source_id = src.source_id
  )
  GROUP BY
    campaign,
    partner_id,
    source_id;

END TRANSACTION;
