START TRANSACTION;

UPDATE core.dim_partner
SET partner         = src.partner,
  partner_type      = src.partner_type,
  partner_category  = src.partner_category,
  last_changed_date = current_timestamp
FROM (
       SELECT
         idpartner    AS partner_id,
         LOWER(name)  AS partner,
         nametype     AS partner_type,
         namecategory AS partner_category
       FROM staging_partners.aba_partners_list
         INNER JOIN staging_partners.aba_partners_types USING (idtype)
         INNER JOIN staging_partners.aba_partners_categories USING (idcategory)
     ) AS src
WHERE dim_partner.partner_id = src.partner_id
      AND (dim_partner.partner != src.partner
           OR dim_partner.partner_type != src.partner_type
           OR dim_partner.partner_category != src.partner_category)
      AND dim_partner.partner_id NOT IN (-1, 0);


INSERT INTO core.dim_partner (
  partner_id,
  partner,
  partner_type,
  partner_category,
  last_changed_date,
  created_date
)
  SELECT
    idpartner         AS partner_id,
    LOWER(name)       AS partner,
    nametype          AS partner_type,
    namecategory      AS partner_category,
    current_timestamp AS last_changed_date,
    current_timestamp AS created_date
  FROM staging_partners.aba_partners_list AS src
    INNER JOIN staging_partners.aba_partners_types USING (idtype)
    INNER JOIN staging_partners.aba_partners_categories USING (idcategory)
  WHERE NOT EXISTS(
      SELECT *
      FROM core.dim_partner
      WHERE partner_id = idpartner
  );

END TRANSACTION;
