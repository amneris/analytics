START TRANSACTION;

DELETE FROM staging_users.consolidated_users
USING staging_users.users
WHERE staging_users.consolidated_users.id = staging_users.users.id;


INSERT INTO staging_users.consolidated_users
  SELECT *
  FROM staging_users.users;


END TRANSACTION;

ANALYZE staging_users.consolidated_users;
