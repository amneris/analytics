START TRANSACTION;

DELETE FROM staging_investment.consolidated_investment
USING staging_investment.investment
WHERE consolidated_investment.day = investment.day;


INSERT INTO staging_investment.consolidated_investment
  SELECT *
  FROM staging_investment.investment;

END TRANSACTION;

ANALYZE staging_investment.consolidated_investment;
