START TRANSACTION;

DELETE FROM staging_activity.consolidated_daily_active_users
USING staging_activity.daily_active_users
WHERE consolidated_daily_active_users.day = daily_active_users.day;


INSERT INTO staging_activity.consolidated_daily_active_users
  SELECT *
  FROM staging_activity.daily_active_users;

END TRANSACTION;

ANALYZE staging_activity.consolidated_daily_active_users;
