START TRANSACTION;

-- Delete each activity type individually because they might have data for different date ranges.
-- Add a new delete statement for any additional activity types here.

DELETE FROM staging_activity.consolidated_daily_premium_activity
USING staging_activity.daily_premium_activity
WHERE consolidated_daily_premium_activity.date_of_activity = daily_premium_activity.date_of_activity
      AND consolidated_daily_premium_activity.class_type = 'Units'
      AND daily_premium_activity.class_type = 'Units';

DELETE FROM staging_activity.consolidated_daily_premium_activity
USING staging_activity.daily_premium_activity
WHERE consolidated_daily_premium_activity.date_of_activity = daily_premium_activity.date_of_activity
      AND consolidated_daily_premium_activity.class_type = 'Videos'
      AND daily_premium_activity.class_type = 'Videos';

DELETE FROM staging_activity.consolidated_daily_premium_activity
USING staging_activity.daily_premium_activity
WHERE consolidated_daily_premium_activity.date_of_activity = daily_premium_activity.date_of_activity
      AND consolidated_daily_premium_activity.class_type = 'Study'
      AND daily_premium_activity.class_type = 'Study';


INSERT INTO staging_activity.consolidated_daily_premium_activity
  SELECT *
  FROM staging_activity.daily_premium_activity;

END TRANSACTION;

ANALYZE staging_activity.consolidated_daily_premium_activity;
