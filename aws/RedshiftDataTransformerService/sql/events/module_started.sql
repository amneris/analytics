INSERT INTO events.module_started (

  SELECT
    uuid()                                          AS event_id,
    activity_timestamp                              AS event_timestamp,
    current_timestamp                               AS insert_timestamp,

    user_id                                         AS user_id,
    device                                          AS device,
    class_type                                      AS event_type,
    quantity                                        AS quantity

  FROM (
         SELECT DISTINCT
           date_of_activity :: TIMESTAMP AS activity_timestamp,
           user_id,
           device,
           class_type,
           quantity
         FROM staging_activity.daily_premium_activity
         WHERE class_type = 'Videos'
       ) t

  WHERE
    activity_timestamp < CURRENT_TIMESTAMP - INTERVAL '5 hours'
    AND activity_timestamp > (SELECT COALESCE(max(event_timestamp), '0000-01-01')::TIMESTAMP FROM events.module_started)
  ORDER BY event_timestamp

);