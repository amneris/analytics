START TRANSACTION;

DELETE FROM staging_installs.consolidated_installs_ios
USING staging_installs.installs_ios
WHERE consolidated_installs_ios.day = installs_ios.day;


INSERT INTO staging_installs.consolidated_installs_ios
  SELECT *
  FROM staging_installs.installs_ios;

END TRANSACTION;

ANALYZE staging_installs.consolidated_installs_ios;
