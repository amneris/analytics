START TRANSACTION;

DELETE FROM staging_installs.consolidated_installs_android
USING staging_installs.installs_android
WHERE consolidated_installs_android.day = installs_android.day;


INSERT INTO staging_installs.consolidated_installs_android
  SELECT *
  FROM staging_installs.installs_android;

END TRANSACTION;

ANALYZE staging_installs.consolidated_installs_android;
