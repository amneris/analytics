START TRANSACTION;

DELETE FROM staging_payments.consolidated_payments
USING staging_payments.payments
WHERE consolidated_payments.id = payments.id;


INSERT INTO staging_payments.consolidated_payments
  SELECT *
  FROM staging_payments.payments;

END TRANSACTION;

ANALYZE staging_payments.consolidated_payments;
