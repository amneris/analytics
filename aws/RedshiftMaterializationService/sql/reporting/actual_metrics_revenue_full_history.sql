-- All history, aggregated to month to keep the record count down

TRUNCATE TABLE reporting.actual_metrics_revenue_full_history_temp;

INSERT INTO reporting.actual_metrics_revenue_full_history_temp (
  SELECT
    DATE_TRUNC('month', date_of_activity) :: DATE AS month_of_activity,
    product,
    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,
    payment_country,
    payment_cgroup,
    payment_region,
    payment_continent,
    payment_main_market,
    payment_channel,
    sale_type,
    detailed_sale_type,
    SUM(revenue)                                  AS revenue,
    SUM(revenue_gross)                            AS revenue_gross,
    SUM(sales)                                    AS sales,
    SUM(free_trials)                              AS free_trials
  FROM logic.report_actual_metrics_base
  WHERE date_of_activity <= CURRENT_DATE
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
);

-- Commented out for now while we compare the data
-- START TRANSACTION;
-- ALTER TABLE reporting.actual_metrics_revenue_full_history        RENAME TO actual_metrics_revenue_full_history_backup;
-- ALTER TABLE reporting.actual_metrics_revenue_full_history_temp   RENAME TO actual_metrics_revenue_full_history;
-- ALTER TABLE reporting.actual_metrics_revenue_full_history_backup RENAME TO actual_metrics_revenue_full_history_temp;
-- END TRANSACTION;
