-- Day level view for actual metrics, last two years only to keep the record count down

TRUNCATE TABLE reporting.actual_metrics_recent_revenue_temp;

INSERT INTO reporting.actual_metrics_recent_revenue_temp (
  SELECT
    date_of_activity,
    product,
    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,
    payment_country,
    payment_cgroup,
    payment_region,
    payment_continent,
    payment_main_market,
    payment_channel,
    sale_type,
    detailed_sale_type,
    revenue,
    revenue_gross,
    sales,
    free_trials
  FROM logic.report_actual_metrics_base
  WHERE date_of_activity BETWEEN DATE_TRUNC('month', ADD_MONTHS(CURRENT_DATE, -24)) AND CURRENT_DATE
);

-- Commented out for now while we compare the data
-- START TRANSACTION;
-- ALTER TABLE reporting.actual_metrics_recent_revenue        RENAME TO actual_metrics_recent_revenue_backup;
-- ALTER TABLE reporting.actual_metrics_recent_revenue_temp   RENAME TO actual_metrics_recent_revenue;
-- ALTER TABLE reporting.actual_metrics_recent_revenue_backup RENAME TO actual_metrics_recent_revenue_temp;
-- END TRANSACTION;
