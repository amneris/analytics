-- Actual revenue & sales, investment and margin

TRUNCATE TABLE reporting.actual_metrics_profitability_temp;

INSERT INTO reporting.actual_metrics_profitability_temp (
  SELECT
    date_of_activity,
    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,
    revenue_total,
    investment,
    margin,
    sales_total,
    revenue_first_early,
    revenue_first_late,
    revenue_first_unknown,
    revenue_first_total,
    revenue_next,
    sales_first_early,
    sales_first_late,
    sales_first_unknown,
    sales_first_total,
    sales_next,
    free_trials_first_early,
    free_trials_first_late,
    free_trials_first_unknown,
    free_trials_first_total,
    free_trials_next
  FROM logic.report_actual_metrics_profitability
  WHERE date_of_activity <= CURRENT_DATE
);

-- Commented out for now while we compare the data
-- BEGIN TRANSACTION;
-- ALTER TABLE reporting.actual_metrics_profitability        RENAME TO actual_metrics_profitability_backup;
-- ALTER TABLE reporting.actual_metrics_profitability_temp   RENAME TO actual_metrics_profitability;
-- ALTER TABLE reporting.actual_metrics_profitability_backup RENAME TO actual_metrics_profitability_temp;
-- END TRANSACTION;
