-- Table for Actual Revenue for Finance reporting
-- Includes the buyer payment currency and separate metrics for tax and fee. Last 36 months.

TRUNCATE TABLE reporting.finance_actual_revenue_temp;

INSERT INTO reporting.finance_actual_revenue_temp (
  SELECT
    payment_date,
    payment_country,
    payment_cgroup,
    payment_region,
    payment_continent,
    payment_market,
    payment_gateway,
    payment_channel,
    payment_currency,
    sale_type,
    flag_amex_card,
    sales_amount_buyercurrency_fee,
    sales_amount_buyercurrency_gross,
    sales_amount_buyercurrency_net,
    sales_amount_buyercurrency_tax,
    sales_discount_buyercurrency,
    sales_amount_eur_fee,
    sales_amount_eur_gross,
    sales_amount_eur_net,
    sales_amount_eur_tax,
    sales_discount_eur,
    sale_units,
    return_amount_buyercurrency_fee,
    return_amount_buyercurrency_gross,
    return_amount_buyercurrency_net,
    return_amount_buyercurrency_tax,
    return_amount_eur_fee,
    return_amount_eur_gross,
    return_amount_eur_net,
    return_amount_eur_tax,
    return_units,
    revenue_amount_buyercurrency_fee,
    revenue_amount_buyercurrency_gross,
    revenue_amount_buyercurrency_net,
    revenue_amount_buyercurrency_tax,
    revenue_amount_eur_fee,
    revenue_amount_eur_gross,
    revenue_amount_eur_net,
    revenue_amount_eur_tax,
    net_units
  FROM logic.report_finance_actual_revenue
);

-- Commented out for now while we compare the data
-- START TRANSACTION;
-- ALTER TABLE reporting.finance_actual_revenue        RENAME TO finance_actual_revenue_backup;
-- ALTER TABLE reporting.finance_actual_revenue_temp   RENAME TO finance_actual_revenue;
-- ALTER TABLE reporting.finance_actual_revenue_backup RENAME TO finance_actual_revenue_temp;
-- END TRANSACTION;
