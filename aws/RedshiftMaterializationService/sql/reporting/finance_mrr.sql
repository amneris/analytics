-- Table for calculating the MRR (Finance view)

TRUNCATE TABLE reporting.finance_mrr_temp;

INSERT INTO reporting.finance_mrr_temp (
  SELECT
    month_of_payment,
    month_of_conversion,
    month_of_mrr,
    months_after_conversion,
    product,
    sale_type,
    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,
    payment_country,
    payment_cgroup,
    payment_region,
    payment_continent,
    payment_main_market,
    payment_channel,
    active_subscriptions,
    net_mrr,
    gross_mrr
  FROM logic.report_finance_mrr
  WHERE month_of_payment <= CURRENT_DATE
);

-- Commented out for now while we compare the data
-- START TRANSACTION;
-- ALTER TABLE reporting.finance_mrr        RENAME TO finance_mrr_backup;
-- ALTER TABLE reporting.finance_mrr_temp   RENAME TO finance_mrr;
-- ALTER TABLE reporting.finance_mrr_backup RENAME TO finance_mrr_temp;
-- END TRANSACTION;
