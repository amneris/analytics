-- Table for calculating Retention Rate.
-- Data should be aggregated to monthly level for more reliable results.

TRUNCATE TABLE reporting.retention_performance_retention_rate_temp;

INSERT INTO reporting.retention_performance_retention_rate_temp (
  SELECT
    date_of_payment,
    product,
    country,
    cgroup,
    region,
    continent,
    main_market,
    channel,
    sale,
    sale_no_change,
    sale_upgrade,
    sale_downgrade,
    sale_expremium,
    previous_sale,
    revenue,
    revenue_no_change,
    revenue_upgrade,
    revenue_downgrade,
    revenue_expremium,
    previous_revenue
  FROM logic.report_retention_performance_retention_rate
  WHERE date_of_payment <= CURRENT_DATE
);

-- Commented out for now while we compare the data
-- START TRANSACTION;
-- ALTER TABLE reporting.retention_performance_retention_rate        RENAME TO retention_performance_retention_rate_backup;
-- ALTER TABLE reporting.retention_performance_retention_rate_temp   RENAME TO retention_performance_retention_rate;
-- ALTER TABLE reporting.retention_performance_retention_rate_backup RENAME TO retention_performance_retention_rate_temp;
-- END TRANSACTION;
