-- Acquisition component for Conversion Rate analysis, especially to analyze CR distribution over time
-- Last 36 months, on cgroup level (renamed to country in ChartIO)

TRUNCATE TABLE reporting.conversion_performance_acquisition_temp;

INSERT INTO reporting.conversion_performance_acquisition_temp (
  SELECT
    date_of_activity,
    cgroup,
    region,
    continent,
    main_market,
    channel,
    acquisition
  FROM logic.report_conversion_performance_acquisition
);

-- Commented out for now while we compare the data
-- START TRANSACTION;
-- ALTER TABLE reporting.conversion_performance_acquisition        RENAME TO conversion_performance_acquisition_backup;
-- ALTER TABLE reporting.conversion_performance_acquisition_temp   RENAME TO conversion_performance_acquisition;
-- ALTER TABLE reporting.conversion_performance_acquisition_backup RENAME TO conversion_performance_acquisition_temp;
-- END TRANSACTION;
