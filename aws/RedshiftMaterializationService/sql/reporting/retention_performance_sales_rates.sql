-- Table for calculating cancel rate, fail rate, refund rate, success rate; and average ticket for each

TRUNCATE TABLE reporting.retention_performance_sales_rates_temp;

INSERT INTO reporting.retention_performance_sales_rates_temp (
  SELECT
    date_of_activity,
    payment_type,
    product,
    country,
    cgroup,
    region,
    continent,
    main_market,
    channel,
    days_to_cancel,
    fail_units,
    fail_revenue,
    fail_revenue_gross,
    cancel_units,
    cancel_revenue,
    cancel_revenue_gross,
    refund_units,
    refund_revenue,
    refund_revenue_gross,
    sales_units,
    sales_revenue,
    sales_revenue_gross,
    sales_discount,
    net_units,
    net_revenue,
    net_revenue_gross,
    net_discount
  FROM logic.report_retention_performance_sales_rates
  WHERE date_of_activity <= CURRENT_DATE
);

-- Commented out for now while we compare the data
-- START TRANSACTION;
-- ALTER TABLE reporting.retention_performance_sales_rates        RENAME TO retention_performance_sales_rates_backup;
-- ALTER TABLE reporting.retention_performance_sales_rates_temp   RENAME TO retention_performance_sales_rates;
-- ALTER TABLE reporting.retention_performance_sales_rates_backup RENAME TO retention_performance_sales_rates_temp;
-- END TRANSACTION;
