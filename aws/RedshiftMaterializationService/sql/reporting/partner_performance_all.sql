-- Materialize Partner Performance view into table, last six months only to keep the row count down

TRUNCATE reporting.partner_performance_all_temp;

INSERT INTO reporting.partner_performance_all_temp (
  SELECT
    date_of_activity,

    -- No campaign_id for partner performance
    -- No campaign for partner performance
    -- No adgroup_id for partner performance
    -- No adgroup for partner performance
    -- No ad_source for partner performance (use network - on partner level - instead)
    -- No table_source for partner performance

    partner_id,
    partner,
    partner_group,
    partner_type,
    partner_category,
    source_id,
    network,
    channel,
    country,
    cgroup,
    region,
    continent,
    main_market,

    impressions,
    clicks,
    investment,
    acquisition,
    installs,

    revenue,
    revenue_1d,
    revenue_3d,
    revenue_7d,
    revenue_15d,
    revenue_30d,
    revenue_90d,
    revenue_180d,
    revenue_360d,

    conversions,
    conversions_1d,
    conversions_3d,
    conversions_7d,
    conversions_15d,
    conversions_30d,
    conversions_90d,
    conversions_180d,
    conversions_360d
  FROM logic.report_partner_performance_all
  WHERE date_of_activity BETWEEN DATE_TRUNC('month', ADD_MONTHS(CURRENT_DATE, -6)) AND CURRENT_DATE
);

-- Commented out for now while we compare the data
-- BEGIN TRANSACTION;
-- ALTER TABLE reporting.partner_performance_all        RENAME TO partner_performance_all_backup;
-- ALTER TABLE reporting.partner_performance_all_temp   RENAME TO partner_performance_all;
-- ALTER TABLE reporting.partner_performance_all_backup RENAME TO partner_performance_all_temp;
-- END TRANSACTION;
