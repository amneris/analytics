-- Materialize Adgroup Performance view into table, last three months only to keep the row count down

TRUNCATE reporting.adgroup_performance_all_temp;

INSERT INTO reporting.adgroup_performance_all_temp (
  SELECT
    date_of_activity,

    campaign_id,
    campaign_name,
    adgroup_id,
    adgroup_name,
    ad_source,
    table_source,

    partner_id,
    partner_name,
    partner_group,
    partner_type,
    partner_category,
    -- No source_id for adgroup performance (it's obsolete)
    -- No network for adgroup performance (use ad_source - on campaign level - instead)
    channel,
    country,
    cgroup,
    region,
    continent,
    main_market,

    impressions,
    clicks,
    investment,
    acquisition,
    installs,

    revenue_total,
    revenue_1d,
    revenue_3d,
    revenue_7d,
    revenue_15d,
    revenue_30d,
    revenue_90d,
    revenue_180d,
    revenue_360d,

    conversions_total,
    conversions_1d,
    conversions_3d,
    conversions_7d,
    conversions_15d,
    conversions_30d,
    conversions_90d,
    conversions_180d,
    conversions_360d
  FROM logic.report_adgroup_performance_all
  WHERE date_of_activity BETWEEN DATE_TRUNC('month', ADD_MONTHS(CURRENT_DATE, -3)) AND CURRENT_DATE
);

BEGIN TRANSACTION;
ALTER TABLE reporting.adgroup_performance_all        RENAME TO adgroup_performance_all_backup;
ALTER TABLE reporting.adgroup_performance_all_temp   RENAME TO adgroup_performance_all;
ALTER TABLE reporting.adgroup_performance_all_backup RENAME TO adgroup_performance_all_temp;
END TRANSACTION;
