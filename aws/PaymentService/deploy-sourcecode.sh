#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/PaymentService/build
rm -f CurrencyXChange.zip



cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/LambdaZipMaker
sh build.sh PaymentService CurrencyXChange



aws s3 cp ${ENV_ANALYTICS_DEPLOY_PATH}/aws/PaymentService/build \
s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/PaymentService/build --recursive


aws lambda update-function-code --function-name analytics-PaymentServiceCurrencyXChange \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key PaymentService/build/CurrencyXChange.zip


