import boto3
import xml.etree.ElementTree as ET
import AWS
import json
import time, pytz, datetime
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


cube_tag = '{http://www.ecb.int/vocabulary/2002-08-01/eurofxref}Cube'
date_format = "%Y-%m-%d %H:%M:%S"
tz = 'Europe/Berlin' #BCE Timezone
xRate_publish_time = '16:00:00' #BCE publishes xRates at 4pm CET


def lambda_handler(event, context):

    currency = event['currency']
    result = {"originalCurrency": currency}

    if currency == "EUR":
        result["time"] = time.strftime(date_format)
        result["xRate"] = 1.0

    else:

        bucket = AWS.get_config('S3Buckets')['data'].replace("s3://", "")
        key = AWS.get_config('PaymentService')['latestCurrencyXratesKey']

        data = boto3.client('s3').get_object(Bucket=bucket, Key=key)['Body'].read()
        cube = ET.fromstring(data).find(cube_tag)[0]

        naive = datetime.datetime.strptime('{} {}'.format(cube.attrib['time'], xRate_publish_time), date_format)
        result['time'] =  pytz.timezone(tz).localize(naive, is_dst=None).astimezone(pytz.utc).strftime(date_format)

        max_age = datetime.datetime.now() - datetime.timedelta(hours=24)

        if datetime.datetime.strptime(result['time'], date_format) < max_age:
            logger.warning('Que hacemos si la fecha es vieja')

        xRates = {item.attrib['currency']: item.attrib['rate'] for item in cube}

        if currency in xRates:
            result["xRate"] = float(xRates.get(currency))
        else:
            result["Error"] = "xRate does not exist"

    return json.dumps(result)






