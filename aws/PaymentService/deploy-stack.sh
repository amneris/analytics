#!/usr/bin/env bash


aws s3 cp ${ENV_ANALYTICS_DEPLOY_PATH}/aws/PaymentService/definition/templates s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/PaymentService/definition/templates --recursive

aws cloudformation create-stack --stack-name analytics-PaymentService \
  --region eu-west-1 \
  --template-url https://s3-eu-west-1.amazonaws.com/infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/PaymentService/definition/templates/CurrencyXChange/function.template


