def get_rule_elements(rule_id, conf):
    """Extract the elements from the configuration of the rule from DynamoDB."""
    for rule in conf['rules'] :
        if rule['id'] == rule_id :
            return rule['settings']

    return None


def get_loader_class(class_name):
    """Instantiate dynamically the loader class"""
    module = __import__('{0}Call'.format(class_name))
    class_ = getattr(module, '{0}Call'.format(class_name))
    return class_()

