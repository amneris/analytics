-- View for calculating the MRR (Finance view)
CREATE OR REPLACE VIEW logic.report_finance_mrr AS (
  SELECT
    month_of_payment,
    month_of_conversion,
    ADD_MONTHS(month_of_payment, month_offset) :: DATE                        AS month_of_mrr,
    DATE_DIFF('month', month_of_conversion, ADD_MONTHS(month_of_payment,
                                                       month_offset) :: DATE) AS months_after_conversion,
    product_id :: VARCHAR(5)                                                  AS product,
    sale_type,

    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,

    payment_country,
    payment_cgroup,
    payment_region,
    payment_continent,
    payment_main_market,
    payment_channel,

    SUM(active_subscriptions)                                                 AS active_subscriptions,
    SUM(net_revenue / divisor)                                                AS net_mrr,
    SUM(gross_revenue / divisor)                                              AS gross_mrr
  FROM (
         SELECT
           DATE_TRUNC('month', payment_timestamp) :: DATE AS month_of_payment,
           DATE_TRUNC('month', conversion_date) :: DATE   AS month_of_conversion,
           product_id,
           sale_type,

           -- For acquisition attributes use:
           -- 1) Investment partner country & channel when they are defined
           -- 2) As a fallback, use acquisition partner country & channel
           CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
             THEN inv_country.country
           ELSE acq_country.country END                   AS acquisition_country,
           CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
             THEN inv_country.cgroup
           ELSE acq_country.cgroup END                    AS acquisition_cgroup,
           CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
             THEN inv_country.region
           ELSE acq_country.region END                    AS acquisition_region,
           CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
             THEN inv_country.continent
           ELSE acq_country.continent END                 AS acquisition_continent,
           CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
             THEN inv_country.main_market
           ELSE acq_country.main_market END               AS acquisition_main_market,
           CASE WHEN inv_channel.channel_id IS NOT NULL AND inv_channel.channel_id != -1
             THEN inv_channel.channel
           ELSE acq_channel.channel END                   AS acquisition_channel,

           pay_country.country                            AS payment_country,
           pay_country.cgroup                             AS payment_cgroup,
           pay_country.region                             AS payment_region,
           pay_country.main_market                        AS payment_main_market,
           pay_country.continent                          AS payment_continent,
           pay_channel.channel                            AS payment_channel,

           amount_net_eur                                 AS net_revenue,
           amount_gross_eur                               AS gross_revenue,
           1                                              AS active_subscriptions
         FROM logic.payments
           INNER JOIN logic.dim_country AS acq_country ON acquisition_country_id = acq_country.country_id
           INNER JOIN logic.dim_channel AS acq_channel ON acquisition_channel_id = acq_channel.channel_id
           INNER JOIN logic.dim_country AS pay_country ON payment_country_id = pay_country.country_id
           INNER JOIN logic.dim_payment_gateway USING (payment_gateway_id)
           INNER JOIN logic.dim_channel AS pay_channel ON dim_payment_gateway.channel_id = pay_channel.channel_id

           -- Try to look up investment partner for acquisition country & acquisition channel
           LEFT JOIN logic.lkp_investment_partner USING (campaign_id)
           LEFT JOIN logic.dim_partner AS inv_partner ON investment_partner_id = inv_partner.partner_id
           LEFT JOIN logic.dim_country AS inv_country ON inv_partner.partner_country_id = inv_country.country_id
           LEFT JOIN logic.dim_channel AS inv_channel ON inv_partner.partner_channel_id = inv_channel.channel_id

         -- Only clean sales
         WHERE clean_sale_number IS NOT NULL
       ) AS clean_sales
    INNER JOIN core.lkp_mrr_series USING (product_id)
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
);
