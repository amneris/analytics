-- A lookup view to resolve investment partner_id using campaign_id.
-- Investment partner_id is at the end of a campaign name and separated by three underscores.
-- For example, the investment partner_id of the campaign "FB_APP_and_ES_[p=400108]___600076" is 600076.

CREATE OR REPLACE VIEW logic.lkp_investment_partner AS (
  SELECT DISTINCT
    campaign_id,
    CASE WHEN campaign_id = '-1'
      THEN -1
    ELSE COALESCE(FIRST_VALUE(partner_id IGNORE NULLS)
                  OVER (
                    PARTITION BY campaign_id
                    ORDER BY date_of_activity DESC
                    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ), -1) END AS investment_partner_id
  FROM logic.investment
);
