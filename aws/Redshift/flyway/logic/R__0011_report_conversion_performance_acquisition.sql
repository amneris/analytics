-- Acquisition component for Conversion Rate analysis, especially to analyze CR distribution over time
-- Last 36 months, on cgroup level (renamed to country in ChartIO)

CREATE OR REPLACE VIEW logic.report_conversion_performance_acquisition AS (
  SELECT
    TRUNC(event_timestamp)           AS date_of_activity,

    -- For acquisition attributes use:
    -- 1) Investment partner country & channel when they are defined
    -- 2) As a fallback, use acquisition partner country & channel
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.cgroup
    ELSE acq_country.cgroup END      AS cgroup,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.region
    ELSE acq_country.region END      AS region,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.continent
    ELSE acq_country.continent END   AS continent,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.main_market
    ELSE acq_country.main_market END AS main_market,
    CASE WHEN inv_channel.channel_id IS NOT NULL AND inv_channel.channel_id != -1
      THEN inv_channel.channel
    ELSE acq_channel.channel END     AS channel,

    COUNT(*)                         AS acquisition
  FROM events.user_registered
    INNER JOIN logic.dim_country AS acq_country
      ON CASE WHEN acquisition_country_id IS NULL OR acquisition_country_id = 0
      THEN -1
         ELSE acquisition_country_id END = country_id
    INNER JOIN logic.dim_channel AS acq_channel ON COALESCE(acquisition_channel_id, -1) = channel_id

    -- Try to look up investment partner for acquisition country & acquisition channel
    LEFT JOIN logic.lkp_investment_partner USING (campaign_id)
    LEFT JOIN logic.dim_partner AS inv_partner ON investment_partner_id = inv_partner.partner_id
    LEFT JOIN logic.dim_country AS inv_country ON inv_partner.partner_country_id = inv_country.country_id
    LEFT JOIN logic.dim_channel AS inv_channel ON inv_partner.partner_channel_id = inv_channel.channel_id

  WHERE TRUNC(event_timestamp) BETWEEN DATE_TRUNC('month', ADD_MONTHS(CURRENT_DATE, -36)) AND CURRENT_DATE
  GROUP BY 1, 2, 3, 4, 5, 6
);
