-- View for Actual Revenue for Finance reporting
-- Includes the buyer payment currency and separate metrics for tax and fee. Last 36 months.

CREATE OR REPLACE VIEW logic.report_finance_actual_revenue AS (
  SELECT
    TRUNC(payment_timestamp)                                           AS payment_date,
    country                                                            AS payment_country,
    cgroup                                                             AS payment_cgroup,
    region                                                             AS payment_region,
    continent                                                          AS payment_continent,
    main_market                                                        AS payment_market,
    payment_gateway,
    channel                                                            AS payment_channel,
    currency_code                                                      AS payment_currency,
    sale_type,
    CASE WHEN is_amex
      THEN 'Yes'
    ELSE 'No' END                                                      AS flag_amex_card,
    -- Sale metrics
    COALESCE(SUM(CASE WHEN status = 30 THEN fee_original               END), 0) AS sales_amount_buyercurrency_fee,
    COALESCE(SUM(CASE WHEN status = 30 THEN amount_gross_original      END), 0) AS sales_amount_buyercurrency_gross,
    COALESCE(SUM(CASE WHEN status = 30 THEN amount_net_original        END), 0) AS sales_amount_buyercurrency_net,
    COALESCE(SUM(CASE WHEN status = 30 THEN tax_original               END), 0) AS sales_amount_buyercurrency_tax,
    COALESCE(SUM(CASE WHEN status = 30 THEN discount_gross_original    END), 0) AS sales_discount_buyercurrency,
    COALESCE(SUM(CASE WHEN status = 30 THEN fee_eur                    END), 0) AS sales_amount_eur_fee,
    COALESCE(SUM(CASE WHEN status = 30 THEN amount_gross_eur           END), 0) AS sales_amount_eur_gross,
    COALESCE(SUM(CASE WHEN status = 30 THEN amount_net_eur             END), 0) AS sales_amount_eur_net,
    COALESCE(SUM(CASE WHEN status = 30 THEN tax_eur                    END), 0) AS sales_amount_eur_tax,
    COALESCE(SUM(CASE WHEN status = 30 THEN discount_gross_eur         END), 0) AS sales_discount_eur,
    COALESCE(SUM(CASE WHEN status = 30 THEN 1                          END), 0) AS sale_units,
    -- Return metrics
    COALESCE(SUM(CASE WHEN status = 40 THEN ABS(fee_original)          END), 0) AS return_amount_buyercurrency_fee,
    COALESCE(SUM(CASE WHEN status = 40 THEN ABS(amount_gross_original) END), 0) AS return_amount_buyercurrency_gross,
    COALESCE(SUM(CASE WHEN status = 40 THEN ABS(amount_net_original)   END), 0) AS return_amount_buyercurrency_net,
    COALESCE(SUM(CASE WHEN status = 40 THEN ABS(tax_original)          END), 0) AS return_amount_buyercurrency_tax,
    COALESCE(SUM(CASE WHEN status = 40 THEN ABS(fee_eur)               END), 0) AS return_amount_eur_fee,
    COALESCE(SUM(CASE WHEN status = 40 THEN ABS(amount_gross_eur)      END), 0) AS return_amount_eur_gross,
    COALESCE(SUM(CASE WHEN status = 40 THEN ABS(amount_net_eur)        END), 0) AS return_amount_eur_net,
    COALESCE(SUM(CASE WHEN status = 40 THEN ABS(tax_eur)               END), 0) AS return_amount_eur_tax,
    COALESCE(SUM(CASE WHEN status = 40 THEN 1                          END), 0) AS return_units,
    -- Net metrics, simply sum up because refund amounts are negative
    COALESCE(SUM(fee_original), 0)                                     AS revenue_amount_buyercurrency_fee,
    COALESCE(SUM(amount_gross_original), 0)                            AS revenue_amount_buyercurrency_gross,
    COALESCE(SUM(amount_net_original), 0)                              AS revenue_amount_buyercurrency_net,
    COALESCE(SUM(tax_original), 0)                                     AS revenue_amount_buyercurrency_tax,
    COALESCE(SUM(fee_eur), 0)                                          AS revenue_amount_eur_fee,
    COALESCE(SUM(amount_gross_eur), 0)                                 AS revenue_amount_eur_gross,
    COALESCE(SUM(amount_net_eur), 0)                                   AS revenue_amount_eur_net,
    COALESCE(SUM(tax_eur), 0)                                          AS revenue_amount_eur_tax,
    -- Deduct refunds (40) from payments (30)
    SUM(CASE
          WHEN status = 30 THEN  1
          WHEN status = 40 THEN -1
          ELSE 0
        END)                                                           AS net_units
  FROM logic.payments
    INNER JOIN logic.dim_country ON payment_country_id = country_id
    INNER JOIN logic.dim_payment_gateway USING (payment_gateway_id)
    INNER JOIN logic.dim_channel USING (channel_id)
  WHERE TRUNC(payment_timestamp) BETWEEN DATE_TRUNC('month', ADD_MONTHS(CURRENT_DATE, -36)) AND CURRENT_DATE
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
);
