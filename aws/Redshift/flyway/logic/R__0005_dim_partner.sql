-- Partner dimension
CREATE OR REPLACE VIEW logic.dim_partner AS (
  SELECT
    idpartner                     AS partner_id,
    name                          AS partner,
    nametype                      AS partner_type,
    namecategory                  AS partner_category,
    aba_partners_groups.namegroup AS partner_group,
    CASE
    WHEN idcountry IN (0, -1)
      THEN -1
    ELSE idcountry END            AS partner_country_id,
    CASE WHEN idchannel = 1
      THEN -1
    ELSE idchannel END            AS partner_channel_id
  FROM staging_partners.aba_partners_list
    INNER JOIN staging_partners.aba_partners_types USING (idtype)
    INNER JOIN staging_partners.aba_partners_categories USING (idcategory)
    INNER JOIN staging_partners.aba_partners_groups USING (idgroup)
  WHERE idpartner != -1

  UNION ALL

  SELECT
    -1        AS partner_id,
    'Unknown' AS partner,
    'Unknown' AS partner_type,
    'Unknown' AS partner_category,
    'Unknown' AS partner_group,
    -1        AS partner_country_id,
    -1        AS partner_channel_id
);
