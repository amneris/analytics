-- Conversion component for Conversion Rate analysis, especially to analyze CR distribution over time
--
-- Takes last 36 months of conversions.
-- To keep the record count down the results are on cgroup level (renamed to Country in ChartIO).

CREATE OR REPLACE VIEW logic.report_conversion_performance_conversions AS (
  SELECT
    TRUNC(acquisition_timestamp)         AS date_of_acquisition,
    TRUNC(payment_timestamp)             AS date_of_conversion,
    product_id :: VARCHAR(5)             AS product,

    -- For acquisition attributes use:
    -- 1) Investment partner country & channel when they are defined
    -- 2) As a fallback, use acquisition partner country & channel
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.cgroup
    ELSE acq_country.cgroup END          AS acquisition_cgroup,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.region
    ELSE acq_country.region END          AS acquisition_region,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.continent
    ELSE acq_country.continent END       AS acquisition_continent,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.main_market
    ELSE acq_country.main_market END     AS acquisition_main_market,
    CASE WHEN inv_channel.channel_id IS NOT NULL AND inv_channel.channel_id != -1
      THEN inv_channel.channel
    ELSE acq_channel.channel END         AS acquisition_channel,

    pay_country.cgroup                   AS payment_cgroup,
    pay_country.region                   AS payment_region,
    pay_country.continent                AS payment_continent,
    pay_country.main_market              AS payment_main_market,
    pay_channel.channel                  AS payment_channel,
    date_diff                            AS days_to_conversion,
    FLOOR(date_diff / 7)                 AS weeks_to_conversion,
    FLOOR(date_diff / 30)                AS months_to_conversion,
    -- "3 Bucket Days to Pay" in ChartIO
    CASE
    WHEN date_diff BETWEEN 0 AND 15
      THEN '0-15'
    WHEN date_diff BETWEEN 16 AND 30
      THEN '16-30'
    WHEN date_diff > 30
      THEN '30+'
    END                                  AS days_to_pay,
    -- "12 Bucket Days to Pay" in ChartIO
    COALESCE(exclusive_range, 'Unknown') AS grouped_days_to_pay,

    -- Deduct refunds (40) from payments (30)
    SUM(CASE
        WHEN status = 30
          THEN 1
        WHEN status = 40
          THEN -1
        ELSE 0
        END)                             AS conversions,
    COALESCE(SUM(amount_net_eur), 0)     AS revenue
  FROM logic.payments
    INNER JOIN logic.dim_country AS acq_country ON acquisition_country_id = acq_country.country_id
    INNER JOIN logic.dim_channel AS acq_channel ON acquisition_channel_id = acq_channel.channel_id
    INNER JOIN logic.dim_country AS pay_country ON payment_country_id = pay_country.country_id
    INNER JOIN logic.dim_payment_gateway USING (payment_gateway_id)
    INNER JOIN logic.dim_channel AS pay_channel ON dim_payment_gateway.channel_id = pay_channel.channel_id
    LEFT JOIN core.dim_range ON date_diff BETWEEN exclusive_range_low AND range_high

    -- Try to look up investment partner for acquisition country & acquisition channel
    LEFT JOIN logic.lkp_investment_partner USING (campaign_id)
    LEFT JOIN logic.dim_partner AS inv_partner ON investment_partner_id = inv_partner.partner_id
    LEFT JOIN logic.dim_country AS inv_country ON inv_partner.partner_country_id = inv_country.country_id
    LEFT JOIN logic.dim_channel AS inv_channel ON inv_partner.partner_channel_id = inv_channel.channel_id

  -- Conversions only
  WHERE clean_sale_number = 1
        -- Filter out inconsistent data
        AND date_diff IS NOT NULL AND date_diff >= 0
        AND TRUNC(payment_timestamp) BETWEEN DATE_TRUNC('month', ADD_MONTHS(CURRENT_DATE, -36)) AND CURRENT_DATE
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
);
