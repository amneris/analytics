-- A lookup view to resolve network using partner_id.
-- Returns the latest ad_source value per each partner_id.

CREATE OR REPLACE VIEW logic.lkp_network AS (
  SELECT DISTINCT
    partner_id,
    CASE WHEN partner_id = -1
      THEN 'Unknown'
    ELSE COALESCE(FIRST_VALUE(ad_source IGNORE NULLS)
                  OVER (
                    PARTITION BY partner_id
                    ORDER BY date_of_activity DESC
                    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ), 'Unknown') END AS network
  FROM logic.investment
);
