-- View for calculating cancel rate, fail rate, refund rate, success rate; and average ticket for each

CREATE OR REPLACE VIEW logic.report_retention_performance_sales_rates AS (
  SELECT
    date_of_activity,
    payment_type,
    product_id :: VARCHAR(5)               AS product,
    country,
    cgroup,
    region,
    continent,
    main_market,
    channel,
    CASE
      WHEN days_to_cancel > 720 THEN '01. Over 720'
      -- Create buckets of 30 days for values between 31 and 720:
      -- '02. 720-691', '03. 690-661', ..., '23. 90-61', '24. 60-31'
      WHEN days_to_cancel BETWEEN 31 AND 720
      -- Create the prefix '02. ', '03. ' etc. for ordering
      THEN LPAD(((720 - days_to_cancel) / 30 + 2) :: TEXT, 2, '0') || '. ' ||
           -- Append the range '720-691', '690-661' etc. after the prefix
           CEILING(days_to_cancel / 30.0) * 30 || '-' || CEILING(days_to_cancel / 30.0) * 30 - 29
      -- More detailed buckets close to the renewal
      WHEN days_to_cancel BETWEEN 16 AND 30 THEN '25. 30-16'
      WHEN days_to_cancel BETWEEN 8 AND 15 THEN '26. 15-8'
      WHEN days_to_cancel BETWEEN 0 AND 7 THEN '27. 7-0'
      ELSE '28. Unknown'
    END                                    AS days_to_cancel,
    COALESCE(SUM(fail_units), 0)           AS fail_units,
    COALESCE(SUM(fail_revenue), 0)         AS fail_revenue,
    COALESCE(SUM(fail_revenue_gross), 0)   AS fail_revenue_gross,
    COALESCE(SUM(cancel_units), 0)         AS cancel_units,
    COALESCE(SUM(cancel_revenue), 0)       AS cancel_revenue,
    COALESCE(SUM(cancel_revenue_gross), 0) AS cancel_revenue_gross,
    COALESCE(SUM(refund_units), 0)         AS refund_units,
    COALESCE(SUM(refund_revenue), 0)       AS refund_revenue,
    COALESCE(SUM(refund_revenue_gross), 0) AS refund_revenue_gross,
    COALESCE(SUM(sales_units), 0)          AS sales_units,
    COALESCE(SUM(sales_revenue), 0)        AS sales_revenue,
    COALESCE(SUM(sales_revenue_gross), 0)  AS sales_revenue_gross,
    COALESCE(SUM(sales_discount), 0)       AS sales_discount,
    COALESCE(SUM(net_units), 0)            AS net_units,
    COALESCE(SUM(net_revenue), 0)          AS net_revenue,
    COALESCE(SUM(net_revenue_gross), 0)    AS net_revenue_gross,
    COALESCE(SUM(sales_discount), 0)       AS net_discount
  FROM (
         SELECT
           TRUNC(event_timestamp) AS date_of_activity,
           'Next'                 AS payment_type,
           payment_country_id     AS country_id,
           product_id,
           payment_gateway_id,
           1                      AS fail_units,
           amount_net_eur         AS fail_revenue,
           amount_gross_eur       AS fail_revenue_gross,
           NULL                   AS cancel_units,
           NULL                   AS cancel_revenue,
           NULL                   AS cancel_revenue_gross,
           NULL                   AS days_to_cancel,
           NULL                   AS refund_units,
           NULL                   AS refund_revenue,
           NULL                   AS refund_revenue_gross,
           NULL                   AS sales_units,
           NULL                   AS sales_revenue,
           NULL                   AS sales_revenue_gross,
           NULL                   AS sales_discount,
           NULL                   AS net_units,
           NULL                   AS net_revenue,
           NULL                   AS net_revenue_gross
         FROM events.payments_failed

         UNION ALL

         SELECT
           TRUNC(event_timestamp) AS date_of_activity,
           'Next'                 AS payment_type,
           payment_country_id     AS country_id,
           product_id,
           payment_gateway_id,
           NULL                   AS fail_units,
           NULL                   AS fail_revenue,
           NULL                   AS fail_revenue_gross,
           1                      AS cancel_units,
           amount_net_eur         AS cancel_revenue,
           amount_gross_eur       AS cancel_revenue_gross,
           -- Calculate the date difference between when a user cancelled and when the next renewal
           -- was programmed (future_payment_date). The lower the number the closer to the programmed
           -- renewal the user cancelled. Default to NULL for inconsistent data.
           CASE
             WHEN DATEDIFF(day, TRUNC(event_timestamp), TRUNC(future_payment_date)) >= 0
             THEN DATEDIFF(day, TRUNC(event_timestamp), TRUNC(future_payment_date))
           END                    AS days_to_cancel,
           NULL                   AS refund_units,
           NULL                   AS refund_revenue,
           NULL                   AS refund_revenue_gross,
           NULL                   AS sales_units,
           NULL                   AS sales_revenue,
           NULL                   AS sales_revenue_gross,
           NULL                   AS sales_discount,
           NULL                   AS net_units,
           NULL                   AS net_revenue,
           NULL                   AS net_revenue_gross
         FROM events.subscription_canceled

         UNION ALL

         SELECT
           TRUNC(payment_timestamp)                                    AS date_of_activity,
           sale_type                                                   AS payment_type,
           payment_country_id                                          AS country_id,
           product_id,
           payment_gateway_id,
           NULL                                                        AS fail_units,
           NULL                                                        AS fail_revenue,
           NULL                                                        AS fail_revenue_gross,
           NULL                                                        AS cancel_units,
           NULL                                                        AS cancel_revenue,
           NULL                                                        AS cancel_revenue_gross,
           NULL                                                        AS days_to_cancel,
           CASE WHEN status = 40 THEN 1 ELSE 0 END                     AS refund_units,
           CASE WHEN status = 40 THEN ABS(amount_net_eur) ELSE 0 END   AS refund_revenue,
           CASE WHEN status = 40 THEN ABS(amount_gross_eur) ELSE 0 END AS refund_revenue_gross,
           CASE WHEN status = 30 THEN 1 ELSE 0 END                     AS sales_units,
           CASE WHEN status = 30 THEN amount_net_eur ELSE 0 END        AS sales_revenue,
           CASE WHEN status = 30 THEN amount_gross_eur ELSE 0 END      AS sales_revenue_gross,
           -- Refunds can't have discount
           CASE WHEN status = 40 THEN 0 ELSE discount_gross_eur END    AS sales_discount,
           -- Deduct refunds (40) from payments (30)
           CASE
             WHEN status = 30 THEN  1
             WHEN status = 40 THEN -1
             ELSE 0
           END                                                         AS net_units,
           amount_net_eur                                              AS net_revenue,
           amount_gross_eur                                            AS net_revenue_gross
         FROM logic.payments
       ) AS fact
    INNER JOIN core.dim_payment_gateway USING (payment_gateway_id)
    INNER JOIN core.dim_channel USING (channel_id)
    INNER JOIN core.dim_country USING (country_id)
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
);
