--- A base view for performance reporting - can be grouped to either adgroup or partner level
CREATE OR REPLACE VIEW logic.report_performance_base AS (

  SELECT
    date_of_activity,
    campaign_id,
    adgroup_id,
    acquisition_country_id,
    acquisition_channel_id,
    acquisition_partner_id,

    campaign,
    adgroup,
    ad_source,
    table_source,

    impressions,
    clicks,
    investment,
    installs,
    acquisition,
    revenue_total,
    revenue_1d,
    revenue_3d,
    revenue_7d,
    revenue_15d,
    revenue_30d,
    revenue_90d,
    revenue_180d,
    revenue_360d,
    conversions_total,
    conversions_1d,
    conversions_3d,
    conversions_7d,
    conversions_15d,
    conversions_30d,
    conversions_90d,
    conversions_180d,
    conversions_360d
  FROM (

         SELECT
           date_of_activity,
           campaign_id,
           adgroup_id,
           acquisition_country_id,
           acquisition_channel_id,
           acquisition_partner_id,

           -- Use the latest campaign, adgroup, ad_source and table_source values for all records under the same campaign_id
           COALESCE(FIRST_VALUE(campaign IGNORE NULLS)
                    OVER (
                      PARTITION BY campaign_id
                      ORDER BY date_of_activity DESC
                      ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ), 'Unknown')
             AS campaign,
           COALESCE(FIRST_VALUE(adgroup IGNORE NULLS)
                    OVER (
                      PARTITION BY adgroup_id
                      ORDER BY date_of_activity DESC
                      ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ), 'Unknown')
             AS adgroup,
           COALESCE(FIRST_VALUE(ad_source IGNORE NULLS)
                    OVER (
                      PARTITION BY campaign_id
                      ORDER BY date_of_activity DESC
                      ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ), 'Others')
             AS ad_source,
           COALESCE(FIRST_VALUE(table_source IGNORE NULLS)
                    OVER (
                      PARTITION BY campaign_id
                      ORDER BY date_of_activity DESC
                      ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ), 'Others')
             AS table_source,

           impressions,
           clicks,
           investment,
           installs,
           acquisition,
           revenue_total,
           revenue_1d,
           revenue_3d,
           revenue_7d,
           revenue_15d,
           revenue_30d,
           revenue_90d,
           revenue_180d,
           revenue_360d,
           conversions_total,
           conversions_1d,
           conversions_3d,
           conversions_7d,
           conversions_15d,
           conversions_30d,
           conversions_90d,
           conversions_180d,
           conversions_360d
         FROM (
                -- Investment
                SELECT
                  date_of_activity,
                  campaign_id,
                  adgroup_id,
                  -1                       AS acquisition_country_id,
                  -1                       AS acquisition_channel_id,
                  -1                       AS acquisition_partner_id,

                  -- Investment defines campaign, adgroup, ad_source and table_source for all campaigns
                  campaign,
                  adgroup,
                  ad_source,
                  table_source,

                  COALESCE(impressions, 0) AS impressions,
                  COALESCE(clicks, 0)      AS clicks,
                  COALESCE(cost, 0)        AS investment,
                  0                        AS installs,
                  0                        AS acquisition,
                  0                        AS revenue_total,
                  0                        AS revenue_1d,
                  0                        AS revenue_3d,
                  0                        AS revenue_7d,
                  0                        AS revenue_15d,
                  0                        AS revenue_30d,
                  0                        AS revenue_90d,
                  0                        AS revenue_180d,
                  0                        AS revenue_360d,
                  0                        AS conversions_total,
                  0                        AS conversions_1d,
                  0                        AS conversions_3d,
                  0                        AS conversions_7d,
                  0                        AS conversions_15d,
                  0                        AS conversions_30d,
                  0                        AS conversions_90d,
                  0                        AS conversions_180d,
                  0                        AS conversions_360d
                FROM logic.investment

                UNION ALL

                -- Installs
                SELECT
                  DATE(install_timestamp)     AS date_of_activity,
                  COALESCE(campaign_id, '-1') AS campaign_id,
                  COALESCE(adgroup_id, '-1')  AS adgroup_id,
                  -1                          AS acquisition_country_id,
                  -1                          AS acquisition_channel_id,
                  -1                          AS acquisition_partner_id,

                  -- Values for campaign, adgroup, ad_source and table_source come from investment
                  NULL                        AS campaign,
                  NULL                        AS adgroup,
                  NULL                        AS ad_source,
                  NULL                        AS table_source,

                  0                           AS impressions,
                  0                           AS clicks,
                  0                           AS investment,
                  1                           AS installs,
                  0                           AS acquisition,
                  0                           AS revenue_total,
                  0                           AS revenue_1d,
                  0                           AS revenue_3d,
                  0                           AS revenue_7d,
                  0                           AS revenue_15d,
                  0                           AS revenue_30d,
                  0                           AS revenue_90d,
                  0                           AS revenue_180d,
                  0                           AS revenue_360d,
                  0                           AS conversions_total,
                  0                           AS conversions_1d,
                  0                           AS conversions_3d,
                  0                           AS conversions_7d,
                  0                           AS conversions_15d,
                  0                           AS conversions_30d,
                  0                           AS conversions_90d,
                  0                           AS conversions_180d,
                  0                           AS conversions_360d
                FROM events.installs

                UNION ALL

                -- Acquisition
                SELECT
                  DATE(event_timestamp)                   AS date_of_activity,
                  COALESCE(NULLIF(campaign_id, ''), '-1') AS campaign_id,
                  COALESCE(NULLIF(adgroup_id, ''), '-1')  AS adgroup_id,
                  acquisition_country_id,
                  acquisition_channel_id,
                  acquisition_partner                     AS acquisition_partner_id,

                  -- Values for campaign, adgroup, ad_source and table_source come from investment
                  NULL                                    AS campaign,
                  NULL                                    AS adgroup,
                  NULL                                    AS ad_source,
                  NULL                                    AS table_source,

                  0                                       AS impressions,
                  0                                       AS clicks,
                  0                                       AS investment,
                  0                                       AS installs,
                  1                                       AS acquisition,
                  0                                       AS revenue_total,
                  0                                       AS revenue_1d,
                  0                                       AS revenue_3d,
                  0                                       AS revenue_7d,
                  0                                       AS revenue_15d,
                  0                                       AS revenue_30d,
                  0                                       AS revenue_90d,
                  0                                       AS revenue_180d,
                  0                                       AS revenue_360d,
                  0                                       AS conversions_total,
                  0                                       AS conversions_1d,
                  0                                       AS conversions_3d,
                  0                                       AS conversions_7d,
                  0                                       AS conversions_15d,
                  0                                       AS conversions_30d,
                  0                                       AS conversions_90d,
                  0                                       AS conversions_180d,
                  0                                       AS conversions_360d
                FROM events.user_registered

                UNION ALL

                -- Cohorted metrics
                SELECT
                  DATE(acquisition_timestamp) AS date_of_activity,
                  campaign_id,
                  adgroup_id,
                  acquisition_country_id,
                  acquisition_channel_id,
                  acquisition_partner_id,

                  -- Values for campaign, adgroup, ad_source and table_source come from investment
                  NULL                        AS campaign,
                  NULL                        AS adgroup,
                  NULL                        AS ad_source,
                  NULL                        AS table_source,

                  0                           AS impressions,
                  0                           AS clicks,
                  0                           AS investment,
                  0                           AS installs,
                  0                           AS acquisition,
                  COALESCE(amount_net_eur, 0) AS revenue_total,
                  CASE WHEN date_diff BETWEEN 0 AND 1
                    THEN amount_net_eur
                  ELSE 0 END                  AS revenue_1d,
                  CASE WHEN date_diff BETWEEN 0 AND 3
                    THEN amount_net_eur
                  ELSE 0 END                  AS revenue_3d,
                  CASE WHEN date_diff BETWEEN 0 AND 7
                    THEN amount_net_eur
                  ELSE 0 END                  AS revenue_7d,
                  CASE WHEN date_diff BETWEEN 0 AND 15
                    THEN amount_net_eur
                  ELSE 0 END                  AS revenue_15d,
                  CASE WHEN date_diff BETWEEN 0 AND 30
                    THEN amount_net_eur
                  ELSE 0 END                  AS revenue_30d,
                  CASE WHEN date_diff BETWEEN 0 AND 90
                    THEN amount_net_eur
                  ELSE 0 END                  AS revenue_90d,
                  CASE WHEN date_diff BETWEEN 0 AND 180
                    THEN amount_net_eur
                  ELSE 0 END                  AS revenue_180d,
                  CASE WHEN date_diff BETWEEN 0 AND 360
                    THEN amount_net_eur
                  ELSE 0 END                  AS revenue_360d,
                  CASE WHEN clean_sale_number = 1
                    THEN 1
                  ELSE 0 END                  AS conversions_total,
                  CASE WHEN clean_sale_number = 1 AND date_diff BETWEEN 0 AND 1
                    THEN 1
                  ELSE 0 END                  AS conversions_1d,
                  CASE WHEN clean_sale_number = 1 AND date_diff BETWEEN 0 AND 3
                    THEN 1
                  ELSE 0 END                  AS conversions_3d,
                  CASE WHEN clean_sale_number = 1 AND date_diff BETWEEN 0 AND 7
                    THEN 1
                  ELSE 0 END                  AS conversions_7d,
                  CASE WHEN clean_sale_number = 1 AND date_diff BETWEEN 0 AND 15
                    THEN 1
                  ELSE 0 END                  AS conversions_15d,
                  CASE WHEN clean_sale_number = 1 AND date_diff BETWEEN 0 AND 30
                    THEN 1
                  ELSE 0 END                  AS conversions_30d,
                  CASE WHEN clean_sale_number = 1 AND date_diff BETWEEN 0 AND 90
                    THEN 1
                  ELSE 0 END                  AS conversions_90d,
                  CASE WHEN clean_sale_number = 1 AND date_diff BETWEEN 0 AND 180
                    THEN 1
                  ELSE 0 END                  AS conversions_180d,
                  CASE WHEN clean_sale_number = 1 AND date_diff BETWEEN 0 AND 360
                    THEN 1
                  ELSE 0 END                  AS conversions_360d
                FROM logic.payments

              ) AS federated_data

       ) AS lookup_values
);
