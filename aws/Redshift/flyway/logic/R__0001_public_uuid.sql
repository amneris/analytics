-- Function to create an UUID
CREATE OR REPLACE FUNCTION public.uuid()
  RETURNS character varying AS
' import uuid
 return uuid.uuid1().__str__()
 '
LANGUAGE plpythonu VOLATILE;
