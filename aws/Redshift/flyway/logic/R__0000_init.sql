GRANT USAGE ON SCHEMA                logic                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA logic                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   logic GRANT SELECT ON TABLES TO analyticsuser;

GRANT USAGE                ON SCHEMA logic                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA logic                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA logic GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA logic                      FROM GROUP redash_group;

GRANT ALL                  ON SCHEMA logic                        TO GROUP service_group;
REVOKE ALL                 ON SCHEMA logic                      FROM GROUP chartio_group;
