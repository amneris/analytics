-- View for calculating Retention Rate.
-- Data should be aggregated to monthly level for more reliable results.

CREATE VIEW logic.report_retention_performance_retention_rate AS (
  SELECT
    date_of_payment,
    product_id :: VARCHAR(5)                                                         AS product,
    country,
    cgroup,
    region,
    continent,
    main_market,
    channel,

    SUM(sale_no_change + sale_upgrade + sale_downgrade + sale_expremium)             AS sale,
    SUM(sale_no_change)                                                              AS sale_no_change,
    SUM(sale_upgrade)                                                                AS sale_upgrade,
    SUM(sale_downgrade)                                                              AS sale_downgrade,
    SUM(sale_expremium)                                                              AS sale_expremium,
    SUM(previous_sale)                                                               AS previous_sale,

    SUM(revenue_no_change + revenue_upgrade + revenue_downgrade + revenue_expremium) AS revenue,
    SUM(revenue_no_change)                                                           AS revenue_no_change,
    SUM(revenue_upgrade)                                                             AS revenue_upgrade,
    SUM(revenue_downgrade)                                                           AS revenue_downgrade,
    SUM(revenue_expremium)                                                           AS revenue_expremium,
    SUM(previous_revenue)                                                            AS previous_revenue
  FROM (
         -- Current quantity and revenue
         SELECT
           date_of_payment,
           product_id,
           country_id,
           payment_gateway_id,

           -- Ex-premium takes precedence
           CASE WHEN NOT is_expremium AND product_change_type = 'Downgrade'
             THEN 1
           ELSE 0
           END AS sale_downgrade,
           CASE WHEN NOT is_expremium AND product_change_type = 'Upgrade'
             THEN 1
           ELSE 0
           END AS sale_upgrade,
           CASE WHEN NOT is_expremium AND product_change_type = 'No change'
             THEN 1
           ELSE 0
           END AS sale_no_change,
           CASE WHEN is_expremium
             THEN 1
           ELSE 0
           END AS sale_expremium,
           0   AS previous_sale,

           -- Ex-premium takes precedence
           CASE WHEN NOT is_expremium AND product_change_type = 'Downgrade'
             THEN amount_net_eur
           ELSE 0
           END AS revenue_downgrade,
           CASE WHEN NOT is_expremium AND product_change_type = 'Upgrade'
             THEN amount_net_eur
           ELSE 0
           END AS revenue_upgrade,
           CASE WHEN NOT is_expremium AND product_change_type = 'No change'
             THEN amount_net_eur
           ELSE 0
           END AS revenue_no_change,
           CASE WHEN is_expremium
             THEN amount_net_eur
           ELSE 0
           END AS revenue_expremium,
           0   AS previous_revenue
         FROM (
                SELECT
                  TRUNC(payment_timestamp) AS date_of_payment,
                  product_id,
                  payment_country_id       AS country_id,
                  payment_gateway_id,
                  -- If a user was out for more than a month we consider her ex-premium
                  CASE WHEN days_off IS NOT NULL AND days_off > 30
                    THEN TRUE
                  ELSE FALSE
                  END                      AS is_expremium,
                  -- Track product upgrades and downgrades
                  CASE
                  WHEN product_id > previous_product_id
                    THEN 'Upgrade'
                  WHEN product_id < previous_product_id
                    THEN 'Downgrade'
                  ELSE 'No change'
                  END                      AS product_change_type,
                  amount_net_eur
                FROM logic.payments
                -- Only "Next" clean sales
                WHERE clean_sale_number > 1
              ) AS current_sales

         UNION ALL

         -- Previous quantity and revenue
         SELECT
           -- Move date forward to the end of the premium period
           DATEADD(MONTH, product_id / 30, TRUNC(payment_timestamp)) :: DATE AS date_of_payment,
           product_id,
           payment_country_id                                                AS country_id,
           payment_gateway_id,

           0                                                                 AS sale_downgrade,
           0                                                                 AS sale_upgrade,
           0                                                                 AS sale_no_change,
           0                                                                 AS sale_expremium,
           1                                                                 AS previous_sale,

           0                                                                 AS revenue_downgrade,
           0                                                                 AS revenue_upgrade,
           0                                                                 AS revenue_no_change,
           0                                                                 AS revenue_expremium,
           amount_net_eur                                                    AS previous_revenue
         FROM logic.payments
         -- Only clean sales
         WHERE clean_sale_number IS NOT NULL
       ) AS fact
    INNER JOIN logic.dim_country USING (country_id)
    INNER JOIN logic.dim_payment_gateway USING (payment_gateway_id)
    INNER JOIN logic.dim_channel USING (channel_id)
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8
);
