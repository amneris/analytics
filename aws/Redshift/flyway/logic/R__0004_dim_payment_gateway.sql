-- Payment gateway dimension

CREATE OR REPLACE VIEW logic.dim_payment_gateway AS (
  SELECT
    id   AS payment_gateway_id,
    name AS payment_gateway,
    -- Payment channel can be derived from the payment gateway
    CASE id
    -- payment gateway "Android-PlayStore" --> channel "Android"
    WHEN 12
      THEN 4
    -- payment gateway "iOs-AppStore" --> channel "iOS"
    WHEN 9
      THEN 3
    -- Any other payment gateway --> channel "Web"
    ELSE 2
    END  AS channel_id
  FROM staging_payments.pay_suppliers
  WHERE id != -1

  UNION ALL

  SELECT
    -1        AS payment_gateway_id,
    'Unknown' AS payment_gateway,
    -1        AS channel_id
);
