-- Calculates payments (status=30) and refunds (status=40)
--
-- For ordering the payments we use:
-- 1) payment_order_number (autoid)
-- 2) payment_timestamp
-- 3) Since there are still some records with the same combination of these two, we also use payment_id to have
--    consistent results on every execution.


CREATE OR REPLACE VIEW logic.payments AS
  SELECT
    add_clean_sale_columns.*,

    -- User's first sale and refund, and user's first clean sale are "First"; others are "Next"
    CASE
    WHEN total_sale_number = 1 OR clean_sale_number = 1
      THEN 'First'
    ELSE 'Next'
    END        AS sale_type,

    -- User's conversion date (that is, date of the first clean sale)
    TRUNC(FIRST_VALUE(payment_timestamp)
          OVER (
            PARTITION BY user_id
            ORDER BY clean_sale_number NULLS LAST
            ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
            )) AS conversion_date,

    -- Days the user was without subscription
    CASE WHEN isextend
      THEN 0
    ELSE DATEDIFF(DAYS, DATEADD(MONTH, previous_product_id / 30, TRUNC(previous_payment_timestamp)),
                  TRUNC(payment_timestamp))
    END        AS days_off

  FROM (

         SELECT
           match_refund.*,

           -- These columns only apply for clean sales
           CASE WHEN status = 30 AND refund_payment_id IS NULL
             THEN ROW_NUMBER()
             OVER (
               PARTITION BY user_id
               ORDER BY status, refund_payment_id NULLS FIRST, payment_order_number, payment_timestamp, payment_id )
           ELSE NULL END AS clean_sale_number,

           CASE WHEN status = 30 AND refund_payment_id IS NULL
             THEN LAG(product_id)
             OVER (
               PARTITION BY user_id
               ORDER BY status, refund_payment_id NULLS FIRST, payment_order_number, payment_timestamp, payment_id )
           ELSE NULL END AS previous_product_id,

           CASE WHEN status = 30 AND refund_payment_id IS NULL
             THEN LAG(payment_timestamp)
             OVER (
               PARTITION BY user_id
               ORDER BY status, refund_payment_id NULLS FIRST, payment_order_number, payment_timestamp, payment_id )
           ELSE NULL END AS previous_payment_timestamp

         FROM (

                SELECT
                  payment_acquisition.*,

                  LEAD(CASE WHEN status = 40
                    THEN payment_id
                       ELSE NULL END, 1)
                  OVER (
                    PARTITION BY user_id, product_id, payment_gateway_id, amount
                    ORDER BY payment_order_number, payment_timestamp, status, payment_id ) AS refund_payment_id,

                  LEAD(CASE WHEN status = 40
                    THEN payment_timestamp
                       ELSE NULL END, 1)
                  OVER (
                    PARTITION BY user_id, product_id, payment_gateway_id, amount
                    ORDER BY payment_order_number, payment_timestamp, status, payment_id ) AS refund_date,

                  CASE WHEN status = 40
                    THEN
                      LAG(row_num, 1)
                      OVER (
                        PARTITION BY user_id, product_id, payment_gateway_id, amount
                        ORDER BY payment_order_number, payment_timestamp, status, payment_id )
                  ELSE
                    row_num
                  END                                                                      AS total_sale_number

                FROM (

                       SELECT
                         -- User acquisition information
                         COALESCE(u.event_timestamp, '1900-01-01' :: TIMESTAMP)           AS acquisition_timestamp,
                         user_id,
                         u.email,
                         COALESCE(u.network_name, 'Unknown')                              AS network,
                         COALESCE(u.creative_name, 'Unknown')                             AS creative,

                         COALESCE(NULLIF(u.campaign_id, ''), '-1')                        AS campaign_id,
                         COALESCE(NULLIF(u.adgroup_id, ''), '-1')                         AS adgroup_id,
                         COALESCE(u.acquisition_partner, -1)                              AS acquisition_partner_id,
                         CASE WHEN u.acquisition_country_id IS NULL OR u.acquisition_country_id = 0
                           THEN -1
                         ELSE u.acquisition_country_id END                                AS acquisition_country_id,
                         COALESCE(u.acquisition_channel_id, -1)                           AS acquisition_channel_id,

                         DATE_DIFF('day', COALESCE(u.event_timestamp, '1900-01-01' :: TIMESTAMP), p.event_timestamp)
                                                                                          AS date_diff,

                         -- Payment information
                         p.event_timestamp                                                AS payment_timestamp,
                         p.payment_id,
                         COALESCE(p.payment_country_id, -1)                               AS payment_country_id,
                         p.product_id,
                         CASE WHEN p.payment_gateway_id IS NULL OR p.payment_gateway_id = 0
                           THEN -1
                         ELSE p.payment_gateway_id END                                    AS payment_gateway_id,
                         p.status,
                         p.currency_code,
                         p.isrecurring,
                         p.isextend,
                         p.is_amex,
                         p.original_amount,
                         p.amount_gross_eur,
                         p.amount_net_eur,
                         p.discount_gross_eur,
                         p.tax_eur,
                         p.fee_eur,
                         p.amount_gross_original,
                         p.amount_net_original,
                         p.discount_gross_original,
                         p.tax_original,
                         p.fee_original,

                         -- Partition row_number by status to avoid skipping numbers
                         ROW_NUMBER()
                         OVER (
                           PARTITION BY user_id, p.status
                           ORDER BY payment_order_number, payment_timestamp, payment_id ) AS row_num,
                         p.amount                                                         AS amount,
                         p.payment_order_number                                           AS payment_order_number
                       FROM events.payments p
                         LEFT JOIN events.user_registered u USING (user_id)

                     ) payment_acquisition
              ) match_refund
       ) add_clean_sale_columns;
