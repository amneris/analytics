-- Country dimension

CREATE OR REPLACE VIEW logic.dim_country AS (
  SELECT
    id           AS country_id,
    name         AS country,
    iso,
    countrygroup AS cgroup,
    region,
    continent,
    -- Main market is the country name for the most important countries, else "Others"
    -- 30 - Brazil
    -- 73 - France
    -- 80 - Germany
    -- 105 - Italy
    -- 138 - Mexico
    -- 177 - Russian Federation
    -- 199 - Spain
    CASE
    WHEN country_id IN (30, 73, 80, 105, 138, 177, 199)
      THEN name
    ELSE 'Others'
    END          AS main_market
  FROM staging_countries.country
  WHERE id != -1

  UNION ALL

  SELECT
    -1        AS country_id,
    'Unknown' AS country,
    NULL      AS iso,
    'Unknown' AS cgroup,
    'Unknown' AS region,
    'Unknown' AS continent,
    'Unknown' AS main_market
);
