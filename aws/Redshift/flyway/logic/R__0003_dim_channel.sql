-- Channel dimension

CREATE OR REPLACE VIEW logic.dim_channel AS (
  SELECT
    idchannel   AS channel_id,
    namechannel AS channel
  FROM staging_partners.aba_partners_channels
  WHERE idchannel != -1

  UNION ALL

  SELECT
    -1        AS channel_id,
    'Unknown' AS channel
);
