--- View for adgroup reporting
CREATE OR REPLACE VIEW logic.report_adgroup_performance_all AS (

  SELECT
    date_of_activity,

    campaign_id,
    campaign                                                                 AS campaign_name,
    adgroup_id,
    adgroup                                                                  AS adgroup_name,
    ad_source,
    table_source,

    -- For partner attributes use:
    -- 1) Investment partner if it's defined
    -- 2) As a fallback, use acquisition partner
    COALESCE(NULLIF(inv_partner.partner_id, -1), acq_partner.partner_id, -1) AS partner_id,
    CASE WHEN inv_partner.partner_id IS NOT NULL AND inv_partner.partner_id != -1
      THEN inv_partner.partner
    ELSE COALESCE(acq_partner.partner, 'Unknown') END                        AS partner_name,
    CASE WHEN inv_partner.partner_id IS NOT NULL AND inv_partner.partner_id != -1
      THEN inv_partner.partner_group
    ELSE COALESCE(acq_partner.partner_group, 'Unknown') END                  AS partner_group,
    CASE WHEN inv_partner.partner_id IS NOT NULL AND inv_partner.partner_id != -1
      THEN inv_partner.partner_type
    ELSE COALESCE(acq_partner.partner_type, 'Unknown') END                   AS partner_type,
    CASE WHEN inv_partner.partner_id IS NOT NULL AND inv_partner.partner_id != -1
      THEN inv_partner.partner_category
    ELSE COALESCE(acq_partner.partner_category, 'Unknown') END               AS partner_category,

    -- No source_id for adgroup performance (it's obsolete)
    -- No network for adgroup performance (use ad_source - on campaign level - instead)

    -- For acquisition attributes use:
    -- 1) Investment partner country & channel when they are defined
    -- 2) As a fallback, use acquisition partner country & channel
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.country
    ELSE COALESCE(acq_country.country, 'Unknown') END                        AS country,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.cgroup
    ELSE COALESCE(acq_country.cgroup, 'Unknown') END                         AS cgroup,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.region
    ELSE COALESCE(acq_country.region, 'Unknown') END                         AS region,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.continent
    ELSE COALESCE(acq_country.continent, 'Unknown') END                      AS continent,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.main_market
    ELSE COALESCE(acq_country.main_market, 'Unknown') END                    AS main_market,
    CASE WHEN inv_channel.channel_id IS NOT NULL AND inv_channel.channel_id != -1
      THEN inv_channel.channel
    ELSE COALESCE(acq_channel.channel, 'Unknown') END                        AS channel,

    SUM(impressions)                                                         AS impressions,
    SUM(clicks)                                                              AS clicks,
    SUM(investment)                                                          AS investment,
    SUM(acquisition)                                                         AS acquisition,
    SUM(installs)                                                            AS installs,

    SUM(revenue_total)                                                       AS revenue_total,
    SUM(revenue_1d)                                                          AS revenue_1d,
    SUM(revenue_3d)                                                          AS revenue_3d,
    SUM(revenue_7d)                                                          AS revenue_7d,
    SUM(revenue_15d)                                                         AS revenue_15d,
    SUM(revenue_30d)                                                         AS revenue_30d,
    SUM(revenue_90d)                                                         AS revenue_90d,
    SUM(revenue_180d)                                                        AS revenue_180d,
    SUM(revenue_360d)                                                        AS revenue_360d,

    SUM(conversions_total)                                                   AS conversions_total,
    SUM(conversions_1d)                                                      AS conversions_1d,
    SUM(conversions_3d)                                                      AS conversions_3d,
    SUM(conversions_7d)                                                      AS conversions_7d,
    SUM(conversions_15d)                                                     AS conversions_15d,
    SUM(conversions_30d)                                                     AS conversions_30d,
    SUM(conversions_90d)                                                     AS conversions_90d,
    SUM(conversions_180d)                                                    AS conversions_180d,
    SUM(conversions_360d)                                                    AS conversions_360d
  FROM logic.report_performance_base
    -- Join with the acquisition country from *report_performance_base* because it has the following logic:
    -- 1) Acquisition partner country if it's defined
    -- 2) Country based on the user IP
    LEFT JOIN logic.dim_country AS acq_country ON acquisition_country_id = acq_country.country_id
    -- Join with the acquisition channel from *report_performance_base* for consistency with other reports
    LEFT JOIN logic.dim_channel AS acq_channel ON acquisition_channel_id = acq_channel.channel_id
    LEFT JOIN logic.dim_partner AS acq_partner ON acquisition_partner_id = acq_partner.partner_id

    -- Try to look up investment partner for acquisition country & acquisition channel
    LEFT JOIN logic.lkp_investment_partner USING (campaign_id)
    LEFT JOIN logic.dim_partner AS inv_partner ON investment_partner_id = inv_partner.partner_id
    LEFT JOIN logic.dim_country AS inv_country ON inv_partner.partner_country_id = inv_country.country_id
    LEFT JOIN logic.dim_channel AS inv_channel ON inv_partner.partner_channel_id = inv_channel.channel_id
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
);
