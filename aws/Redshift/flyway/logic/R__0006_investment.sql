-- Returns ad groups from all investment platforms
CREATE OR REPLACE VIEW logic.investment AS (
  SELECT
    date_of_activity,
    COALESCE(campaign_id, '-1')   AS campaign_id,
    COALESCE(campaign, 'Unknown') AS campaign,
    COALESCE(adgroup_id, '-1')    AS adgroup_id,
    COALESCE(adgroup, 'Unknown')  AS adgroup,
    COALESCE(partner_id, -1)      AS partner_id,
    impressions,
    clicks,
    cost,
    COALESCE(ad_source, 'Others') AS ad_source,
    table_source
  FROM (
         SELECT
           date_start                                               AS date_of_activity,
           campaign_id :: VARCHAR,
           campaign_name                                            AS campaign,
           adset_id :: VARCHAR                                      AS adgroup_id,
           adset_name                                               AS adgroup,
           CASE WHEN SPLIT_PART(campaign_name, '___', 2) ~ '^[0-9]+$'
             THEN SPLIT_PART(campaign_name, '___', 2) :: BIGINT END AS partner_id,
           impressions,
           clicks,
           spend                                                    AS cost,
           'Facebook paid'                                          AS ad_source,
           'Facebook Ads'                                           AS table_source
         FROM transactional.consolidated_facebook_ads

         UNION ALL

         SELECT
           day                                                      AS date_of_activity,
           campaign_id :: VARCHAR,
           campaign_name                                            AS campaign,
           adgroup_id :: VARCHAR,
           adgroup_name                                             AS adgroup,
           CASE WHEN SPLIT_PART(campaign_name, '___', 2) ~ '^[0-9]+$'
             THEN SPLIT_PART(campaign_name, '___', 2) :: BIGINT END AS partner_id,
           impressions,
           clicks,
           cost / 1000000 :: NUMERIC                                AS cost,
           'AdWords'                                                AS ad_source,
           'Google AdWords'                                         AS table_source
         FROM transactional.consolidated_google_adwords

         UNION ALL

         SELECT
           day                                                      AS date_of_activity,
           campaign_id :: VARCHAR,
           campaign_name                                            AS campaign,
           REGEXP_REPLACE(ad_network, ' .*', '')                    AS adgroup_id,
           REGEXP_REPLACE(ad_network, ' .*', '')                    AS adgroup,
           CASE WHEN SPLIT_PART(campaign_name, '___', 2) ~ '^[0-9]+$'
             THEN SPLIT_PART(campaign_name, '___', 2) :: BIGINT END AS partner_id,
           impressions,
           clicks,
           cost / 1000000 :: NUMERIC                                AS cost,
           'AdWords'                                                AS ad_source,
           'Google UAC'                                             AS table_source
         FROM transactional.consolidated_google_uac

         UNION ALL

         SELECT
           day                                                 AS date_of_activity,
           -- Currently we don't receive campaign id or any adgroup level data from Funnel
           campagin                                            AS campaign_id,
           campagin                                            AS campaign,
           '-1'                                                AS adgroup_id,
           '-1'                                                AS adgroup,
           CASE WHEN SPLIT_PART(campagin, '___', 2) ~ '^[0-9]+$'
             THEN SPLIT_PART(campagin, '___', 2) :: BIGINT END AS partner_id,
           0                                                   AS impressions,
           common_clicks                                       AS clicks,
           common_cost / 1000 :: NUMERIC                       AS cost,
           channel                                             AS ad_source,
           'Others'                                            AS table_source
         FROM staging_investment.consolidated_investment
         WHERE channel NOT IN ('Facebook paid', 'AdWords')
       ) AS src
);
