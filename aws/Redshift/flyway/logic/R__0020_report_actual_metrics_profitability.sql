-- Actual revenue & sales, investment and margin
--
-- This table pivots sale type to columns to make sale type and investment compatible.
--
-- Splits "First" to "First Early" and "First Late" depending on the date difference between acquisition and
-- payment/refund. "First" with inconsistent data - negative date_diff - becomes "First Unknown".
-- Note that while first sales are quite evenly distributed between early and late, first refunds mostly fall to
-- "First Late". (If this is a problem you can group them back together in ChartIO reports.)
--
-- Free Trials are included as a separate metric to allow reporting on them. Sales already include free trials.
-- If you need to get only the sales where amount > 0 you can calculate that as "Sales" - "Free Trials" in ChartIO.

CREATE OR REPLACE VIEW logic.report_actual_metrics_profitability AS (
  SELECT
    date_of_activity,

    -- For acquisition attributes use:
    -- 1) Investment partner country & channel when they are defined
    -- 2) As a fallback, use acquisition partner country & channel
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.country
    ELSE COALESCE(acq_country.country, 'Unknown') END     AS acquisition_country,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.cgroup
    ELSE COALESCE(acq_country.cgroup, 'Unknown') END      AS acquisition_cgroup,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.region
    ELSE COALESCE(acq_country.region, 'Unknown') END      AS acquisition_region,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.continent
    ELSE COALESCE(acq_country.continent, 'Unknown') END   AS acquisition_continent,
    CASE WHEN inv_country.country_id IS NOT NULL AND inv_country.country_id != -1
      THEN inv_country.main_market
    ELSE COALESCE(acq_country.main_market, 'Unknown') END AS acquisition_main_market,
    CASE WHEN inv_channel.channel_id IS NOT NULL AND inv_channel.channel_id != -1
      THEN inv_channel.channel
    ELSE COALESCE(acq_channel.channel, 'Unknown') END     AS acquisition_channel,

    -- Total metrics for the majority of use cases
    SUM(revenue_first_early
        + revenue_first_late
        + revenue_first_unknown
        + revenue_next)                                   AS revenue_total,
    COALESCE(SUM(investment), 0)                          AS investment,
    SUM(revenue_first_early
        + revenue_first_late
        + revenue_first_unknown
        + revenue_next
        - COALESCE(investment, 0))                        AS margin,
    SUM(sales_first_early
        + sales_first_late
        + sales_first_unknown
        + sales_next)                                     AS sales_total,

    -- Metrics by sale type for detailed analysis
    SUM(revenue_first_early)                              AS revenue_first_early,
    SUM(revenue_first_late)                               AS revenue_first_late,
    SUM(revenue_first_unknown)                            AS revenue_first_unknown,
    SUM(revenue_first_early
        + revenue_first_late
        + revenue_first_unknown)                          AS revenue_first_total,
    SUM(revenue_next)                                     AS revenue_next,
    SUM(sales_first_early)                                AS sales_first_early,
    SUM(sales_first_late)                                 AS sales_first_late,
    SUM(sales_first_unknown)                              AS sales_first_unknown,
    SUM(sales_first_early
        + sales_first_late
        + sales_first_unknown)                            AS sales_first_total,
    SUM(sales_next)                                       AS sales_next,
    SUM(free_trials_first_early)                          AS free_trials_first_early,
    SUM(free_trials_first_late)                           AS free_trials_first_late,
    SUM(free_trials_first_unknown)                        AS free_trials_first_unknown,
    SUM(free_trials_first_early
        + free_trials_first_late
        + free_trials_first_unknown)                      AS free_trials_first_total,
    SUM(free_trials_next)                                 AS free_trials_next
  FROM (
         SELECT
           date_of_activity,
           campaign_id,
           -1   AS acquisition_country_id,
           -1   AS acquisition_channel_id,
           -1   AS acquisition_partner_id,

           cost AS investment,

           0    AS revenue_first_early,
           0    AS revenue_first_late,
           0    AS revenue_first_unknown,
           0    AS revenue_next,

           0    AS sales_first_early,
           0    AS sales_first_late,
           0    AS sales_first_unknown,
           0    AS sales_next,

           0    AS free_trials_first_early,
           0    AS free_trials_first_late,
           0    AS free_trials_first_unknown,
           0    AS free_trials_next
         FROM logic.investment

         UNION ALL

         SELECT
           TRUNC(payment_timestamp) AS date_of_activity,
           campaign_id,
           acquisition_country_id,
           acquisition_channel_id,
           acquisition_partner_id,

           0                        AS investment,

           -- Revenue by sale type
           CASE
           WHEN sale_type = 'First' AND date_diff BETWEEN 0 AND 30
             THEN amount_net_eur
           ELSE 0 END               AS revenue_first_early,
           CASE
           WHEN sale_type = 'First' AND date_diff > 30
             THEN amount_net_eur
           ELSE 0 END               AS revenue_first_late,
           CASE
           WHEN sale_type = 'First' AND (date_diff IS NULL OR date_diff < 0)
             THEN amount_net_eur
           ELSE 0 END               AS revenue_first_unknown,
           CASE
           WHEN sale_type = 'Next'
             THEN amount_net_eur
           ELSE 0 END               AS revenue_next,

           -- Sale quantity by sale type
           CASE
           WHEN status = 30 AND sale_type = 'First' AND date_diff BETWEEN 0 AND 30
             THEN 1
           WHEN status = 40 AND sale_type = 'First' AND date_diff BETWEEN 0 AND 30
             THEN -1
           ELSE 0 END               AS sales_first_early,
           CASE
           WHEN status = 30 AND sale_type = 'First' AND date_diff > 30
             THEN 1
           WHEN status = 40 AND sale_type = 'First' AND date_diff > 30
             THEN -1
           ELSE 0 END               AS sales_first_late,
           CASE
           WHEN status = 30 AND sale_type = 'First' AND (date_diff IS NULL OR date_diff < 0)
             THEN 1
           WHEN status = 40 AND sale_type = 'First' AND (date_diff IS NULL OR date_diff < 0)
             THEN -1
           ELSE 0 END               AS sales_first_unknown,
           CASE
           WHEN status = 30 AND sale_type = 'Next'
             THEN 1
           WHEN status = 40 AND sale_type = 'Next'
             THEN -1
           ELSE 0 END               AS sales_next,

           -- Free trials by sale type
           CASE
           WHEN status = 30 AND sale_type = 'First' AND date_diff BETWEEN 0 AND 30 AND
                amount_gross_eur BETWEEN 0 AND 0.01
             THEN 1
           ELSE 0 END               AS free_trials_first_early,
           CASE
           WHEN status = 30 AND sale_type = 'First' AND date_diff > 30 AND amount_gross_eur BETWEEN 0 AND 0.01
             THEN 1
           ELSE 0 END               AS free_trials_first_late,
           CASE
           WHEN status = 30 AND sale_type = 'First' AND (date_diff IS NULL OR date_diff < 0)
                AND amount_gross_eur BETWEEN 0 AND 0.01
             THEN 1
           ELSE 0 END               AS free_trials_first_unknown,
           CASE
           WHEN status = 30 AND sale_type = 'Next' AND amount_gross_eur BETWEEN 0 AND 0.01
             THEN 1
           ELSE 0 END               AS free_trials_next
         FROM logic.payments
       ) AS federated_data

    -- Join with the acquisition country from *payments* because it has the following logic:
    -- 1) Acquisition partner country if it's defined
    -- 2) Country based on the user IP
    LEFT JOIN logic.dim_country AS acq_country ON acquisition_country_id = acq_country.country_id
    -- Join with the acquisition channel from *payments* for consistency with other reports
    LEFT JOIN logic.dim_channel AS acq_channel ON acquisition_channel_id = acq_channel.channel_id
    LEFT JOIN logic.dim_partner AS acq_partner ON acquisition_partner_id = acq_partner.partner_id

    -- Try to look up investment partner for acquisition country & acquisition channel
    LEFT JOIN logic.lkp_investment_partner USING (campaign_id)
    LEFT JOIN logic.dim_partner AS inv_partner ON investment_partner_id = inv_partner.partner_id
    LEFT JOIN logic.dim_country AS inv_country ON inv_partner.partner_country_id = inv_country.country_id
    LEFT JOIN logic.dim_channel AS inv_channel ON inv_partner.partner_channel_id = inv_channel.channel_id
  GROUP BY 1, 2, 3, 4, 5, 6, 7
);
