GRANT USAGE                ON SCHEMA staging_installs                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_installs                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA staging_installs GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA staging_installs                      FROM GROUP redash_group;

GRANT USAGE                ON SCHEMA staging_installs                        TO GROUP chartio_group;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_installs                        TO GROUP chartio_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA staging_installs GRANT SELECT ON TABLES TO GROUP chartio_group;
REVOKE CREATE              ON SCHEMA staging_installs                      FROM GROUP chartio_group;

GRANT ALL                  ON SCHEMA staging_installs                        TO GROUP service_group;
