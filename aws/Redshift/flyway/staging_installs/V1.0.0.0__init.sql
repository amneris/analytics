GRANT USAGE ON SCHEMA                staging_installs                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_installs                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   staging_installs GRANT SELECT ON TABLES TO analyticsuser;
