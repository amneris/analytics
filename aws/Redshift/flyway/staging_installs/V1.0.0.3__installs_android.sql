CREATE TABLE staging_installs.installs_android
(
  day               DATE          NOT NULL    ENCODE DELTA,
  tracker           varchar(50)   NOT NULL    ENCODE LZO,
  network           VARCHAR(200)  NOT NULL    ENCODE LZO,
  campaign          VARCHAR(200)              ENCODE LZO,
  adgroup           VARCHAR(200)              ENCODE LZO,
  country           varchar(2)                ENCODE BYTEDICT,
  channel           VARCHAR(200)              ENCODE RAW,
  installs          INT                       ENCODE MOSTLY8,


  PRIMARY KEY (day, tracker, network)

);