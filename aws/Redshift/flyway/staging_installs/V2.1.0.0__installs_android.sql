TRUNCATE TABLE staging_installs.installs_android;

INSERT INTO staging_installs.installs_android
  SELECT *
  FROM staging_installs.consolidated_installs_android;
