TRUNCATE TABLE staging_installs.installs_ios;

INSERT INTO staging_installs.installs_ios
  SELECT *
  FROM staging_installs.consolidated_installs_ios;
