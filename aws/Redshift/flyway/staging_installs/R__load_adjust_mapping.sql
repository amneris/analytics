TRUNCATE TABLE staging_installs.adjust_mapping;

COPY staging_installs.adjust_mapping
FROM '${S3_data_bucket}/static/staging_installs/'
IAM_ROLE 'arn:aws:iam::${aws_account_id}:role/analytics-redshift-loader-role'

DELIMITER '\t'
IGNOREHEADER AS 1
REMOVEQUOTES;
