CREATE TABLE staging_installs.adjust_mapping
(

  adj_network        VARCHAR(200)  NOT NULL    ENCODE LZO,
  adj_campaign       VARCHAR(200)  NOT NULL    ENCODE LZO,
  partner_id         BIGINT                    ENCODE MOSTLY16,
  channel_id         SMALLINT      NOT NULL    ENCODE BYTEDICT,



  PRIMARY KEY (adj_network,adj_campaign)

);



