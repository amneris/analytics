GRANT USAGE ON SCHEMA                staging_users                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_users                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   staging_users GRANT SELECT ON TABLES TO analyticsuser;
