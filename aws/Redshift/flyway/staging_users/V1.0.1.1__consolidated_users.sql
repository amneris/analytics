CREATE TABLE staging_users.new_consolidated_users
(
  id                  INT              PRIMARY KEY  NOT NULL,
  email               VARCHAR(800)                  NOT NULL,
  countryId           INT                           NOT NULL,
  entryDate           TIMESTAMP,
  idPartnerSource     BIGINT,
  idSourceList        BIGINT

)
  DISTKEY(id)
  INTERLEAVED SORTKEY(entryDate);

INSERT INTO staging_users.new_consolidated_users SELECT * FROM staging_users.consolidated_users;

ALTER TABLE staging_users.consolidated_users RENAME TO old_users;

ALTER TABLE staging_users.new_consolidated_users RENAME TO consolidated_users;
