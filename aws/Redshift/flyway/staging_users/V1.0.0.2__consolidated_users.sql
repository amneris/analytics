CREATE TABLE staging_users.consolidated_users
(
  id                  INT              PRIMARY KEY  NOT NULL,
  email               VARCHAR(800)                  NOT NULL,
  countryId           INT                           NOT NULL,
  entryDate           TIMESTAMP,
  idPartnerSource     INT,
  idSourceList        BIGINT

)
DISTKEY(id)
INTERLEAVED SORTKEY(entryDate);


