DROP TABLE IF EXISTS staging_users.users;

CREATE TABLE staging_users.users
(
  id                  INT              PRIMARY KEY  NOT NULL,
  email               VARCHAR(800)                  NOT NULL,
  countryId           INT                           NOT NULL,
  entryDate           TIMESTAMP,
  idPartnerSource     BIGINT,
  idSourceList        BIGINT

)
  DISTKEY(id)
  INTERLEAVED SORTKEY(entryDate);

INSERT INTO staging_users.users SELECT * FROM staging_users.consolidated_users;

