TRUNCATE TABLE staging_investment.campaign_mapping;

COPY staging_investment.campaign_mapping
FROM '${S3_data_bucket}/static/staging_investment/campaign_mapping.tsv'
IAM_ROLE 'arn:aws:iam::${aws_account_id}:role/analytics-redshift-loader-role'

DELIMITER '\t'
IGNOREHEADER AS 1
REMOVEQUOTES;
