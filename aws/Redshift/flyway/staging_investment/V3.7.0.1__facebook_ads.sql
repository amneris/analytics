
CREATE TABLE staging_investment.facebook_ads
(
  campaign_id       BIGINT          NOT NULL        ENCODE ZSTD,
  campaign_name     VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  adset_id          BIGINT          NOT NULL        ENCODE ZSTD,
  adset_name        VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  clicks            INT                             ENCODE ZSTD,
  impressions       INT                             ENCODE ZSTD,
  spend             NUMERIC(12,2)                   ENCODE ZSTD,
  date_start        DATE            NOT NULL        ENCODE ZSTD,
  date_stop         DATE            NOT NULL        ENCODE ZSTD,

  PRIMARY KEY (date_start, campaign_id, adset_id)

);
