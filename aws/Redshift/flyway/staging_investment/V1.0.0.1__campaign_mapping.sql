
CREATE TABLE staging_investment.campaign_mapping
(

  name              VARCHAR(200)  NOT NULL    ENCODE LZO,
  network           VARCHAR(200)  NOT NULL    ENCODE LZO,
  channel_id        SMALLINT                  ENCODE BYTEDICT,
  country_id        SMALLINT                  ENCODE BYTEDICT,
  partner_id        BIGINT                    ENCODE MOSTLY16,
  source_id         INT                       ENCODE MOSTLY16,


  PRIMARY KEY (name,network)

);



