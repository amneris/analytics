
CREATE TABLE staging_investment.investment
(
  common_cost       NUMERIC(12, 2)            ENCODE MOSTLY16,
  common_clicks     INT                       ENCODE MOSTLY8,
  day               DATE          NOT NULL    ENCODE DELTA,
  campagin          VARCHAR(200)  NOT NULL    ENCODE LZO,
  channel           VARCHAR(200)  NOT NULL    ENCODE BYTEDICT,

  PRIMARY KEY (day, campagin, channel)

);



