GRANT USAGE ON SCHEMA                staging_investment                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_investment                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   staging_investment GRANT SELECT ON TABLES TO analyticsuser;

