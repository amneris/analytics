
CREATE TABLE staging_investment.google_uac
(
  day               DATE            NOT NULL        ENCODE ZSTD,
  campaign_id       BIGINT          NOT NULL        ENCODE ZSTD,
  campaign_name     VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  ad_network        VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  clicks            INT                             ENCODE ZSTD,
  impressions       INT                             ENCODE ZSTD,
  cost              BIGINT                          ENCODE ZSTD,

  PRIMARY KEY (day, campaign_id, ad_network)

);
