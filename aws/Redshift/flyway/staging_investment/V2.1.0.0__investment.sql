TRUNCATE TABLE staging_investment.investment;

INSERT INTO staging_investment.investment
  SELECT *
  FROM staging_investment.consolidated_investment;
