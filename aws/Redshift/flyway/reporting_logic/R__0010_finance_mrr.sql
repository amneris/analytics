-- View for calculating the MRR (Finance view)
CREATE OR REPLACE VIEW reporting_logic.finance_mrr AS (
  SELECT
    month_of_payment,
    month_of_conversion,
    add_months(month_of_payment, month_offset) :: DATE                        AS month_of_mrr,
    date_diff('month', month_of_conversion, add_months(month_of_payment,
                                                       month_offset) :: DATE) AS months_after_conversion,
    product_id :: VARCHAR(5)                                                  AS product,
    sale_type,

    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_main_market,
    acquisition_continent,
    acquisition_channel,

    payment_country,
    payment_cgroup,
    payment_region,
    payment_main_market,
    payment_continent,
    payment_channel,

    sum(sales)                                                                AS active_subscriptions,
    sum(net_revenue / divisor)                                                AS net_mrr,
    sum(gross_revenue / divisor)                                              AS gross_mrr

  FROM
    (
      SELECT
        DATE_TRUNC('month', usr.date_of_activity) :: DATE AS month_of_payment,
        DATE_TRUNC('month', conversion_date) :: DATE      AS month_of_conversion,
        product_id,
        CASE WHEN class_subtype = '1'
          THEN 'First'
        ELSE 'Next' END                                   AS sale_type,
        acq_country.country                               AS acquisition_country,
        acq_country.cgroup                                AS acquisition_cgroup,
        acq_country.region                                AS acquisition_region,
        acq_country.main_market                           AS acquisition_main_market,
        acq_country.continent                             AS acquisition_continent,
        acq_channel.channel                               AS acquisition_channel,
        pay_country.country                               AS payment_country,
        pay_country.cgroup                                AS payment_cgroup,
        pay_country.region                                AS payment_region,
        pay_country.main_market                           AS payment_main_market,
        pay_country.continent                             AS payment_continent,
        pay_channel.channel                               AS payment_channel,
        sum(amount_net_eur)                               AS net_revenue,
        sum(amount_gross_eur)                             AS gross_revenue,
        count(*)                                          AS sales
      FROM core.fct_user_lifecycle AS usr
        INNER JOIN (
                     SELECT
                       user_id,
                       date_of_activity AS conversion_date
                     FROM core.fct_user_lifecycle
                     WHERE class_type = 'Clean Sale' AND class_subtype = '1'
                   ) AS conversions USING (user_id)
        INNER JOIN core.dim_payment_gateway AS gateway USING (payment_gateway_id)
        INNER JOIN core.dim_channel AS pay_channel ON gateway.channel_id = pay_channel.channel_id
        INNER JOIN core.dim_campaign AS campaign USING (campaign_id)
        INNER JOIN core.dim_channel AS acq_channel ON campaign.channel_id = acq_channel.channel_id
        INNER JOIN core.dim_country AS acq_country
          ON
            CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
              THEN campaign_country_id
            ELSE acquisition_country_id
            END = acq_country.country_id
        INNER JOIN core.dim_country AS pay_country ON pay_country.country_id = usr.country_id
      WHERE class_type = 'Clean Sale'
      GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
    ) tgt
    INNER JOIN core.lkp_mrr_series USING (product_id)

  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
);
