-- View for calculating the MRR (Finance view)
CREATE OR REPLACE VIEW reporting_logic.usage_premium_cohorted_activity AS (
  SELECT
    date_of_conversion,
    partner               AS acquisition_partner,
    network               AS acquisition_network,
    partner_category      AS acquisition_category,
    partner_type          AS acquisition_type,
    country               AS acquisition_country,
    cgroup                AS acquisition_cgroup,
    region                AS acquisition_region,
    continent             AS acquisition_continent,
    main_market           AS acquisition_main_market,
    channel               AS acquisition_channel,
    device                AS activity_device,
    conversion_product_id AS conversion_product,

    -- To calculate the active users from the conversion date, we count the number of active users
    -- who did some study activity
    SUM(CASE WHEN class_type = 'Study' AND week_diff = 0
      THEN user_count
        ELSE 0 END)       AS active_users_0W,
    SUM(CASE WHEN class_type = 'Study' AND week_diff = 1
      THEN user_count
        ELSE 0 END)       AS active_users_1W,
    SUM(CASE WHEN class_type = 'Study' AND week_diff = 4
      THEN user_count
        ELSE 0 END)       AS active_users_4W,
    SUM(CASE WHEN class_type = 'Study' AND week_diff = 10
      THEN user_count
        ELSE 0 END)       AS active_users_10W,
    SUM(CASE WHEN class_type = 'Study' AND week_diff = 25
      THEN user_count
        ELSE 0 END)       AS active_users_25W,
    SUM(CASE WHEN class_type = 'Study' AND week_diff = 51
      THEN user_count
        ELSE 0 END)       AS active_users_51W,

    -- To compute the number of watched videos N weeks after conversion
    SUM(CASE WHEN class_type = 'Videos' AND week_diff = 0
      THEN quantity
        ELSE 0 END)       AS watched_videos_0W,
    SUM(CASE WHEN class_type = 'Videos' AND week_diff = 1
      THEN quantity
        ELSE 0 END)       AS watched_videos_1W,
    SUM(CASE WHEN class_type = 'Videos' AND week_diff = 4
      THEN quantity
        ELSE 0 END)       AS watched_videos_4W,
    SUM(CASE WHEN class_type = 'Videos' AND week_diff = 10
      THEN quantity
        ELSE 0 END)       AS watched_videos_10W,
    SUM(CASE WHEN class_type = 'Videos' AND week_diff = 25
      THEN quantity
        ELSE 0 END)       AS watched_videos_25W,
    SUM(CASE WHEN class_type = 'Videos' AND week_diff = 51
      THEN quantity
        ELSE 0 END)       AS watched_videos_51W,

    -- To compute the number of finished units N weeks after conversion
    SUM(CASE WHEN class_type = 'Units' AND week_diff = 0
      THEN quantity
        ELSE 0 END)       AS finished_units_0W,
    SUM(CASE WHEN class_type = 'Units' AND week_diff = 1
      THEN quantity
        ELSE 0 END)       AS finished_units_1W,
    SUM(CASE WHEN class_type = 'Units' AND week_diff = 4
      THEN quantity
        ELSE 0 END)       AS finished_units_4W,
    SUM(CASE WHEN class_type = 'Units' AND week_diff = 10
      THEN quantity
        ELSE 0 END)       AS finished_units_10W,
    SUM(CASE WHEN class_type = 'Units' AND week_diff = 25
      THEN quantity
        ELSE 0 END)       AS finished_units_25W,
    SUM(CASE WHEN class_type = 'Units' AND week_diff = 51
      THEN quantity
        ELSE 0 END)       AS finished_units_51W

  FROM
    (

      SELECT
        date_of_conversion,
        week_diff,
        campaign_id,
        country_id,
        device,
        conversion_product_id,
        class_type,
        SUM(quantity)           AS quantity,
        COUNT(DISTINCT user_id) AS user_count

      FROM
        (
          SELECT
            *,
            (date_of_activity - date_of_conversion) / 7 :: INT AS week_diff
          FROM core.fct_premium_activity
          WHERE date_of_conversion IS NOT NULL
        ) tgt
      WHERE week_diff IN (0, 1, 4, 10, 25, 51)
      GROUP BY 1, 2, 3, 4, 5, 6, 7
    ) src
    INNER JOIN core.dim_campaign USING (campaign_id)
    INNER JOIN core.dim_partner USING (partner_id)
    INNER JOIN core.dim_country USING (country_id)
    INNER JOIN core.dim_channel USING (channel_id)

  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13

);
