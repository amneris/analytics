-- Conversion component for Conversion Rate analysis, especially to analyze CR distribution over time
--
-- Takes last 36 months of conversions.
-- To keep the record count down the results are on cgroup level (renamed to Country in ChartIO).
CREATE OR REPLACE VIEW reporting_logic.conversion_performance_conversions AS (
  SELECT
    date_of_acquisition,
    conv.date_of_activity                                     AS date_of_conversion,
    product_id :: VARCHAR(5)                                  AS product,
    acq_country.cgroup                                        AS acquisition_cgroup,
    acq_country.region                                        AS acquisition_region,
    acq_country.continent                                     AS acquisition_continent,
    acq_country.main_market                                   AS acquisition_main_market,
    acq_channel.channel                                       AS acquisition_channel,
    conv_country.cgroup                                       AS payment_cgroup,
    conv_country.region                                       AS payment_region,
    conv_country.continent                                    AS payment_continent,
    conv_country.main_market                                  AS payment_main_market,
    conv_channel.channel                                      AS payment_channel,
    (conv.date_of_activity - date_of_acquisition)             AS days_to_conversion,
    FLOOR((conv.date_of_activity - date_of_acquisition) / 7)  AS weeks_to_conversion,
    FLOOR((conv.date_of_activity - date_of_acquisition) / 30) AS months_to_conversion,
    CASE WHEN (conv.date_of_activity - date_of_acquisition) BETWEEN 0 AND 15
      THEN '0-15'
    WHEN (conv.date_of_activity - date_of_acquisition) BETWEEN 16 AND 30
      THEN '16-30'
    WHEN (conv.date_of_activity - date_of_acquisition) > 30
      THEN '30+'
    END                                                       AS days_to_pay,
    exclusive_range                                           AS grouped_days_to_pay,
    COUNT(*)                                                  AS conversions,
    COALESCE(SUM(amount_net_eur), 0)                          AS revenue
  FROM core.fct_user_lifecycle AS conv
    INNER JOIN core.dim_campaign USING (campaign_id)
    INNER JOIN core.dim_country AS acq_country
      ON
        CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
          THEN campaign_country_id
        ELSE acquisition_country_id
        END = acq_country.country_id
    INNER JOIN core.dim_channel AS acq_channel ON dim_campaign.channel_id = acq_channel.channel_id
    INNER JOIN core.dim_country AS conv_country ON conv.country_id = conv_country.country_id
    INNER JOIN core.dim_payment_gateway USING (payment_gateway_id)
    INNER JOIN core.dim_channel AS conv_channel ON dim_payment_gateway.channel_id = conv_channel.channel_id
    INNER JOIN core.dim_range
      ON (conv.date_of_activity - date_of_acquisition) BETWEEN exclusive_range_low AND range_high
  WHERE class_type = 'Clean Sale'
        AND class_subtype = '1'
        -- Filter out inconsistent data
        AND conv.date_of_activity >= date_of_acquisition
        AND conv.date_of_activity BETWEEN DATE_TRUNC('month',CURRENT_DATE - INTERVAL '36 months') AND CURRENT_DATE
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18

);
