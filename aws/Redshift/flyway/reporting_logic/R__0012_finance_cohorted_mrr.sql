-- View for calculating the MRR (Finance view)
CREATE OR REPLACE VIEW reporting_logic.finance_cohorted_mrr AS (

  SELECT *,

    sum(net_mrr)
    OVER (PARTITION BY month_of_acquisition, country, cgroup, region, main_market, continent, channel
    ORDER BY month_of_mrr rows unbounded preceding)
    AS net_mrr_cumulative,

    sum(gross_mrr)
    OVER (PARTITION BY month_of_acquisition, country, cgroup, region, main_market, continent, channel
    ORDER BY month_of_mrr rows unbounded preceding)
    AS gross_mrr_cumulative,


    sum(investment)
    OVER (PARTITION BY month_of_acquisition, country, cgroup, region, main_market, continent, channel
    ORDER BY month_of_mrr rows unbounded preceding)
    AS investment_cumulative

  FROM (

      SELECT
        month_of_acquisition,
        add_months(month_of_payment, month_offset) :: DATE                        AS month_of_mrr,

        acq_country.country,
        acq_country.cgroup,
        acq_country.region,
        acq_country.main_market,
        acq_country.continent,
        acq_channel.channel,


        sum(sales)                                                                AS active_subscriptions,
        sum(net_revenue / divisor)                                                AS net_mrr,
        sum(gross_revenue / divisor)                                              AS gross_mrr,
        sum(investment)                                                           AS investment

      FROM
        (
          SELECT
            DATE_TRUNC('month', usr.date_of_activity) :: DATE AS month_of_payment,

            DATE_TRUNC('month', date_of_acquisition) :: DATE      AS month_of_acquisition,
            product_id,

            campaign_id,
            acquisition_country_id                             AS country_id,

            sum(amount_net_eur)                               AS net_revenue,
            sum(amount_gross_eur)                             AS gross_revenue,
            count(*)                                          AS sales,
            0                                                 AS investment
          FROM core.fct_user_lifecycle AS usr

          WHERE class_type = 'Clean Sale'
          GROUP BY 1, 2, 3, 4, 5

          UNION ALL


          select
            DATE_TRUNC('month',date_of_activity)::DATE         AS month_of_acquisition,
            DATE_TRUNC('month',date_of_activity)::DATE         AS month_of_mrr,
            30                                                 AS product_id,

            campaign_id,
            country_id,

            0 active_subscriptions,
            0 net_mrr,
            0 gross_mrr,
            sum(amount_net_eur)          AS investment
          from core.fct_campaign_performance  where class_type = 'Investment'
          GROUP BY 1, 2, 3, 4, 5


        ) t
        INNER JOIN core.lkp_mrr_series USING (product_id)
        INNER JOIN core.dim_campaign AS campaign USING (campaign_id)
        INNER JOIN core.dim_channel AS acq_channel ON campaign.channel_id = acq_channel.channel_id
        INNER JOIN core.dim_country AS acq_country
          ON
            CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
              THEN campaign_country_id
            ELSE t.country_id
            END = acq_country.country_id
      GROUP BY 1,2,3,4,5,6,7,8
        )
 );
