-- View for Actual Revenue for Finance reporting
-- Includes the buyer payment currency and separate metrics for tax and fee. Last 36 months.

CREATE OR REPLACE VIEW reporting_logic.finance_actual_revenue AS (
  SELECT
    date_of_activity                                                 AS payment_date,
    country                                                          AS payment_country,
    cgroup                                                           AS payment_cgroup,
    region                                                           AS payment_region,
    continent                                                        AS payment_continent,
    main_market                                                      AS payment_market,
    payment_gateway,
    channel                                                          AS payment_channel,
    currency_code                                                    AS payment_currency,
    class_subtype                                                    AS sale_type,
    CASE WHEN is_amex
      THEN 'Yes'
    ELSE 'No' END                                                    AS flag_amex_card,
    -- "Sale" metrics
    COALESCE(SUM(sales_amount_buyercurrency_fee), 0)                 AS sales_amount_buyercurrency_fee,
    COALESCE(SUM(sales_amount_buyercurrency_gross), 0)               AS sales_amount_buyercurrency_gross,
    COALESCE(SUM(sales_amount_buyercurrency_net), 0)                 AS sales_amount_buyercurrency_net,
    COALESCE(SUM(sales_amount_buyercurrency_tax), 0)                 AS sales_amount_buyercurrency_tax,
    COALESCE(SUM(sales_discount_buyercurrency), 0)                   AS sales_discount_buyercurrency,
    COALESCE(SUM(sales_amount_eur_fee), 0)                           AS sales_amount_eur_fee,
    COALESCE(SUM(sales_amount_eur_gross), 0)                         AS sales_amount_eur_gross,
    COALESCE(SUM(sales_amount_eur_net), 0)                           AS sales_amount_eur_net,
    COALESCE(SUM(sales_amount_eur_tax), 0)                           AS sales_amount_eur_tax,
    COALESCE(SUM(sales_discount_eur), 0)                             AS sales_discount_eur,
    COALESCE(SUM(sale_units), 0)                                     AS sale_units,
    -- "Refund" metrics
    COALESCE(SUM(return_amount_buyercurrency_fee), 0)                AS return_amount_buyercurrency_fee,
    COALESCE(SUM(return_amount_buyercurrency_gross), 0)              AS return_amount_buyercurrency_gross,
    COALESCE(SUM(return_amount_buyercurrency_net), 0)                AS return_amount_buyercurrency_net,
    COALESCE(SUM(return_amount_buyercurrency_tax), 0)                AS return_amount_buyercurrency_tax,
    COALESCE(SUM(return_amount_eur_fee), 0)                          AS return_amount_eur_fee,
    COALESCE(SUM(return_amount_eur_gross), 0)                        AS return_amount_eur_gross,
    COALESCE(SUM(return_amount_eur_net), 0)                          AS return_amount_eur_net,
    COALESCE(SUM(return_amount_eur_tax), 0)                          AS return_amount_eur_tax,
    COALESCE(SUM(return_units), 0)                                   AS return_units,
    -- "Sale" - "Refund" metrics
    SUM(COALESCE(sales_amount_buyercurrency_fee, 0)   - COALESCE(return_amount_buyercurrency_fee, 0))   AS revenue_amount_buyercurrency_fee,
    SUM(COALESCE(sales_amount_buyercurrency_gross, 0) - COALESCE(return_amount_buyercurrency_gross, 0)) AS revenue_amount_buyercurrency_gross,
    SUM(COALESCE(sales_amount_buyercurrency_net, 0)   - COALESCE(return_amount_buyercurrency_net, 0))   AS revenue_amount_buyercurrency_net,
    SUM(COALESCE(sales_amount_buyercurrency_tax, 0)   - COALESCE(return_amount_buyercurrency_tax, 0))   AS revenue_amount_buyercurrency_tax,
    SUM(COALESCE(sales_amount_eur_fee, 0)             - COALESCE(return_amount_eur_fee, 0))             AS revenue_amount_eur_fee,
    SUM(COALESCE(sales_amount_eur_gross, 0)           - COALESCE(return_amount_eur_gross, 0))           AS revenue_amount_eur_gross,
    SUM(COALESCE(sales_amount_eur_net, 0)             - COALESCE(return_amount_eur_net, 0))             AS revenue_amount_eur_net,
    SUM(COALESCE(sales_amount_eur_tax, 0)             - COALESCE(return_amount_eur_tax, 0))             AS revenue_amount_eur_tax,
    SUM(COALESCE(sale_units, 0)                       - COALESCE(return_units, 0))                      AS net_units
  FROM (
         SELECT
           date_of_activity,
           country_id,
           payment_gateway_id,
           is_amex,
           currency_code,
           class_subtype,
           fee_original          AS sales_amount_buyercurrency_fee,
           amount_gross_original AS sales_amount_buyercurrency_gross,
           amount_net_original   AS sales_amount_buyercurrency_net,
           tax_original          AS sales_amount_buyercurrency_tax,
           discount_original     AS sales_discount_buyercurrency,
           fee_eur               AS sales_amount_eur_fee,
           amount_gross_eur      AS sales_amount_eur_gross,
           amount_net_eur        AS sales_amount_eur_net,
           tax_eur               AS sales_amount_eur_tax,
           discount_eur          AS sales_discount_eur,
           1                     AS sale_units,
           NULL                  AS return_amount_buyercurrency_fee,
           NULL                  AS return_amount_buyercurrency_gross,
           NULL                  AS return_amount_buyercurrency_net,
           NULL                  AS return_amount_buyercurrency_tax,
           NULL                  AS return_amount_eur_fee,
           NULL                  AS return_amount_eur_gross,
           NULL                  AS return_amount_eur_net,
           NULL                  AS return_amount_eur_tax,
           NULL                  AS return_units
         FROM core.fct_finance
         WHERE class_type = 'Sale'
               AND date_of_activity BETWEEN DATE_TRUNC('month', CURRENT_DATE - INTERVAL '36 months') AND CURRENT_DATE

         UNION ALL

         SELECT
           date_of_activity,
           country_id,
           payment_gateway_id,
           is_amex,
           currency_code,
           -- Only "First" or "Next". Unmatched refunds with type "Unknown" are "Next" in this context.
           CASE
             WHEN class_subtype = 'Unknown' THEN 'Next'
             ELSE class_subtype
           END                   AS class_subtype,
           NULL                  AS sales_amount_buyercurrency_fee,
           NULL                  AS sales_amount_buyercurrency_gross,
           NULL                  AS sales_amount_buyercurrency_net,
           NULL                  AS sales_amount_buyercurrency_tax,
           NULL                  AS sales_discount_buyercurrency,
           NULL                  AS sales_amount_eur_fee,
           NULL                  AS sales_amount_eur_gross,
           NULL                  AS sales_amount_eur_net,
           NULL                  AS sales_amount_eur_tax,
           NULL                  AS sales_discount_eur,
           NULL                  AS sale_units,
           fee_original          AS return_amount_buyercurrency_fee,
           amount_gross_original AS return_amount_buyercurrency_gross,
           amount_net_original   AS return_amount_buyercurrency_net,
           tax_original          AS return_amount_buyercurrency_tax,
           fee_eur               AS return_amount_eur_fee,
           amount_gross_eur      AS return_amount_eur_gross,
           amount_net_eur        AS return_amount_eur_net,
           tax_eur               AS return_amount_eur_tax,
           1                     AS return_units
         FROM core.fct_finance
         WHERE class_type = 'Refund'
               AND date_of_activity BETWEEN DATE_TRUNC('month', CURRENT_DATE - INTERVAL '36 months') AND CURRENT_DATE
       ) AS fact
    INNER JOIN core.dim_country USING (country_id)
    INNER JOIN core.dim_payment_gateway USING (payment_gateway_id)
    INNER JOIN core.dim_channel USING (channel_id)
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
);
