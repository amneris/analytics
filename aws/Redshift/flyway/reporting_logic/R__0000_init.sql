GRANT USAGE ON SCHEMA                reporting_logic                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA reporting_logic                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   reporting_logic GRANT SELECT ON TABLES TO analyticsuser;

GRANT USAGE                ON SCHEMA reporting_logic                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA reporting_logic                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA reporting_logic GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA reporting_logic                      FROM GROUP redash_group;

REVOKE ALL                 ON SCHEMA reporting_logic         FROM GROUP chartio_group;
GRANT ALL                  ON SCHEMA reporting_logic         TO GROUP   service_group;
