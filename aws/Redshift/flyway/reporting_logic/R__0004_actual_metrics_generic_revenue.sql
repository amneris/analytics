-- Generic view for actual metrics in order to have the logic in one place only. Not exposed to users in ChartIO.
--
-- Calculates Actual Revenue as "Sale" - "Refund". Uses user fact because we need the payment dimensions. Separates
-- "First" to "First Early" and "First Late" depending on the date difference between acquisition and payment/refund.
-- Note that while first sales are quite evenly distributed between early and late, first refunds mostly fall to "First
-- Late". (If this is a problem you can group them back together in ChartIO reports.)

CREATE OR REPLACE VIEW reporting_logic.actual_metrics_generic_revenue AS (
  SELECT
    payments.date_of_activity,
    product_id :: VARCHAR(5) AS product,
    acq_country.country      AS acquisition_country,
    acq_country.cgroup       AS acquisition_cgroup,
    acq_country.region       AS acquisition_region,
    acq_country.continent    AS acquisition_continent,
    acq_country.main_market  AS acquisition_main_market,
    acq_channel.channel      AS acquisition_channel,
    pay_country.country      AS payment_country,
    pay_country.cgroup       AS payment_cgroup,
    pay_country.region       AS payment_region,
    pay_country.continent    AS payment_continent,
    pay_country.main_market  AS payment_main_market,
    pay_channel.channel      AS payment_channel,
    -- Only "First" or "Next". Unmatched refunds with type "Unknown" are "Next" in this context.
    CASE WHEN class_subtype = 'Unknown' THEN 'Next'
      ELSE class_subtype END AS sale_type,
    -- More detail around first sale:
    -- "First Early" (0-30 days),
    -- "First Late" (>30 days),
    -- "First Unknown" (inconsistent data)
    -- and "Next". Unmatched refunds with type "Unknown" are "Next" in this context.
    CASE WHEN class_subtype = 'First' AND payments.date_of_activity - date_of_acquisition BETWEEN 0 AND 30
      THEN 'First Early'
    WHEN class_subtype = 'First' AND payments.date_of_activity - date_of_acquisition > 30
      THEN 'First Late'
    WHEN class_subtype = 'First' AND payments.date_of_activity - date_of_acquisition < 0
      THEN 'First Unknown'
    ELSE 'Next' END          AS detailed_sale_type,
    COALESCE(SUM(
      CASE WHEN class_type = 'Sale'   THEN  amount_net_eur
           WHEN class_type = 'Refund' THEN -amount_net_eur
      END), 0)               AS revenue,
    COALESCE(SUM(
      CASE WHEN class_type = 'Sale'   THEN  amount_gross_eur
           WHEN class_type = 'Refund' THEN -amount_gross_eur
      END), 0)               AS revenue_gross,
    COALESCE(SUM(
      CASE WHEN class_type = 'Sale'   THEN  1
           WHEN class_type = 'Refund' THEN -1
      END), 0)               AS sales,
    COALESCE(SUM(
      CASE WHEN class_type = 'Sale' AND amount_gross_eur BETWEEN 0 AND 0.01
        THEN 1
      END), 0)               AS free_trials
  FROM core.fct_user_lifecycle AS payments
    INNER JOIN core.dim_campaign USING (campaign_id)
    INNER JOIN core.dim_country AS acq_country
      ON
        CASE WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
          THEN campaign_country_id
        ELSE acquisition_country_id
        END = acq_country.country_id
    INNER JOIN core.dim_channel AS acq_channel ON dim_campaign.channel_id = acq_channel.channel_id
    INNER JOIN core.dim_country AS pay_country ON payments.country_id = pay_country.country_id
    INNER JOIN core.dim_payment_gateway USING (payment_gateway_id)
    INNER JOIN core.dim_channel AS pay_channel ON dim_payment_gateway.channel_id = pay_channel.channel_id
  WHERE class_type IN ('Sale', 'Refund')
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
);
