CREATE OR REPLACE VIEW reporting_logic.campaign_cohorted_performance_1_get_cohorted_metrics AS (
  SELECT
    date_of_activity,
    campaign_id,
    country_id,
    COALESCE(SUM(investment), 0)                    AS investment,
    COALESCE(SUM(acquisition), 0)                   AS acquisition,
    COALESCE(SUM(installs), 0)                      AS installs,
    COALESCE(SUM(conversions), 0)                   AS conversions,
    COALESCE(SUM(revenue), 0)                       AS revenue
  FROM (
    -- INVESTMENT
    SELECT
      date_of_activity,
      campaign_id,
      country_id,
      amount_net_eur                                AS investment,
      NULL                                          AS acquisition,
      NULL                                          AS installs,
      NULL                                          AS conversions,
      NULL                                          AS revenue
    FROM core.fct_campaign_performance
    WHERE class_type = 'Investment'

    UNION ALL

    -- ACQUISITION
    SELECT
      date_of_activity,
      campaign_id,
      country_id,
      NULL                                          AS investment,
      quantity                                      AS acquisition,
      NULL                                          AS installs,
      NULL                                          AS conversions,
      NULL                                          AS revenue
    FROM core.fct_campaign_performance
    WHERE class_type = 'Acquisition'

    UNION ALL

    -- INSTALLS
    SELECT
      date_of_activity,
      campaign_id,
      country_id,
      NULL                                          AS investment,
      NULL                                          AS acquisition,
      quantity                                      AS installs,
      NULL                                          AS conversions,
      NULL                                          AS revenue
    FROM core.fct_campaign_performance
    WHERE class_type = 'Installs'

    UNION ALL

    -- CONVERSIONS (Sale - Refund), adjusted to date of acquisition
    SELECT
      (date_of_activity - date_diff)                AS date_of_activity,
      campaign_id,
      country_id,
      NULL                                          AS investment,
      NULL                                          AS acquisition,
      NULL                                          AS installs,
      CASE
        WHEN class_type = 'Sale'   THEN  quantity
        WHEN class_type = 'Refund' THEN -quantity
      END                                           AS conversions,
      NULL                                          AS revenue
    FROM core.fct_campaign_performance
    WHERE class_type IN ('Sale', 'Refund')
          AND class_subtype = 'First'

    UNION ALL

    -- REVENUE (Sale - Refund), adjusted to date of acquisition
    SELECT
      (date_of_activity - date_diff)                AS date_of_activity,
      campaign_id,
      country_id,
      NULL                                          AS investment,
      NULL                                          AS acquisition,
      NULL                                          AS installs,
      NULL                                          AS conversions,
      CASE
        WHEN class_type = 'Sale'   THEN  amount_net_eur
        WHEN class_type = 'Refund' THEN -amount_net_eur
      END                                           AS revenue
    FROM core.fct_campaign_performance
    WHERE class_type IN ('Sale', 'Refund')
    ) fact
  GROUP BY 1,2,3
);
