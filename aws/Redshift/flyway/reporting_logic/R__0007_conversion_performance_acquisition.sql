-- Acquisition component for Conversion Rate analysis, especially to analyze CR distribution over time
-- Last 36 months, on cgroup level (renamed to country in ChartIO)
CREATE OR REPLACE VIEW reporting_logic.conversion_performance_acquisition AS (
  SELECT
    date_of_activity,
    channel,
    cgroup,
    region,
    continent,
    main_market,
    SUM(quantity) AS acquisition
  FROM core.fct_campaign_performance
    INNER JOIN core.dim_campaign USING (campaign_id)
    INNER JOIN core.dim_channel USING (channel_id)
    INNER JOIN core.dim_country USING (country_id)
  WHERE class_type = 'Acquisition'
        AND date_of_activity BETWEEN DATE_TRUNC('month', CURRENT_DATE - INTERVAL '36 months') AND CURRENT_DATE
  GROUP BY 1, 2, 3, 4, 5, 6
);
