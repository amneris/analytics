-- Cohorted performance view with full detail - i.e. campaign and date.
-- Used as a source for all other campaign cohorted performance views and tables.
CREATE OR REPLACE VIEW reporting_logic.campaign_cohorted_performance AS (
  SELECT
    date_of_activity,
    campaign,
    partner,
    partner_type,
    partner_category,
    source_id,
    network,
    channel,
    country,
    investment,
    acquisition,
    installs,
    conversions,
    revenue,
    revenue_1d,
    revenue_3d,
    revenue_7d,
    revenue_15d,
    revenue_30d,
    revenue_90d,
    revenue_180d,
    revenue_360d,
    conversions_1d,
    conversions_3d,
    conversions_7d,
    conversions_15d,
    conversions_30d,
    conversions_90d,
    conversions_180d,
    conversions_360d,
    cgroup,
    region,
    continent,
    main_market
  FROM reporting_logic.campaign_cohorted_performance_2_get_closed_metrics
    INNER JOIN core.dim_campaign USING (campaign_id)
    INNER JOIN core.dim_partner USING (partner_id)
    INNER JOIN core.dim_channel USING (channel_id)
    INNER JOIN core.dim_country USING (country_id)
);
