-- Actual revenue & sales, investment and margin
--
-- Calculates Actual Revenue as "Sale" - "Refund". Uses campaign fact because we need investment. Pivot sale type to
-- columns to make sale type and investment compatible.
--
-- Splits "First" to "First Early" and "First Late" depending on the date difference between acquisition and
-- payment/refund. "First" with inconsistent data - negative date_diff - becomes "First Unknown".
-- Note that while first sales are quite evenly distributed between early and late, first refunds mostly fall to
-- "First Late". (If this is a problem you can group them back together in ChartIO reports.)
--
-- Free Trials are included as a separate metric to allow reporting on them. Sales already include free trials.
-- If you need to get only the sales where amount > 0 you can calculate that as "Sales" - "Free Trials" in ChartIO.

CREATE OR REPLACE VIEW reporting_logic.actual_metrics_profitability AS (
  SELECT
    date_of_activity,
    country                                     AS acquisition_country,
    cgroup                                      AS acquisition_cgroup,
    region                                      AS acquisition_region,
    continent                                   AS acquisition_continent,
    main_market                                 AS acquisition_main_market,
    channel                                     AS acquisition_channel,
    -- Total metrics for the majority of use cases
    SUM(COALESCE(revenue_first_early, 0)
      + COALESCE(revenue_first_late, 0)
      + COALESCE(revenue_first_unknown, 0)
      + COALESCE(revenue_next, 0))              AS revenue_total,
    COALESCE(SUM(investment), 0)                AS investment,
    SUM(COALESCE(revenue_first_early, 0)
      + COALESCE(revenue_first_late, 0)
      + COALESCE(revenue_first_unknown, 0)
      + COALESCE(revenue_next, 0)
      - COALESCE(investment, 0))                AS margin,
    SUM(COALESCE(sales_first_early, 0)
      + COALESCE(sales_first_late, 0)
      + COALESCE(sales_first_unknown, 0)
      + COALESCE(sales_next, 0))                AS sales_total,
    -- Metrics by sale type for detailed analysis
    COALESCE(SUM(revenue_first_early), 0)       AS revenue_first_early,
    COALESCE(SUM(revenue_first_late), 0)        AS revenue_first_late,
    COALESCE(SUM(revenue_first_unknown), 0)     AS revenue_first_unknown,
    SUM(COALESCE(revenue_first_early, 0)
      + COALESCE(revenue_first_late, 0)
      + COALESCE(revenue_first_unknown, 0))     AS revenue_first_total,
    COALESCE(SUM(revenue_next), 0)              AS revenue_next,
    COALESCE(SUM(sales_first_early), 0)         AS sales_first_early,
    COALESCE(SUM(sales_first_late), 0)          AS sales_first_late,
    COALESCE(SUM(sales_first_unknown), 0)       AS sales_first_unknown,
    SUM(COALESCE(sales_first_early, 0)
      + COALESCE(sales_first_late, 0)
      + COALESCE(sales_first_unknown, 0))       AS sales_first_total,
    COALESCE(SUM(sales_next), 0)                AS sales_next,
    COALESCE(SUM(free_trials_first_early), 0)   AS free_trials_first_early,
    COALESCE(SUM(free_trials_first_late), 0)    AS free_trials_first_late,
    COALESCE(SUM(free_trials_first_unknown), 0) AS free_trials_first_unknown,
    SUM(COALESCE(free_trials_first_early, 0)
      + COALESCE(free_trials_first_late, 0)
      + COALESCE(free_trials_first_unknown, 0)) AS free_trials_first_total,
    COALESCE(SUM(free_trials_next), 0)          AS free_trials_next
  FROM (
    -- Investment
    SELECT
      date_of_activity,
      campaign_id,
      country_id,
      amount_net_eur AS investment,
      NULL           AS revenue_first_early,
      NULL           AS revenue_first_late,
      NULL           AS revenue_first_unknown,
      NULL           AS revenue_next,
      NULL           AS sales_first_early,
      NULL           AS sales_first_late,
      NULL           AS sales_first_unknown,
      NULL           AS sales_next,
      NULL           AS free_trials_first_early,
      NULL           AS free_trials_first_late,
      NULL           AS free_trials_first_unknown,
      NULL           AS free_trials_next
    FROM core.fct_campaign_performance
    WHERE class_type = 'Investment'

    UNION ALL

    -- First Sale and First Refund
    SELECT
      date_of_activity,
      campaign_id,
      country_id,
      NULL AS investment,
      CASE
        WHEN class_type = 'Sale'   AND range_id BETWEEN 0 AND 30 THEN  amount_net_eur
        WHEN class_type = 'Refund' AND range_id BETWEEN 0 AND 30 THEN -amount_net_eur
      END  AS revenue_first_early,
      CASE
        WHEN class_type = 'Sale'   AND range_id > 30             THEN  amount_net_eur
        WHEN class_type = 'Refund' AND range_id > 30             THEN -amount_net_eur
      END  AS revenue_first_late,
      CASE
        WHEN class_type = 'Sale'   AND range_id < 0              THEN  amount_net_eur
        WHEN class_type = 'Refund' AND range_id < 0              THEN -amount_net_eur
      END  AS revenue_first_unknown,
      NULL AS revenue_next,
      CASE
        WHEN class_type = 'Sale'   AND range_id BETWEEN 0 AND 30 THEN  quantity
        WHEN class_type = 'Refund' AND range_id BETWEEN 0 AND 30 THEN -quantity
      END  AS sales_first_early,
      CASE
        WHEN class_type = 'Sale'   AND range_id > 30             THEN  quantity
        WHEN class_type = 'Refund' AND range_id > 30             THEN -quantity
      END  AS sales_first_late,
      CASE
        WHEN class_type = 'Sale'   AND range_id < 0              THEN  quantity
        WHEN class_type = 'Refund' AND range_id < 0              THEN -quantity
      END  AS sales_first_unknown,
      NULL AS sales_next,
      NULL AS free_trials_first_early,
      NULL AS free_trials_first_late,
      NULL AS free_trials_first_unknown,
      NULL AS free_trials_next
    FROM core.fct_campaign_performance
    WHERE class_type IN ('Sale', 'Refund')
          AND class_subtype = 'First'

    UNION ALL

    -- Next Sale and Next/Unknown Refund
    SELECT
      date_of_activity,
      campaign_id,
      country_id,
      NULL AS investment,
      NULL AS revenue_first_early,
      NULL AS revenue_first_late,
      NULL AS revenue_first_unknown,
      CASE
        WHEN class_type = 'Sale'   THEN  amount_net_eur
        WHEN class_type = 'Refund' THEN -amount_net_eur
      END  AS revenue_next,
      NULL AS sales_first_early,
      NULL AS sales_first_late,
      NULL AS sales_first_unknown,
      CASE
        WHEN class_type = 'Sale'   THEN  quantity
        WHEN class_type = 'Refund' THEN -quantity
      END  AS sales_next,
      NULL AS free_trials_first_early,
      NULL AS free_trials_first_late,
      NULL AS free_trials_first_unknown,
      NULL AS free_trials_next
    FROM core.fct_campaign_performance
    WHERE class_type IN ('Sale', 'Refund')
          -- Treat Unknown subtype as Next
          -- Unknown refunds can't be matched to a sale so we don't really know whether they are First or Next
          AND class_subtype IN ('Next', 'Unknown')

  UNION ALL

  -- First Free Trials
  SELECT
    date_of_activity,
    campaign_id,
    country_id,
    NULL AS investment,
    NULL AS revenue_first_early,
    NULL AS revenue_first_late,
    NULL AS revenue_first_unknown,
    NULL AS revenue_next,
    NULL AS sales_first_early,
    NULL AS sales_first_late,
    NULL AS sales_first_unknown,
    NULL AS sales_next,
    CASE WHEN range_id BETWEEN 0 AND 30 THEN quantity END AS free_trials_first_early,
    CASE WHEN range_id > 30             THEN quantity END AS free_trials_first_late,
    CASE WHEN range_id < 0              THEN quantity END AS free_trials_first_unknown,
    NULL AS free_trials_next
  FROM core.fct_campaign_performance
  WHERE class_type = 'Free Trial'
        AND class_subtype = 'First'

  UNION ALL

  -- Next Free Trials
  SELECT
    date_of_activity,
    campaign_id,
    country_id,
    NULL     AS investment,
    NULL     AS revenue_first_early,
    NULL     AS revenue_first_late,
    NULL     AS revenue_first_unknown,
    NULL     AS revenue_next,
    NULL     AS sales_first_early,
    NULL     AS sales_first_late,
    NULL     AS sales_first_unknown,
    NULL     AS sales_next,
    NULL     AS free_trials_first_early,
    NULL     AS free_trials_first_late,
    NULL     AS free_trials_first_unknown,
    quantity AS free_trials_next
  FROM core.fct_campaign_performance
  WHERE class_type = 'Free Trial'
        AND class_subtype = 'Next'
  ) AS fact
    INNER JOIN core.dim_campaign USING (campaign_id)
    INNER JOIN core.dim_channel USING (channel_id)
    INNER JOIN core.dim_country USING (country_id)
  GROUP BY 1, 2, 3, 4, 5, 6, 7
);
