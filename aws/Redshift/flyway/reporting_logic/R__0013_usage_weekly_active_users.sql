-- View for calculating the MRR (Finance view)
CREATE OR REPLACE VIEW reporting_logic.usage_weekly_active_users AS (
    SELECT
      week_of_activity,
      user_type,
      country,
      cgroup,
      region,
      main_market,
      continent,
      device,
      wau,
      sessions
    FROM core.fct_weekly_active_users
    INNER JOIN core.dim_country USING (country_id)

 );
