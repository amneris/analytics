CREATE OR REPLACE VIEW reporting_logic.campaign_cohorted_performance_2_get_closed_metrics AS (
  SELECT
    date_of_activity,
    campaign_id,
    country_id,
    COALESCE(SUM(investment), 0)                    AS investment,
    COALESCE(SUM(acquisition), 0)                   AS acquisition,
    COALESCE(SUM(installs), 0)                      AS installs,
    COALESCE(SUM(conversions), 0)                   AS conversions,
    COALESCE(SUM(revenue), 0)                       AS revenue,
    COALESCE(SUM(revenue_1d), 0)                    AS revenue_1d,
    COALESCE(SUM(revenue_3d), 0)                    AS revenue_3d,
    COALESCE(SUM(revenue_7d), 0)                    AS revenue_7d,
    COALESCE(SUM(revenue_15d), 0)                   AS revenue_15d,
    COALESCE(SUM(revenue_30d), 0)                   AS revenue_30d,
    COALESCE(SUM(revenue_90d), 0)                   AS revenue_90d,
    COALESCE(SUM(revenue_180d), 0)                  AS revenue_180d,
    COALESCE(SUM(revenue_360d), 0)                  AS revenue_360d,
    COALESCE(SUM(conversions_1d), 0)                AS conversions_1d,
    COALESCE(SUM(conversions_3d), 0)                AS conversions_3d,
    COALESCE(SUM(conversions_7d), 0)                AS conversions_7d,
    COALESCE(SUM(conversions_15d), 0)               AS conversions_15d,
    COALESCE(SUM(conversions_30d), 0)               AS conversions_30d,
    COALESCE(SUM(conversions_90d), 0)               AS conversions_90d,
    COALESCE(SUM(conversions_180d), 0)              AS conversions_180d,
    COALESCE(SUM(conversions_360d), 0)              AS conversions_360d
  FROM (
    -- Conversions and revenue, adjusted to date of acquisition
    SELECT
      date_of_activity,
      campaign_id,
      country_id,
      investment,
      acquisition,
      installs,
      conversions,
      revenue,
      NULL                                          AS revenue_1d,
      NULL                                          AS revenue_3d,
      NULL                                          AS revenue_7d,
      NULL                                          AS revenue_15d,
      NULL                                          AS revenue_30d,
      NULL                                          AS revenue_90d,
      NULL                                          AS revenue_180d,
      NULL                                          AS revenue_360d,
      NULL                                          AS conversions_1d,
      NULL                                          AS conversions_3d,
      NULL                                          AS conversions_7d,
      NULL                                          AS conversions_15d,
      NULL                                          AS conversions_30d,
      NULL                                          AS conversions_90d,
      NULL                                          AS conversions_180d,
      NULL                                          AS conversions_360d
    FROM reporting_logic.campaign_cohorted_performance_1_get_cohorted_metrics

    UNION ALL

    -- CLOSED REVENUE
    -- Sale - Refund, adjusted to date of acquisition
    SELECT
      (date_of_activity - date_diff)                AS date_of_activity,
      campaign_id,
      country_id,
      NULL                                          AS investment,
      NULL                                          AS acquisition,
      NULL                                          AS installs,
      NULL                                          AS conversions,
      NULL                                          AS revenue,
      CASE
        WHEN range_id BETWEEN 0 AND   1 AND class_type = 'Sale'   THEN  amount_net_eur
        WHEN range_id BETWEEN 0 AND   1 AND class_type = 'Refund' THEN -amount_net_eur
      END                                           AS revenue_1d,
      CASE
        WHEN range_id BETWEEN 0 AND   3 AND class_type = 'Sale'   THEN  amount_net_eur
        WHEN range_id BETWEEN 0 AND   3 AND class_type = 'Refund' THEN -amount_net_eur
      END                                           AS revenue_3d,
      CASE
        WHEN range_id BETWEEN 0 AND   7 AND class_type = 'Sale'   THEN  amount_net_eur
        WHEN range_id BETWEEN 0 AND   7 AND class_type = 'Refund' THEN -amount_net_eur
      END                                           AS revenue_7d,
      CASE
        WHEN range_id BETWEEN 0 AND  15 AND class_type = 'Sale'   THEN  amount_net_eur
        WHEN range_id BETWEEN 0 AND  15 AND class_type = 'Refund' THEN -amount_net_eur
      END                                           AS revenue_15d,
      CASE
        WHEN range_id BETWEEN 0 AND  30 AND class_type = 'Sale'   THEN  amount_net_eur
        WHEN range_id BETWEEN 0 AND  30 AND class_type = 'Refund' THEN -amount_net_eur
      END                                           AS revenue_30d,
      CASE
        WHEN range_id BETWEEN 0 AND  90 AND class_type = 'Sale'   THEN  amount_net_eur
        WHEN range_id BETWEEN 0 AND  90 AND class_type = 'Refund' THEN -amount_net_eur
      END                                           AS revenue_90d,
      CASE
        WHEN range_id BETWEEN 0 AND 180 AND class_type = 'Sale'   THEN  amount_net_eur
        WHEN range_id BETWEEN 0 AND 180 AND class_type = 'Refund' THEN -amount_net_eur
      END                                           AS revenue_180d,
      CASE
        WHEN range_id BETWEEN 0 AND 360 AND class_type = 'Sale'   THEN  amount_net_eur
        WHEN range_id BETWEEN 0 AND 360 AND class_type = 'Refund' THEN -amount_net_eur
      END                                           AS revenue_360d,
      NULL                                          AS conversions_1d,
      NULL                                          AS conversions_3d,
      NULL                                          AS conversions_7d,
      NULL                                          AS conversions_15d,
      NULL                                          AS conversions_30d,
      NULL                                          AS conversions_90d,
      NULL                                          AS conversions_180d,
      NULL                                          AS conversions_360d
    FROM core.fct_campaign_performance
    WHERE class_type IN ('Sale', 'Refund')
    -- date_diff / range_id can be -1, for example when no acquisition for the user exists or entrydate is null.
    -- But since we use date_diff to adjust the date, having negative date_diffs would lead to weird results.
    AND date_diff >= 0

    UNION ALL

    -- CLOSED CONVERSIONS
    -- Sale - Refund, adjusted to date of acquisition
    SELECT
      (date_of_activity - date_diff)                AS date_of_activity,
      campaign_id,
      country_id,
      NULL                                          AS investment,
      NULL                                          AS acquisition,
      NULL                                          AS installs,
      NULL                                          AS conversions,
      NULL                                          AS revenue,
      NULL                                          AS revenue_1d,
      NULL                                          AS revenue_3d,
      NULL                                          AS revenue_7d,
      NULL                                          AS revenue_15d,
      NULL                                          AS revenue_30d,
      NULL                                          AS revenue_90d,
      NULL                                          AS revenue_180d,
      NULL                                          AS revenue_360d,
      CASE
        WHEN range_id BETWEEN 0 AND   1 AND class_type = 'Sale'   THEN  quantity
        WHEN range_id BETWEEN 0 AND   1 AND class_type = 'Refund' THEN -quantity
      END                                           AS conversions_1d,
      CASE
        WHEN range_id BETWEEN 0 AND   3 AND class_type = 'Sale'   THEN  quantity
        WHEN range_id BETWEEN 0 AND   3 AND class_type = 'Refund' THEN -quantity
      END                                           AS conversions_3d,
      CASE
        WHEN range_id BETWEEN 0 AND   7 AND class_type = 'Sale'   THEN  quantity
        WHEN range_id BETWEEN 0 AND   7 AND class_type = 'Refund' THEN -quantity
      END                                           AS conversions_7d,
      CASE
        WHEN range_id BETWEEN 0 AND  15 AND class_type = 'Sale'   THEN  quantity
        WHEN range_id BETWEEN 0 AND  15 AND class_type = 'Refund' THEN -quantity
      END                                           AS conversions_15d,
      CASE
        WHEN range_id BETWEEN 0 AND  30 AND class_type = 'Sale'   THEN  quantity
        WHEN range_id BETWEEN 0 AND  30 AND class_type = 'Refund' THEN -quantity
      END                                           AS conversions_30d,
      CASE
        WHEN range_id BETWEEN 0 AND  90 AND class_type = 'Sale'   THEN  quantity
        WHEN range_id BETWEEN 0 AND  90 AND class_type = 'Refund' THEN -quantity
      END                                           AS conversions_90d,
      CASE
        WHEN range_id BETWEEN 0 AND 180 AND class_type = 'Sale'   THEN  quantity
        WHEN range_id BETWEEN 0 AND 180 AND class_type = 'Refund' THEN -quantity
      END                                           AS conversions_180d,
      CASE
        WHEN range_id BETWEEN 0 AND 360 AND class_type = 'Sale'   THEN  quantity
        WHEN range_id BETWEEN 0 AND 360 AND class_type = 'Refund' THEN -quantity
      END                                           AS conversions_360d
    FROM core.fct_campaign_performance
    WHERE class_type IN ('Sale', 'Refund')
    AND class_subtype = 'First'
    -- date_diff / range_id can be -1, for example when no acquisition for the user exists or entrydate is null.
    -- But since we use date_diff to adjust the date, having negative date_diffs would lead to weird results.
    AND date_diff >= 0
  ) fact
  GROUP BY 1,2,3
);
