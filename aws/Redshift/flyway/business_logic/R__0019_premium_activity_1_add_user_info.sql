-- This view is used for populating fct_premium_activity.
--
-- Join premium user activity with:
-- * acquisition date and dimensions
-- * conversion date & product
-- * sale number & product corresponding to the activity date

CREATE OR REPLACE VIEW business_logic.premium_activity_1_add_user_info AS (

  SELECT
    date_of_activity,
    user_id,
    device,
    class_type,
    campaign_id,
    country_id,
    date_of_acquisition,
    payment_date,
    conversion_product_id,
    current_product_id,
    sale_number,
    quantity
  FROM
    (
      -- BASELINE FOR ACTIVITY
      -- We include data from the different activity events form cooladata
      SELECT
        date_of_activity,
        user_id,
        device,
        class_type,
        NULL AS campaign_id,
        NULL AS country_id,
        NULL AS date_of_acquisition,
        NULL AS payment_date,
        NULL AS conversion_product_id,
        NULL AS current_product_id,
        NULL AS sale_number,
        quantity
      FROM business_logic.premium_activity_baseline

      UNION ALL

      -- BASELINE FOR ACQUISITION
      -- We include data from the user acquisition as an event
      SELECT
        date_of_activity,
        user_id,
        NULL                      AS device,
        '0'                       AS class_type,
        COALESCE(campaign_id, -1) AS campaign_id,
        CASE
        WHEN campaign_country_id IS NOT NULL AND campaign_country_id NOT IN (-1, 0)
          THEN campaign_country_id
        ELSE user_country_id END  AS country_id,
        date_of_activity          AS date_of_acquisition,
        NULL                      AS payment_date,
        NULL                      AS conversion_product_id,
        NULL                      AS current_product_id,
        NULL                      AS sale_number,
        NULL                      AS quantity
      FROM business_logic.acquisition
        LEFT JOIN core.dim_campaign USING (campaign, partner_id, source_id)

      UNION ALL

      -- BASELINE FOR PAYMENTS
      -- We include data from clean sales as an event
      SELECT
        date_of_activity,
        user_id,
        NULL              AS device,
        '1'               AS class_type,
        NULL              AS campaign_id,
        NULL              AS country_id,
        NULL              AS date_of_acquisition,
        date_of_activity  AS payment_date,
        product_id        AS conversion_product_id,
        product_id        AS current_product_id,
        clean_sale_number AS sale_number,
        NULL              AS quantity
      FROM business_logic.clean_sales
    ) TRG
  ORDER BY user_id, date_of_activity
);
