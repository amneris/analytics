--- Not having a partner_id, channel_id or country_id is considered an error (id=-1)
--- Not having a source_id is OK (id=0)
CREATE OR REPLACE VIEW business_logic.installs AS (
  SELECT
    date_of_activity,
    --In the rare cases of unknown partner, append channel to the campaign name
    CASE WHEN idpartner IS NULL AND campaign != 'Unknown'
      THEN campaign || ' - ' || installs_channel
    ELSE campaign
    END                          AS campaign,
    COALESCE(idpartner, -1)      AS partner_id,
    COALESCE(sources_list.id, 0) AS source_id,
    -- If partner and campaign are unknown then campaign channel is, too
    -- Else 1) campaign channel 2) partner channel 3) installs channel
    CASE WHEN idpartner IS NULL AND campaign = 'Unknown'
      THEN -1
    ELSE COALESCE(campaign_channels.idchannel,
                  aba_partners_list.idchannel,
                  installs_channels.idchannel,
                  -1)
    END                          AS channel_id,
    -- If partner and campaign are unknown then campaign country is, too
    -- Else 1) campaign country 2) partner country
    CASE WHEN idpartner IS NULL AND campaign = 'Unknown'
      THEN -1
    ELSE COALESCE(campaign_countries.country_id, aba_partners_list.idcountry, -1)
    END                          AS campaign_country_id,
    -- Installs country is needed for Organic campaigns when populating the fact
    -- 1) campaign country if not NULL
    -- 2) partner country if not NULL, -1 or 0
    -- 3) installs country (country based on the user IP)
    CASE WHEN campaign_countries.country_id IS NOT NULL
      THEN campaign_countries.country_id
    WHEN aba_partners_list.idcountry IS NOT NULL AND aba_partners_list.idcountry NOT IN (-1, 0)
      THEN aba_partners_list.idcountry
    ELSE COALESCE(installs_countries.country_id, -1)
    END                          AS installs_country_id,
    installs,
    source_table
  FROM business_logic.installs_2_parse
    LEFT JOIN staging_partners.aba_partners_list ON partner_id = idpartner
    LEFT JOIN staging_partners.sources_list ON source_id = sources_list.id
    LEFT JOIN staging_partners.aba_partners_channels campaign_channels
      ON campaign_channel = LOWER(SUBSTRING(campaign_channels.namechannel, 1, 3))
    LEFT JOIN staging_partners.aba_partners_channels installs_channels
      ON installs_channel = LOWER(installs_channels.namechannel)
    LEFT JOIN (
                SELECT
                  LOWER(iso) AS country,
                  MIN(id)    AS country_id
                FROM staging_countries.country
                GROUP BY
                  LOWER(iso)
              ) campaign_countries ON campaign_country = campaign_countries.country
    LEFT JOIN (
                SELECT
                  LOWER(iso) AS country,
                  MIN(id)    AS country_id
                FROM staging_countries.country
                GROUP BY
                  LOWER(iso)
              ) installs_countries ON installs_country = installs_countries.country
);
