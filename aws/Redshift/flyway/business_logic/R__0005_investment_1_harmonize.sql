CREATE OR REPLACE VIEW business_logic.investment_1_harmonize AS (
  SELECT
    day                                                                            AS date_of_activity,
    campagin                                                                       AS campaign,
    REPLACE(REPLACE(REPLACE(LOWER(campagin), 'adquant_', ''), '-', '_'), ' ', '_') AS harmonized_campaign,
    channel                                                                        AS network,
    common_cost / 1000                                                             AS investment
  FROM staging_investment.investment
);
