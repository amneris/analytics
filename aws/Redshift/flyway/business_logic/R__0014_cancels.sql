CREATE OR REPLACE VIEW business_logic.cancels AS (
  SELECT
    date_of_activity,
    payment_id AS cancel_id,
    user_id,
    country_id,
    product_id,
    payment_gateway_id,
    amount_gross_eur,
    amount_net_eur,
    discount_gross_eur,
    -- Calculate the date difference between when a user cancelled (date_of_activity for "Cancel" records)
    -- and when the next renewal was programmed (future_payment_date). The lower the number the closer to the programmed
    -- renewal the user cancelled. Default to NULL for inconsistent data.
    CASE
    WHEN DATEDIFF(day, date_of_activity, future_payment_date) >= 0
      THEN DATEDIFF(day, date_of_activity, future_payment_date)
    END        AS days_to_cancel
  FROM business_logic.payments_2_lookup_fees_calculate payments
  WHERE payment_type = 'Cancel'
);
