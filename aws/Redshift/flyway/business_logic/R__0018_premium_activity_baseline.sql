-- Select the activity from premium users, and check problems with the data

CREATE OR REPLACE VIEW business_logic.premium_activity_baseline AS (
  SELECT
    date_of_activity,
    user_id,
    device,
    class_type,

    quantity

  FROM staging_activity.consolidated_daily_premium_activity
  WHERE date_of_activity >= (SELECT MIN(date_of_activity)
                             FROM staging_activity.daily_premium_activity)

);
