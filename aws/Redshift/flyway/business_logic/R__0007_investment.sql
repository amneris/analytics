-- Static campaign mappings take precedence over investment data
-- Not having a partner_id, channel_id or country_id is considered an error (id=-1)
-- Not having a source_id is OK (id=0)
CREATE OR REPLACE VIEW business_logic.investment AS (
  SELECT
    date_of_activity,
    campaign,
    COALESCE(campaign_mapping.partner_id, idpartner, -1)     AS partner_id,
    COALESCE(campaign_mapping.source_id, sources_list.id, 0) AS source_id,
    investment_2_parse.network,
    --1) channel from mapping table 2) campaign channel 3) partner channel
    COALESCE(campaign_mapping.channel_id,
             aba_partners_channels.idchannel,
             aba_partners_list.idchannel,
             -1)                                             AS channel_id,
    --1) country from mapping table 2) campaign country 3) partner country
    COALESCE(campaign_mapping.country_id,
             countries.country_id,
             idcountry,
             -1)                                             AS campaign_country_id,
    'transactional.investment'                          AS source_table,
    investment
  FROM business_logic.investment_2_parse
    LEFT JOIN staging_partners.aba_partners_list ON partner_id = idpartner
    LEFT JOIN staging_partners.sources_list ON source_id = sources_list.id
    LEFT JOIN staging_partners.aba_partners_channels
      ON channel = LOWER(SUBSTRING(namechannel, 1, 3))
    LEFT JOIN (
                SELECT
                  LOWER(iso) AS country,
                  MIN(id)    AS country_id
                FROM staging_countries.country
                GROUP BY
                  LOWER(iso)
              ) countries USING (country)
    --Use static campaign mappings to overcome inconsistent data
    LEFT JOIN staging_investment.campaign_mapping ON campaign = campaign_mapping.name
);
