-- This view is used for populating fct_premium_activity.
--
-- Join premium user activity with:
-- * acquisition date and dimensions
-- * conversion date & product
-- * sale number & product corresponding to the activity date

CREATE OR REPLACE VIEW business_logic.fct_premium_activity AS (
  SELECT
    date_of_activity,
    user_id,
    device,
    class_type,

    campaign_id,
    country_id,
    date_of_acquisition,
    date_of_conversion,
    conversion_product_id,

    current_product_id,
    sale_number,

    quantity
  FROM (
         SELECT *
         FROM (

                SELECT
                  date_of_activity,
                  user_id,
                  device,
                  class_type,
                  -- We want that all the values related with the permanent status of the user, are propagated for all the activity events.
                  -- Due to this, we use first_value ignoring all the null values, and order the hole user partition by date_of_activity, user_id and class_type
                  first_value(campaign_id IGNORE NULLS)
                  OVER (
                    PARTITION BY user_id
                    ORDER BY date_of_activity, user_id, class_type
                    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ) AS campaign_id,
                  first_value(country_id IGNORE NULLS)
                  OVER (
                    PARTITION BY user_id
                    ORDER BY date_of_activity, user_id, class_type
                    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ) AS country_id,
                  first_value(date_of_acquisition IGNORE NULLS)
                  OVER (
                    PARTITION BY user_id
                    ORDER BY date_of_activity, user_id, class_type
                    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ) AS date_of_acquisition,
                  first_value(payment_date IGNORE NULLS)
                  OVER (
                    PARTITION BY user_id
                    ORDER BY date_of_activity, user_id, class_type
                    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ) AS date_of_conversion,
                  first_value(conversion_product_id IGNORE NULLS)
                  OVER (
                    PARTITION BY user_id
                    ORDER BY date_of_activity, user_id, class_type
                    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ) AS conversion_product_id,

                  -- We want that the properties from the user that changes through time, so we propagate those information till any new value is found
                  -- Due to this, we use LAG function, ignoring all the null values, in order to propagate the correct values, we divide the data
                  -- for each user_id and it's ordered by date_of_activity, user_id and class_type
                  LAG(current_product_id, 1 IGNORE NULLS)
                  OVER (
                    PARTITION BY user_id
                    ORDER BY date_of_activity, user_id, class_type )           AS current_product_id,
                  LAG(sale_number, 1 IGNORE NULLS)
                  OVER (
                    PARTITION BY user_id
                    ORDER BY date_of_activity, user_id, class_type )           AS sale_number,
                  quantity

                FROM business_logic.premium_activity_1_add_user_info
                ORDER BY user_id, date_of_activity
              ) SRC
         WHERE class_type NOT IN ('0', '1')
         ORDER BY user_id, date_of_activity
       ) TGT
);
