CREATE OR REPLACE VIEW business_logic.refunds AS (
  SELECT
    date_of_activity,
    payment_id AS refund_id,
    user_id,
    country_id,
    product_id,
    payment_gateway_id,
    is_amex,
    amount_gross_eur,
    amount_net_eur,
    tax_eur,
    fee_eur,
    currency_code,
    amount_gross_original,
    amount_net_original,
    tax_original,
    fee_original
  FROM business_logic.payments_2_lookup_fees_calculate payments
  WHERE payment_type = 'Refund'
);
