--- Not having a partner_id, channel_id or country_id is considered an error (id=-1)
--- Not having a source_id is OK (id=0)
CREATE OR REPLACE VIEW business_logic.acquisition AS (
  SELECT
    date(entrydate)                  AS date_of_activity,
    users.id                         AS user_id,
    'Unknown'                        AS campaign,
    COALESCE(partners.idpartner, -1) AS partner_id,
    COALESCE(sources.id, 0)          AS source_id,
    'Unknown'                        AS network,
    COALESCE(partners.idchannel, -1) AS channel_id,
    -- User country is needed when populating the fact
    -- 1) partner country if not NULL, -1 or 0   2) user country
    CASE WHEN partners.idcountry IS NOT NULL AND partners.idcountry NOT IN (-1, 0)
      THEN partners.idcountry
    ELSE COALESCE(users.countryid, -1)
    END                              AS user_country_id,
    COALESCE(partners.idcountry, -1) AS partner_country_id,
    'staging_users.users'            AS source_table
  FROM staging_users.consolidated_users AS users
    LEFT JOIN staging_partners.aba_partners_list AS partners ON users.idpartnersource = partners.idpartner
    LEFT JOIN staging_partners.sources_list AS sources ON users.idsourcelist = sources.id
  -- Exclude B2B and Flashsales
--  WHERE (idbusinessarea IS NULL OR idbusinessarea NOT IN (3, 4))
    -- We don't exclude B2B users right now
);
