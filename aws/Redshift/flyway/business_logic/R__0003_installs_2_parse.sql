--- Install data is variably either on the partner or campaign level
CREATE OR REPLACE VIEW business_logic.installs_2_parse AS (
  SELECT
    date_of_activity,
    CASE
    --Make organic easy to recognize for use down the line
    WHEN network = 'Organic'
      THEN 'Organic'
    --If campaign column contains partner_id then campaign name is 'Unknown'
    WHEN campaign ~ '^[0-9]+$'
      THEN 'Unknown'
    WHEN campaign = 'unknown'
      THEN 'Unknown'
    WHEN campaign = 'organic'
      THEN 'Organic'
    --Remove trailing id from the campaign name, for example "campaign_abc (123)" --> "campaign_abc"
    ELSE REGEXP_REPLACE(campaign, ' \\(.*\\)$', '')
    END AS campaign,
    --Campaign column might contain partner_id directly, else we parse it from the campaign name
    CASE WHEN campaign ~ '^[0-9]+$'
      THEN campaign :: INT
    WHEN campaign LIKE '%p=%'
      THEN NULLIF(REGEXP_SUBSTR(REGEXP_REPLACE(campaign, '^.*p=', ''), '^[0-9]+'), '') :: INT
    END AS partner_id,
    CASE WHEN campaign LIKE '%s=%'
      THEN NULLIF(REGEXP_SUBSTR(REGEXP_REPLACE(campaign, '^.*s=', ''), '^[0-9]+'), '') :: BIGINT
    ELSE source_id :: BIGINT
    END AS source_id,
    CASE WHEN network IN ('Facebook Installs', 'Twitter Installs', 'Off-Facebook Installs', 'Instagram Installs')
      THEN NULLIF(SPLIT_PART(harmonized_campaign, '_', 3), '')
    WHEN network IN ('Apple Search Ads', 'Yahoo Gemini Installs')
      THEN NULLIF(SPLIT_PART(harmonized_campaign, '_', 6), '')
    END AS campaign_channel,
    installs_channel,
    CASE WHEN network IN ('Facebook Installs', 'Twitter Installs', 'Off-Facebook Installs', 'Instagram Installs',
                          'Apple Search Ads', 'Yahoo Gemini Installs')
      THEN NULLIF(SPLIT_PART(harmonized_campaign, '_', 4), '')
    END AS campaign_country,
    installs_country,
    installs,
    source_table
  FROM business_logic.installs_1_harmonize_federate
);