CREATE OR REPLACE VIEW business_logic.payments_1_rename_columns AS (
  SELECT
    DATE(dateendtransaction)                AS date_of_activity,
    autoid                                  AS payment_order_number,
    payments.id                             AS payment_id,
    payments.userid                         AS user_id,
    CASE status
    WHEN 0
      THEN 'Future payment'
    WHEN 10
      THEN 'Fail'
    WHEN 30
      THEN 'Sale'
    WHEN 40
      THEN 'Refund'
    WHEN 50
      THEN 'Cancel' END                     AS payment_type,
    idcountry                               AS country_id,
    idperiodpay                             AS product_id,
    DATE(datetopay)                         AS future_payment_date,
    COALESCE(isextend :: BOOLEAN, FALSE)    AS isextend,
    COALESCE(isrecurring :: BOOLEAN, FALSE) AS isrecurring,
    payment_gateway                         AS payment_gateway_id,
    CASE WHEN ucf.kind = 2
      THEN TRUE
    ELSE FALSE END                          AS is_amex,
    amountoriginal                          AS original_amount,
    amountPrice                             AS amount,
    amountdiscount                          AS discount,
    CASE WHEN xratetoeur != 0
      THEN xratetoeur
    ELSE 1 END                              AS xrate_to_eur,
    currencytrans                           AS currency_code,
    taxratevalue / 100                      AS tax_rate
  FROM staging_payments.consolidated_payments AS payments
    LEFT JOIN staging_payments.user_credit_forms ucf ON ucf.id = idusercreditform
  WHERE
    -------
    -- NOT sure what to do with payments of 0 euros
    -------
    amountprice >= 0
    AND status IN (0, 10, 30, 40, 50)
    -------
    --- Remove B2B
    -------
    AND payment_gateway NOT IN (3, 5)
);
