CREATE OR REPLACE VIEW business_logic.sales AS (
  SELECT
    date_of_activity,
    payment_id                        AS sale_id,
    user_id,
    -- Generate a number telling how many sales user has had
    ROW_NUMBER()
    OVER (
      PARTITION BY user_id
      ORDER BY payment_order_number ) AS sale_number,
    payment_type                      AS sale_type,
    -- User's first sale and user's first sale that's not refunded are 'First', others are 'Next'
    CASE WHEN ROW_NUMBER()
              OVER (
                PARTITION BY user_id
                ORDER BY payment_order_number ) = 1
      THEN 'First'
    WHEN refunded IS NULL
         AND ROW_NUMBER()
             OVER (
               PARTITION BY user_id
               ORDER BY refunded NULLS FIRST, payment_order_number ) = 1
      THEN 'First'
    ELSE 'Next' END                   AS sale_subtype,
    country_id,
    product_id,
    isextend,
    payment_gateway_id,
    is_amex,
    amount_gross_eur,
    amount_net_eur,
    discount_gross_eur,
    tax_eur,
    fee_eur,
    currency_code,
    amount_gross_original,
    amount_net_original,
    discount_gross_original,
    tax_original,
    fee_original,
    COALESCE(refunded, FALSE)         AS refunded,
    refund_date,
    refund_id
  FROM business_logic.payments_2_lookup_fees_calculate payments
    LEFT JOIN (
                SELECT
                  previous_sale_id,
                  TRUE             AS refunded,
                  date_of_activity AS refund_date,
                  refund_id
                FROM business_logic.payments_3_match_sale_and_refund
              ) refunds ON refunds.previous_sale_id = payments.payment_id
  WHERE payment_type = 'Sale'
);
