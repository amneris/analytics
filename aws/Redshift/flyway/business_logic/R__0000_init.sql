GRANT USAGE ON SCHEMA                business_logic                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA business_logic                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   business_logic GRANT SELECT ON TABLES TO analyticsuser;


-- Function to create an UUID
CREATE OR REPLACE FUNCTION public.uuid()
RETURNS character varying AS
' import uuid
 return uuid.uuid1().__str__()
 '
LANGUAGE plpythonu VOLATILE;

GRANT USAGE                ON SCHEMA business_logic                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA business_logic                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA business_logic GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA business_logic                      FROM GROUP redash_group;


REVOKE ALL                 ON SCHEMA business_logic                      FROM GROUP chartio_group;
GRANT ALL                  ON SCHEMA business_logic                        TO GROUP service_group;