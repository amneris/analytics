CREATE OR REPLACE VIEW business_logic.payments_2_lookup_fees_calculate AS (
  SELECT
    date_of_activity,
    payment_order_number,
    payment_id,
    user_id,
    payment_type,
    country_id,
    product_id,
    future_payment_date,
    isextend,
    isrecurring,
    payment_gateway_id,
    is_amex,
    -- Convert to euros and deduct currency exchange commission
    amount / (xrate_to_eur * (1 + fee_currency))        AS amount_gross_eur,
    -- Deduct tax and gateway fee, then convert to euros and deduct currency exchange commission
    (amount
     - (amount - amount / (1 + tax_rate))
     - (amount / (1 + tax_rate) * fee_payment_gateway))
    / (xrate_to_eur * (1 + fee_currency))               AS amount_net_eur,
    -- Convert to euros and deduct currency exchange commission
    discount / (xrate_to_eur * (1 + fee_currency))      AS discount_gross_eur,
    -- Calculate tax, then convert to euros
    (amount - amount / (1 + tax_rate))
    / (xrate_to_eur * (1 + fee_currency))               AS tax_eur,
    -- Deduct tax, calculate gateway fee, then convert to euros
    (amount / (1 + tax_rate) * fee_payment_gateway)
    / (xrate_to_eur * (1 + fee_currency))               AS fee_eur,
    currency_code,
    amount                                              AS amount_gross_original,
    -- Deduct tax and gateway fee
    (amount
     - (amount - amount / (1 + tax_rate))
     - (amount / (1 + tax_rate) * fee_payment_gateway)) AS amount_net_original,
    discount                                            AS discount_gross_original,
    amount - amount / (1 + tax_rate)                    AS tax_original,
    -- Deduct tax, then calculate gateway fee
    amount / (1 + tax_rate) * fee_payment_gateway       AS fee_original
  FROM (
         SELECT
           date_of_activity,
           payment_order_number,
           payment_id,
           user_id,
           payment_type,
           country_id,
           product_id,
           future_payment_date,
           isextend,
           isrecurring,
           payment_gateway_id,
           is_amex,
           amount,
           discount,
           xrate_to_eur,
           tax_rate,
           -- Default currency exchange commission is 2.63 percent
           COALESCE(fee_currency, 0.0263)   AS fee_currency,
           COALESCE(fee_payment_gateway, 0) AS fee_payment_gateway,
           currency_code
         FROM business_logic.payments_1_rename_columns
           LEFT JOIN staging_fees.fees_currency fcu ON currencyid = currency_code
           LEFT JOIN staging_fees.fees_payment_gateway fpg ON payment_gatewayid = payment_gateway_id
       ) AS payments_fees
);
