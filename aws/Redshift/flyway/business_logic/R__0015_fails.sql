CREATE OR REPLACE VIEW business_logic.fails AS (
  SELECT
    date_of_activity,
    payment_id AS fail_id,
    user_id,
    country_id,
    product_id,
    payment_gateway_id,
    amount_gross_eur,
    amount_net_eur,
    discount_gross_eur
  FROM business_logic.payments_2_lookup_fees_calculate payments
  WHERE payment_type = 'Fail'
);
