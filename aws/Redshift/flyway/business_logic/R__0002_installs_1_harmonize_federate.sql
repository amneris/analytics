CREATE OR REPLACE VIEW business_logic.installs_1_harmonize_federate AS (
  SELECT
    day AS date_of_activity,
    campaign,
    REPLACE(REPLACE(REPLACE(LOWER(campaign), 'adquant_', ''), '-', '_'), ' ', '_') AS harmonized_campaign,
    CASE WHEN adgroup ~ '^[0-9]+$' THEN adgroup ELSE NULL END AS source_id,
    network,
    channel AS installs_channel,
    country AS installs_country,
    installs,
    'staging_installs.installs_ios' AS source_table
  FROM staging_installs.installs_ios
  UNION ALL
  SELECT
    day AS date_of_activity,
    campaign,
    REPLACE(REPLACE(REPLACE(LOWER(campaign), 'adquant_', ''), '-', '_'), ' ', '_') AS harmonized_campaign,
    CASE WHEN adgroup ~ '^[0-9]+$' THEN adgroup ELSE NULL END AS source_id,
    network,
    channel AS installs_channel,
    country AS installs_country,
    installs,
    'staging_installs.installs_android' AS source_table
  FROM staging_installs.installs_android
);
