CREATE OR REPLACE VIEW business_logic.payments_3_match_sale_and_refund AS (
  SELECT
    date_of_activity,
    payment_id AS refund_id,
    previous_sale_id
  FROM (
         SELECT
           date_of_activity,
           payment_id,
           payment_type,
           LAG(payment_id, 1)
           OVER (
             PARTITION BY
               user_id,
               product_id,
               payment_gateway_id,
               amount
             ORDER BY payment_order_number, payment_type DESC ) AS previous_sale_id
         FROM business_logic.payments_1_rename_columns
         WHERE payment_type IN ('Sale', 'Refund')
       ) match
  WHERE payment_type = 'Refund'
);
