-- Calculate begin and end date for premium period. Add ordinal "clean_sale_number" that shows the clean sale order.
-- Classify the (so far) latest clean sale as "Last". Check whether the product has changed from the previous
-- clean sale (Upgrade/Downgrade/No change/Not applicable).

CREATE OR REPLACE VIEW business_logic.clean_sales_1_calculate_begin_end_product AS (
  SELECT
    date_of_activity,
    user_id,
    clean_sale_number,
    -- Calculate begin and end dates based on the previous clean sales when renewed in advance (when isextend=TRUE)
    -- Several sales might have been paid in advance. For simplicity we're checking only three sales back.
    -- lag1 = previous
    -- lag2 = previous to previous
    -- lag3 = three sales back
    CASE WHEN lag2_isextend AND lag1_isextend AND isextend
      THEN DATEADD(month, (lag3_product_id + lag2_product_id + lag1_product_id) / 30, lag3_date_of_activity)
    WHEN lag1_isextend AND isextend
      THEN DATEADD(month, (lag2_product_id + lag1_product_id) / 30, lag2_date_of_activity)
    WHEN isextend
      THEN DATEADD(month, lag1_product_id / 30, lag1_date_of_activity)
    -- When not isextend simply use date_of_activity
    ELSE date_of_activity
    END AS begin_date,
    CASE WHEN lag2_isextend AND lag1_isextend AND isextend
      THEN DATEADD(month, (lag3_product_id + lag2_product_id + lag1_product_id + product_id) / 30,
                   lag3_date_of_activity)
    WHEN lag1_isextend AND isextend
      THEN DATEADD(month, (lag2_product_id + lag1_product_id + product_id) / 30, lag2_date_of_activity)
    WHEN isextend
      THEN DATEADD(month, (lag1_product_id + product_id) / 30, lag1_date_of_activity)
    -- When not isextend simply use date_of_activity + product_id
    ELSE DATEADD(month, product_id / 30, date_of_activity)
    END AS end_date,
    -- The first clean sale is always "First". Clean sale is "Last" if it's the latest for now, else "Next".
    CASE WHEN sale_subtype = 'Next' AND inverse_clean_sale_number = 1
      THEN 'Last'
    ELSE sale_subtype
    END AS clean_sale_type,
    -- Track product upgrades and downgrades
    CASE WHEN sale_subtype = 'First'
      THEN 'Not applicable'
    WHEN sale_subtype = 'Next' AND product_id > lag1_product_id
      THEN 'Upgrade'
    WHEN sale_subtype = 'Next' AND product_id < lag1_product_id
      THEN 'Downgrade'
    ELSE 'No change'
    END AS product_change_type,
    country_id,
    product_id,
    payment_gateway_id,
    amount_gross_eur,
    amount_net_eur,
    discount_gross_eur,
    sale_id
  FROM (
         SELECT
           date_of_activity,
           user_id,
           -- Generate a number telling how many clean sales user has had
           ROW_NUMBER()
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number )      AS clean_sale_number,
           ROW_NUMBER()
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number DESC ) AS inverse_clean_sale_number,
           isextend,
           LAG(isextend, 1)
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number )      AS lag1_isextend,
           LAG(isextend, 2)
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number )      AS lag2_isextend,
           LAG(date_of_activity, 1)
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number )      AS lag1_date_of_activity,
           LAG(date_of_activity, 2)
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number )      AS lag2_date_of_activity,
           LAG(date_of_activity, 3)
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number )      AS lag3_date_of_activity,
           country_id,
           product_id,
           LAG(product_id, 1)
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number )      AS lag1_product_id,
           LAG(product_id, 2)
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number )      AS lag2_product_id,
           LAG(product_id, 3)
           OVER (
             PARTITION BY user_id
             ORDER BY sale_number )      AS lag3_product_id,
           sale_subtype,
           payment_gateway_id,
           amount_gross_eur,
           amount_net_eur,
           discount_gross_eur,
           sale_id
         FROM business_logic.sales
         WHERE NOT refunded
               AND date_of_activity IS NOT NULL
       ) AS lookup_previous
);
