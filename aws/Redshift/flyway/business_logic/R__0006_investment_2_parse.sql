CREATE OR REPLACE VIEW business_logic.investment_2_parse AS (
  SELECT
    date_of_activity,
    campaign,
    CASE WHEN campaign LIKE '%p=%'
      THEN NULLIF(REGEXP_SUBSTR(REGEXP_REPLACE(campaign, '^.*p=', ''), '^[0-9]+'), '') :: INT
    END AS partner_id,
    CASE WHEN campaign LIKE '%s=%'
      THEN NULLIF(REGEXP_SUBSTR(REGEXP_REPLACE(campaign, '^.*s=', ''), '^[0-9]+'), '') :: SMALLINT
    END AS source_id,
    network,
    CASE WHEN network IN ('Facebook paid', 'Twitter paid')
      THEN NULLIF(SPLIT_PART(harmonized_campaign, '_', 3), '')
    WHEN network IN ('Applovin', 'Liftoff', 'Powerspace', 'Unity Ads', 'Outbrain')
      THEN NULLIF(SPLIT_PART(harmonized_campaign, '_', 4), '')
    WHEN network IN ('AdWords', 'Apple Ads', 'Bing', 'Yahoo', 'Yandex')
      THEN NULLIF(SPLIT_PART(harmonized_campaign, '_', 6), '')
    WHEN network = 'Affiliates-Partners' AND harmonized_campaign LIKE '%n=%'
      THEN NULLIF(REGEXP_SUBSTR(REGEXP_REPLACE(harmonized_campaign, '^.*n=', ''), '^[a-z]+'), '')
    ELSE NULLIF(SUBSTRING(harmonized_campaign, 15, 3), '')
    END AS channel,
    CASE WHEN network IN ('Applovin', 'Liftoff', 'Powerspace', 'TV CAMPAIGNS', 'Unity Ads', 'Outbrain')
      THEN NULLIF(SPLIT_PART(harmonized_campaign, '_', 2), '')
    WHEN network IN ('AdWords', 'Apple Ads', 'Bing', 'Facebook paid', 'Twitter paid', 'Yahoo', 'Yandex')
      THEN NULLIF(SPLIT_PART(harmonized_campaign, '_', 4), '')
    WHEN network = 'Affiliates-Partners' AND harmonized_campaign LIKE '%c=%'
      THEN NULLIF(REGEXP_SUBSTR(REGEXP_REPLACE(harmonized_campaign, '^.*c=', ''), '^[a-z]+'), '')
    ELSE NULLIF(SUBSTRING(harmonized_campaign, 9, 2), '')
    END AS country,
    investment
  FROM business_logic.investment_1_harmonize
);
