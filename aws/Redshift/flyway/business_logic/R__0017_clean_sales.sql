-- Show the non-refunded -good- sales of a user. In this context we call them clean sales.
-- One clean sale = one non-refunded sale.
--
-- Calculate how many days the user was out of premium before the sale. Zero if there was no gap in the premium status.
-- The calculation is approximate, so we should allow for some buffer to decide whether the user really is ex-premium.

CREATE OR REPLACE VIEW business_logic.clean_sales AS (
  SELECT
    date_of_activity,
    user_id,
    clean_sale_number,
    begin_date,
    end_date,
    clean_sale_type,
    product_change_type,
    country_id,
    product_id,
    payment_gateway_id,
    amount_gross_eur,
    amount_net_eur,
    discount_gross_eur,
    sale_id,
    -- Compare the end_date of the previous sale to the current sale's begin_date. This will tell us for how many days
    -- the user was out between the sales. Default to NULL for inconsistent data.
    CASE
    WHEN DATEDIFF(day, LAG(end_date)
    OVER (
      PARTITION BY user_id
      ORDER BY clean_sale_number ), begin_date) >= 0
      THEN DATEDIFF(day, LAG(end_date)
      OVER (
        PARTITION BY user_id
        ORDER BY clean_sale_number ), begin_date)
    END AS days_out
  FROM business_logic.clean_sales_1_calculate_begin_end_product
);
