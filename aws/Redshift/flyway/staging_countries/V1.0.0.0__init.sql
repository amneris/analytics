GRANT USAGE ON SCHEMA                staging_countries                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_countries                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   staging_countries GRANT SELECT ON TABLES TO analyticsuser;