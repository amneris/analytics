
CREATE TABLE staging_countries.country
(
  id INT PRIMARY KEY NOT NULL,
  days VARCHAR(7) DEFAULT '' NOT NULL,
  ID_month INT DEFAULT '0' NOT NULL,
  iso CHAR(2) DEFAULT '' NOT NULL,
  name_m VARCHAR(100) DEFAULT '' NOT NULL,
  name VARCHAR(100) DEFAULT '' NOT NULL,
  iso3 CHAR(3),
  numcode INT DEFAULT '0',
  officialIdCurrency VARCHAR(3) NOT NULL,
  officialIdCurrency_tmpDisabled VARCHAR(3) DEFAULT 'EUR' NOT NULL,
  ABAIdCurrency VARCHAR(3) DEFAULT 'EUR' NOT NULL,
  ABALanguage VARCHAR(3) DEFAULT 'en' NOT NULL,
  ABAOldCampusIdCurrency VARCHAR(3) NOT NULL,
  decimalPoint VARCHAR(2) DEFAULT ',' NOT NULL,
  invoice INT DEFAULT '0' NOT NULL,
  continent VARCHAR(75) DEFAULT 'Unclassified',
  region VARCHAR(75) DEFAULT 'Unclassified',
  countryGroup VARCHAR(75) DEFAULT 'Unclassified'
);

