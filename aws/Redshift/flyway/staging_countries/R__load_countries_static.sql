TRUNCATE TABLE staging_countries.country;

COPY staging_countries.country
FROM '${S3_data_bucket}/static/staging_countries/'
IAM_ROLE 'arn:aws:iam::${aws_account_id}:role/analytics-redshift-loader-role'

DELIMITER '\t'
IGNOREHEADER AS 1
REMOVEQUOTES;
