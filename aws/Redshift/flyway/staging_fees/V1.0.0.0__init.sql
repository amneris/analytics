GRANT USAGE ON SCHEMA                staging_fees                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_fees                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   staging_fees GRANT SELECT ON TABLES TO analyticsuser;
