-- fees payment gateway

TRUNCATE TABLE staging_fees.fees_payment_gateway;

COPY staging_fees.fees_payment_gateway
FROM '${S3_data_bucket}/static/staging_fees/fees_payment_gateway.tsv'
IAM_ROLE 'arn:aws:iam::${aws_account_id}:role/analytics-redshift-loader-role'

DELIMITER '\t'
IGNOREHEADER AS 1
REMOVEQUOTES;



-- currency fees loader

TRUNCATE TABLE staging_fees.fees_currency;

COPY staging_fees.fees_currency
FROM '${S3_data_bucket}/static/staging_fees/fees_currency.tsv'
IAM_ROLE 'arn:aws:iam::${aws_account_id}:role/analytics-redshift-loader-role'

DELIMITER '\t'
IGNOREHEADER AS 1
REMOVEQUOTES