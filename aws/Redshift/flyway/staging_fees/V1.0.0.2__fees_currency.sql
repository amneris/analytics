
CREATE TABLE staging_fees.fees_currency
(
  currencyid VARCHAR(5) PRIMARY KEY,
  fee_currency NUMERIC(5, 5)
);

