
CREATE TABLE staging_fees.fees_payment_gateway
(
  payment_gatewayid INT PRIMARY KEY,
  fee_payment_gateway DECIMAL(5, 5)
);

