CREATE TABLE staging_activity.weekly_active_users
(
  week_of_activity       DATE             NOT NULL     ENCODE ZSTD,
  device                 VARCHAR(15)      NOT NULL     ENCODE ZSTD,
  country_id             INT              NOT NULL     ENCODE ZSTD,
  user_type              VARCHAR(20)      NOT NULL     ENCODE ZSTD,
  wau                    BIGINT                        ENCODE ZSTD,
  sessions               BIGINT                        ENCODE ZSTD,

  PRIMARY KEY (week_of_activity, device, country_id, user_type)
)
DISTKEY(week_of_activity)
INTERLEAVED SORTKEY (week_of_activity, device, country_id, user_type);
