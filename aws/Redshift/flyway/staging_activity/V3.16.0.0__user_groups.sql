GRANT USAGE                ON SCHEMA staging_activity                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_activity                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA staging_activity GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA staging_activity                      FROM GROUP redash_group;

GRANT USAGE                ON SCHEMA staging_activity                        TO GROUP chartio_group;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_activity                        TO GROUP chartio_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA staging_activity GRANT SELECT ON TABLES TO GROUP chartio_group;
REVOKE CREATE              ON SCHEMA staging_activity                      FROM GROUP chartio_group;

GRANT ALL                  ON SCHEMA staging_activity                        TO GROUP service_group;
