GRANT USAGE ON SCHEMA                staging_activity                         TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_activity                         TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   staging_activity  GRANT SELECT ON TABLES TO analyticsuser;
