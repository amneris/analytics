CREATE TABLE staging_activity.consolidated_daily_active_users
(
  day       DATE            NOT NULL    ENCODE ZSTD,
  device    VARCHAR(15)                 ENCODE ZSTD,
  dau       BIGINT                      ENCODE ZSTD,
  PRIMARY KEY (day, device)
)
DISTKEY(day)
INTERLEAVED SORTKEY (day, device);
