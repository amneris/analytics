CREATE TABLE staging_activity.daily_premium_activity
(
  date_of_activity      DATE            NOT NULL    ENCODE ZSTD,
  user_id               BIGINT          NOT NULL    ENCODE ZSTD,
  device                CHAR(15)        NOT NULL    ENCODE ZSTD,
  class_type            CHAR(30)        NOT NULL    ENCODE ZSTD,
  quantity              INT                         ENCODE ZSTD,

  PRIMARY KEY (date_of_activity, user_id, device, class_type)
)
  DISTKEY(date_of_activity)
  INTERLEAVED SORTKEY (date_of_activity, user_id, class_type, device);
