-- Delete all records due to duplicates insertions
TRUNCATE TABLE  staging_activity.daily_premium_activity;

INSERT INTO staging_activity.daily_premium_activity
  SELECT *
  FROM staging_activity.consolidated_daily_premium_activity;
