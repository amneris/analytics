
CREATE TABLE staging_partners.aba_partners_channels
(
  idChannel INT PRIMARY KEY NOT NULL,
  nameChannel VARCHAR(75) DEFAULT '' NOT NULL,
  dateAdd TIMESTAMP
);

