
CREATE TABLE staging_partners.aba_partners_list
(
  idPartner INT PRIMARY KEY NOT NULL,
  name VARCHAR(240) COLLATE utf8_unicode_ci NOT NULL,
  idPartnerGroup INT DEFAULT 0 NOT NULL,
  nameGroup VARCHAR(45),
  idCategory INT DEFAULT 2,
  idBusinessArea INT DEFAULT 2,
  idType INT DEFAULT 1,
  idGroup INT DEFAULT 1,
  idChannel INT DEFAULT 1,
  idCountry INT DEFAULT 0 NOT NULL
);
