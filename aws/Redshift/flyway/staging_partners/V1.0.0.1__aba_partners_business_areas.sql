
CREATE TABLE staging_partners.aba_partners_business_areas
(
  idBusinessArea INT PRIMARY KEY NOT NULL,
  nameBusinessArea VARCHAR(75) DEFAULT '' NOT NULL,
  dateAdd TIMESTAMP
);

