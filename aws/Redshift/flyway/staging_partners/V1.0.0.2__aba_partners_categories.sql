
CREATE TABLE staging_partners.aba_partners_categories
(
  idCategory INT PRIMARY KEY NOT NULL,
  nameCategory VARCHAR(75) DEFAULT '' NOT NULL,
  dateAdd TIMESTAMP
);

