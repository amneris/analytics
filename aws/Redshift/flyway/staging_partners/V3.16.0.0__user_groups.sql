GRANT USAGE                ON SCHEMA staging_partners                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_partners                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA staging_partners GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA staging_partners                      FROM GROUP redash_group;

GRANT USAGE                ON SCHEMA staging_partners                        TO GROUP chartio_group;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_partners                        TO GROUP chartio_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA staging_partners GRANT SELECT ON TABLES TO GROUP chartio_group;
REVOKE CREATE              ON SCHEMA staging_partners                      FROM GROUP chartio_group;

GRANT ALL                  ON SCHEMA staging_partners                        TO GROUP service_group;
