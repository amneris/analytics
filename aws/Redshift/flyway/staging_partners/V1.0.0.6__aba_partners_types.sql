
CREATE TABLE staging_partners.aba_partners_types
(
  idType INT PRIMARY KEY NOT NULL,
  nameType VARCHAR(75) DEFAULT '' NOT NULL,
  dateAdd TIMESTAMP
);

