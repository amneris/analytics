GRANT USAGE ON SCHEMA                staging_partners                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_partners                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   staging_partners GRANT SELECT ON TABLES TO analyticsuser;