
CREATE TABLE staging_partners.aba_partners_groups
(
  idGroup INT PRIMARY KEY NOT NULL,
  nameGroup VARCHAR(75) DEFAULT '' NOT NULL,
  dateAdd TIMESTAMP
);

