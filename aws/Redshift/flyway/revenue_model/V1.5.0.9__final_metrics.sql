CREATE OR REPLACE VIEW revenue_model.final_metrics AS
  (
    SELECT
      date_first_sale,
      date_renewal,

      real_first_sale_30 + real_first_sale_180 + real_first_sale_360 + real_first_sale_720                              AS real_first_sale,
      estimated_first_sale_30 + estimated_first_sale_180 + estimated_first_sale_360 + estimated_first_sale_720          AS estimated_first_sale,

      investment,
      cr_30d,
      cpl,
      ticket_first,
      use_real_first_sale,
      use_real_renewal_sale,
      ticket_renewals,
      coalesce(real_renewals,0)                                                                                         AS real_renewals,

      estimated_estimated_030_renewal + estimated_estimated_180_renewal + estimated_estimated_360_renewal +
      estimated_estimated_720_renewal                                                                                   AS estimated_estimated_renewal,
      estimated_real_030_renewal + estimated_real_180_renewal + estimated_real_360_renewal +
      estimated_real_720_renewal                                                                                        AS estimated_real_renewal

    FROM revenue_model.renewal_sale
      INNER JOIN (SELECT date_month AS date_renewal, use_real_first_sale AS use_real_renewal_sale FROM revenue_model.use_real_flag) USING(date_renewal)


  );