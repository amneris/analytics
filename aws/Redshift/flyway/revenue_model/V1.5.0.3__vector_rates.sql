CREATE TABLE revenue_model.vector_rates (

  first_sale_month_offset    int            NOT NULL    ENCODE MOSTLY8,
  renewal_month_offset       int            NOT NULL    ENCODE MOSTLY8,

  conversion_vector          numeric(10,4)              ENCODE MOSTLY16,

  renewal_030_vector         numeric(10,4)              ENCODE MOSTLY16,
  renewal_180_vector         numeric(10,4)              ENCODE MOSTLY16,
  renewal_360_vector         numeric(10,4)              ENCODE MOSTLY16,
  renewal_720_vector         numeric(10,4)              ENCODE MOSTLY16,

  flag_first_sale            int                        ENCODE MOSTLY8,
  flag_renewal               int                        ENCODE MOSTLY8

)
;


COPY revenue_model.vector_rates
FROM '${S3_data_bucket}/static/revenue_model/vector_rates.txt'
IAM_ROLE 'arn:aws:iam::${aws_account_id}:role/analytics-redshift-loader-role'
DELIMITER ',';
