CREATE OR REPLACE VIEW revenue_model.projection AS (

  SELECT
    date_month,
    investment,
    cpl,
    cr_30d,
    sales_30,
    sales_180,
    sales_360,
    sales_720,
    avg_ticket_first      AS ticket_first,
    avg_ticket_renewals   AS ticket_renewals

  FROM revenue_model.base_metrics

  UNION ALL
-- We select the first 25 rows from vector_rates, and we expand each month to 25 rows, with the avg of each metrics
  SELECT add_months(date_trunc('month', current_date), s)::DATE, t2.* FROM (SELECT row_number() OVER (ORDER BY first_sale_month_offset) AS s FROM revenue_model.vector_rates limit 25) t1
    CROSS JOIN (
                 SELECT
                   AVG(investment)            AS investment,
                   AVG(cpl)                   AS cpl,
                   AVG(cr_30d)                AS cr_30d,
                   AVG(sales_30)              AS sales_30,
                   AVG(sales_180)             AS sales_180,
                   AVG(sales_360)             AS sales_360,
                   AVG(sales_720)             AS sales_720,
                   AVG(avg_ticket_first)      AS ticket_first,
                   AVG(avg_ticket_renewals)   AS ticket_renewals

                 FROM revenue_model.base_metrics
                 WHERE date_month BETWEEN add_months(date_trunc('month',current_date),-3)::DATE  AND add_months(date_trunc('month',current_date),-1)::DATE
                    -- For the projection of the input metrics, we select the average for the last three months

               ) t2
);
