CREATE TABLE revenue_model.base_actual
(
    date_month              DATE          NOT NULL    ENCODE DELTA,
    investment              NUMERIC(18,4)              ENCODE MOSTLY16,
    cr_30d                  NUMERIC(10,8)              ENCODE MOSTLY16,
    cpl                     NUMERIC(10,4)              ENCODE MOSTLY16,

    PRIMARY KEY (date_month)
);

