CREATE OR REPLACE VIEW revenue_model.first_sale AS (

  SELECT

    t1.*,
    coalesce(real_first_sale_30, 0)  * flag_first_sale AS real_first_sale_30,
    coalesce(real_first_sale_180, 0) * flag_first_sale AS real_first_sale_180,
    coalesce(real_first_sale_360, 0) * flag_first_sale AS real_first_sale_360,
    coalesce(real_first_sale_720, 0) * flag_first_sale AS real_first_sale_720,
    use_real_first_sale

  FROM
    (

      SELECT

        add_months(date_month, first_sale_month_offset) :: DATE                 AS date_month,
        investment / cpl * cr_30d * round(sales_30, 4) * conversion_vector      AS estimated_first_sale_30,
        investment / cpl * cr_30d * round(sales_180, 4) * conversion_vector     AS estimated_first_sale_180,
        investment / cpl * cr_30d * round(sales_360, 4) * conversion_vector     AS estimated_first_sale_360,
        investment / cpl * cr_30d * round(sales_720, 4) * conversion_vector     AS estimated_first_sale_720,
        investment * flag_first_sale * flag_renewal                             AS investment,
        cpl * flag_first_sale * flag_renewal                                    AS cpl,
        cr_30d * flag_first_sale * flag_renewal                                 AS cr_30d,
        ticket_first * flag_first_sale * flag_renewal                           AS ticket_first,
        ticket_renewals * flag_first_sale * flag_renewal                        AS ticket_renewals,
        t.*

      FROM revenue_model.projection
        CROSS JOIN (SELECT * FROM revenue_model.vector_rates) t

    ) t1 LEFT JOIN (

                     SELECT

                       date_month,
                       first_sales * round(sales_30,  4)  AS real_first_sale_30,
                       first_sales * round(sales_180, 4)  AS real_first_sale_180,
                       first_sales * round(sales_360, 4)  AS real_first_sale_360,
                       first_sales * round(sales_720, 4)  AS real_first_sale_720

                     FROM revenue_model.base_metrics
                   ) t2 USING (date_month)

    INNER JOIN revenue_model.use_real_flag USING (date_month)

  where date_month < '2019-04-01'

);

