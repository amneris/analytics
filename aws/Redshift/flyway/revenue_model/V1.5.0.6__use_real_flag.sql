CREATE OR REPLACE VIEW revenue_model.use_real_flag AS
(
  SELECT

    date_month,
    LEAST((current_date - date_month - 1)/date_part('day',add_months(date_trunc('month',date_month),1)-1) * (sign(current_date - date_month) + 1)/2,1.0)  AS use_real_first_sale
    -- for the previous months, this metric will be 1, for the future months will be 0, but for the current month, will be the percentage of the curren_day / last_day_month

  FROM revenue_model.projection ORDER BY 1
);