GRANT USAGE ON SCHEMA                revenue_model                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA revenue_model                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   revenue_model GRANT SELECT ON TABLES TO analyticsuser;
