GRANT USAGE                ON SCHEMA revenue_model                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA revenue_model                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA revenue_model GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA revenue_model                      FROM GROUP redash_group;

GRANT USAGE                ON SCHEMA revenue_model                        TO GROUP chartio_group;
GRANT SELECT ON ALL TABLES IN SCHEMA revenue_model                        TO GROUP chartio_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA revenue_model GRANT SELECT ON TABLES TO GROUP chartio_group;
REVOKE CREATE              ON SCHEMA revenue_model                      FROM GROUP chartio_group;

GRANT ALL                  ON SCHEMA revenue_model                        TO GROUP service_group;
