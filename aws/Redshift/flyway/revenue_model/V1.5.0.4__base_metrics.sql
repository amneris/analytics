CREATE OR REPLACE VIEW revenue_model.base_metrics AS (
  -- Merge from cohorted metrics and actual metrics
  SELECT

    -- We want to detect if date_month is the current month, for that we use convert (if_clause)to int
    date_month,
    -- We estimate for the current month, the projection of investment (Investent * num_days_of_month/current_day_of_month)
    investment * (1 + convert(int,current_date - date_month < 30)*(date_part('day',add_months(date_trunc('month', current_date),1)-1)/(date_part('day',current_date)) -1))  AS investment,
    -- For the current month, we take the CR of the previous month, for that we use lag function
    cr_30d *(1-convert(int,current_date - date_month < 30)) + lag(cr_30d,1) over (order by date_month) * convert(int,current_date - date_month < 30)                        AS cr_30d,
    cpl,
    -- We estimate for the current month, the projection of first_sales (first_sales * num_days_of_month/current_day_of_month)
    first_sales * (1 + convert(int,current_date - date_month < 30)*(date_part('day',add_months(date_trunc('month', current_date),1)-1)/(date_part('day',current_date)) -1)) AS first_sales,
    sales_30,
    sales_180,
    sales_360,
    sales_720,
    avg_ticket_first,
    -- We estimate for the current month, the projection of renewals (renewals * num_days_of_month/current_day_of_month)
    renewals * (1 + convert(int,current_date - date_month < 30)*(date_part('day',add_months(date_trunc('month', current_date),1)-1)/(date_part('day',current_date)) -1))    AS renewals,
    avg_ticket_renewals

  FROM revenue_model.base_actual

  INNER JOIN revenue_model.base_cohorted USING (date_month)
);
