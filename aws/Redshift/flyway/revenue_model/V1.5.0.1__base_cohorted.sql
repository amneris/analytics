CREATE TABLE revenue_model.base_cohorted
(
    date_month            DATE          NOT NULL    ENCODE DELTA,
    first_sales           BIGINT                    ENCODE MOSTLY8,
    sales_30              NUMERIC(8,6)              ENCODE MOSTLY16,
    sales_180             NUMERIC(8,6)              ENCODE MOSTLY16,
    sales_360             NUMERIC(8,6)              ENCODE MOSTLY16,
    sales_720             NUMERIC(8,6)              ENCODE MOSTLY16,
    avg_ticket_first      NUMERIC(6,4)              ENCODE MOSTLY16,
    renewals              BIGINT                    ENCODE MOSTLY8,
    avg_ticket_renewals   NUMERIC(6,4)              ENCODE MOSTLY16,

    PRIMARY KEY (date_month)
);

