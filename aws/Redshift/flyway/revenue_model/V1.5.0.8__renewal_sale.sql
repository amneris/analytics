CREATE OR REPLACE VIEW revenue_model.renewal_sale AS (

  SELECT

    -- Date data
    date_month                                                                       AS date_first_sale,
    add_months(date_month, renewal_month_offset) :: DATE                             AS date_renewal,
    renewal_month_offset,
    flag_renewal * use_real_first_sale                                               AS use_real_first_sale,
    investment,
    cpl,
    cr_30d,
    ticket_first,
    ticket_renewals,

    flag_renewal * flag_first_sale * real_renewals                                   AS real_renewals,

    -- First sales
    -- Product 30
    flag_renewal * estimated_first_sale_30 * (1-use_real_first_sale)   AS estimated_first_sale_30,
    flag_renewal * real_first_sale_30      * use_real_first_sale       AS real_first_sale_30,
    -- Product 180
    flag_renewal * estimated_first_sale_180 * (1-use_real_first_sale)   AS estimated_first_sale_180,
    flag_renewal * real_first_sale_180      * use_real_first_sale       AS real_first_sale_180,

    -- Product 360
    flag_renewal * estimated_first_sale_360 * (1-use_real_first_sale)   AS estimated_first_sale_360,
    flag_renewal * real_first_sale_360      * use_real_first_sale       AS real_first_sale_360,

    -- Product 720
    flag_renewal * estimated_first_sale_720 * (1-use_real_first_sale)   AS estimated_first_sale_720,
    flag_renewal * real_first_sale_720      * use_real_first_sale       AS real_first_sale_720,

    -- Renewals
    -- Product 30
    estimated_first_sale_30    * renewal_030_vector      * (1-use_real_first_sale)    AS estimated_estimated_030_renewal,
    real_first_sale_30         * renewal_030_vector      * use_real_first_sale        AS estimated_real_030_renewal,
    -- Product 180
    estimated_first_sale_180    * renewal_180_vector      * (1-use_real_first_sale)   AS estimated_estimated_180_renewal,
    real_first_sale_180         * renewal_180_vector      * use_real_first_sale       AS estimated_real_180_renewal,

    -- Product 360
    estimated_first_sale_360    * renewal_360_vector      * (1-use_real_first_sale)   AS estimated_estimated_360_renewal,
    real_first_sale_360         * renewal_360_vector      * use_real_first_sale       AS estimated_real_360_renewal,

    -- Product 720
    estimated_first_sale_720    * renewal_720_vector      * (1-use_real_first_sale)   AS estimated_estimated_720_renewal,
    real_first_sale_720         * renewal_720_vector      * use_real_first_sale       AS estimated_real_720_renewal

  FROM revenue_model.first_sale

    LEFT JOIN (
                SELECT

                  date_month     AS date_renewal,
                  renewals       AS real_renewals

                FROM revenue_model.base_metrics
              ) RNWL ON add_months(date_month, renewal_month_offset) :: DATE = date_renewal
  ORDER BY 1,2

);
