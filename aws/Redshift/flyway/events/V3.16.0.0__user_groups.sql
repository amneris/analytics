GRANT USAGE                ON SCHEMA events                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA events                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA events GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA events                      FROM GROUP redash_group;

GRANT USAGE                ON SCHEMA events                        TO GROUP chartio_group;
GRANT SELECT ON ALL TABLES IN SCHEMA events                        TO GROUP chartio_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA events GRANT SELECT ON TABLES TO GROUP chartio_group;
REVOKE CREATE              ON SCHEMA events                      FROM GROUP chartio_group;

GRANT ALL                  ON SCHEMA events                        TO GROUP service_group;
