CREATE TABLE events.user_signed_up
(
    event_id                VARCHAR(36)     PRIMARY KEY  NOT NULL    ENCODE ZSTD,
    event_type              VARCHAR(40)                  NOT NULL    ENCODE ZSTD,
    event_timestamp         TIMESTAMP                    NOT NULL    ENCODE ZSTD,
    os_name                 VARCHAR(10)                              ENCODE ZSTD,

    user_id                 INT                         NOT NULL     ENCODE ZSTD,
    network_name            VARCHAR(100)                             ENCODE ZSTD,
    campaign_name           VARCHAR(300)                             ENCODE ZSTD,
    adgroup_name            VARCHAR(500)                             ENCODE ZSTD,
    creative_name           VARCHAR(500)                             ENCODE ZSTD,
    adjust_id               VARCHAR(100)                             ENCODE ZSTD,
    tracker                 VARCHAR(10)                              ENCODE ZSTD,
    user_ip                 VARCHAR(16)                              ENCODE ZSTD,
    country_code            VARCHAR(4)                               ENCODE ZSTD,
    device_type             VARCHAR(10)                              ENCODE ZSTD,
    source_ip               VARCHAR(16)                              ENCODE ZSTD
)
DISTKEY(user_id)
INTERLEAVED SORTKEY(event_timestamp)
;




