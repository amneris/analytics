
DROP TABLE IF EXISTS events.temporary_users;

CREATE TABLE events.temporary_users
(
  id                  INT              PRIMARY KEY  NOT NULL,
  email               VARCHAR(800)                  NOT NULL,
  countryId           INT                           NOT NULL,
  entryDate           TIMESTAMP,
  idPartnerSource     BIGINT,
  idSourceList        BIGINT

)
  DISTKEY(id)
  INTERLEAVED SORTKEY(entryDate)
;

INSERT INTO events.temporary_users SELECT * FROM staging_users.consolidated_users;

INSERT INTO events.user_registered (
    event_id,
    event_timestamp,
    insert_timestamp,
    user_id,

    email,
    campaign_id,
    adgroup_id,

    acquisition_country_id,
    acquisition_channel_id,
    acquisition_partner,
    acquisition_source,
    network_name,
    creative_name,
    adjust_id
)
  SELECT
    -- Event properties
    uuid()                                                                          AS event_id,
    COALESCE(stag.event_timestamp, '1900-01-01') :: TIMESTAMP                       AS event_timestamp,
    current_timestamp                                                               AS insert_timestamp,
    -- User information
    user_id,
    email                                                                           AS email,

    -- Attribution key (campaign and adgroup)
    COALESCE(campaign_id,'-1')                                                      AS campaign_id,
    COALESCE(adgroup_id,'-1')                                                       AS adgroup_id,
    -- Attribution key (partner - source - country - channel)
    CASE
      WHEN network_name IS NULL THEN partner_country_id
      ELSE -1 END                                                                 AS acquisition_country_id,
    CASE
      WHEN network_name IS NULL THEN partner_channel_id
      ELSE -1 END                                                                 AS acquisition_channel_id,
    CASE
      WHEN network_name IS NULL THEN acquisition_partner
      ELSE -1 END                                                                 AS acquisition_partner,
    CASE
      WHEN network_name IS NULL THEN acquisition_source
      ELSE -1 END                                                                 AS acquisition_source,
    -- Extra attribution information
    network_name,
    creative_name,
    adjust_id

  FROM (
         SELECT
           id                           AS user_id,
           email,
           entrydate                    AS event_timestamp,
           idpartnersource              AS acquisition_partner,
           idsourcelist                 AS acquisition_source,
           CASE WHEN idcountry NOT IN (0,-1)
             THEN idcountry
           ELSE countryid END           AS partner_country_id,
           idchannel                    AS partner_channel_id
         FROM events.temporary_users
           LEFT JOIN staging_partners.aba_partners_list ON idpartnersource = idpartner
           WHERE entrydate < CURRENT_DATE - INTERVAL '15 minutes'
           AND entrydate > (SELECT COALESCE(max(event_timestamp), '0000-01-01') FROM events.user_registered)
       ) AS stag

  LEFT JOIN
    (
      SELECT
        event_timestamp AS signed_up_timestamp,
        user_id,
        network_name,
        CASE
        WHEN network_name IN ('Facebook Installs', 'Instagram Installs', 'Off-Facebook Installs', 'Apple Search Ads', 'Yahoo Gemini Installs')
          THEN CAST(REGEXP_SUBSTR(REGEXP_REPLACE(campaign_name, '^.* ', ''), '[0-9]+') AS VARCHAR)
        WHEN network_name = ('Organic')
          THEN NULL
        ELSE CAST(campaign_name AS VARCHAR) END                               AS campaign_id,
        CASE
        WHEN network_name IN ('Facebook Installs', 'Instagram Installs', 'Off-Facebook Installs', 'Apple Search Ads', 'Yahoo Gemini Installs')
          THEN CAST(REGEXP_SUBSTR(REGEXP_REPLACE(adgroup_name, '^.* ', ''), '[0-9]+') AS VARCHAR)
        WHEN network_name = 'Google Universal App Campaigns'
          THEN REGEXP_REPLACE(adgroup_name, ' .*', '')
        WHEN network_name = ('Organic')
          THEN NULL
        ELSE CAST(adgroup_name AS VARCHAR) END                                AS adgroup_id,
        creative_name,
        adjust_id

      FROM events.user_signed_up

      ) AS sign USING (user_id)

  ORDER BY event_timestamp
;

DROP TABLE events.temporary_users;