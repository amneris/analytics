INSERT INTO events.payments_failed (

  SELECT

    uuid()                                              AS event_id,
    dateendtransaction                                  AS event_timestamp,
    current_timestamp                                   AS insert_timestamp,
    id                                                  AS payment_id,
    userid                                              AS user_id,
    autoid                                              AS payment_order_number,
    idcountry                                           AS payment_country_id,
    idperiodpay                                         AS product_id,
    payment_gateway                                     AS payment_gateway_id,
    status                                              AS status,
    datetopay                                           AS future_payment_date,
    currencytrans                                       AS currency_code,
    xrate_to_eur                                        AS xrate_to_eur,
    idUserCreditForm                                    AS user_credit_form,
    amountOriginal                                      AS original_amount,
    amountPrice                                         AS amount,
    amountDiscount                                      AS discount,
    taxRateValue                                        AS tax_rate_value,

    COALESCE(isrecurring :: BOOLEAN, FALSE)             AS isrecurring,
    COALESCE(isextend :: BOOLEAN, FALSE)                AS isexted,
    CASE WHEN ucf_kind = 2
      THEN TRUE
    ELSE FALSE END                                      AS is_amex,

    -- Convert to euros and deduct currency exchange commission
    amountPrice / (xrate_to_eur * (1 + fee_currency))   AS amount_gross_eur,

    -- Deduct tax and gateway fee, then convert to euros and deduct currency exchange commission
    (amountPrice
     - (amountPrice - amountPrice / (1 + tax_rate))
     - (amountPrice / (1 + tax_rate) * fee_payment_gateway))
    / (xrate_to_eur * (1 + fee_currency))       AS amount_net_eur,

    -- Convert to euros and deduct currency exchange commission
    amountDiscount / (xrate_to_eur * (1 + fee_currency))
                                                        AS discount_gross_eur,

    -- Calculate tax, then convert to euros
    (amountPrice - amountPrice / (1 + tax_rate))
    / (xrate_to_eur * (1 + fee_currency))       AS tax_eur,

    -- Deduct tax, calculate gateway fee, then convert to euros
    (amountPrice / (1 + tax_rate) * fee_payment_gateway)
    / (xrate_to_eur * (1 + fee_currency))       AS fee_eur,

    amountPrice                                         AS amount_gross_original,
    -- Deduct tax and gateway fee
    amountPrice
    - (amountPrice - amountPrice / (1 + tax_rate))
    - (amountPrice / (1 + tax_rate) * fee_payment_gateway)
                                                        AS amount_net_original,

    amountDiscount                                      AS discount_gross_original,

    amountPrice - amountPrice / (1 + tax_rate)          AS tax_original,

    -- Deduct tax, then calculate gateway fee
    amountPrice / (1 + tax_rate) * fee_payment_gateway
                                                        AS fee_original
  FROM
    (
      SELECT DISTINCT
        consolidated_payments.*,
        -- Default currency exchange commission is 2.63 percent
        taxRateValue / 100               AS tax_rate,
        COALESCE(fee_currency, 0.0263)   AS fee_currency,
        COALESCE(fee_payment_gateway, 0) AS fee_payment_gateway,
        kind                             AS ucf_kind,
        CASE WHEN xratetoeur != 0
          THEN xratetoeur
        ELSE 1 END                       AS xrate_to_eur

      FROM staging_payments.consolidated_payments
        LEFT JOIN staging_fees.fees_currency fcu ON currencyid = currencytrans
        LEFT JOIN staging_fees.fees_payment_gateway fpg ON payment_gatewayid = payment_gateway
        LEFT JOIN staging_payments.user_credit_forms ucf ON ucf.id = idusercreditform
      WHERE status = 10
            AND amountprice >= 0
            AND payment_gateway NOT IN (3, 5)

    ) src
  WHERE
    dateendtransaction < CURRENT_TIMESTAMP - INTERVAL '5 hours'
    AND dateendtransaction > (SELECT COALESCE(max(event_timestamp), '0000-01-01')::TIMESTAMP
                              FROM events.payments_failed)
  ORDER BY event_timestamp
)