INSERT INTO events.user_registered (

  SELECT
    uuid()                                          AS event_id,
    entrydate                                       AS event_timestamp,
    current_timestamp                               AS insert_timestamp,

    id                                              AS user_id,
    email                                           AS email,
    countryid                                       AS user_country_id,
    COALESCE(partner_country_id,countryid,-1)       AS acquisition_country_id,
    COALESCE(partner_channel_id,-1)                 AS acquisition_channel_id,
    idpartnersource                                 AS acquisition_partner,
    idsourcelist                                    AS acquisition_source

  FROM (
         SELECT
           id,
           email,
           countryid,
           entrydate,
           idpartnersource,
           idsourcelist,
           CASE WHEN idcountry NOT IN (0,-1)
             THEN idcountry
           ELSE NULL END                AS partner_country_id,
           idchannel                    AS partner_channel_id
         FROM staging_users.consolidated_users
           LEFT JOIN staging_partners.aba_partners_list ON idpartnersource = idpartner
       ) t

  WHERE
    entrydate < CURRENT_DATE - INTERVAL '5 hours'
    AND entrydate > (SELECT COALESCE(max(event_timestamp), '0000-01-01') FROM events.user_registered)
  ORDER BY event_timestamp

);