
CREATE TABLE events.installs
(
  -- Event Information
  event_id                  VARCHAR(36)      PRIMARY KEY  NOT NULL    ENCODE ZSTD,
  install_timestamp         TIMESTAMP                     NOT NULL    ENCODE ZSTD,
  adjust_id                 VARCHAR(100)                              ENCODE ZSTD,

  -- Network information
  network_name              VARCHAR(100)                              ENCODE ZSTD,
  install_channel_id        INT                                       ENCODE ZSTD,
  install_country_id        INT                                       ENCODE ZSTD,

  -- Attribution Key (partner, source, country and channel)
  campaign_id               VARCHAR(100)                              ENCODE ZSTD,
  adgroup_id                VARCHAR(100)                              ENCODE ZSTD,
  new_partner_id            BIGINT                                    ENCODE ZSTD


)
DISTKEY(adjust_id)
INTERLEAVED SORTKEY(install_timestamp);



