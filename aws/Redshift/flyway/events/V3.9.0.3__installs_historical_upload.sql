INSERT INTO events.installs (
    event_id,
    install_timestamp,
    adjust_id,
    network_name,
    install_channel_id,
    install_country_id,
    campaign_id,
    adgroup_id,
    new_partner_id

)
    SELECT
    event_id,
    event_timestamp AS install_timestamp,
    adjust_id,
    network_name,
    idchannel       AS install_channel_id,
    id              AS install_country_id,

    CASE
    WHEN network_name IN ('Facebook Installs', 'Instagram Installs', 'Off-Facebook Installs', 'Apple Search Ads', 'Yahoo Gemini Installs')
      THEN CAST(REGEXP_SUBSTR(REGEXP_REPLACE(campaign_name, '^.* ', ''), '[0-9]+') AS VARCHAR)
    WHEN network_name = 'Organic'
      THEN '-1'
    ELSE CAST(campaign_name AS VARCHAR) END                               AS campaign_id,

    CASE
    WHEN network_name IN ('Facebook Installs', 'Instagram Installs', 'Off-Facebook Installs', 'Apple Search Ads', 'Yahoo Gemini Installs')
      THEN CAST(REGEXP_SUBSTR(REGEXP_REPLACE(adgroup_name, '^.* ', ''), '[0-9]+') AS VARCHAR)
    WHEN network_name = 'Google Universal App Campaigns'
      THEN REGEXP_REPLACE(adgroup_name, ' .*', '')
    WHEN network_name = 'Organic'
      THEN '-1'
    ELSE CAST(adgroup_name AS VARCHAR) END                                AS adgroup_id,

    NULLIF(split_part(campaign_name,'___',2),'') :: BIGINT                AS new_partner_id

  FROM events.app_installed
  LEFT JOIN staging_countries.country ON lower(country_code) = lower(iso)
  LEFT JOIN staging_partners.aba_partners_channels ON lower(os_name) = lower(namechannel)

  WHERE event_timestamp < CURRENT_DATE - INTERVAL '15 minutes'
  AND event_timestamp > (SELECT COALESCE(max(install_timestamp), '0000-01-01') FROM events.installs)

  ORDER BY event_timestamp
;

