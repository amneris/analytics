CREATE TABLE events.module_finished
(
  event_id                VARCHAR(36) PRIMARY KEY  NOT NULL    ENCODE ZSTD,
  event_timestamp         TIMESTAMP                NOT NULL    ENCODE ZSTD,
  insert_timestamp        TIMESTAMP                NOT NULL    ENCODE ZSTD,
  user_id                 BIGINT                   NOT NULL    ENCODE ZSTD,

  device                  CHAR(15)                 NOT NULL    ENCODE ZSTD,
  event_type              CHAR(30)                 NOT NULL    ENCODE ZSTD,
  quantity                INT                                  ENCODE ZSTD

)
  DISTKEY (event_timestamp)
  INTERLEAVED SORTKEY (event_timestamp);

