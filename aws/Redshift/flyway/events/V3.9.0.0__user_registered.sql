DROP TABLE IF EXISTS events.user_registered CASCADE;

CREATE TABLE events.user_registered
(
  -- Event Information
  event_id                  VARCHAR(36)      PRIMARY KEY  NOT NULL    ENCODE ZSTD,
  event_timestamp           TIMESTAMP                     NOT NULL    ENCODE ZSTD,
  insert_timestamp          TIMESTAMP                     NOT NULL    ENCODE ZSTD,
  user_id                   INT                           NOT NULL    ENCODE ZSTD,

  -- User properties
  email                     VARCHAR(800)                  NOT NULL    ENCODE ZSTD,

  -- Attribution Key (campaign and adgroup)
  campaign_id               VARCHAR(200)                              ENCODE ZSTD,
  adgroup_id                VARCHAR(200)                              ENCODE ZSTD,

  -- Attribution Key (partner, source, country and channel)
  acquisition_country_id    INT                                       ENCODE ZSTD,
  acquisition_channel_id    INT                                       ENCODE ZSTD,
  acquisition_partner       BIGINT                                    ENCODE ZSTD,
  acquisition_source        BIGINT                                    ENCODE ZSTD,
  -- Extra attribution information
  network_name              VARCHAR(100)                              ENCODE ZSTD,
  creative_name             VARCHAR(500)                              ENCODE ZSTD,
  adjust_id                 VARCHAR(100)                              ENCODE ZSTD

)
DISTKEY(user_id)
INTERLEAVED SORTKEY(event_timestamp);



