CREATE TABLE events.payments
(
  event_id                VARCHAR(36) PRIMARY KEY  NOT NULL    ENCODE ZSTD,
  event_timestamp         TIMESTAMP                NOT NULL    ENCODE ZSTD,
  insert_timestamp        TIMESTAMP                NOT NULL    ENCODE ZSTD,
  payment_id              VARCHAR(15)              NOT NULL    ENCODE ZSTD,
  user_id                 INT                      NOT NULL    ENCODE ZSTD,


  payment_order_number    INT                      NOT NULL    ENCODE ZSTD,
  payment_country_id      INT                      NOT NULL    ENCODE ZSTD,
  product_id              INT                      NOT NULL    ENCODE ZSTD,
  payment_gateway         INT                      NOT NULL    ENCODE ZSTD,
  status                  INT                      NOT NULL    ENCODE ZSTD,
  future_payment_date     TIMESTAMP                            ENCODE ZSTD,

  currency_code           VARCHAR(3)               NOT NULL    ENCODE ZSTD,
  xrate_to_eur            DECIMAL(10, 5)           NOT NULL    ENCODE ZSTD,
  user_credit_form        INT                                  ENCODE ZSTD,
  original_amount         DECIMAL(12, 2)                       ENCODE ZSTD,
  amount                  DECIMAL(12, 2)                       ENCODE ZSTD,
  discount                DECIMAL(12, 2)                       ENCODE ZSTD,
  tax_rate_value          DECIMAL(12, 2)                       ENCODE ZSTD,
  isrecurring             BOOLEAN                              ENCODE ZSTD,
  isexted                 BOOLEAN                              ENCODE ZSTD,

  is_amex                 BOOLEAN                              ENCODE ZSTD,
  amount_gross_eur        DECIMAL(14, 4)                       ENCODE ZSTD,
  amount_net_eur          DECIMAL(14, 4)                       ENCODE ZSTD,
  discount_gross_eur      DECIMAL(14, 4)                       ENCODE ZSTD,
  tax_eur                 DECIMAL(14, 4)                       ENCODE ZSTD,
  fee_eur                 DECIMAL(14, 4)                       ENCODE ZSTD,

  amount_gross_original   DECIMAL(14, 4)                       ENCODE ZSTD,
  amount_net_original     DECIMAL(14, 4)                       ENCODE ZSTD,
  discount_gross_original DECIMAL(14, 4)                       ENCODE ZSTD,
  tax_original            DECIMAL(14, 4)                       ENCODE ZSTD,
  fee_original            DECIMAL(14, 4)                       ENCODE ZSTD

)
  DISTKEY (payment_id)
  INTERLEAVED SORTKEY (event_timestamp);
