GRANT USAGE ON SCHEMA                events                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA events                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   events GRANT SELECT ON TABLES TO analyticsuser;
