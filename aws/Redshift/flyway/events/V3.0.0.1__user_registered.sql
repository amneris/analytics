CREATE TABLE events.user_registered
(
  event_id                  VARCHAR(36)      PRIMARY KEY  NOT NULL    ENCODE ZSTD,
  event_timestamp           TIMESTAMP                     NOT NULL    ENCODE ZSTD,
  insert_timestamp          TIMESTAMP                     NOT NULL    ENCODE ZSTD,
  user_id                   INT                           NOT NULL    ENCODE ZSTD,

  email                     VARCHAR(800)                  NOT NULL    ENCODE ZSTD,
  user_country_id           INT                           NOT NULL    ENCODE ZSTD,

  acquisition_country_id    INT                                       ENCODE ZSTD,
  acquisition_channel_id    INT                                       ENCODE ZSTD,
  acquisition_partner       BIGINT                                    ENCODE ZSTD,
  acquisition_source        BIGINT                                    ENCODE ZSTD

)
DISTKEY(user_id)
INTERLEAVED SORTKEY(event_timestamp);





