
CREATE TABLE transactional.consolidated_google_adwords
(
  day               DATE            NOT NULL        ENCODE ZSTD,
  campaign_id       BIGINT          NOT NULL        ENCODE ZSTD,
  campaign_name     VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  adgroup_id        BIGINT          NOT NULL        ENCODE ZSTD,
  adgroup_name      VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  clicks            INT                             ENCODE ZSTD,
  impressions       INT                             ENCODE ZSTD,
  cost              BIGINT                          ENCODE ZSTD,

  CONSTRAINT consolidated_google_adwords_pk PRIMARY KEY (day, campaign_id, adgroup_id)

)
  DISTKEY(day)
  INTERLEAVED SORTKEY(day,campaign_id, adgroup_id)