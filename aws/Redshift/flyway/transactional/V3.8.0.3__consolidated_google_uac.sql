
CREATE TABLE transactional.consolidated_google_uac
(
  day               DATE            NOT NULL        ENCODE ZSTD,
  campaign_id       BIGINT          NOT NULL        ENCODE ZSTD,
  campaign_name     VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  ad_network        VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  clicks            INT                             ENCODE ZSTD,
  impressions       INT                             ENCODE ZSTD,
  cost              BIGINT                          ENCODE ZSTD,

  CONSTRAINT consolidated_google_uac_pk PRIMARY KEY (day, campaign_id, ad_network)

)
  DISTKEY(day)
  INTERLEAVED SORTKEY(day,campaign_id, ad_network)
