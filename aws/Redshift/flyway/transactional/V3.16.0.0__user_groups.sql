GRANT USAGE                ON SCHEMA transactional                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA transactional                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA transactional GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA transactional                      FROM GROUP redash_group;

GRANT USAGE                ON SCHEMA transactional                        TO GROUP chartio_group;
GRANT SELECT ON ALL TABLES IN SCHEMA transactional                        TO GROUP chartio_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA transactional GRANT SELECT ON TABLES TO GROUP chartio_group;
REVOKE CREATE              ON SCHEMA transactional                      FROM GROUP chartio_group;

GRANT ALL                  ON SCHEMA transactional                        TO GROUP service_group;
