GRANT USAGE ON SCHEMA                transactional                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA transactional                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   transactional GRANT SELECT ON TABLES TO analyticsuser;

