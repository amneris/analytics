-- First fill for this table
-- Only add actions, with delay of 21 days (

INSERT INTO transactional.consolidated_google_uac(

    day,
    campaign_id,
    campaign_name,
    ad_network,
    clicks,
    impressions,
    cost
)

SELECT
    day,
    campaign_id,
    campaign_name,
    ad_network,
    clicks,
    impressions,
    cost
FROM staging_investment.google_uac;
