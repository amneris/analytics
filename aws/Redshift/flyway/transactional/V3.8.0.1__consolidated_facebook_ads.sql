
CREATE TABLE transactional.consolidated_facebook_ads
(
  campaign_id       BIGINT          NOT NULL        ENCODE ZSTD,
  campaign_name     VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  adset_id          BIGINT          NOT NULL        ENCODE ZSTD,
  adset_name        VARCHAR(200)    NOT NULL        ENCODE ZSTD,
  clicks            INT                             ENCODE ZSTD,
  impressions       INT                             ENCODE ZSTD,
  spend             NUMERIC(12,2)                   ENCODE ZSTD,
  date_start        DATE            NOT NULL        ENCODE ZSTD,
  date_stop         DATE            NOT NULL        ENCODE ZSTD,

  CONSTRAINT consolidated_facebook_ads_pk PRIMARY KEY (date_start, campaign_id, adset_id)

)
  DISTKEY(date_start)
  INTERLEAVED SORTKEY(date_start,campaign_id, adset_id)
;
