-- First fill for this table
-- Only add actions, with delay of 21 days (

INSERT INTO transactional.consolidated_google_adwords(

    day,
    campaign_id,
    campaign_name,
    adgroup_id,
    adgroup_name,
    clicks,
    impressions,
    cost
)

SELECT
    day,
    campaign_id,
    campaign_name,
    adgroup_id,
    adgroup_name,
    clicks,
    impressions,
    cost
FROM staging_investment.google_adwords;
