-- First fill for this table

INSERT INTO transactional.consolidated_facebook_ads(

    campaign_id,
    campaign_name,
    adset_id,
    adset_name,
    clicks,
    impressions,
    spend,
    date_start,
    date_stop
)

SELECT

    campaign_id,
    campaign_name,
    adset_id,
    adset_name,
    clicks,
    impressions,
    spend,
    date_start,
    date_stop

FROM staging_investment.facebook_ads;