CREATE TABLE core.dim_partner (
  partner_id        BIGINT       NOT NULL ENCODE DELTA,
  partner           VARCHAR(300) NOT NULL ENCODE LZO,
  partner_type      VARCHAR(100) NOT NULL ENCODE TEXT255,
  partner_category  VARCHAR(100) NOT NULL ENCODE TEXT255,
  last_changed_date TIMESTAMPTZ  NOT NULL ENCODE DELTA32K,
  created_date      TIMESTAMPTZ  NOT NULL ENCODE DELTA32K,
  CONSTRAINT dim_partner_pk PRIMARY KEY (partner_id)
)
  DISTKEY (partner_id)
  INTERLEAVED SORTKEY (partner_id);

--Insert the Error record (partner_id = -1) and Unknown record (partner_id = 0)
INSERT INTO core.dim_partner (partner_id, partner, partner_type, partner_category, last_changed_date, created_date)
VALUES
  (-1, 'Error',   'Error',   'Error',   current_timestamp, current_timestamp),
  ( 0, 'Unknown', 'Unknown', 'Unknown', current_timestamp, current_timestamp);
