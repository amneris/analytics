GRANT USAGE ON SCHEMA                core                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA core                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   core GRANT SELECT ON TABLES TO analyticsuser;
