CREATE TABLE core.dim_range (
  range_id                INT           NOT NULL  ENCODE RAW,
  exclusive_range         VARCHAR(100)  NOT NULL  ENCODE RAW,
  accumulated_range       VARCHAR(100)  NOT NULL  ENCODE RAW,
  exclusive_range_low     INT                     ENCODE RAW,
  accumulated_range_low   INT                     ENCODE RAW,
  range_high              INT                     ENCODE RAW,
  days_in_range           INT           NOT NULL  ENCODE RAW,
  CONSTRAINT dim_range_pk PRIMARY KEY (range_id)
)
  DISTKEY(range_id)
  INTERLEAVED SORTKEY(range_id);

--Range data is static
--Add any new intermediate ranges below, please keep ordered by range_id and adjust the adjacent entries
INSERT INTO core.dim_range (range_id, exclusive_range, accumulated_range, exclusive_range_low, accumulated_range_low,
                            range_high, days_in_range) VALUES
  (  -1, 'Error',    'Error',   NULL, NULL, NULL,   -1),
  (   0, 'Unknown',  'Unknown', NULL, NULL, NULL,    0),
  (   1, '0-1',      '0-1',        0,    0,    1,    1),
  (   3, '2-3',      '0-3',        2,    0,    3,    2),
  (   7, '4-7',      '0-7',        4,    0,    7,    4),
  (  15, '8-15',     '0-15',       8,    0,   15,    8),
  (  30, '16-30',    '0-30',      16,    0,   30,   15),
  (  60, '31-60',    '0-60',      31,    0,   60,   30),
  (  90, '61-90',    '0-90',      61,    0,   90,   30),
  ( 180, '91-180',   '0-180',     91,    0,  180,   90),
  ( 360, '181-360',  '0-360',    181,    0,  360,  180),
  ( 720, '361-720',  '0-720',    361,    0,  720,  360),
  (3600, 'Over 720', '0-3600',   721,    0, 3600, 2880)
;
