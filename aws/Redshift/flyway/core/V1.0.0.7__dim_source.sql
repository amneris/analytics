CREATE TABLE core.dim_source (
  source_id         BIGINT        NOT NULL ENCODE RAW,
  source            VARCHAR(300)  NOT NULL ENCODE RAW,
  last_changed_date TIMESTAMPTZ   NOT NULL ENCODE RAW,
  created_date      TIMESTAMPTZ   NOT NULL ENCODE RAW,
  CONSTRAINT dim_source_pk PRIMARY KEY (source_id)
)
  DISTKEY (source_id)
  INTERLEAVED SORTKEY (source_id);

--Insert the Error record (source_id = -1) and Unknown record (source_id = 0)
INSERT INTO core.dim_source (source_id, source, last_changed_date, created_date)
VALUES
  (-1, 'Error',   SYSDATE, SYSDATE),
  ( 0, 'Unknown', SYSDATE, SYSDATE);
