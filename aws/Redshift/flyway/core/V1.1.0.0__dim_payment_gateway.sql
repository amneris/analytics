CREATE TABLE core.dim_payment_gateway (
  payment_gateway_id INT         NOT NULL ENCODE RAW,
  payment_gateway    VARCHAR(75) NOT NULL ENCODE RAW,
  -- Payment channel
  channel_id         INT         NOT NULL ENCODE RAW,
  last_changed_date  TIMESTAMPTZ NOT NULL ENCODE RAW,
  created_date       TIMESTAMPTZ NOT NULL ENCODE RAW,
  CONSTRAINT dim_payment_gateway_pk PRIMARY KEY (payment_gateway_id),
  CONSTRAINT dim_payment_gateway_fk_channel FOREIGN KEY (channel_id) REFERENCES core.dim_channel (channel_id)
)
  DISTKEY (payment_gateway_id)
  INTERLEAVED SORTKEY (payment_gateway_id);

--Insert the Error record (payment_gateway_id = -1) and Unknown record (payment_gateway_id = 0)
INSERT INTO core.dim_payment_gateway (payment_gateway_id, payment_gateway, channel_id, last_changed_date, created_date) VALUES
  (-1, 'Error',   -1, current_timestamp, current_timestamp),
  ( 0, 'Unknown',  0, current_timestamp, current_timestamp);
