CREATE TABLE core.dim_country (
  country_id    SMALLINT            NOT NULL  ENCODE DELTA,
  country       VARCHAR(100)        NOT NULL  ENCODE LZO,
  iso           CHAR(2)                       ENCODE RAW,
  cgroup        VARCHAR(75)                   ENCODE TEXT255,
  region        VARCHAR(75)                   ENCODE TEXT255,
  continent     VARCHAR(75)                   ENCODE TEXT255,
  main_market   VARCHAR(100)                  ENCODE TEXT255,

  CONSTRAINT dim_country_pk PRIMARY KEY (country_id)
)
  DISTKEY(country_id)
  INTERLEAVED SORTKEY(country_id);

--Insert the Error record (country_id = -1) and Unknown record (country_id = 0)
INSERT INTO core.dim_country (country_id, country, iso, cgroup, region, continent, main_market) VALUES
  (-1, 'Error',   NULL, 'Error',   'Error',   'Error',   'Error'),
  ( 0, 'Unknown', NULL, 'Unknown', 'Unknown', 'Unknown', 'Unknown');

--Country data is static
--We took the information from staging_countries, and create logic for main market
--This could be changed anytime needed.
-- 30 - Brazil
-- 73 - France
-- 80 - Germany
-- 105 - Italy
-- 138 - Mexico
-- 177 - Russian Federation
-- 199 - Spain
INSERT INTO core.dim_country
  (
    SELECT
      id            country_id,
      name          country,
      iso,
      countrygroup  cgroup,
      region,
      continent,
      CASE
      WHEN country_id in (30,73,80,105,138,177,199) THEN name
      ELSE 'Others' END AS main_market

    FROM staging_countries.country
    WHERE NOT EXISTS (
      SELECT *
      FROM core.dim_country
      WHERE dim_country.country_id = country.id
    )
  );
