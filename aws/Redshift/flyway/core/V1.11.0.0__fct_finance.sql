CREATE TABLE core.fct_finance (
  date_of_activity      DATE            NOT NULL  ENCODE ZSTD,
  payment_id            VARCHAR(15)     NOT NULL  ENCODE ZSTD,
  country_id            SMALLINT        NOT NULL  ENCODE ZSTD,
  payment_gateway_id    INT             NOT NULL  ENCODE ZSTD,
  is_amex               BOOLEAN         NOT NULL  ENCODE ZSTD,
  class_type            VARCHAR(30)     NOT NULL  ENCODE ZSTD,
  class_subtype         VARCHAR(30)     NOT NULL  ENCODE ZSTD,
  amount_gross_eur      DECIMAL(14, 4)            ENCODE ZSTD,
  amount_net_eur        DECIMAL(14, 4)            ENCODE ZSTD,
  discount_eur          DECIMAL(14, 4)            ENCODE ZSTD,
  tax_eur               DECIMAL(14, 4)            ENCODE ZSTD,
  fee_eur               DECIMAL(14, 4)            ENCODE ZSTD,
  currency_code         VARCHAR(5)      NOT NULL  ENCODE ZSTD,
  amount_gross_original DECIMAL(14, 4)            ENCODE ZSTD,
  amount_net_original   DECIMAL(14, 4)            ENCODE ZSTD,
  discount_original     DECIMAL(14, 4)            ENCODE ZSTD,
  tax_original          DECIMAL(14, 4)            ENCODE ZSTD,
  fee_original          DECIMAL(14, 4)            ENCODE ZSTD,
  CONSTRAINT fct_finance_pk PRIMARY KEY (payment_id),
  CONSTRAINT fct_finance_fk_country FOREIGN KEY (country_id) REFERENCES core.dim_country (country_id),
  CONSTRAINT fct_finance_fk_pg FOREIGN KEY (payment_gateway_id) REFERENCES core.dim_payment_gateway (payment_gateway_id)
)
  DISTKEY(date_of_activity)
  INTERLEAVED SORTKEY(date_of_activity, class_type, class_subtype, country_id, payment_gateway_id, currency_code, is_amex)
;
