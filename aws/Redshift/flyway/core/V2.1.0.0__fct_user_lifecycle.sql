DROP TABLE IF EXISTS core.fct_user_lifecycle CASCADE;

CREATE TABLE core.fct_user_lifecycle (
  date_of_activity       DATE           NOT NULL  ENCODE ZSTD,
  user_id                BIGINT         NOT NULL  ENCODE ZSTD,
  campaign_id            BIGINT         NOT NULL  ENCODE ZSTD,
  country_id             SMALLINT       NOT NULL  ENCODE ZSTD,
  product_id             SMALLINT       NOT NULL  ENCODE ZSTD,
  payment_gateway_id     INT            NOT NULL  ENCODE ZSTD,
  class_type             VARCHAR(30)    NOT NULL  ENCODE ZSTD,
  class_subtype          VARCHAR(30)    NOT NULL  ENCODE ZSTD,
  product_change_type    VARCHAR(30)    NOT NULL  ENCODE ZSTD,
  begin_date             DATE           NOT NULL  ENCODE ZSTD,
  date_of_acquisition    DATE           NOT NULL  ENCODE ZSTD,
  acquisition_country_id SMALLINT       NOT NULL  ENCODE ZSTD,
  amount_gross_eur       DECIMAL(14, 4)           ENCODE ZSTD,
  amount_net_eur         DECIMAL(14, 4)           ENCODE ZSTD,
  discount_eur           DECIMAL(14, 4)           ENCODE ZSTD,
  days_out               INT                      ENCODE ZSTD,
  days_to_cancel         INT                      ENCODE ZSTD,
  CONSTRAINT fct_user_lifecycle_fk_cam FOREIGN KEY (campaign_id) REFERENCES core.dim_campaign (campaign_id),
  CONSTRAINT fct_user_lifecycle_fk_country FOREIGN KEY (country_id) REFERENCES core.dim_country (country_id),
  CONSTRAINT fct_user_lifecycle_fk_pg FOREIGN KEY (payment_gateway_id) REFERENCES core.dim_payment_gateway (payment_gateway_id)
)
  DISTKEY(user_id)
  INTERLEAVED SORTKEY(user_id, class_type, date_of_activity, class_subtype, campaign_id, country_id, payment_gateway_id, product_id)
;

