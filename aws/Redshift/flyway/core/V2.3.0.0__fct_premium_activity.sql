CREATE TABLE core.fct_premium_activity
(
  date_of_activity      DATE     NOT NULL     ENCODE ZSTD,
  user_id               BIGINT   NOT NULL     ENCODE ZSTD,
  device                CHAR(15) NOT NULL     ENCODE ZSTD,
  class_type            CHAR(30) NOT NULL     ENCODE ZSTD,
  campaign_id           BIGINT                ENCODE ZSTD,
  country_id            SMALLINT              ENCODE ZSTD,
  date_of_acquisition   DATE                  ENCODE ZSTD,
  date_of_conversion    DATE                  ENCODE ZSTD,
  conversion_product_id SMALLINT              ENCODE ZSTD,
  current_product_id    SMALLINT              ENCODE ZSTD,
  sale_number           INT                   ENCODE ZSTD,

  quantity              INT                   ENCODE ZSTD,

  CONSTRAINT fct_premium_activity_pk PRIMARY KEY (date_of_activity, user_id, device, class_type),
  CONSTRAINT fct_premium_activity_fk_cam FOREIGN KEY (campaign_id) REFERENCES core.dim_campaign (campaign_id),
  CONSTRAINT fct_premium_activity_fk_country FOREIGN KEY (country_id) REFERENCES core.dim_country (country_id)
)
  DISTKEY (date_of_activity
)
  INTERLEAVED SORTKEY (date_of_activity, class_type, device, user_id
);
