DROP TABLE IF EXISTS core.fct_campaign_performance CASCADE;

CREATE TABLE core.fct_campaign_performance (
  date_of_activity  DATE          NOT NULL  ENCODE ZSTD,
  campaign_id       BIGINT        NOT NULL  ENCODE ZSTD,
  country_id        SMALLINT      NOT NULL  ENCODE ZSTD,
  class_type        CHAR(30)      NOT NULL  ENCODE ZSTD,
  class_subtype     CHAR(30)      NOT NULL  ENCODE ZSTD,
  date_diff         INT           NOT NULL  ENCODE ZSTD,
  range_id          INT           NOT NULL  ENCODE ZSTD,
  quantity          INT                     ENCODE ZSTD,
  amount_gross_eur  DECIMAL(14,4)           ENCODE ZSTD,
  amount_net_eur    DECIMAL(14,4)           ENCODE ZSTD,
  CONSTRAINT fct_campaign_performance_pk PRIMARY KEY (date_of_activity, campaign_id, country_id, class_type, class_subtype, date_diff),
  CONSTRAINT fct_campaign_performance_fk_cam FOREIGN KEY (campaign_id) REFERENCES core.dim_campaign (campaign_id),
  CONSTRAINT fct_campaign_performance_fk_country FOREIGN KEY (country_id) REFERENCES core.dim_country (country_id),
  CONSTRAINT fct_campaign_performance_fk_range FOREIGN KEY (range_id) REFERENCES core.dim_range (range_id)
)
  DISTKEY(date_of_activity)
  INTERLEAVED SORTKEY(date_of_activity, class_type, class_subtype, range_id, date_diff, campaign_id, country_id)
;
