CREATE TABLE core.fct_weekly_active_users
(
  id                     VARCHAR(36)      NOT NULL     ENCODE ZSTD,
  insert_timestamp       TIMESTAMP        NOT NULL     ENCODE ZSTD,
  week_of_activity       DATE             NOT NULL     ENCODE ZSTD,
  device                 VARCHAR(15)      NOT NULL     ENCODE ZSTD,
  country_id             INT              NOT NULL     ENCODE ZSTD,
  user_type              VARCHAR(20)      NOT NULL     ENCODE ZSTD,
  wau                    BIGINT                        ENCODE ZSTD,
  sessions               BIGINT                        ENCODE ZSTD,


  CONSTRAINT fct_weekly_active_users_pk PRIMARY KEY (id)
)
  DISTKEY (week_of_activity)
  INTERLEAVED SORTKEY (week_of_activity);
