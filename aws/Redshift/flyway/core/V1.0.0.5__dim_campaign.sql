CREATE TABLE core.dim_campaign (
  campaign_id               BIGINT IDENTITY(-1,1)    NOT NULL  ENCODE DELTA,
  campaign                  VARCHAR(500)             NOT NULL  ENCODE LZO,
  partner_id                BIGINT                   NOT NULL  ENCODE MOSTLY16,
  source_id                 SMALLINT                 NOT NULL  ENCODE MOSTLY8,
  network                   VARCHAR(100)             NOT NULL  ENCODE TEXT255,
  channel_id                SMALLINT                 NOT NULL  ENCODE MOSTLY8,
  campaign_country_id       SMALLINT                 NOT NULL  ENCODE MOSTLY8,
  last_changed_date         TIMESTAMPTZ              NOT NULL  ENCODE DELTA32K,
  created_date              TIMESTAMPTZ              NOT NULL  ENCODE DELTA32K,
  last_changed_source_table VARCHAR(200)             NOT NULL  ENCODE TEXT255,
  created_source_table      VARCHAR(200)             NOT NULL  ENCODE TEXT255,
  CONSTRAINT dim_campaign_pk PRIMARY KEY (campaign_id),
  CONSTRAINT dim_campaign_fk_partner FOREIGN KEY (partner_id) REFERENCES core.dim_partner (partner_id),
  CONSTRAINT dim_campaign_fk_channel FOREIGN KEY (channel_id) REFERENCES core.dim_channel (channel_id)
)
  DISTKEY(campaign_id)
  INTERLEAVED SORTKEY(campaign_id);

--Insert the Error record (campaign_id = -1) and Unknown record (campaign_id = 0)
INSERT INTO core.dim_campaign (
  campaign,
  partner_id,
  source_id,
  network,
  channel_id,
  campaign_country_id,
  last_changed_date,
  created_date,
  last_changed_source_table,
  created_source_table
) VALUES
  ('Error',  -1, -1, 'Error',  -1, -1, current_timestamp, current_timestamp, 'Error',   'Error'),
  ('Unknown', 0,  0, 'Unknown', 0,  0, current_timestamp, current_timestamp, 'Unknown', 'Unknown');
