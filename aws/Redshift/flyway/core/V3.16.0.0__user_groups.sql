GRANT USAGE                ON SCHEMA core                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA core                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA core GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA core                      FROM GROUP redash_group;

GRANT USAGE                ON SCHEMA core                        TO GROUP chartio_group;
GRANT SELECT ON ALL TABLES IN SCHEMA core                        TO GROUP chartio_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA core GRANT SELECT ON TABLES TO GROUP chartio_group;
REVOKE CREATE              ON SCHEMA core                      FROM GROUP chartio_group;

GRANT ALL                  ON SCHEMA core                        TO GROUP service_group;
