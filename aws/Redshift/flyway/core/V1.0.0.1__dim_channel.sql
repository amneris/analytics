CREATE TABLE core.dim_channel (
  channel_id        INT         NOT NULL ENCODE RAW,
  channel           VARCHAR(75) NOT NULL ENCODE RAW,
  last_changed_date TIMESTAMPTZ NOT NULL ENCODE RAW,
  created_date      TIMESTAMPTZ NOT NULL ENCODE RAW,
  CONSTRAINT dim_channel_pk PRIMARY KEY (channel_id)
)
  DISTKEY (channel_id)
  INTERLEAVED SORTKEY (channel_id);

--Insert the Error record (channel_id = -1) and Unknown record (channel_id = 0)
INSERT INTO core.dim_channel (channel_id, channel, last_changed_date, created_date) VALUES
  (-1, 'Error',   current_timestamp, current_timestamp),
  ( 0, 'Unknown', current_timestamp, current_timestamp);
