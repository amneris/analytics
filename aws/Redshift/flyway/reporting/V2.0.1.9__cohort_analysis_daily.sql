-- Daily cohorted metrics, last 2 years
--
-- To keep record count low we use cgroup instead of country
CREATE TABLE reporting.cohort_analysis_daily (
  date_of_activity DATE           NOT NULL  ENCODE ZSTD,
  partner_type     VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  partner_category VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  channel          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  country          VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  region           VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  continent        VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  main_market      VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  investment       DECIMAL(14, 4)           ENCODE ZSTD,
  acquisition      INT                      ENCODE ZSTD,
  installs         INT                      ENCODE ZSTD,
  conversions      INT                      ENCODE ZSTD,
  revenue          DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_1d       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_3d       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_7d       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_15d      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_30d      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_90d      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_180d     DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_360d     DECIMAL(14, 4)           ENCODE ZSTD,
  conversions_1d   INT                      ENCODE ZSTD,
  conversions_3d   INT                      ENCODE ZSTD,
  conversions_7d   INT                      ENCODE ZSTD,
  conversions_15d  INT                      ENCODE ZSTD,
  conversions_30d  INT                      ENCODE ZSTD,
  conversions_90d  INT                      ENCODE ZSTD,
  conversions_180d INT                      ENCODE ZSTD,
  conversions_360d INT                      ENCODE ZSTD
)
  DISTKEY (date_of_activity)
  INTERLEAVED SORTKEY (date_of_activity, channel, country, partner_type, partner_category, region, main_market);
