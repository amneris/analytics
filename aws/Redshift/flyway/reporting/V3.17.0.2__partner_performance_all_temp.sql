-- Generic table for Partner Performance. table_source specific views depend on this table.
-- The date range is limited so that it fits to a ChartIO stored table, 6 months.

CREATE TABLE reporting.partner_performance_all_temp
(
  date_of_activity                 DATE          NOT NULL   ENCODE ZSTD,

  -- No campaign_id for partner performance
  -- No campaign for partner performance
  -- No adgroup_id for partner performance
  -- No adgroup for partner performance
  -- No ad_source for partner performance (use network - on partner level - instead)
  -- No table_source for partner performance

  partner_id                       BIGINT        NOT NULL   ENCODE ZSTD,
  partner                          VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  partner_group                    VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  partner_type                     VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  partner_category                 VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  source_id                        SMALLINT      NOT NULL   ENCODE ZSTD,
  network                          VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  channel                          VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  country                          VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  cgroup                           VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  region                           VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  continent                        VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  main_market                      VARCHAR(100)  NOT NULL   ENCODE ZSTD,

  impressions                      BIGINT                   ENCODE ZSTD,
  clicks                           INT                      ENCODE ZSTD,
  investment                       DECIMAL(14, 4)           ENCODE ZSTD,
  acquisition                      INT                      ENCODE ZSTD,
  installs                         INT                      ENCODE ZSTD,

  revenue                          DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_1d                       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_3d                       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_7d                       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_15d                      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_30d                      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_90d                      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_180d                     DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_360d                     DECIMAL(14, 4)           ENCODE ZSTD,

  conversions                      INT                      ENCODE ZSTD,
  conversions_1d                   INT                      ENCODE ZSTD,
  conversions_3d                   INT                      ENCODE ZSTD,
  conversions_7d                   INT                      ENCODE ZSTD,
  conversions_15d                  INT                      ENCODE ZSTD,
  conversions_30d                  INT                      ENCODE ZSTD,
  conversions_90d                  INT                      ENCODE ZSTD,
  conversions_180d                 INT                      ENCODE ZSTD,
  conversions_360d                 INT                      ENCODE ZSTD
)
BACKUP NO
DISTKEY (date_of_activity);
