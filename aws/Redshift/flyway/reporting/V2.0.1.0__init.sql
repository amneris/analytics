GRANT USAGE ON SCHEMA                reporting                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA reporting                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   reporting GRANT SELECT ON TABLES TO analyticsuser;
