-- Table for represent the weekly active users (Usage Product view)
CREATE TABLE reporting.usage_premium_activity_acquisition_cohorted (

  date_of_acquisition         DATE           NOT NULL  ENCODE ZSTD,
  acquisition_partner         VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_network         VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_category        VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_type            VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_country         VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_cgroup          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_region          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_continent       VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_main_market     VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_channel         VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  activity_device             VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  conversion_product          VARCHAR(5)     NOT NULL  ENCODE ZSTD,

  total_videos                INT                      ENCODE ZSTD,
  videos_1D                   INT                      ENCODE ZSTD,
  videos_7D                   INT                      ENCODE ZSTD,
  videos_30D                  INT                      ENCODE ZSTD,
  videos_180D                 INT                      ENCODE ZSTD,
  videos_360D                 INT                      ENCODE ZSTD,
  total_units                 INT                      ENCODE ZSTD,
  units_1D                    INT                      ENCODE ZSTD,
  units_7D                    INT                      ENCODE ZSTD,
  units_30D                   INT                      ENCODE ZSTD,
  units_180D                  INT                      ENCODE ZSTD,
  units_360D                  INT                      ENCODE ZSTD,
  total_study                 INT                      ENCODE ZSTD,
  study_1D                    INT                      ENCODE ZSTD,
  study_7D                    INT                      ENCODE ZSTD,
  study_30D                   INT                      ENCODE ZSTD,
  study_180D                  INT                      ENCODE ZSTD,
  study_360D                  INT                      ENCODE ZSTD
)
  DISTKEY (date_of_acquisition)
  INTERLEAVED SORTKEY (date_of_acquisition, acquisition_country, acquisition_channel);


