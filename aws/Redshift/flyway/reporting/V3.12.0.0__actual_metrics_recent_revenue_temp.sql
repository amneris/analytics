-- Day level view for actual metrics, last two years only to keep the record count down
CREATE TABLE reporting.actual_metrics_recent_revenue_temp (
  date_of_activity        DATE           NOT NULL  ENCODE ZSTD,
  product                 VARCHAR(5)     NOT NULL  ENCODE ZSTD,
  acquisition_country     VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_cgroup      VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_region      VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_continent   VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_main_market VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_channel     VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_country         VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  payment_cgroup          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_region          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_continent       VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_main_market     VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_channel         VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  sale_type               VARCHAR(30)    NOT NULL  ENCODE ZSTD,
  detailed_sale_type      VARCHAR(30)    NOT NULL  ENCODE ZSTD,
  revenue                 DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_gross           DECIMAL(14, 4)           ENCODE ZSTD,
  sales                   INT                      ENCODE ZSTD,
  free_trials             INT                      ENCODE ZSTD
)
  DISTKEY (date_of_activity)
  INTERLEAVED SORTKEY (date_of_activity, sale_type, detailed_sale_type, product, acquisition_channel, payment_channel,
  acquisition_country, payment_country);
