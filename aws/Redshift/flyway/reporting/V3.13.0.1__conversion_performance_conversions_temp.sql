-- Conversion component for Conversion Rate analysis, especially to analyze CR distribution over time
--
-- Takes last 36 months of conversions.
-- To keep the record count down the results are on cgroup level (renamed to Country in ChartIO).
CREATE TABLE reporting.conversion_performance_conversions_temp (
  date_of_acquisition     DATE         NOT NULL  ENCODE ZSTD,
  date_of_conversion      DATE         NOT NULL  ENCODE ZSTD,
  product                 VARCHAR(5)   NOT NULL  ENCODE ZSTD,
  acquisition_cgroup      VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  acquisition_region      VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  acquisition_continent   VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  acquisition_main_market VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  acquisition_channel     VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  payment_cgroup          VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  payment_region          VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  payment_continent       VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  payment_main_market     VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  payment_channel         VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  days_to_conversion      INT          NOT NULL  ENCODE ZSTD,
  weeks_to_conversion     INT          NOT NULL  ENCODE ZSTD,
  months_to_conversion    INT          NOT NULL  ENCODE ZSTD,
  -- "3 Bucket Days to Pay" in ChartIO
  days_to_pay             VARCHAR(30)  NOT NULL  ENCODE ZSTD,
  -- "12 Bucket Days to Pay" in ChartIO
  grouped_days_to_pay     VARCHAR(100) NOT NULL  ENCODE ZSTD,
  conversions             INT                    ENCODE ZSTD,
  revenue                 DECIMAL(14, 4)         ENCODE ZSTD
)
  DISTKEY (date_of_acquisition)
  INTERLEAVED SORTKEY (date_of_acquisition, acquisition_channel, payment_channel, acquisition_cgroup, payment_cgroup,
  days_to_pay, grouped_days_to_pay, product);
