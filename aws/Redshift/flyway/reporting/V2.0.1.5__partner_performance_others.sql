-- Specific view for Other networks Partner Performance.
CREATE OR REPLACE VIEW reporting.partner_performance_others AS (
  SELECT
    *
  FROM reporting.partner_performance_all
  WHERE network NOT IN ('AdWords', 'Facebook paid', 'Unknown')
);
