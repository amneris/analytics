-- Acquisition component for Conversion Rate analysis, especially to analyze CR distribution over time
-- Last 36 months, on cgroup level (renamed to country in ChartIO)
CREATE TABLE reporting.conversion_performance_acquisition (
  date_of_activity   DATE        NOT NULL  ENCODE ZSTD,
  cgroup             VARCHAR(75) NOT NULL  ENCODE ZSTD,
  region             VARCHAR(75) NOT NULL  ENCODE ZSTD,
  continent          VARCHAR(75) NOT NULL  ENCODE ZSTD,
  main_market        VARCHAR(75) NOT NULL  ENCODE ZSTD,
  channel            VARCHAR(75) NOT NULL  ENCODE ZSTD,
  acquisition        INT                   ENCODE ZSTD
)
  DISTKEY (date_of_activity)
  INTERLEAVED SORTKEY (date_of_activity, channel, cgroup, region, main_market);
