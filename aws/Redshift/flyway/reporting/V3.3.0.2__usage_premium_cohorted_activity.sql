-- Table for represent the weekly active users (Usage Product view)
CREATE TABLE reporting.usage_premium_cohorted_activity (

  date_of_conversion          DATE           NOT NULL  ENCODE ZSTD,
  acquisition_partner         VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_network         VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_category        VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_type            VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_country         VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_cgroup          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_region          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_continent       VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_main_market     VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_channel         VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  activity_device             VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  conversion_product          VARCHAR(5)     NOT NULL  ENCODE ZSTD,

  active_users_0W             INT                      ENCODE ZSTD,
  active_users_1W             INT                      ENCODE ZSTD,
  active_users_4W             INT                      ENCODE ZSTD,
  active_users_10W            INT                      ENCODE ZSTD,
  active_users_25W            INT                      ENCODE ZSTD,
  active_users_51W            INT                      ENCODE ZSTD,
  watched_videos_0W           INT                      ENCODE ZSTD,
  watched_videos_1W           INT                      ENCODE ZSTD,
  watched_videos_4W           INT                      ENCODE ZSTD,
  watched_videos_10W          INT                      ENCODE ZSTD,
  watched_videos_25W          INT                      ENCODE ZSTD,
  watched_videos_51W          INT                      ENCODE ZSTD,
  finished_units_0W           INT                      ENCODE ZSTD,
  finished_units_1W           INT                      ENCODE ZSTD,
  finished_units_4W           INT                      ENCODE ZSTD,
  finished_units_10W          INT                      ENCODE ZSTD,
  finished_units_25W          INT                      ENCODE ZSTD,
  finished_units_51W          INT                      ENCODE ZSTD
)
  DISTKEY (date_of_conversion)
  INTERLEAVED SORTKEY (date_of_conversion, acquisition_country, acquisition_channel);
