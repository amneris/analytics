-- Specific view for Facebook Partner Performance.
CREATE OR REPLACE VIEW reporting.partner_performance_facebook AS (
  SELECT
    *
  FROM reporting.partner_performance_all
  WHERE network = 'Facebook paid'
);
