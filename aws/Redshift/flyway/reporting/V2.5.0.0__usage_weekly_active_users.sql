-- Table for represent the weekly active users (Usage Product view)
CREATE TABLE reporting.usage_weekly_active_users (

  week_of_activity          DATE                NOT NULL   ENCODE ZSTD,
  user_type                 VARCHAR(20)         NOT NULL   ENCODE ZSTD,
  country                   VARCHAR(100)        NOT NULL   ENCODE ZSTD,
  cgroup                    VARCHAR(75)         NOT NULL   ENCODE ZSTD,
  region                    VARCHAR(75)         NOT NULL   ENCODE ZSTD,
  main_market               VARCHAR(100)        NOT NULL   ENCODE ZSTD,
  continent                 VARCHAR(75)         NOT NULL   ENCODE ZSTD,
  device                    VARCHAR(75)         NOT NULL   ENCODE ZSTD,
  wau                       BIGINT                         ENCODE ZSTD,
  sessions                  BIGINT                         ENCODE ZSTD
)
  DISTKEY (week_of_activity)
  INTERLEAVED SORTKEY (week_of_activity, user_type, country, device);
