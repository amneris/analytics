ALTER TABLE reporting.finance_cohorted_mrr ADD COLUMN  net_mrr_cumulative NUMERIC(38, 4) ENCODE ZSTD;
ALTER TABLE reporting.finance_cohorted_mrr ADD COLUMN  gross_mrr_cumulative NUMERIC(38, 4) ENCODE ZSTD;
ALTER TABLE reporting.finance_cohorted_mrr ADD COLUMN  investment_cumulative NUMERIC(38, 4) ENCODE ZSTD;