-- Specific view for Organic Partner Performance.
CREATE OR REPLACE VIEW reporting.partner_performance_organic AS (
  SELECT
    *
  FROM reporting.partner_performance_all
  WHERE network = 'Unknown'
);
