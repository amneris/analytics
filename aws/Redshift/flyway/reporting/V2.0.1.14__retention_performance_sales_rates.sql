-- Table for calculating cancel rate, fail rate, refund rate, success rate, and average ticket for each
CREATE TABLE reporting.retention_performance_sales_rates (
  date_of_activity     DATE         NOT NULL  ENCODE ZSTD,
  payment_type         VARCHAR(30)  NOT NULL  ENCODE ZSTD,
  product              VARCHAR(5)   NOT NULL  ENCODE ZSTD,
  country              VARCHAR(100) NOT NULL  ENCODE ZSTD,
  cgroup               VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  region               VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  continent            VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  main_market          VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  channel              VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  fail_units           INT                    ENCODE ZSTD,
  fail_revenue         DECIMAL(14, 4)         ENCODE ZSTD,
  fail_revenue_gross   DECIMAL(14, 4)         ENCODE ZSTD,
  cancel_units         INT                    ENCODE ZSTD,
  cancel_revenue       DECIMAL(14, 4)         ENCODE ZSTD,
  cancel_revenue_gross DECIMAL(14, 4)         ENCODE ZSTD,
  refund_units         INT                    ENCODE ZSTD,
  refund_revenue       DECIMAL(14, 4)         ENCODE ZSTD,
  refund_revenue_gross DECIMAL(14, 4)         ENCODE ZSTD,
  sales_units          INT                    ENCODE ZSTD,
  sales_revenue        DECIMAL(14, 4)         ENCODE ZSTD,
  sales_revenue_gross  DECIMAL(14, 4)         ENCODE ZSTD,
  sales_discount       DECIMAL(14, 4)         ENCODE ZSTD,
  net_units            INT                    ENCODE ZSTD,
  net_revenue          DECIMAL(14, 4)         ENCODE ZSTD,
  net_revenue_gross    DECIMAL(14, 4)         ENCODE ZSTD,
  net_discount         DECIMAL(14, 4)         ENCODE ZSTD
)
  DISTKEY (date_of_activity)
  INTERLEAVED SORTKEY (date_of_activity, payment_type, channel, product, country, region, main_market);
