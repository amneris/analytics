-- Table for calculating the MRR (Finance view)
CREATE TABLE reporting.finance_mrr_temp (
  month_of_payment        DATE           NOT NULL  ENCODE ZSTD,
  month_of_conversion     DATE           NOT NULL  ENCODE ZSTD,
  month_of_mrr            DATE           NOT NULL  ENCODE ZSTD,
  months_after_conversion INT            NOT NULL  ENCODE ZSTD,
  product                 VARCHAR (5)    NOT NULL  ENCODE ZSTD,
  sale_type               VARCHAR(30)    NOT NULL  ENCODE ZSTD,
  acquisition_country     VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_cgroup      VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_region      VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_continent   VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_main_market VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_channel     VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_country         VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  payment_cgroup          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_region          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_continent       VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_main_market     VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  payment_channel         VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  active_subscriptions    INT                      ENCODE ZSTD,
  net_mrr                 DECIMAL(14, 4)           ENCODE ZSTD,
  gross_mrr               DECIMAL(14, 4)           ENCODE ZSTD
)
  DISTKEY (month_of_payment)
  INTERLEAVED SORTKEY (month_of_payment, month_of_mrr, months_after_conversion, sale_type, acquisition_channel,
  payment_channel, acquisition_country, payment_country);
