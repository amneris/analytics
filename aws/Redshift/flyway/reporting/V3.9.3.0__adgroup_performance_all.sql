-- Generic table for Adgroup Performance. table_source specific views depend on this table.
-- The date range is limited so that it fits to a ChartIO stored table, 3 months.
CREATE TABLE reporting.adgroup_performance_all
(
  date_of_activity                 DATE          NOT NULL   ENCODE ZSTD,
  campaign_name                    VARCHAR(300)             ENCODE ZSTD,
  campaign_id                      VARCHAR(200)             ENCODE ZSTD,
  adgroup_name                     VARCHAR(300)             ENCODE ZSTD,
  adgroup_id                       VARCHAR(200)             ENCODE ZSTD,
  partner_id                       BIGINT                   ENCODE ZSTD,
  country_id                       BIGINT                   ENCODE ZSTD,
  channel_id                       BIGINT                   ENCODE ZSTD,
  country                          VARCHAR(100)             ENCODE ZSTD,
  cgroup                           VARCHAR(100)             ENCODE ZSTD,
  region                           VARCHAR(100)             ENCODE ZSTD,
  channel                          VARCHAR(100)             ENCODE ZSTD,
  network_name                     VARCHAR(100)             ENCODE ZSTD,

  ad_source                        VARCHAR(50)              ENCODE ZSTD,
  table_source                     VARCHAR(50)              ENCODE ZSTD,

  partner_name                     VARCHAR(100),
  partner_namegroup                VARCHAR(100),
  partner_category_id              INT                      ENCODE ZSTD,
  partner_businessarea_id          INT                      ENCODE ZSTD,
  partner_type_id                  INT                      ENCODE ZSTD,
  partner_group_id                 INT                      ENCODE ZSTD,
  partner_channel_id               INT                      ENCODE ZSTD,
  partner_country_id               INT                      ENCODE ZSTD,

  impressions                      BIGINT                   ENCODE ZSTD,
  clicks                           INT                      ENCODE ZSTD,
  investment                       DECIMAL(14, 4)           ENCODE ZSTD,
  installs                         INT                      ENCODE ZSTD,
  acquisition                      INT                      ENCODE ZSTD,

  revenue_total                    DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_1d                       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_3d                       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_7d                       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_15d                      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_30d                      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_90d                      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_180d                     DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_360d                     DECIMAL(14, 4)           ENCODE ZSTD,

  conversions_total                INT                      ENCODE ZSTD,
  conversions_1d                   INT                      ENCODE ZSTD,
  conversions_3d                   INT                      ENCODE ZSTD,
  conversions_7d                   INT                      ENCODE ZSTD,
  conversions_15d                  INT                      ENCODE ZSTD,
  conversions_30d                  INT                      ENCODE ZSTD,
  conversions_90d                  INT                      ENCODE ZSTD,
  conversions_180d                 INT                      ENCODE ZSTD,
  conversions_360d                 INT                      ENCODE ZSTD
)
BACKUP NO
DISTKEY (date_of_activity);
