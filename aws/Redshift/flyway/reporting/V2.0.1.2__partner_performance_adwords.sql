-- Specific view for Adwords Partner Performance.
CREATE OR REPLACE VIEW reporting.partner_performance_adwords AS (
  SELECT
    *
  FROM reporting.partner_performance_all
  WHERE network = 'AdWords'
);
