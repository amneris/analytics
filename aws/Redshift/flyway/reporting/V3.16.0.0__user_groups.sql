GRANT USAGE                ON SCHEMA reporting                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA reporting                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA reporting GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA reporting                      FROM GROUP redash_group;

GRANT USAGE                ON SCHEMA reporting                        TO GROUP chartio_group;
GRANT SELECT ON ALL TABLES IN SCHEMA reporting                        TO GROUP chartio_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA reporting GRANT SELECT ON TABLES TO GROUP chartio_group;
REVOKE CREATE              ON SCHEMA reporting                      FROM GROUP chartio_group;

GRANT ALL                  ON SCHEMA reporting                        TO GROUP service_group;
