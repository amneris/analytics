-- Table for calculating the cohorted MRR (Finance view)
CREATE TABLE reporting.finance_cohorted_mrr (

  month_of_acquisition      DATE                NOT NULL   ENCODE ZSTD,
  month_of_mrr              DATE                NOT NULL   ENCODE ZSTD,
  country                   VARCHAR(100)        NOT NULL   ENCODE ZSTD,
  cgroup                    VARCHAR(75)         NOT NULL   ENCODE ZSTD,
  region                    VARCHAR(75)         NOT NULL   ENCODE ZSTD,
  main_market               VARCHAR(100)        NOT NULL   ENCODE ZSTD,
  continent                 VARCHAR(75)         NOT NULL   ENCODE ZSTD,
  channel                   VARCHAR(75)         NOT NULL   ENCODE ZSTD,
  active_subscriptions      BIGINT                         ENCODE ZSTD,
  net_mrr                   NUMERIC(38, 4)                 ENCODE ZSTD,
  gross_mrr                 NUMERIC(38, 4)                 ENCODE ZSTD,
  investment                NUMERIC(38, 4)                 ENCODE ZSTD
)
  DISTKEY (month_of_acquisition)
  INTERLEAVED SORTKEY (month_of_acquisition, month_of_mrr, country, channel);
