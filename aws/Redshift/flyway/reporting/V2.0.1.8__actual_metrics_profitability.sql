-- Actual revenue & sales, investment and margin
CREATE TABLE reporting.actual_metrics_profitability (
  date_of_activity          DATE           NOT NULL  ENCODE ZSTD,
  acquisition_country       VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  acquisition_cgroup        VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_region        VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_continent     VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_main_market   VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  acquisition_channel       VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  revenue_total             DECIMAL(14, 4)           ENCODE ZSTD,
  investment                DECIMAL(14, 4)           ENCODE ZSTD,
  margin                    DECIMAL(14, 4)           ENCODE ZSTD,
  sales_total               INT                      ENCODE ZSTD,
  revenue_first_early       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_first_late        DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_first_unknown     DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_first_total       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_next              DECIMAL(14, 4)           ENCODE ZSTD,
  sales_first_early         INT                      ENCODE ZSTD,
  sales_first_late          INT                      ENCODE ZSTD,
  sales_first_unknown       INT                      ENCODE ZSTD,
  sales_first_total         INT                      ENCODE ZSTD,
  sales_next                INT                      ENCODE ZSTD,
  free_trials_first_early   INT                      ENCODE ZSTD,
  free_trials_first_late    INT                      ENCODE ZSTD,
  free_trials_first_unknown INT                      ENCODE ZSTD,
  free_trials_first_total   INT                      ENCODE ZSTD,
  free_trials_next          INT                      ENCODE ZSTD
)
  DISTKEY (date_of_activity)
  INTERLEAVED SORTKEY (date_of_activity, acquisition_channel, acquisition_country, acquisition_region,
  acquisition_main_market, acquisition_continent);
