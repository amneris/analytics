-- Generic table for Partner Performance. Network specific views depend on this table.
-- The date range is limited so that it fits to a ChartIO stored table, 6 months.
CREATE TABLE reporting.partner_performance_all (

  date_of_activity DATE           NOT NULL  ENCODE ZSTD,
  partner          VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  partner_type     VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  partner_category VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  source_id        SMALLINT       NOT NULL  ENCODE ZSTD,
  network          VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  channel          VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  country          VARCHAR(100)   NOT NULL  ENCODE ZSTD,
  cgroup           VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  region           VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  continent        VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  main_market      VARCHAR(75)    NOT NULL  ENCODE ZSTD,
  investment       DECIMAL(14, 4)           ENCODE ZSTD,
  acquisition      INT                      ENCODE ZSTD,
  installs         INT                      ENCODE ZSTD,
  conversions      INT                      ENCODE ZSTD,
  revenue          DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_1d       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_3d       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_7d       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_15d      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_30d      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_90d      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_180d     DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_360d     DECIMAL(14, 4)           ENCODE ZSTD,
  conversions_1d   INT                      ENCODE ZSTD,
  conversions_3d   INT                      ENCODE ZSTD,
  conversions_7d   INT                      ENCODE ZSTD,
  conversions_15d  INT                      ENCODE ZSTD,
  conversions_30d  INT                      ENCODE ZSTD,
  conversions_90d  INT                      ENCODE ZSTD,
  conversions_180d INT                      ENCODE ZSTD,
  conversions_360d INT                      ENCODE ZSTD
)
  DISTKEY (date_of_activity)
  INTERLEAVED SORTKEY (network, date_of_activity, channel, country, partner_type, partner_category, partner, source_id);
