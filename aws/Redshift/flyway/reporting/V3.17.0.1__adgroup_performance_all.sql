-- Generic table for Adgroup Performance. table_source specific views depend on this table.
-- The date range is limited so that it fits to a ChartIO stored table, 3 months.

DROP TABLE IF EXISTS reporting.adgroup_performance_all;

CREATE TABLE reporting.adgroup_performance_all
(
  date_of_activity                 DATE          NOT NULL   ENCODE ZSTD,

  campaign_id                      VARCHAR(200)  NOT NULL   ENCODE ZSTD,
  campaign_name                    VARCHAR(300)  NOT NULL   ENCODE ZSTD,
  adgroup_id                       VARCHAR(200)  NOT NULL   ENCODE ZSTD,
  adgroup_name                     VARCHAR(300)  NOT NULL   ENCODE ZSTD,
  ad_source                        VARCHAR(50)   NOT NULL   ENCODE ZSTD,
  table_source                     VARCHAR(50)   NOT NULL   ENCODE ZSTD,

  partner_id                       BIGINT        NOT NULL   ENCODE ZSTD,
  partner_name                     VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  partner_group                    VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  partner_type                     VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  partner_category                 VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  -- No source_id for adgroup performance (it's obsolete)
  -- No network for adgroup performance (use ad_source - on campaign level - instead)
  channel                          VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  country                          VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  cgroup                           VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  region                           VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  continent                        VARCHAR(100)  NOT NULL   ENCODE ZSTD,
  main_market                      VARCHAR(100)  NOT NULL   ENCODE ZSTD,

  impressions                      BIGINT                   ENCODE ZSTD,
  clicks                           INT                      ENCODE ZSTD,
  investment                       DECIMAL(14, 4)           ENCODE ZSTD,
  acquisition                      INT                      ENCODE ZSTD,
  installs                         INT                      ENCODE ZSTD,

  revenue_total                    DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_1d                       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_3d                       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_7d                       DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_15d                      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_30d                      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_90d                      DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_180d                     DECIMAL(14, 4)           ENCODE ZSTD,
  revenue_360d                     DECIMAL(14, 4)           ENCODE ZSTD,

  conversions_total                INT                      ENCODE ZSTD,
  conversions_1d                   INT                      ENCODE ZSTD,
  conversions_3d                   INT                      ENCODE ZSTD,
  conversions_7d                   INT                      ENCODE ZSTD,
  conversions_15d                  INT                      ENCODE ZSTD,
  conversions_30d                  INT                      ENCODE ZSTD,
  conversions_90d                  INT                      ENCODE ZSTD,
  conversions_180d                 INT                      ENCODE ZSTD,
  conversions_360d                 INT                      ENCODE ZSTD
)
BACKUP NO
DISTKEY (date_of_activity);
