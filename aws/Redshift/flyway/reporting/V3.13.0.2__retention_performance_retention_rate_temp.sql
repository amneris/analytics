-- Table for calculating Retention Rate.
-- Data should be aggregated to monthly level for more reliable results.
CREATE TABLE reporting.retention_performance_retention_rate_temp (
  date_of_payment   DATE         NOT NULL  ENCODE ZSTD,
  product           VARCHAR(5)   NOT NULL  ENCODE ZSTD,
  country           VARCHAR(100) NOT NULL  ENCODE ZSTD,
  cgroup            VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  region            VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  continent         VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  main_market       VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  channel           VARCHAR(75)  NOT NULL  ENCODE ZSTD,
  sale              INT                    ENCODE ZSTD,
  sale_no_change    INT                    ENCODE ZSTD,
  sale_upgrade      INT                    ENCODE ZSTD,
  sale_downgrade    INT                    ENCODE ZSTD,
  sale_expremium    INT                    ENCODE ZSTD,
  previous_sale     INT                    ENCODE ZSTD,
  revenue           DECIMAL(14, 4)         ENCODE ZSTD,
  revenue_no_change DECIMAL(14, 4)         ENCODE ZSTD,
  revenue_upgrade   DECIMAL(14, 4)         ENCODE ZSTD,
  revenue_downgrade DECIMAL(14, 4)         ENCODE ZSTD,
  revenue_expremium DECIMAL(14, 4)         ENCODE ZSTD,
  previous_revenue  DECIMAL(14, 4)         ENCODE ZSTD
)
  DISTKEY (date_of_payment)
  INTERLEAVED SORTKEY (date_of_payment, channel, product, country, region, main_market);
