CREATE TABLE staging_payments.consolidated_payments (
  id                             VARCHAR(15)    NOT NULL       ENCODE ZSTD,
  userId                         INT            NOT NULL       ENCODE ZSTD,
  idCountry                      INT            NOT NULL       ENCODE ZSTD,
  idPeriodPay                    INT            NOT NULL       ENCODE ZSTD,
  idUserCreditForm               INT                           ENCODE ZSTD,
  payment_gateway                INT            NOT NULL       ENCODE ZSTD,
  status                         INT            NOT NULL       ENCODE ZSTD,
  dateEndTransaction             TIMESTAMP                     ENCODE ZSTD,
  dateToPay                      TIMESTAMP                     ENCODE ZSTD,
  xRateToEUR                     DECIMAL(10, 5) NOT NULL       ENCODE ZSTD,
  currencyTrans                  VARCHAR(3)     NOT NULL       ENCODE ZSTD,
  amountOriginal                 DECIMAL(12, 2)                ENCODE ZSTD,
  amountDiscount                 DECIMAL(12, 2)                ENCODE ZSTD,
  amountPrice                    DECIMAL(12, 2) NOT NULL       ENCODE ZSTD,
  autoId                         INT            NOT NULL       ENCODE ZSTD,
  isRecurring                    INT            NOT NULL       ENCODE ZSTD,
  taxRateValue                   DECIMAL(12, 2)                ENCODE ZSTD,
  isExtend                       INT                           ENCODE ZSTD,
  PRIMARY KEY (id)
)
  DISTKEY (userId)
  INTERLEAVED SORTKEY (userId, dateEndTransaction);
