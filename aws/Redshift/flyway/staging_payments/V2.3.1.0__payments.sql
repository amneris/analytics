BEGIN TRANSACTION;

TRUNCATE TABLE staging_payments.payments;

INSERT INTO staging_payments.payments
  SELECT *
  FROM staging_payments.consolidated_payments;

END TRANSACTION;