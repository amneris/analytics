-- Load static payments data
TRUNCATE TABLE staging_payments.pay_suppliers;

COPY staging_payments.pay_suppliers
FROM '${S3_data_bucket}/static/staging_payments/'
IAM_ROLE 'arn:aws:iam::${aws_account_id}:role/analytics-redshift-loader-role'

DELIMITER '\t'
IGNOREHEADER AS 1
REMOVEQUOTES;

