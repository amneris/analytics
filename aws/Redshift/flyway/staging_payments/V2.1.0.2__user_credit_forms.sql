CREATE TABLE staging_payments.user_credit_forms (
  userId INT NOT NULL  ENCODE delta32k,
  id     INT NOT NULL  ENCODE ZSTD,
  kind   INT           ENCODE ZSTD,
  PRIMARY KEY (id)
)
  DISTSTYLE EVEN
  SORTKEY (userId);
