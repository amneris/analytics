GRANT USAGE                ON SCHEMA staging_payments                        TO GROUP redash_group;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_payments                        TO GROUP redash_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA staging_payments GRANT SELECT ON TABLES TO GROUP redash_group;
REVOKE CREATE              ON SCHEMA staging_payments                      FROM GROUP redash_group;

GRANT USAGE                ON SCHEMA staging_payments                        TO GROUP chartio_group;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_payments                        TO GROUP chartio_group;
ALTER DEFAULT PRIVILEGES   IN SCHEMA staging_payments GRANT SELECT ON TABLES TO GROUP chartio_group;
REVOKE CREATE              ON SCHEMA staging_payments                      FROM GROUP chartio_group;

GRANT ALL                  ON SCHEMA staging_payments                        TO GROUP service_group;
