CREATE TABLE staging_payments.pay_suppliers (
  id      INT         NOT NULL    ENCODE RAW,
  name    VARCHAR(45) NOT NULL    ENCODE RAW,
  invoice INT         NOT NULL    ENCODE RAW,
  PRIMARY KEY (id)
)
  DISTSTYLE ALL
  SORTKEY (id);
