GRANT USAGE ON SCHEMA                staging_payments                        TO analyticsuser;
GRANT SELECT ON ALL TABLES IN SCHEMA staging_payments                        TO analyticsuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA   staging_payments GRANT SELECT ON TABLES TO analyticsuser;
