
# Analytics - User Attribution

## Reporting Company Needs

1. CROSS COMPANY
   - Attribution Dimensions
     - Channel, Country and Network
   - Data Split
     - SEM, SMM, ASO,...
     - Google, Facebook, Bing,...
     
2. MARKETING DETAILED
   - Attribution Dimensions
     - Campaign and Adgroup (+ Country, Channel, ...)
   - External Data Sources
     - Google
     - Facebook

3. MARKETING OTHERS
   - Attribution Dimensions
     - Partner_id (+ Country, Channel, ...)


## User Information by Data Source


|  MARKETING SOURCE | Partner | Channel | Country | ## | CID | AID | Network | Country_ISO | Spend_ID | Spend_Name |
| ------ | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| FB Mvl | NO | YES | YES | ## | YES | YES | YES | YES | YES | YES |
| UAC Mvl |  NO | YES | YES | ## | YES | YES | YES | YES | YES | YES |
| Organic Mvl | YES | YES | YES | ## | --- | --- | YES | YES | --- | --- |
| Other Mvl | YES | YES | YES | ## | NO (Partner_id) | NO (Source_id) | NO (Partner_name) | YES | NO | YES |
| Google Web | YES | YES | YES | ## | YES | YES | YES | YES | YES | YES |
| Bing Web | YES | YES | YES | ## | YES | YES | YES | YES | NO | YES |
| Other Web | YES | YES | YES | ## | NO | NO | NO | NO | NO | YES |
| FB Web | YES | YES | YES | ## | YES | YES | YES | YES | YES | YES |
| Organic Web | YES | YES | YES | ## | --- | --- | NO | NO | --- | --- | 


## events.user_registered (Maestro de usuarios)

### Special Cases
**Facebook Installs**

For all the attribution of Facebook campaigns, we can get three different network (Facebook Installs, 
Instagram Installs and Off-Facebook Installs), we decide to unify this parameters to Facebook Installs.

### Specifications

Our two data sources for attribution will be: 
 - staging_users.users 
   - Attribution key: **Partner_id, Channel_id, Country_id**
   - If Partner_id has no country associated, we take it from the ip of the user. Channel has a similar logic.
 - events.user_signed_up
   - Attribution key: **Campaign_id, Adgroup_id**
   - Extra dimensions: *Network, Creative*

In order to populate correctly all the acquisition information, for each user we will inform all the *Attribution Key* 
from each source table.

Depends on the focus of the report, the grouping of the metrics will be **Partner+Channel+Country** or **Campaign+Adgroup**