#!/usr/bin/env bash

psql -h ${ENV_ANALYTICS_DEPLOY_CLUSTER} -p 5439 -U abaadmin -d analyticsdb \
-c "CREATE DATABASE analyticsdwh WITH OWNER abaadmin CONNECTION LIMIT UNLIMITED;"