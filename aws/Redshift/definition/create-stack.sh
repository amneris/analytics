#!/bin/bash
set -e

# ensure param.json exists
if [ ! -f parameters.json ]; then
    cat <<EOUSAGE
parameters.json file should exist.
EOUSAGE
    exit 1
fi

find_param() {
  local keyName="$1"
  cat parameters.json | jq -r --arg keyName $keyName \
    '.[] | select(.ParameterKey == $keyName) | .ParameterValue'
}

BUCKET_NAME=$(find_param S3Bucket)
KEY_NAME=$(find_param S3Key)

#aws s3 mb s3://${BUCKET_NAME} --region eu-west-1

# upload archive to S3
#aws s3 cp ./metabase-aws-eb.zip s3://${BUCKET_NAME}/${KEY_NAME}

aws cloudformation create-stack --stack-name analytics-redshift-cluster \
  --region eu-west-1 \
  --template-body file://./analytics.template \
  --parameters file://./parameters.json
