#!/usr/bin/env bash

node ${ENV_ANALYTICS_DEPLOY_PATH}/aws/ConfigurationService/setup/setup.js ${ENV_ANALYTICS_DEPLOY_PATH}/aws/APIQueryService/config.json
aws lambda invoke --function-name analytics-APIQueryServiceRuleFactory --payload '{"state":"ENABLED"}' result.txt
