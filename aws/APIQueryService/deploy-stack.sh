#!/usr/bin/env bash


aws s3 cp ${ENV_ANALYTICS_DEPLOY_PATH}/aws/APIQueryService/definition s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/APIQueryService/RuleFactory/definition --recursive

aws cloudformation create-stack --stack-name analytics-APIQueryService \
  --region eu-west-1 --template-url https://s3-eu-west-1.amazonaws.com/infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/APIQueryService/RuleFactory/definition/templates/APIQueryService.template
