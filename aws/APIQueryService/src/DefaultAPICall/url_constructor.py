from transform import get_date

"""
In the UrlConstructor, we have included all the functions that we will need in order to get the final URL call.
"""


def url_builder(rule_elements):
    """
    First, we get the url from the parameters of the configuration.

    Then, in case that those parameters we have parameters to include to this call, we insert it.
    At the end, we return this url.
    """
    url = rule_elements["url"]

    if "urlparams" in rule_elements:
        params = "&".join(
            ["=".join((param["name"], get_param_value(param, rule_elements))) for param in rule_elements["urlparams"]])
        url = "?".join((url, params))

    return url


def get_param_value(param, elements):
    """
    In the previous function url_builder, we call this function, in order distict value or variable inside the url parameters.

    If the parameter has a value, we return this number/string. If the parameter contains a variable, it means, that we need to
    compute its value. (Date Range)
    """

    result = None

    if "value" in param:
        result = param["value"]
    else:
        result = get_date(get_variable_value(param["variable"], elements["variables"]))

    return result


def get_variable_value(id_var, elements):
    """
    As we need the value of the variable, it's necessary to iterate in the params file, to get those values
    """
    for element in elements:
        if element["name"] == id_var:
            return element["value"]

    return None
