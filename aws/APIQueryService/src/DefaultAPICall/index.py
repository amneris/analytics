
import helpers
from url_constructor import url_builder
import AWS
import time
from urllib.error import HTTPError
import urllib.response as resp

import logging
import urllib.request as request

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class ExceededRetries(Exception):
    pass


def lambda_handler(event, context):
    """Calling the function get_config with the name of the service whose configuration we want. In this case
    APIQueryService."""
    conf = AWS.get_config('APIQueryService')

    # Get the id of the rule from the event
    rule_id = event["id"]

    # Get the parameters for the construction of the rule
    rule_elements = helpers.get_rule_elements(rule_id, conf)

    # Constract the url for the HTTP call
    url = url_builder(rule_elements)

    # Construct of the call
    req = request.Request(url=url)

    # In case that the call needs a header to connect with the client, we insert it
    if "headers" in rule_elements:
        req.headers = dict([(header["name"], header["value"]) for header in rule_elements["headers"]])

    # We include an error handler in case the client webpage doesn't respond
    logging.info('Trying to connect to: {0}'.format(url))
    for attempts in range(5):
        logging.info('Attempt number {0}!'.format(attempts+1))
        logging.info('URL_Request: {0}'.format(req.__dict__))
        try:
            response = request.urlopen(req).read()
            logging.info(response)
            break
        except HTTPError as e:
            logging.error(e)
            logging.error('Full_url_failed: {0}'.format(req.full_url))
            logging.error('Headers: {0}'.format(e.hdrs))
            logging.error('FP: {0}'.format(e.fp.read()))
            time.sleep(30)
    else:
        # Couldn't fetch data even after several tries, raise the latest ClientError
        raise ExceededRetries('Fail to connect to the API {0}'.format(rule_id))

    AWS.put_object_S3(response, rule_elements["s3key"])
    return "Success"

