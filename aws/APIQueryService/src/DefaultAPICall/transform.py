from datetime import datetime, timedelta
import ast

def get_date(value):
    """
    With the value that the previous function gives (a number), we compute the day diff comparing with today
    """
    day_t = datetime.now() - timedelta(days=ast.literal_eval(value))
    return day_t.strftime('%Y-%m-%d')
