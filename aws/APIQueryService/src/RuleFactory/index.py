import logging
import json
import datetime
import AWS

from botocore.exceptions import ClientError

logger = logging.getLogger()
logger.setLevel(logging.INFO)

config = None


def lambda_handler(event, context):
    """
    Replace the existing CloudWatch rules with the configuration from DynamoDB.

    Fetch the configuration from DynamoDB. Delete all current rules. Add new rules one by one. Rules are created either
    as ENABLED or DISABLED depending on event['state'].
    """
    global config
    config = AWS.get_config('APIQueryService')

    functions_list = list(set([_get_function_name(rule, config) for rule in config['rules']]))

    delete_current_rules(functions_list)

    for rule in config['rules']:
        add_rule(name=rule["id"],
                 schedule=rule["schedule"],
                 function_name=_get_function_name(rule, config),
                 state=event.get('state'))

    return "Success"


def delete_current_rules(functions_list):
    """
    Delete all rules that have the API Query Lambda as target.

    First delete all permissions related to the API Query Lambda. Then delete all rules in CloudWatch that have the API
    Query Lambda as the target.
    """
    for function_name in functions_list:

        permissions = _get_current_permissions(function_name)

        for permission in permissions:
            _delete_permission(permission, function_name)

        rules = _get_current_rules(function_name)

        for rule in rules:
            _delete_rule(rule, function_name)


def _get_function_name(rule, config):
    """Return the Lambda function name such as analytics-APIQueryServiceAPICall."""

    if "function_name" in rule:
        function_name = rule["function_name"]
    else:
        function_name = config["DefaultAPICallFunctionName"]

    return function_name


def _get_function_arn(function_name):
    """Return the Lambda function ARN."""
    return 'arn:aws:lambda:eu-west-1:{0}:function:{1}'.format(config["AccountId"], function_name)


def add_rule(name, schedule, function_name, state):
    """
    Add a rule to CloudWatch.

    Add the CloudWatch rule as disabled, and then configure a Lambda target function for it. Give the rule permissions
    to invoke the API Query Lambda function. Finally enable the rule if so instructed.
    """
    unique_name = _get_unique_name(name)

    if state == 'ENABLED':
        logging.info('Adding and enabling the rule {0} ...'.format(unique_name))
    else:
        logging.info('Adding the rule {0} ...'.format(unique_name))

    schedule = 'rate({0})'.format(schedule)
    rule = AWS.CLOUDWATCH.put_rule(Name=unique_name, ScheduleExpression=schedule, State='DISABLED')

    AWS.CLOUDWATCH.put_targets(Rule=unique_name,
                           Targets=[{'Id': function_name,
                                     'Arn': _get_function_arn(function_name),
                                     'Input': json.dumps({'id': name})}]
                           )

    logging.info('Adding the permission {0} ...'.format(_get_statement_id(unique_name)))

    AWS.LAMBDA.add_permission(
        FunctionName=function_name,
        StatementId=_get_statement_id(unique_name),
        Action='lambda:InvokeFunction',
        Principal='events.amazonaws.com',
        SourceArn=rule['RuleArn']
    )

    if state == 'ENABLED':
        AWS.CLOUDWATCH.enable_rule(Name=unique_name)


def _get_unique_name(name):
    """Return name with datetime suffix."""
    return name + '-' + datetime.datetime.now().strftime("%Y%m%d%H%M%S")


def _get_current_permissions(function_name):
    """
    Return current permissions related to the API Query Lambda function.

    If there are no permissions, then get_policy() throws an exception. For us, that simply means there's nothing to
    delete.

    :return: list of statement_id's of the permissions, an empty list if there are no permissions
    """
    permissions = []

    try:
        permissions = json.loads(AWS.LAMBDA.get_policy(FunctionName=function_name)['Policy'])['Statement']
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            logging.info('No current permissions exist to invoke API Query Lambda. Nothing to do.')
        else:
            logging.warning(e)
            raise e

    return [permission['Sid'] for permission in permissions]


def _get_current_rules(function_name):
    """
    Return current CloudWatch rules.

    :return: list of current rule names
    """
    return AWS.CLOUDWATCH.list_rule_names_by_target(TargetArn=_get_function_arn(function_name))['RuleNames']


def _delete_permission(statement_id, function_name):
    """Delete a single Lambda invoke permission."""
    logging.info('Deleting the permission {0} ...'.format(statement_id))

    AWS.LAMBDA.remove_permission(FunctionName=function_name, StatementId=statement_id)


def _delete_rule(name,function_name):
    """
    Delete a single CloudWatch rule.

    First remove the target function from the rule. Then remove the rule itself.
    """
    logging.info('Deleting the rule {0} ...'.format(name))

    AWS.CLOUDWATCH.disable_rule(Name=name)
    AWS.CLOUDWATCH.remove_targets(Rule=name, Ids=[function_name])
    AWS.CLOUDWATCH.delete_rule(Name=name)


def _get_statement_id(name):
    """Return a statement id that AWS qualifies (no hyphens allowed)."""
    return name.replace('-', '')
