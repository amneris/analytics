import unittest
import index
from unittest.mock import MagicMock
from botocore.exceptions import ClientError
import logging


class AnyStringWith(str):
    def __eq__(self, other):
        return self in other


class TestRuleFactory(unittest.TestCase):
    def setUp(self):
        # index._get_current_permissions is sometimes mocked sometimes not - save the original to be able to revert back
        self._get_current_permissions = index._get_current_permissions
        # Fetch config before running the test
        index.config = index.get_config('APIQueryService')
        # Hide info messages
        logging.disable(logging.INFO)

    def tearDown(self):
        # Revert back to the original index._get_current_permissions
        index._get_current_permissions = self._get_current_permissions

    def test_delete_permission(self):
        # Mock AWS
        index.LAMBDA.remove_permission = MagicMock()

        index._delete_permission('testpermission')

        index.LAMBDA.remove_permission.assert_called_once_with(FunctionName=index._get_function_name(),
                                                               StatementId='testpermission')

    def test_delete_rule(self):
        # Mock AWS
        index.CLOUDWATCH.disable_rule = MagicMock()
        index.CLOUDWATCH.remove_targets = MagicMock()
        index.CLOUDWATCH.delete_rule = MagicMock()

        index._delete_rule('testrule')

        index.CLOUDWATCH.disable_rule.assert_called_once_with(Name='testrule')
        index.CLOUDWATCH.remove_targets.assert_called_once_with(Rule='testrule', Ids=[index._get_function_name()])
        index.CLOUDWATCH.delete_rule.assert_called_once_with(Name='testrule')

    def test_get_current_permissions(self):
        # Mock AWS
        index.LAMBDA.get_policy = MagicMock()
        index.LAMBDA.get_policy.return_value = {
            'Policy': '{"Statement": [{"Sid": "testpermission3"},{"Sid": "testpermission4"}]}'}

        result = index._get_current_permissions()

        self.assertEqual(result, ['testpermission3', 'testpermission4'])

    def test_get_current_permissions_when_no_permissions(self):
        """
        Test _get_current_permissions handles ResourceNotFoundException.

        If there are no permissions then Lambda's get_policy() function throws an exception. We catch that and return
        an empty list.
        """
        # Mock AWS
        index.LAMBDA.get_policy = MagicMock()
        index.LAMBDA.get_policy.side_effect = ClientError(
            error_response={'Error': {'Code': 'ResourceNotFoundException'}}, operation_name=None)

        result = index._get_current_permissions()

        self.assertEqual(result, [])

    def test_delete_current_rules(self):
        # Mock test permissions and test rules
        index._get_current_permissions = MagicMock()
        index._get_current_permissions.return_value = ['testpermission1', 'testpermission2']
        index._get_current_rules = MagicMock()
        index._get_current_rules.return_value = ['testrule1', 'testrule2']

        # Mock AWS
        index.CLOUDWATCH.disable_rule = MagicMock()
        index.LAMBDA.remove_permission = MagicMock()
        index.CLOUDWATCH.remove_targets = MagicMock()
        index.CLOUDWATCH.delete_rule = MagicMock()

        index.delete_current_rules()

        # Checks
        self.assertEqual(index.CLOUDWATCH.disable_rule.call_count, 2)
        index.CLOUDWATCH.disable_rule.assert_any_call(Name='testrule1')
        index.CLOUDWATCH.disable_rule.assert_any_call(Name='testrule2')

        self.assertEqual(index.LAMBDA.remove_permission.call_count, 2)
        index.LAMBDA.remove_permission.assert_any_call(FunctionName=index._get_function_name(),
                                                       StatementId='testpermission1')
        index.LAMBDA.remove_permission.assert_any_call(FunctionName=index._get_function_name(),
                                                       StatementId='testpermission2')

        self.assertEqual(index.CLOUDWATCH.remove_targets.call_count, 2)
        index.CLOUDWATCH.remove_targets.assert_any_call(Rule='testrule1', Ids=[index._get_function_name()])
        index.CLOUDWATCH.remove_targets.assert_any_call(Rule='testrule2', Ids=[index._get_function_name()])

        self.assertEqual(index.CLOUDWATCH.delete_rule.call_count, 2)
        index.CLOUDWATCH.delete_rule.assert_any_call(Name='testrule1')
        index.CLOUDWATCH.delete_rule.assert_any_call(Name='testrule2')

    def test_add_rule_disabled(self):
        # Mock AWS
        index.CLOUDWATCH.put_rule = MagicMock()
        index.CLOUDWATCH.put_targets = MagicMock()
        index.LAMBDA.add_permission = MagicMock()
        index.CLOUDWATCH.enable_rule = MagicMock()

        index.add_rule('testrule', 'testschedule', 'DISABLED')

        index.CLOUDWATCH.put_rule.assert_called_once_with(Name=AnyStringWith('testrule'),
                                                          ScheduleExpression='rate({0})'.format('testschedule'),
                                                          State='DISABLED')
        index.CLOUDWATCH.put_targets.assert_called_once()
        index.LAMBDA.add_permission.assert_called_once()
        index.CLOUDWATCH.enable_rule.assert_not_called()

    def test_add_rule_enabled(self):
        # Mock AWS
        index.CLOUDWATCH.put_rule = MagicMock()
        index.CLOUDWATCH.put_targets = MagicMock()
        index.LAMBDA.add_permission = MagicMock()
        index.CLOUDWATCH.enable_rule = MagicMock()

        index.add_rule('testrule', 'testschedule', 'ENABLED')

        index.CLOUDWATCH.put_rule.assert_called_once_with(Name=AnyStringWith('testrule'),
                                                          ScheduleExpression='rate({0})'.format('testschedule'),
                                                          State='DISABLED')
        index.CLOUDWATCH.put_targets.assert_called_once()
        index.LAMBDA.add_permission.assert_called_once()
        index.CLOUDWATCH.enable_rule.assert_called_once()

if __name__ == '__main__':
    unittest.main()
