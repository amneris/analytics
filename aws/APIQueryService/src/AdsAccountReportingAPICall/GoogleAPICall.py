import logging
from googleads import adwords
import AWS
import yaml

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class GoogleAPICall:

    def get_report(self, event):
        conf = AWS.get_config('{0}Authorization'.format(event['class_type']))
        yaml_doc = dict(adwords=conf["tokens"])
        yaml_doc['adwords']['client_customer_id'] = event['account_id']

        report = self.call_adwords_report(yaml_doc, event['query'])
        AWS.put_object_S3(report, '{0}/{1}.csv'.format(event['s3key'], event['account_id']))

        return "Success"

    def call_adwords_report(self, yaml_doc, query):
        client = adwords.AdWordsClient.LoadFromString(yaml_doc=yaml.dump(yaml_doc))
        report_downloader = client.GetReportDownloader(version='v201708')

        return report_downloader.DownloadReportAsStringWithAwql(
            query, 'CSV',
            skip_report_header=True,
            skip_column_header=True,
            skip_report_summary=True,
            include_zero_impressions=False)
