import logging
import json
from facebookads.adobjects.adaccount import AdAccount
from facebookads.api import FacebookAdsApi

import AWS

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class FacebookAPICall:

    def get_report(self, event):
        conf = AWS.get_config('{0}Authorization'.format(event['class_type']))
        FacebookAdsApi.init(access_token=conf["tokens"]["access_token"])

        api_result = AdAccount(event['account_id']).get_insights(fields=event['fields'], params=event['params'])

        result = ""
        for item in list(api_result):
            result += json.dumps(item.__dict__['_data'])

        AWS.put_object_S3(result, '{0}/{1}.json'.format(event['s3key'], event['account_id']))

        return "Success"
