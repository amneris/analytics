import logging
import helpers


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    instance = helpers.get_loader_class(event['class_type'])
    instance.get_report(event)

    return "Success"
