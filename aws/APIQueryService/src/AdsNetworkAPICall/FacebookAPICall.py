import logging
import json
import AWS

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class FacebookAPICall:

    def get_account_id(self, conf, rule_settings):
        rules_dict = rule_settings
        rules_dict['fields'] = list(rule_settings['fields'])
        rules_dict['params'] = rule_settings["params"]
        rules_dict['params']['filtering'][0]['value'] = list(rules_dict['params']['filtering'][0]['value'])

        for account in rule_settings["accounts"]:
            rules_dict['account_id'] = account["id"]

            lambda_name = 'arn:aws:lambda:eu-west-1:{0}:function:{1}'.format(
                conf["AccountId"], 'analytics-APIQueryServiceAdsAccountReportingAPICall'
            )

            AWS.LAMBDA.invoke(
                InvocationType='Event',
                FunctionName=lambda_name,
                LogType='Tail',
                Payload=json.dumps(rules_dict)
            )

            logging.info('AdsAccountReportingAPICall invoked for Facebook account : {0}'.format(account["id"]))

        return "Success"
