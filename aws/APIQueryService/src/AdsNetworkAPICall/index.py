import logging
import AWS
import helpers


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):

    service_config = AWS.get_config('APIQueryService')
    rule_settings = helpers.get_rule_elements(event['id'], service_config)
    class_name = rule_settings["class_type"]

    # Call to DynamoDB to get the API configuration
    conf = AWS.get_config('{0}Authorization'.format(class_name))

    instance = helpers.get_loader_class(class_name)
    instance.get_account_id(conf, rule_settings)

    return "Success"
