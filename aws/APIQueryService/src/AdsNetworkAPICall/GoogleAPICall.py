import logging
from googleads import adwords
import AWS
import yaml
import json

logger = logging.getLogger()
logger.setLevel(logging.INFO)

PAGE_SIZE = 1000


class GoogleAPICall:

    def get_account_id(self, conf, rule_settings):

        # Select MCC ids to get the client_customer_ids for each account
        yaml_doc = dict(adwords=conf["tokens"])

        # Loop to get all the MCC accounts
        for mcc_id in [mcc_account["id"] for mcc_account in rule_settings["mcc"]]:

            # complete the yaml credential document with the mcc account id
            yaml_doc['adwords']['client_customer_id'] = mcc_id

            # call AdwordsClient with credentials
            client = adwords.AdWordsClient.LoadFromString(yaml_doc=yaml.dump(yaml_doc))
            managed_customer_service = client.GetService('ManagedCustomerService', version='v201708')

            # report to download all the acount ids from the current MCC
            client_id_selector = {
                'fields': ['CustomerId'],
                'paging': {'startIndex': '0', 'numberResults': str(PAGE_SIZE)}
            }

            page = managed_customer_service.get(client_id_selector)

            lambda_params = rule_settings

            # for each id we want to get the report
            for number_id in [str(link.clientCustomerId) for link in page.links]:

                lambda_params['account_id'] = "{0}-{1}-{2}".format(number_id[0:3], number_id[3:6], number_id[6:13])

                lambda_name = 'arn:aws:lambda:eu-west-1:{0}:function:{1}'.format(
                    conf["AccountId"],
                    'analytics-APIQueryServiceAdsAccountReportingAPICall'
                )

                AWS.LAMBDA.invoke(InvocationType='Event',
                                  FunctionName=lambda_name,
                                  LogType='Tail',
                                  Payload=json.dumps(lambda_params))

                logging.info('AdsAccountReportingAPICall invoked for AdWords account : {0}'.format(lambda_params['account_id']))

        return "Success"
