#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/APIQueryService/build
rm -f APICall.zip
rm -f RuleFactory.zip
rm -f DefaultAPICall.zip
rm -f AdsNetworkAPICall.zip
rm -f AdsAccountReportingAPICall.zip

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/LambdaZipMaker
sh build.sh RuleFactory
sh build.sh DefaultAPICall
sh build.sh AdsNetworkAPICall
sh build.sh AdsAccountReportingAPICall

aws s3 cp ${ENV_ANALYTICS_DEPLOY_PATH}/aws/APIQueryService/build \
s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/APIQueryService/build --recursive


aws lambda update-function-code --function-name analytics-APIQueryServiceRuleFactory \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key APIQueryService/build/RuleFactory.zip

aws lambda update-function-code --function-name analytics-APIQueryServiceDefaultAPICall \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key APIQueryService/build/DefaultAPICall.zip

aws lambda update-function-code --function-name analytics-APIQueryServiceAdsNetworkAPICall \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key APIQueryService/build/AdsNetworkAPICall.zip

aws lambda update-function-code --function-name analytics-APIQueryServiceAdsAccountReportingAPICall \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key APIQueryService/build/AdsAccountReportingAPICall.zip