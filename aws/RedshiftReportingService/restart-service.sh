#!/usr/bin/env bash

node ../ConfigurationService/setup/setup.js config.json
aws s3 cp ./sql s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/RedshiftReportingService/sql --recursive
aws lambda invoke --function-name analytics-DataPipelineFactory --payload '{"ServiceName":"RedshiftReportingService"}' result.txt
