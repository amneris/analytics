START TRANSACTION;

TRUNCATE TABLE revenue_model.base_cohorted;

INSERT INTO revenue_model.base_cohorted (

  -- Actual metrics to fill the base_metrics for revenue_model

  SELECT
    date_month,
    -- amount of first sales (actual concept)
    SUM(coalesce(first_sales, 0))                                             AS first_sales,
    -- percentage for each product of all first_sales
    1.0 * SUM(coalesce(f_sales_30, 0)) / SUM(coalesce(first_sales, 0))        AS sales_30,
    1.0 * SUM(coalesce(f_sales_180, 0)) / SUM(coalesce(first_sales, 0))       AS sales_180,
    1.0 * SUM(coalesce(f_sales_360, 0)) / SUM(coalesce(first_sales, 0))       AS sales_360,
    1.0 * SUM(coalesce(f_sales_720, 0)) / SUM(coalesce(first_sales, 0))       AS sales_720,
    -- average ticket for first sales
    SUM(coalesce(revenue_first_sales)) / SUM(coalesce(first_sales, 0))        AS avg_ticket_first,
    -- amount of next sales
    SUM(coalesce(renewals, 0))                                                AS renewals,
    -- average ticket for next sales
    coalesce(SUM(coalesce(revenue_renewals)) / SUM(coalesce(renewals, 0)), 0) AS avg_ticket_renewals
  FROM (
         SELECT
           date_trunc('month', date_of_activity) :: DATE AS date_month,
           CASE WHEN class_type = 'Sale'
             THEN 1
           WHEN class_type = 'Refund'
             THEN -1
           END                                           AS f_sales_30,
           NULL                                          AS f_sales_180,
           NULL                                          AS f_sales_360,
           NULL                                          AS f_sales_720,
           NULL                                          AS first_sales,
           NULL                                          AS revenue_first_sales,
           NULL                                          AS renewals,
           NULL                                          AS revenue_renewals
         FROM core.fct_user_lifecycle
         WHERE product_id = 30
               AND class_type IN ('Sale', 'Refund')
               AND class_subtype = 'First'
         UNION ALL
         SELECT
           date_trunc('month', date_of_activity) :: DATE AS date_month,
           NULL                                          AS f_sales_30,
           CASE WHEN class_type = 'Sale'
             THEN 1
           WHEN class_type = 'Refund'
             THEN -1
           END                                           AS f_sales_180,
           NULL                                          AS f_sales_360,
           NULL                                          AS f_sales_720,
           NULL                                          AS first_sales,
           NULL                                          AS revenue_first_sales,
           NULL                                          AS renewals,
           NULL                                          AS revenue_renewals
         FROM core.fct_user_lifecycle
         WHERE product_id = 180
               AND class_type IN ('Sale', 'Refund')
               AND class_subtype = 'First'
         UNION ALL
         SELECT
           date_trunc('month', date_of_activity) :: DATE AS date_month,
           NULL                                          AS f_sales_30,
           NULL                                          AS f_sales_180,
           CASE WHEN class_type = 'Sale'
             THEN 1
           WHEN class_type = 'Refund'
             THEN -1
           END                                           AS f_sales_360,
           NULL                                          AS f_sales_720,
           NULL                                          AS first_sales,
           NULL                                          AS revenue_first_sales,
           NULL                                          AS renewals,
           NULL                                          AS revenue_renewals
         FROM core.fct_user_lifecycle
         WHERE product_id = 360
               AND class_type IN ('Sale', 'Refund')
               AND class_subtype = 'First'
         UNION ALL
         SELECT
           date_trunc('month', date_of_activity) :: DATE AS date_month,
           NULL                                          AS f_sales_30,
           NULL                                          AS f_sales_180,
           NULL                                          AS f_sales_360,
           CASE WHEN class_type = 'Sale'
             THEN 1
           WHEN class_type = 'Refund'
             THEN -1
           END                                           AS f_sales_720,
           NULL                                          AS first_sales,
           NULL                                          AS revenue_first_sales,
           NULL                                          AS renewals,
           NULL                                          AS revenue_renewals
         FROM core.fct_user_lifecycle
         WHERE product_id = 720
               AND class_type IN ('Sale', 'Refund')
               AND class_subtype = 'First'
         UNION ALL
         SELECT
           date_trunc('month', date_of_activity) :: DATE AS date_month,
           NULL                                          AS f_sales_30,
           NULL                                          AS f_sales_180,
           NULL                                          AS f_sales_360,
           NULL                                          AS f_sales_720,
           CASE WHEN class_type = 'Sale'
             THEN 1
           WHEN class_type = 'Refund'
             THEN -1
           END                                           AS first_sales,
           CASE WHEN class_type = 'Sale'
             THEN amount_net_eur
           WHEN class_type = 'Refund'
             THEN -amount_net_eur
           END                                           AS revenue_first_sales,
           NULL                                          AS renewals,
           NULL                                          AS revenue_renewals
         FROM core.fct_user_lifecycle
         WHERE product_id IN (30, 180, 360, 720)
               AND class_type IN ('Sale', 'Refund')
               AND class_subtype = 'First'
         UNION ALL
         SELECT
           date_trunc('month', date_of_activity) :: DATE AS date_month,
           NULL                                          AS f_sales_30,
           NULL                                          AS f_sales_180,
           NULL                                          AS f_sales_360,
           NULL                                          AS f_sales_720,
           NULL                                          AS first_sales,
           NULL                                          AS revenue_first_sales,
           CASE WHEN class_type = 'Sale'
             THEN 1
           WHEN class_type = 'Refund'
             THEN -1
           END                                           AS renewals,
           CASE WHEN class_type = 'Sale'
             THEN amount_net_eur
           WHEN class_type = 'Refund'
             THEN -amount_net_eur
           END                                           AS revenue_renewals
         FROM core.fct_user_lifecycle
         WHERE product_id IN (30, 180, 360, 720)
               AND class_type IN ('Sale', 'Refund')
               AND class_subtype = 'Next'
       ) trgt
  WHERE date_month >= '2013-01-01'
  GROUP BY date_month

);

END TRANSACTION;

ANALYZE revenue_model.base_cohorted;
