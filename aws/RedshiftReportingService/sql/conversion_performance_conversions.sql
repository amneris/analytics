-- Conversion component for Conversion Rate analysis, especially to analyze CR distribution over time
--
-- Takes last 36 months of conversions.
-- To keep the record count down the results are on cgroup level (renamed to Country in ChartIO).

START TRANSACTION;

TRUNCATE TABLE reporting.conversion_performance_conversions;

INSERT INTO reporting.conversion_performance_conversions (
  SELECT
    date_of_acquisition,
    date_of_conversion,
    product,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,
    payment_cgroup,
    payment_region,
    payment_continent,
    payment_main_market,
    payment_channel,
    days_to_conversion,
    weeks_to_conversion,
    months_to_conversion,
    days_to_pay,
    grouped_days_to_pay,
    conversions,
    revenue
  FROM reporting_logic.conversion_performance_conversions
);

END TRANSACTION;

ANALYZE reporting.conversion_performance_conversions;
