-- Acquisition component for Conversion Rate analysis, especially to analyze CR distribution over time
-- Last 36 months, on cgroup level (renamed to country in ChartIO)

START TRANSACTION;

TRUNCATE TABLE reporting.conversion_performance_acquisition;

INSERT INTO reporting.conversion_performance_acquisition (
  SELECT
    date_of_activity,
    cgroup,
    region,
    continent,
    main_market,
    channel,
    acquisition
  FROM reporting_logic.conversion_performance_acquisition
);

END TRANSACTION;

ANALYZE reporting.conversion_performance_acquisition;
