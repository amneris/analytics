-- Day level view for actual metrics, last two years only to keep the record count down

START TRANSACTION;

TRUNCATE TABLE reporting.actual_metrics_recent_revenue;

INSERT INTO reporting.actual_metrics_recent_revenue (
  SELECT
    date_of_activity,
    product,
    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,
    payment_country,
    payment_cgroup,
    payment_region,
    payment_continent,
    payment_main_market,
    payment_channel,
    sale_type,
    detailed_sale_type,
    revenue,
    revenue_gross,
    sales,
    free_trials
  FROM reporting_logic.actual_metrics_generic_revenue
  WHERE date_of_activity BETWEEN DATE_TRUNC('month', CURRENT_DATE - INTERVAL '24 months') AND CURRENT_DATE
);

END TRANSACTION;

ANALYZE reporting.actual_metrics_recent_revenue;
