-- Table for cohorted premium activity based on conversion date

START TRANSACTION;

TRUNCATE TABLE reporting.usage_premium_cohorted_activity;

INSERT INTO reporting.usage_premium_cohorted_activity (
  SELECT
    date_of_conversion,
    acquisition_partner,
    acquisition_network,
    acquisition_category,
    acquisition_type,
    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,
    activity_device,
    conversion_product,

    active_users_0W,
    active_users_1W,
    active_users_4W,
    active_users_10W,
    active_users_25W,
    active_users_51W,
    watched_videos_0W,
    watched_videos_1W,
    watched_videos_4W,
    watched_videos_10W,
    watched_videos_25W,
    watched_videos_51W,
    finished_units_0W,
    finished_units_1W,
    finished_units_4W,
    finished_units_10W,
    finished_units_25W,
    finished_units_51W

  FROM reporting_logic.usage_premium_cohorted_activity
  WHERE date_of_conversion BETWEEN DATE_TRUNC('month', CURRENT_DATE - INTERVAL '12 months') AND CURRENT_DATE
);

END TRANSACTION;

ANALYZE reporting.usage_premium_cohorted_activity;

