-- Table for calculating the MRR (Finance view)

START TRANSACTION;

TRUNCATE TABLE reporting.finance_mrr;

INSERT INTO reporting.finance_mrr (
  SELECT
    month_of_payment,
    month_of_conversion,
    month_of_mrr,
    months_after_conversion,
    product,
    sale_type,
    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,
    payment_country,
    payment_cgroup,
    payment_region,
    payment_continent,
    payment_main_market,
    payment_channel,
    active_subscriptions,
    net_mrr,
    gross_mrr
  FROM reporting_logic.finance_mrr
);

END TRANSACTION;

ANALYZE reporting.finance_mrr;
