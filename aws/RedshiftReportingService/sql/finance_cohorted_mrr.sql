-- Table for calculating the Cohorted MRR (Finance view)

START TRANSACTION;

TRUNCATE TABLE reporting.finance_cohorted_mrr;

INSERT INTO reporting.finance_cohorted_mrr (
  SELECT
    month_of_acquisition,
    month_of_mrr,
    country,
    cgroup,
    region,
    main_market,
    continent,
    channel,
    active_subscriptions,
    net_mrr,
    gross_mrr,
    investment,
    net_mrr_cumulative,
    gross_mrr_cumulative,
    investment_cumulative

  FROM reporting_logic.finance_cohorted_mrr
);

END TRANSACTION;

ANALYZE reporting.finance_cohorted_mrr;
