-- Table for calculating Retention Rate.
-- Data should be aggregated to monthly level for more reliable results.

START TRANSACTION;

TRUNCATE TABLE reporting.retention_performance_retention_rate;

INSERT INTO reporting.retention_performance_retention_rate (
  SELECT
    date_of_payment,
    product,
    country,
    cgroup,
    region,
    continent,
    main_market,
    channel,
    sale,
    sale_no_change,
    sale_upgrade,
    sale_downgrade,
    sale_expremium,
    previous_sale,
    revenue,
    revenue_no_change,
    revenue_upgrade,
    revenue_downgrade,
    revenue_expremium,
    previous_revenue
  FROM reporting_logic.retention_performance_retention_rate
);

END TRANSACTION;

ANALYZE reporting.retention_performance_retention_rate;
