START TRANSACTION;

TRUNCATE TABLE revenue_model.base_actual;

INSERT INTO revenue_model.base_actual (
  -- Cohorted metrics to fill the base_metrics for revenue_model
  SELECT
    DATE_TRUNC('month', date_of_activity) :: DATE            AS date_month,
    SUM(investment)                                          AS investment,
    1.0 * SUM(conversions_30d) / NULLIF(SUM(acquisition), 0) AS cr_30d,
    1.0 * SUM(investment) / NULLIF(SUM(acquisition), 0)      AS cpl
  FROM reporting_logic.campaign_cohorted_performance
  WHERE date_of_activity >= '2013-01-01'
  GROUP BY 1
);

END TRANSACTION;

ANALYZE revenue_model.base_actual;
