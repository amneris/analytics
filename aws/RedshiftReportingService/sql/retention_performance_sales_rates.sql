-- Table for calculating cancel rate, fail rate, refund rate, success rate, and average ticket for each

START TRANSACTION;

TRUNCATE TABLE reporting.retention_performance_sales_rates;

INSERT INTO reporting.retention_performance_sales_rates (
  SELECT
    date_of_activity,
    payment_type,
    product,
    country,
    cgroup,
    region,
    continent,
    main_market,
    channel,
    days_to_cancel,
    fail_units,
    fail_revenue,
    fail_revenue_gross,
    cancel_units,
    cancel_revenue,
    cancel_revenue_gross,
    refund_units,
    refund_revenue,
    refund_revenue_gross,
    sales_units,
    sales_revenue,
    sales_revenue_gross,
    sales_discount,
    net_units,
    net_revenue,
    net_revenue_gross,
    net_discount
  FROM reporting_logic.retention_performance_sales_rates
);

END TRANSACTION;

ANALYZE reporting.retention_performance_sales_rates;
