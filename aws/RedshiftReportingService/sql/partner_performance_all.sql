-- Generic table for Partner Performance. Network specific views depend on this table.
-- The date range is limited so that it fits to a ChartIO stored table, 6 months.

START TRANSACTION;

TRUNCATE TABLE reporting.partner_performance_all;

INSERT INTO reporting.partner_performance_all (
  SELECT
    date_of_activity,
    partner,
    partner_type,
    partner_category,
    source_id,
    network,
    channel,
    country,
    cgroup,
    region,
    continent,
    main_market,
    SUM(investment)       AS investment,
    SUM(acquisition)      AS acquisition,
    SUM(installs)         AS installs,
    SUM(conversions)      AS conversions,
    SUM(revenue)          AS revenue,
    SUM(revenue_1d)       AS revenue_1d,
    SUM(revenue_3d)       AS revenue_3d,
    SUM(revenue_7d)       AS revenue_7d,
    SUM(revenue_15d)      AS revenue_15d,
    SUM(revenue_30d)      AS revenue_30d,
    SUM(revenue_90d)      AS revenue_90d,
    SUM(revenue_180d)     AS revenue_180d,
    SUM(revenue_360d)     AS revenue_360d,
    SUM(conversions_1d)   AS conversions_1d,
    SUM(conversions_3d)   AS conversions_3d,
    SUM(conversions_7d)   AS conversions_7d,
    SUM(conversions_15d)  AS conversions_15d,
    SUM(conversions_30d)  AS conversions_30d,
    SUM(conversions_90d)  AS conversions_90d,
    SUM(conversions_180d) AS conversions_180d,
    SUM(conversions_360d) AS conversions_360d
  FROM reporting_logic.campaign_cohorted_performance
  WHERE date_of_activity BETWEEN DATE_TRUNC('month', CURRENT_DATE - INTERVAL '6 months') AND CURRENT_DATE
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
);

END TRANSACTION;

ANALYZE reporting.partner_performance_all;
