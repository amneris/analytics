-- Table for represent the weekly active users (Usage Product view)

START TRANSACTION;

TRUNCATE TABLE reporting.usage_weekly_active_users;

INSERT INTO reporting.usage_weekly_active_users (
  SELECT
    week_of_activity,
    user_type,
    country,
    cgroup,
    region,
    main_market,
    continent,
    device,
    wau,
    sessions

  FROM reporting_logic.usage_weekly_active_users
);

END TRANSACTION;

ANALYZE reporting.usage_weekly_active_users;
