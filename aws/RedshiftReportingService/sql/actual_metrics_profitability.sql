-- Actual revenue & sales, investment and margin

START TRANSACTION;

TRUNCATE TABLE reporting.actual_metrics_profitability;

INSERT INTO reporting.actual_metrics_profitability (
  SELECT
    date_of_activity,
    acquisition_country,
    acquisition_cgroup,
    acquisition_region,
    acquisition_continent,
    acquisition_main_market,
    acquisition_channel,
    revenue_total,
    investment,
    margin,
    sales_total,
    revenue_first_early,
    revenue_first_late,
    revenue_first_unknown,
    revenue_first_total,
    revenue_next,
    sales_first_early,
    sales_first_late,
    sales_first_unknown,
    sales_first_total,
    sales_next,
    free_trials_first_early,
    free_trials_first_late,
    free_trials_first_unknown,
    free_trials_first_total,
    free_trials_next
  FROM reporting_logic.actual_metrics_profitability
);

END TRANSACTION;

ANALYZE reporting.actual_metrics_profitability;
