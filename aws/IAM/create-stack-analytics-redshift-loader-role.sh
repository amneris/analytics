#!/usr/bin/env bash


aws cloudformation create-stack --stack-name analytics-redshift-loader-role \
  --region eu-west-1 \
  --template-body file://./templates/analytics-redshift-loader-role.template \
  --capabilities CAPABILITY_NAMED_IAM

