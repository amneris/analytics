#!/usr/bin/env bash


aws cloudformation create-stack --stack-name analytics-firehose-delivery-role \
  --region eu-west-1 \
  --template-body file://./templates/firehose-delivery-role.template \
  --capabilities CAPABILITY_NAMED_IAM

