#!/usr/bin/env bash


aws cloudformation create-stack --stack-name analytics-lambda-execution-role \
  --region eu-west-1 \
  --template-body file://./templates/lambda-execution-role.template \
  --capabilities CAPABILITY_NAMED_IAM

