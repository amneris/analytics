#!/usr/bin/env bash

aws redshift modify-cluster-iam-roles \
    --cluster-identifier "analytics-redshift-cluster-redshiftcluster-5ni677kqkgeq" \
    --add-iam-roles "arn:aws:iam::880456264579:role/analytics-redshift-loader-role"


