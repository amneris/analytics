#!/usr/bin/env bash

aws redshift modify-cluster-iam-roles \
    --cluster-identifier "analytics-redshift-cluster-redshiftcluster-hh5pxh4gtdwr" \
    --add-iam-roles "arn:aws:iam::065259791921:role/analytics-redshift-loader-role"


