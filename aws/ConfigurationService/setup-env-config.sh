#!/usr/bin/env bash

node setup/setup.js env-config/${ENV_ANALYTICS_DEPLOY_ENVIRONMENT}/NotificationService.json
node setup/setup.js env-config/${ENV_ANALYTICS_DEPLOY_ENVIRONMENT}/RedshiftConnection.json
node setup/setup.js env-config/${ENV_ANALYTICS_DEPLOY_ENVIRONMENT}/S3Buckets.json
node setup/setup.js env-config/${ENV_ANALYTICS_DEPLOY_ENVIRONMENT}/RDSConnection.json