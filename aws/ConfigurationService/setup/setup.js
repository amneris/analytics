
const REGION = "eu-west-1";
const DYNAMO_TABLE_NAME = "AnalyticsConfig"



var dynamodbMarshaler = require('dynamodb-marshaler');

fs = require('fs');

function setup() {


    var AWS = require('aws-sdk');
    var dynamodb = new AWS.DynamoDB({region: REGION});

    var fileName = process.argv[2];


    fs.readFile(fileName, 'utf8', function (err,data) {
        if (err) {
            return console.log('Error: '+ err);
        }

        var params = {

            Item: dynamodbMarshaler.marshalItem(JSON.parse(data)),
            TableName: DYNAMO_TABLE_NAME
        };



        dynamodb.putItem(params, function(err, data) {
            if (err) console.log(err, err.stack);
            else console.log("ok");

        });

    });
};

setup();


