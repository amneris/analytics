#!/usr/bin/env bash

aws s3 cp ./static s3://data.${ENV_ANALYTICS_DEPLOY_BUCKET}/static --recursive
