#!/usr/bin/env bash


aws cloudformation create-stack --stack-name analytics-s3-buckets \
  --region eu-west-1 \
  --template-body file://./templates/data.analytics.aba.template \
  --parameters file://./parameters/${ENV_ANALYTICS_DEPLOY_ENVIRONMENT}.json
