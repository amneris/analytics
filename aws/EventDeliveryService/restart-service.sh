#!/usr/bin/env bash

node ${ENV_ANALYTICS_DEPLOY_PATH}/aws/ConfigurationService/setup/setup.js ${ENV_ANALYTICS_DEPLOY_PATH}/aws/EventDeliveryService/config.json

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/EventDeliveryService/; sh eventDispatcher.sh
