#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/EventDeliveryService/src/Fanout
common="register lambda --function analytics-EventDeliveryServiceFanout --source-type kinesis --source analytics-KinesisBackbone --active true"

sh fanout  $common --id analytics-EventDeliveryServiceAmplitudeForwarder --destination analytics-EventDeliveryServiceAmplitudeForwarder
sh fanout  $common --id analytics-EventDeliveryServiceRedshiftForwarder  --destination analytics-EventDeliveryServiceRedshiftForwarder



