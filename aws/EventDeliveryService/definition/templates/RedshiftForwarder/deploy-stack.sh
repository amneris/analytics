#!/usr/bin/env bash

aws s3 cp ${ENV_ANALYTICS_DEPLOY_PATH}/aws/EventDeliveryService/definition/templates s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/EventDeliveryService/definition/templates --recursive
#aws s3 cp ${ENV_ANALYTICS_DEPLOY_PATH}/aws/StreamDataDeliveryService/DeliveryStreams/definition s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/StreamDataDeliveryService/DeliveryStreams/definition --recursive

aws cloudformation update-stack --stack-name analytics-StreamDataDeliveryService \
  --region eu-west-1 \
  --template-url https://s3-eu-west-1.amazonaws.com/infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/EventDeliveryService/definition/templates/RedshiftForwarder/stream_list.template \
  --parameters \
    ParameterKey=RedshiftConnection,ParameterValue=${ENV_ANALYTICS_DEPLOY_CLUSTER} \
    ParameterKey=RedshiftUserName,ParameterValue=${ENV_ANALYTICS_DEPLOY_REDSHIFT_USER} \
    ParameterKey=RedshiftPassword,ParameterValue=${ENV_ANALYTICS_DEPLOY_REDSHIFT_PWD} \
    ParameterKey=S3Environment,ParameterValue=${ENV_ANALYTICS_DEPLOY_BUCKET} \







