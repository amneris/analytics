{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "Stack with Firehose Delivery Stream definition",
  "Resources": {


    "Stream": {
      "Type": "AWS::KinesisFirehose::DeliveryStream",
      "Properties": {
        "DeliveryStreamName": { "Fn::Join" : [ "_", [ "Analytics", {"Ref" : "StreamName"}] ] },
        "RedshiftDestinationConfiguration": {
          "ClusterJDBCURL": { "Fn::Join" : [ "", [ "jdbc:redshift://", {"Ref" : "RedshiftConnection"}, ":5439/analyticsdwh"] ] },
          "Username": {"Ref": "RedshiftUserName"},
          "Password": {"Ref": "RedshiftPassword"},
          "RoleARN": { "Fn::Join" : [ "", [ "arn:aws:iam::", { "Ref" : "AWS::AccountId"},":role/", {"Ref": "FirehoseRoleName"} ] ] },
          "CopyCommand": {"CopyOptions": { "Fn::Join" : [ " ", [{"Ref":"CopyOptions"}, "MAXERROR AS", {"Ref" : "MaxLoadErrors"}]]}, "DataTableName": {"Ref": "DataTableName"}},
          "CloudWatchLoggingOptions": {"Enabled": "True", "LogGroupName": {"Ref": "LogGroup"}, "LogStreamName": {"Ref": "LogStreamRedshiftDelivery"}},
          "S3Configuration": {
            "BucketARN": { "Fn::Join" : [ ".", [ "arn:aws:s3:::data", {"Ref" : "S3Environment"}] ] },
            "BufferingHints": {"IntervalInSeconds": {"Ref":"IntervalInSeconds"}, "SizeInMBs": {"Ref": "SizeInMBs"}},
            "CloudWatchLoggingOptions": {"Enabled": "True", "LogGroupName": {"Ref": "LogGroup"}, "LogStreamName": {"Ref" : "LogStreamS3Delivery"}},
            "CompressionFormat": {"Ref":"CompressionFormat"},
            "Prefix": { "Fn::Join" : [ "/", [{"Ref": "S3Prefix"}, {"Ref" : "StreamName"}]]},
            "RoleARN": { "Fn::Join" : [ "", [ "arn:aws:iam::", { "Ref" : "AWS::AccountId"},":role/", {"Ref": "FirehoseRoleName"} ] ] }
          }

        }
      }
    },
    "LogGroup": {
      "Type": "AWS::Logs::LogGroup",
      "Properties": {
        "RetentionInDays": 7
      }
    },
    "LogStreamRedshiftDelivery" : {
      "Type": "AWS::Logs::LogStream",
      "Properties": {
        "LogGroupName": {"Ref": "LogGroup"},
        "LogStreamName": "RedshiftDelivery"
      }
    },
    "LogStreamS3Delivery" : {
      "Type": "AWS::Logs::LogStream",
      "Properties": {
        "LogGroupName": {"Ref": "LogGroup"},
        "LogStreamName": "S3Delivery"
      }
    }
  },

  "Outputs": {
    "StreamName": {"Description": "StreamName",  "Value": {"Ref": "Stream"}}
  },

  "Parameters": {
      "StreamName" : {"Type" : "String"},
      "DataTableName" : {"Type" : "String"},
      "RedshiftConnection": {"Type" : "String"},
      "RedshiftUserName": {"Type" : "String"},
      "RedshiftPassword": {"Type" : "String"},
      "S3Environment": {"Type": "String"},
      "S3Prefix" : {"Type": "String", "Default": "streamdata/kinesis"},
      "FirehoseRoleName": {"Type" : "String", "Default": "firehose_delivery_role"},
      "CopyOptions": {"Type": "String", "Default": "json 'auto'"},
      "MaxLoadErrors": {"Type": "String", "Default": "5"},
      "IntervalInSeconds": {"Type": "String", "Default": "300"},
      "SizeInMBs": {"Type": "String", "Default": "5"},
      "CompressionFormat": {"Type":"String", "Default" : "UNCOMPRESSED"}
  }

}
