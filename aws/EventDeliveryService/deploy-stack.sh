#!/usr/bin/env bash


aws s3 cp ${ENV_ANALYTICS_DEPLOY_PATH}/aws/EventDeliveryService/definition/templates s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/EventDeliveryService/definition/templates --recursive

aws cloudformation create-stack --stack-name analytics-EventDeliveryService \
  --region eu-west-1 \
  --template-url https://s3-eu-west-1.amazonaws.com/infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/EventDeliveryService/definition/templates/service.template \
  --parameters \
    ParameterKey=S3Environment,ParameterValue=${ENV_ANALYTICS_DEPLOY_BUCKET}
