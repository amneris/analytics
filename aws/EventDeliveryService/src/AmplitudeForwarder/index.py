import logging
import base64
import requests
import json
import time

from AWS import get_config

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):

    # Get Service configuration from DynamoDB
    try:
        conf = get_config('EventDeliveryService')['Outputs']['AmplitudeForwarder']
    except Exception as e:
        respond_error(errortype=Responses.INTERNALERROR, message="Service can not get its own configuration")

    logger.info('Records from Kinesis: {}'.format(event['Records']))
    # Loop for each records from the stream Records
    for records in event['Records']:

        src_event = json.loads(base64.b64decode(records['kinesis']['data']).decode("utf-8"))
        logger.info('source_event: {}'.format(src_event))

        # Authenticate the event to be forwarded
        if src_event['eventType'] in [item['eventType'] for item in conf["eventList"]]:

            dest_event = create_dest_event(src_event)
            url = conf["amplitudeAPIParams"]["url"]
            api_key = conf["amplitudeAPIParams"]["api_key"]

            amplitude_response = requests.post(url, data={"api_key": api_key, "event": json.dumps(dest_event)})

            if amplitude_response.status_code != 200:
                respond_error(errortype=amplitude_response.reason, message=amplitude_response._content.decode("utf-8"))

    return 'OK'


# Function to write the destination event
def create_dest_event(src_event) :

    # This part should be rewrite when we have more detailed information from payment events.
    user_id = src_event.pop("userId", None)
    event_type = src_event.pop("eventType", None)
    dest_event = [{"user_id": user_id, "event_type": event_type, "event_properties": src_event, "time": time.time()}]

    if event_type == 'paid':
        # Transform the event in a REVENUE-EVENT (Amplitude)
        dest_event[0]["revenue"] = src_event["amount_eur"]


    return dest_event


# Responses class definition
class Responses:
    BADREQUEST="BadRequest"
    FORBIDDEN="Forbidden"
    INTERNALERROR="InternalError"
    CORRECT = "Correct"


# Unify error message
def respond_error(errortype, message):
    response = "{0} message: {1}".format(errortype, message)
    logging.error(response)
    raise Exception(response)