import logging
import base64
import json
import boto3

from AWS import get_config

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    # Get Service configuration from DynamoDB
    try:
        conf = get_config('EventDeliveryService')['Outputs']['RedshiftForwarder']
    except Exception as e:
        respond_error(errortype=Responses.INTERNALERROR, message="Service can not get its own configuration")

    logger.info('Records from Kinesis: {}'.format(event['Records']))

    # Loop for each records from the stream Records
    for record in event['Records']:

        src_event = json.loads(base64.b64decode(record['kinesis']['data']).decode("utf-8"))

        #logger.info('event type recieved {}'.format(src_event['eventType']))
        logger.info('source_event: {}'.format(src_event))


        # Authenticate the event to be forwarded
        if src_event['eventType'] in [item['eventType'] for item in conf["eventList"]]:


            # Write correct structure for Redshift
            dest_event = create_dest_event(src_event)

            stream_name = 'Analytics_{}'.format(dest_event['event_type'])

            try:
                redshift_response = boto3.client('firehose', region_name='eu-west-1').put_record(
                    DeliveryStreamName=stream_name,
                    Record={'Data': json.dumps(dest_event)})

                if redshift_response['ResponseMetadata']['HTTPStatusCode'] != 200:
                    respond_error(errortype=redshift_response['ResponseMetadata']['HTTPStatusCode'],
                                  message=redshift_response['ResponseMetadata'])

            except Exception as e:

                respond_error(errortype=Responses.INTERNALERROR, message=e)

    return "Ok"


# Function to correct the data structure before the Data Stream Delivery
def create_dest_event(src_event):
    base_params = {
        "event_id": src_event.pop("eventId", None),
        "event_timestamp": src_event.pop("eventTimestamp", None),
        "user_id": src_event.pop("userId", None),
        "event_type": src_event.pop("eventType", None)
    }
    return {**base_params, **src_event}


# Responses class definition
class Responses:
    BADREQUEST = "BadRequest"
    FORBIDDEN = "Forbidden"
    INTERNALERROR = "InternalError"
    CORRECT = "Correct"


# Unify error message
def respond_error(errortype, message):
    response = "{0} message: {1}".format(errortype, message)
    logging.error(response)
    raise Exception(response)