import logging
import base64
import json
import boto3
from datetime import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

client = boto3.client('kinesis')
aws_lambda = boto3.client('lambda')


def lambda_handler(event, context):

    logging.info(event)
    record = event['Records']
    response = base64.b64decode(record[0]['kinesis']['data']).decode("utf-8")
    payment_event = json.loads(response)

    currency = payment_event['currency']
    amount = float(payment_event['originalAmount'])
    xRate = getXRate(currency)

    payment_event['amount_eur'] = amount / xRate
    payment_event['xRate'] = xRate

    client.put_record(
        StreamName='analytics-KinesisBackbone',
        Data=json.dumps(payment_event).encode('utf8'),
        PartitionKey='payment'
    )

    return 'OK'


import boto3
import json


def getXRate(currency):
    response = boto3.client('lambda').invoke(
        FunctionName='analytics-PaymentServiceCurrencyXChange',
        Payload=json.dumps({"currency": currency}).encode('utf8')
    )

    returned_value = response['Payload'].read().decode("utf-8")
    returned_value = eval(json.loads(returned_value))

    if 'Error' not in returned_value:
        return returned_value['xRate']
    else:
        return 'Que hacemos si no hay xRate'


