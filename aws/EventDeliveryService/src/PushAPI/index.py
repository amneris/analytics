import ipaddress
import json
import logging
import operator
import urllib
import uuid
import boto3


from AWS import get_config
from functools import reduce
from datetime import datetime


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):


    logger.info(event)
    source_ip = event["context"]["source-ip"]


    # Get Service configuration from DynamoDB
    try:
        conf = get_config('EventDeliveryService')
    except Exception as e:
        respond_error(errortype=Responses.INTERNALERROR, ip=source_ip, message="Service can not get its own configuration")

    # Authenticate the event, True if OK
    if not authenticate_request(event, conf):
        respond_error(errortype=Responses.FORBIDDEN, ip=source_ip, message="This request is forbidden")

    # Create base parameters for the event: eventId, eventTimestamp, userId, eventType and source_ip
    std_event = {
        "eventId": str(uuid.uuid1()),
        "eventTimestamp": str(datetime.now()),
        "userId" : event["params"]["querystring"].pop("user_id", None),
        "eventType": event["params"]["path"]["event_id"].lower(),
        "source_ip": event["context"]["source-ip"]
    }

    # Complete the event information depending on the event channel (web or app)
    if event["params"]["querystring"].get("auth", "none") == "web_attr_key" :
        std_event = {**web_enhancer(event), **std_event}
    else :
        std_event = app_enhancer(std_event, event)


    # Publish event to Kinesis Backbone
    try:
        boto3.client('kinesis').put_record(
            StreamName='analytics-KinesisBackbone',
            Data=json.dumps(std_event).encode('utf8'),
            PartitionKey=std_event['eventType']
        )
    except Exception as e:
        respond_error(errortype=Responses.BADREQUEST, ip=source_ip, message=str(e))

    return Responses.CORRECT




# Function to authenticate each request, depends on the event channel (web or app)
def authenticate_request(event, conf):

    if event["params"]["querystring"].get("auth", "none") == "web_attr_key":
        # For the moment, this authentication is dummy
        return True
    else:
        return event["context"]["source-ip"] in reduce(operator.concat, [get_ip_from_network(network['ip']) for network in conf['Inputs']['PushAPI']['ips_allowed']])

# Function to complete the information for each app event (querystring)
def app_enhancer(result_event, source_event):

    querystring = source_event["params"]["querystring"]
    return {**result_event, **querystring}

# Function to complete the information for each web event (params and manual inputs)
def web_enhancer(event):
    # Parse all the parameters from Web attribution
    track_params_str = urllib.parse.unquote(event["params"]["querystring"]["params"]).replace("unbounce|", "")
    track_params = {y[0]: y[1] for y in [x.split(":") for x in track_params_str.split(",")]}

    return {
        "os_name":          "web",
        "device_type":      "pc",

        "network_name":    track_params.pop("network", 'Unknown'),
        "campaign_name":   track_params.pop("cid", 'Unknown'),
        "adgroup_name":    track_params.pop("aid", 'Unknown'),
        "creative_name":   track_params.pop("creative", 'Unknown'),

        "user_ip":         event["context"]["source-ip"],
        "country_code":    event["params"]["header"]["CloudFront-Viewer-Country"].lower(),
    }

# Function to identify the ip from an app event
def get_ip_from_network(ip_network):
    network = ipaddress.ip_network(ip_network)
    ips = list(network.hosts())
    ips.append(network.network_address)
    return [str(ip) for ip in ips]

# Responses class definition
class Responses:
    BADREQUEST="BadRequest"
    FORBIDDEN="Forbidden"
    INTERNALERROR="InternalError"
    CORRECT = "Correct"

# Unify error message
def respond_error(ip, errortype, message):
    response = "{0} ip: {1} message: {2}".format(errortype, ip,  message)
    logging.warning(response)
    raise Exception(response)

