#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/EventDeliveryService/build
rm -f AmplitudeForwarder.zip
rm -f PaymentsEnhancer.zip
rm -f RedshiftForwarder.zip
rm -f PushAPI.zip


cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/LambdaZipMaker
sh build.sh EventDeliveryService AmplitudeForwarder
sh build.sh EventDeliveryService PaymentsEnhancer
sh build.sh EventDeliveryService RedshiftForwarder
sh build.sh EventDeliveryService PushAPI



aws s3 cp ${ENV_ANALYTICS_DEPLOY_PATH}/aws/EventDeliveryService/build \
s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/EventDeliveryService/build --recursive


aws lambda update-function-code --function-name analytics-EventDeliveryServiceAmplitudeForwarder \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key EventDeliveryService/build/AmplitudeForwarder.zip

aws lambda update-function-code --function-name analytics-EventDeliveryServicePaymentsEnhancer \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key EventDeliveryService/build/PaymentsEnhancer.zip

aws lambda update-function-code --function-name analytics-EventDeliveryServiceRedshiftForwarder \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key EventDeliveryService/build/RedshiftForwarder.zip

##Change this function name when possible
aws lambda update-function-code --function-name analytics-APIPushServicePutRecord \
--s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} \
--s3-key EventDeliveryService/build/PushAPI.zip
