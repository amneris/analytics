#!/usr/bin/env bash


aws cloudformation update-stack --stack-name analytics-SNS-monitorization-topics \
  --region eu-west-1 \
  --template-url https://s3-eu-west-1.amazonaws.com/infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/SNS/templates/SNStopics.template \
