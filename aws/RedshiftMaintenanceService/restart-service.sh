#!/usr/bin/env bash

node ../ConfigurationService/setup/setup.js config.json
aws s3 cp ./sql s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/RedshiftMaintenanceService/sql --recursive
aws lambda invoke --function-name analytics-DataPipelineFactory --payload '{"ServiceName":"RedshiftMaintenanceService"}' result.txt
