-- Vacuum and analyze all tables in Redshift to maintain a good performance

VACUUM;

ANALYZE;
