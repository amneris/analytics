INSERT INTO events.installs (
  event_id,
  install_timestamp,
  adjust_id,
  network_name,
  install_channel_id,
  install_country_id,
  campaign_id,
  adgroup_id,
  new_partner_id

)
  SELECT
    event_id,
    event_timestamp                                                                     AS install_timestamp,
    adjust_id,
    CASE
    WHEN network_name IN ('Facebook Installs', 'Instagram Installs', 'Off-Facebook Installs')
      THEN 'Facebook Installs'
    WHEN network_name IN ('Google Universal App Campaigns', 'Adwords UAC Installs')
      THEN 'Adwords UAC Installs'
    ELSE network_name END                                                               AS network_name,
    idchannel                                                                           AS install_channel_id,
    id                                                                                  AS install_country_id,

    CASE
    WHEN network_name IN
         ('Facebook Installs', 'Instagram Installs', 'Off-Facebook Installs', 'Apple Search Ads', 'Yahoo Gemini Installs', 'Adwords UAC Installs')
      THEN CAST(REGEXP_SUBSTR(REGEXP_REPLACE(campaign_name, '^.* ', ''), '[0-9]+') AS VARCHAR)
    WHEN network_name = 'Organic'
      THEN NULL
    ELSE CAST(campaign_name AS VARCHAR) END                                             AS campaign_id,

    CASE
    WHEN network_name IN
         ('Facebook Installs', 'Instagram Installs', 'Off-Facebook Installs', 'Apple Search Ads', 'Yahoo Gemini Installs')
      THEN CAST(REGEXP_SUBSTR(REGEXP_REPLACE(adgroup_name, '^.* ', ''), '[0-9]+') AS VARCHAR)
    WHEN network_name = 'Google Universal App Campaigns'
      THEN REGEXP_REPLACE(adgroup_name, ' .*', '')
    WHEN network_name = 'Organic'
      THEN NULL
    ELSE CAST(adgroup_name AS VARCHAR) END                                              AS adgroup_id,

    -- 1) Remove trailing id from the campaign name, for example "campaign___600277 (123)" --> "campaign___600277"
    -- 2) Split campaign name at three underscores and keep the second part, for example "campaign___600277" --> "600277"
    -- 3) If the result is numeric then use it, otherwise default to NULL
    CASE WHEN SPLIT_PART(REGEXP_REPLACE(campaign_name, ' .*', ''), '___', 2) ~ '^[0-9]+$'
      THEN SPLIT_PART(REGEXP_REPLACE(campaign_name, ' .*', ''), '___', 2) :: BIGINT END AS new_partner_id

  FROM events.app_installed
    LEFT JOIN staging_countries.country ON lower(country_code) = lower(iso)
    LEFT JOIN staging_partners.aba_partners_channels ON lower(os_name) = lower(namechannel)

  WHERE event_timestamp < (GETDATE() - INTERVAL '15 minutes')
        AND event_timestamp > (SELECT COALESCE(max(install_timestamp), '0000-01-01')
                               FROM events.installs)

  ORDER BY event_timestamp;

ANALYZE events.installs;
