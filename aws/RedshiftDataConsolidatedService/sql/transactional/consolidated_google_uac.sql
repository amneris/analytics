-- Script to fill consolidated investment for google adwords

START TRANSACTION;

DELETE FROM transactional.consolidated_google_uac
WHERE day IN (SELECT DISTINCT day FROM staging_investment.google_uac);

INSERT INTO transactional.consolidated_google_uac(
    day,
    campaign_id,
    campaign_name,
    ad_network,
    clicks,
    impressions,
    cost
)

SELECT
    day,
    campaign_id,
    campaign_name,
    ad_network,
    clicks,
    impressions,
    cost

FROM staging_investment.google_uac;

END TRANSACTION;

