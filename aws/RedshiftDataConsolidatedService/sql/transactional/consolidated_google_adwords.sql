-- Script to fill consolidated investment for google adwords

START TRANSACTION;

DELETE FROM transactional.consolidated_google_adwords
WHERE day IN (SELECT DISTINCT day FROM staging_investment.google_adwords);

INSERT INTO transactional.consolidated_google_adwords(
    day,
    campaign_id,
    campaign_name,
    adgroup_id,
    adgroup_name,
    clicks,
    impressions,
    cost
)

SELECT
    day,
    campaign_id,
    campaign_name,
    adgroup_id,
    adgroup_name,
    clicks,
    impressions,
    cost
FROM staging_investment.google_adwords;

END TRANSACTION;

