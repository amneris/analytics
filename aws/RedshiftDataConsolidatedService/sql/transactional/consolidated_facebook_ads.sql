-- Script to fill consolidated investment for facebook ads

START TRANSACTION;

DELETE FROM transactional.consolidated_facebook_ads
WHERE date_start IN (SELECT DISTINCT date_start FROM staging_investment.facebook_ads);


INSERT INTO transactional.consolidated_facebook_ads(
    campaign_id,
    campaign_name,
    adset_id,
    adset_name,
    clicks,
    impressions,
    spend,
    date_start,
    date_stop
)

SELECT
    campaign_id,
    campaign_name,
    adset_id,
    adset_name,
    clicks,
    impressions,
    spend,
    date_start,
    date_stop
FROM staging_investment.facebook_ads;

END TRANSACTION;
