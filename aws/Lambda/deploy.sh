#!/usr/bin/env bash


aws s3 cp ./definition/templates s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/Lambda/definition/templates --recursive
aws s3 cp ./build s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/Lambda/build --recursive


aws lambda update-function-code --function-name analytics-transformFunnelioJSON2CSV --s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} --s3-key Lambda/build/transformFunnelioJSON2CSV-production.zip