// dependencies
var async = require('async');
var AWS = require('aws-sdk');
var json2csv = require('json2csv');



exports.handler = function (event, context, callback) {

    const noError = null;
    const contentType = 'text/csv';

    var bucket = event.Records[0].s3.bucket.name;
    var inputKey = event.Records[0].s3.object.key;
    var outputKey =  "dynamic/staging_investment/investment/data.csv" //< inputKey + '.transformed';

    var fields = ['common-cost', 'common-clicks', 'day', 'campaign', 'channel'];

    var s3 = new AWS.S3();

    async.waterfall([
            function getFile(next) {


                var params = {
                    Bucket: bucket, // your bucket name,
                    Key: inputKey // path to the object you're looking for
                };


                s3.getObject(params, function(err, data) {

                    if(err) next(err, '');
                    next(noError, data.Body.toString('utf-8'));
                });

            },

            function transform(data, next) {

                json2csv({ data: JSON.parse(data).per_day, fields: fields}, function(err, result) {
                    if(err) next(err, '');
                    next(noError, result);
                });

            },


            function putFile(data, next) {
                s3.putObject({Bucket: bucket, Key: outputKey, Body: data, ContentType: contentType}, function(err, result) {
                    if(err) next(err, '');
                    next(noError);
                });
            }



        ], function (err) {
            if (err) {
                callback(err, "Unable to finish  due to an error.");
            } else {
                console.log('Success');
                callback(null, "Success");
            }
        }
    );
};


