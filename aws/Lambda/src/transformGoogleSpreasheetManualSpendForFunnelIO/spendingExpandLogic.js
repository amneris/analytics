
const DAYS_IN_MONTHS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const CSV_SEPARATOR = ',';

var today = new Date();


function getDaysToSplit(date) {

    var daysToSplit = DAYS_IN_MONTHS[date.getMonth()];
    if (date.setDate(daysToSplit) > today.getTime()) daysToSplit = Math.max(today.getDate()-1,1);
    return daysToSplit;

};


function splitCSVLine(line) {

    var splittedLine = line.split(CSV_SEPARATOR);
    return {

        day: new Date(splittedLine[0]),
        clicks: splittedLine[1],
        spend: splittedLine[2],
        camapignName: splittedLine[3],
        currency: splittedLine[4]

    }
}


module.exports = {
    
    createLines: function (line) {

        var splittedCSVLine = splitCSVLine(line);
        var daysToSplit = getDaysToSplit(splittedCSVLine.day);

        var result = [];
        for (i = 1; i <= daysToSplit; i++) {


            result.push([
                new Date(splittedCSVLine.day.setDate(i)).toISOString().slice(0, 10),
                splittedCSVLine.clicks / daysToSplit,
                splittedCSVLine.spend / daysToSplit,
                splittedCSVLine.camapignName,
                splittedCSVLine.currency
            ].join());
        }

        return result;

    }
    
};
