// dependencies
var async = require('async');
var AWS = require('aws-sdk');
var spendingExpandLogic = require('./spendingExpandLogic.js');


exports.handler = function (event, context, callback) {

    const noError = null;
    const contentType = 'text/csv';
    const publicRead = 'public-read';
    const lineBreak = '\r\n';

    var bucket = event.Records[0].s3.bucket.name;
    var inputKey = event.Records[0].s3.object.key;
    var outputKey = inputKey + '.transformed';

    var s3 = new AWS.S3();

    async.waterfall([
            function getFile(next) {


                var params = {
                    Bucket: bucket, // your bucket name,
                    Key: inputKey // path to the object you're looking for
                };
                

                s3.getObject(params, function(err, data) {

                    if(err) next(err, '');
                    next(noError, data.Body.toString('utf-8'));
                });

            },

            function transform(data, next) {

                var spendingLines = data.split(lineBreak)
                var result = spendingLines[0];

                

                for(j=1; j < spendingLines.length; j++) {
                    result += lineBreak + spendingExpandLogic.createLines(spendingLines[j]).join(lineBreak);
                }


                next(noError, result);

            },


            function putFile(data, next) {
                s3.putObject({Bucket: bucket, Key: outputKey, Body: data, ContentType: contentType, ACL: publicRead}, next);
                //console.log(data);
            }

        ], function (err) {
            if (err) {
                callback(err, "Unable to finish  due to an error.");
            } else {
                console.log('Success');
                callback(null, "Success");
            }
        }
    );

};




