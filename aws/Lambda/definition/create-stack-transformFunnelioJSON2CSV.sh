#!/usr/bin/env bash


aws cloudformation create-stack --stack-name analytics-lambda-transformFunnelioJSON2CSV \
  --region eu-west-1 \
  --template-body file://./templates/transformFunnelioJSON2CSV.template \
  --parameters file://./parameters/${ENV_ANALYTICS_DEPLOY_ENVIRONMENT}.json
