#!/usr/bin/env bash

node ../ConfigurationService/setup/setup.js config.json
aws lambda invoke --function-name analytics-DataPipelineFactory \
--payload '{"ServiceName":"SQLQueryService"}' result.txt