#!/usr/bin/env bash

if [ -z "$1" ]; then
echo "Usage: $0 <ServiceName>"

else
echo "Building zip for service $1 ..."

    if [ -d "${ENV_ANALYTICS_DEPLOY_PATH}/aws/$1/src/$2" ]; then

    cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/$1/src/$2
    zip -r ${ENV_ANALYTICS_DEPLOY_PATH}/aws/$1/build/$2.zip *.py

    cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/lib
    zip -ur ${ENV_ANALYTICS_DEPLOY_PATH}/aws/$1/build/$2.zip *

    else
    echo "Service $1 not found"
    fi
fi