#!/usr/bin/env bash


aws cloudformation create-stack --stack-name analytics-DataPipelineFactory \
  --region eu-west-1 \
  --template-url https://s3-eu-west-1.amazonaws.com/infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/DataPipelineFactory/definition/templates/DataPipelineFactory.template \
  --parameters file://./definition/parameters/${ENV_ANALYTICS_DEPLOY_ENVIRONMENT}.json


