#!/usr/bin/env bash

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/DataPipelineFactory/src/; sh build.sh;

cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/NotificationService; sh build.sh;
cd ${ENV_ANALYTICS_DEPLOY_PATH}/aws/DataPipelineFactory/;

aws s3 cp ./definition/templates s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/DataPipelineFactory/definition/templates --recursive;
aws s3 cp ./build s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/DataPipelineFactory/build --recursive;

aws lambda update-function-code --function-name analytics-DataPipelineFactory --s3-bucket infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET} --s3-key DataPipelineFactory/build/analytics-DataPipelineFactory-production.zip