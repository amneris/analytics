#!/usr/bin/env bash

node-lambda package
aws s3 cp build/transferToS3FromArbitraryURL-development.zip s3://analytics.aba.land/lambda/source/transferToS3FromArbitraryURL-development.zip