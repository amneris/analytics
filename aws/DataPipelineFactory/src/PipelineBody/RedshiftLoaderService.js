const util = require('util');

var PipelineElementsBuilder = require('./PipelineElementsBuilder.js')
var dynamoConfig = {};




module.exports = RedshiftLoaderService;


function RedshiftLoaderService(conf) {
    this.dynamoConfig = conf;


};


RedshiftLoaderService.prototype.getPipelineBody = function () {
    

    var result = [];
    var pipelineElementsBuilder = new PipelineElementsBuilder();
    var tablesToLoad = this.dynamoConfig.service.tablesToLoad;

    var nameConstructor = function (element, params) {return util.format('%s_%s_%s', element, params.schema, params.tableName)};
    var workerGroupRange = this.dynamoConfig.service.workerGroupRange;

    for (i = 0; i < tablesToLoad.length; i++) {

        var table = tablesToLoad[i];

        var S3DataNode = pipelineElementsBuilder.getS3DataNode(table, nameConstructor);
        var RedshiftDataNode = pipelineElementsBuilder.getRedshiftDataNode(table, nameConstructor);
        var CopyActivity = pipelineElementsBuilder.getCopyActivity(table, nameConstructor, workerGroupRange);


        result.push(S3DataNode);
        result.push(RedshiftDataNode);
        result.push(CopyActivity);

    }

    return result;

}




