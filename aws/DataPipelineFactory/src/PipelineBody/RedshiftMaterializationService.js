const util = require('util');

var PipelineElementsBuilder = require('./PipelineElementsBuilder.js')
var dynamoConfig = {};


module.exports = RedshiftMaterializationService;


function RedshiftMaterializationService(conf) {
    this.dynamoConfig = conf;
};


RedshiftMaterializationService.prototype.getPipelineBody = function () {

    var result = [];
    var pipelineElementsBuilder = new PipelineElementsBuilder();

    var scriptsToExecute = this.dynamoConfig.service.scriptsToExecute;
    var nameConstructor = function (element, params) {return util.format('%s_%s', element,params.fileName.replace(".sql", ""))};
    var workerGroupRange = this.dynamoConfig.service.workerGroupRange;



    for (i = 0; i < scriptsToExecute.length; i++) {
        result.push(pipelineElementsBuilder.getSQLActivity(scriptsToExecute[i], nameConstructor, workerGroupRange));

    }
    return result;

}






