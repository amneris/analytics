const util = require('util');


const ACTIVITIES = {
    S3DATANODE : 'S3DataNode',
    REDSHIFTDATANODE : 'RedshiftDataNode',
    REDSHIFTCOPYACTIVITY : 'RedshiftCopyActivity',
    SQLACTIVITY : 'SqlActivity',
    SQLDATANODE : 'SqlDataNode',
    COPYACTIVITY: 'CopyActivity'

}

var PipelineDefinitionBuilder = require('../PipelineDefinitionBuilder.js')



module.exports = PipelineElementsBuilder;


getWorkerGroupName = function (workerGroupRange) {

     low = parseInt(workerGroupRange.low);
     high = parseInt(workerGroupRange.high);
     wg = Math.floor(Math.random() * (high - low + 1) + low);
     return util.format('#{myWorkerGroup}%s', wg)

};



function PipelineElementsBuilder() {};



PipelineElementsBuilder.prototype.getS3DataNode = function (params, nameConstructor) {

   return {

        id: nameConstructor(ACTIVITIES.S3DATANODE, params),
        name: nameConstructor(ACTIVITIES.S3DATANODE, params),
        fields: [
            {key: "parent",     refValue: "Default"},
            {key: "type", stringValue: ACTIVITIES.S3DATANODE},

            {key: "directoryPath", stringValue: util.format('#{myS3DataBucket}/%s/%s/%s/', params.s3Key, params.schema, params.tableName)}



        ]
    }
};


// S3 Node to Write
PipelineElementsBuilder.prototype.getS3DataNodeToWrite = function (params, nameConstructor) {

    return {

        id: nameConstructor(ACTIVITIES.S3DATANODE, params),
        name: nameConstructor(ACTIVITIES.S3DATANODE, params),
        fields: [
            {key: "parent",     refValue: "Default"},
            {key: "type", stringValue: ACTIVITIES.S3DATANODE},

            {key: "filePath", stringValue: util.format('#{myS3DataBucket}/%s/%s/%s/data.csv', params.s3Key, params.schema, params.tableName)}

        ]
    }
};




PipelineElementsBuilder.prototype.getRedshiftDataNode = function(params, nameConstructor) {

    return {
        id: nameConstructor(ACTIVITIES.REDSHIFTDATANODE, params),
        name: nameConstructor(ACTIVITIES.REDSHIFTDATANODE, params),
        fields : [
            {key: "parent",     refValue: "Default"},
            {key: "type", stringValue: ACTIVITIES.REDSHIFTDATANODE},

            {key: "database", refValue: "RedshiftCluster"},
            {key: "schemaName", stringValue: params.schema},
            {key: "tableName", stringValue: params.tableName}

        ]
    }

};




PipelineElementsBuilder.prototype.getCopyActivity =  function (params, nameConstructor, workerGroupRange) {

    var elementName = nameConstructor(ACTIVITIES.REDSHIFTCOPYACTIVITY, params) ;

    return {
        id: nameConstructor(ACTIVITIES.REDSHIFTCOPYACTIVITY, params),
        name: nameConstructor(ACTIVITIES.REDSHIFTCOPYACTIVITY, params),
        fields: [
            {key: "parent",     refValue: "Default"},
            {key: "type", stringValue: ACTIVITIES.REDSHIFTCOPYACTIVITY},


            {key: "input", refValue: nameConstructor(ACTIVITIES.S3DATANODE, params)},
            {key: "output", refValue: nameConstructor(ACTIVITIES.REDSHIFTDATANODE, params)},
            {key: "insertMode", stringValue: params.insertMode},
            {key: "commandOptions", stringValue: params.commandOptions},
            {key: "workerGroup",     stringValue: getWorkerGroupName(workerGroupRange)}

        ]
    }


};



PipelineElementsBuilder.prototype.getSQLActivity = function (params, nameConstructor, workerGroupRange) {

    return {


        id: nameConstructor(ACTIVITIES.SQLACTIVITY, params),
        name: nameConstructor(ACTIVITIES.SQLACTIVITY, params),
        fields : [
            {key: "parent",     refValue: "Default"},
            {key: "type",       stringValue: ACTIVITIES.SQLACTIVITY},

            {key: "scriptUri",  stringValue: util.format("#{myS3InfrastructureBucket}/%s/%s", params.s3Key, params.fileName)},
            {key: "workerGroup",     stringValue: getWorkerGroupName(workerGroupRange)}


        ]

    }

};



PipelineElementsBuilder.prototype.getSqlDataNode = function (params, nameConstructor) {


    return {


        id: nameConstructor(ACTIVITIES.SQLDATANODE, params),
        name: nameConstructor(ACTIVITIES.SQLDATANODE, params),
        fields : [
            {key: "parent",     refValue: "Default"},
            {key: "type",           stringValue: ACTIVITIES.SQLDATANODE},

            {key: "database",   refValue: "RDSSlaveDatabase"},
            {key: "table",          stringValue: util.format("%s", params.table)},
            {key: "selectQuery",    stringValue: util.format("%s", params.sqlquery)}
        ]

    }

};


PipelineElementsBuilder.prototype.getRDSCopyActivity = function (params, nameConstructor, workerGroupRange) {


    return {


        id: nameConstructor(ACTIVITIES.COPYACTIVITY, params),
        name: nameConstructor(ACTIVITIES.COPYACTIVITY, params),
        fields : [
            {key: "parent",     refValue: "Default"},
            {key: "type",      stringValue: ACTIVITIES.COPYACTIVITY},

            {key: "input",     refValue: nameConstructor(ACTIVITIES.SQLDATANODE, params)},
            {key: "output",    refValue: nameConstructor(ACTIVITIES.S3DATANODE, params)},
            {key: "workerGroup",     stringValue: getWorkerGroupName(workerGroupRange)}



        ]

    }

};
