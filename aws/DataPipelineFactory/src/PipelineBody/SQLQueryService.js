const util = require('util');

var PipelineElementsBuilder = require('./PipelineElementsBuilder.js')
var dynamoConfig = {};




module.exports = SQLQueryService;


function SQLQueryService(conf) {
    this.dynamoConfig = conf;


};


SQLQueryService.prototype.getPipelineBody = function () {
    

    var result = [];
    var pipelineElementsBuilder = new PipelineElementsBuilder();
    var queriesToExecute = this.dynamoConfig.service.queriesToExecute;
    var workerGroupRange = this.dynamoConfig.service.workerGroupRange;

var nameConstructor = function (element, params) {return util.format('%s_%s_%s', element, params.schema, params.tableName)};


    for (i = 0; i < queriesToExecute.length; i++) {

        var table = queriesToExecute[i];


        var SqlDataNode = pipelineElementsBuilder.getSqlDataNode(table,nameConstructor);
        var RDSCopyActivity = pipelineElementsBuilder.getRDSCopyActivity(table,nameConstructor, workerGroupRange);
        var S3DataNodeToWrite = pipelineElementsBuilder.getS3DataNodeToWrite(table,nameConstructor);


        result.push(SqlDataNode);
        result.push(RDSCopyActivity);
        result.push(S3DataNodeToWrite);

    }

    return result;

}




