// dependencies
const util = require('util');
var async = require('async');
var AWS = require('aws-sdk');
var lodash = require('lodash/core');
var config = require('./config.js');


var PipelineDefinitionBuilder = require('./PipelineDefinitionBuilder.js');



var datapipeline = new AWS.DataPipeline();
var dynamodb = new AWS.DynamoDB();
var dynamodbMarshaler = require('dynamodb-marshaler');



exports.handler = function (event, context, callback) {

    const noError = null;

    var dynamoConfig = new Object();
    var serviceName = event.ServiceName;
    
    
    async.waterfall([


            function readCommonConfigFromDynamoDB(next) {
                dynamodb.batchGetItem(config.dynamoSettings, function(err, data) {
                    if (err) next(err, err.stack);
                    else {
                        var common = {};
                        data.Responses.AnalyticsConfig.forEach( item => common = Object.assign(
                            common, JSON.parse(dynamodbMarshaler.unmarshalJson(item))));
                        dynamoConfig.common = common;
                        next(noError);

                    }
                });
            },



            function readServiceConfigFromDynamoDB(next) {

                var params = {
                    TableName : 'AnalyticsConfig',
                    Key: {"id": {S: serviceName}}

                };



                dynamodb.getItem(params, function(err, data) {
                    if (err) console.log(err);
                    else {

                        dynamoConfig.service = JSON.parse(dynamodbMarshaler.unmarshalJson(data.Item));
                        next(noError);

                    }
                });


            },

 //     //    function test(next) {

 //             var def = new PipelineDefinitionBuilder(serviceName, dynamoConfig);


 //             o = dynamoConfig.service.workerGroupRange.low;
 //             console.log(JSON.stringify(o));

 //             //


 //             //var params = {
 //             //    pipelineId : 123, //data.pipelineId,
 //             //    pipelineObjects : def.getPipelineObjects(),
 //             //    parameterObjects: def.getParameterObjects(),
 //             //    parameterValues: def.getValuesObjects()
 //             //}
///
 //             //o = params;
 //             //console.log(JSON.stringify(o));

 //         },



            function findCurrentPipelineId(next) {


                datapipeline.listPipelines(null, function(err, data) {
                    if (err) next(err);

                    else {

                        var pipeline = lodash.filter(data.pipelineIdList, {name: dynamoConfig.service.pipelineName })[0]  ;

                        if(pipeline) {
                            next(noError, pipeline.id);
                        } else {
                            next(noError, null);
                        }

                    }
                });
            },

            function deleteCurrentPipeline (pipelineId, next) {


                if(pipelineId) {
                    datapipeline.deletePipeline({pipelineId: pipelineId}, function(err, data) {
                        if (err) next(err);
                        else     next(noError);
                    });
                } else {
                    next(noError);
                }
            },

            function createPipeline(next) {

                
                var params = {

                    name: dynamoConfig.service.pipelineName,
                    uniqueId: dynamoConfig.service.pipelineName + new Date().getTime(),
                    description: dynamoConfig.service.pipelineDescription,
                    tags: dynamoConfig.service.pipelineTags

                };
                
                
                
                datapipeline.createPipeline(params, function(err, data) {
                    if (err) next(err);
                    else     next(noError, data);
                });
                

            },


            function putPipelineDefinition(data, next) {

                
                
                var def = new PipelineDefinitionBuilder(serviceName, dynamoConfig);

                var params = {
                    pipelineId : data.pipelineId,
                    pipelineObjects : def.getPipelineObjects(),
                    parameterObjects: def.getParameterObjects(),
                    parameterValues: def.getValuesObjects()
                }



                datapipeline.putPipelineDefinition(params, function(err, data) {

                    if (err) next(err, err.stack);
                    else     next(noError, data);



                });

            }


        ], function (err) {
            if (err) {
                callback(err, "Unable to finish  due to an error.");
            } else {
                console.log('Success');
                callback(noError, "Success");
            }
        }
    );
};

