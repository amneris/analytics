var PipelineCommonElementsClass = require('./PipelineCommonElements/PipelineCommonElements.js');
const util = require('util');

const PIPELINE_BODY_CLASSPATH = './PipelineBody/';
const JS = '.js';




var dynamoConfig = {};
var pipelineBodyBuilder = {};
var pipelineCommonElements = {};



module.exports = PipelineDefinitionBuilder;


function PipelineDefinitionBuilder(event, conf) {

    this.dynamoConfig = conf;
    this.pipelineCommonElements = new PipelineCommonElementsClass();

    var PipelineBodyBuilderClass = require(PIPELINE_BODY_CLASSPATH + event + JS);
    this.pipelineBodyBuilder = new PipelineBodyBuilderClass(conf);


    
};


PipelineDefinitionBuilder.prototype.getPipelineObjects = function () {


    var pipelineObjects = this.pipelineCommonElements.getPipelineHeader();
    var pipelineBody = this.pipelineBodyBuilder.getPipelineBody();
    return pipelineObjects.concat(pipelineBody);

    
};



PipelineDefinitionBuilder.prototype.getParameterObjects = function () {


    return this.pipelineCommonElements.getPipelineParametersObjects();
    
    
};


PipelineDefinitionBuilder.prototype.getValuesObjects = function () {

    return [
        {id: "myPipelineLogUri", stringValue: util.format("%s/DataPipeline/%s", this.dynamoConfig.common.logs, this.dynamoConfig.service.id)},


        {id: "myRedshiftJdbcConnectStr", stringValue: this.dynamoConfig.common.RedshiftJdbcConnectStr},
        {id: "myRedshiftPassword", stringValue: this.dynamoConfig.common.RedshiftPassword},
        {id: "myRedshiftUsername", stringValue: this.dynamoConfig.common.RedshiftUsername},


        {id: "myRDSJdbcConnectStr", stringValue: this.dynamoConfig.common.RDSJdbcConnectStr},
        {id: "myRDSPassword", stringValue: this.dynamoConfig.common.RDSPassword},
        {id: "myRDSUsername", stringValue: this.dynamoConfig.common.RDSUsername},


        {id: "mySchedulePeriod", stringValue: this.dynamoConfig.service.SchedulePeriod },
        {id: "myWorkerGroup", stringValue: this.dynamoConfig.service.workerGroup},

        {id: "myARNTopic", stringValue: util.format("%s:Analytics_%s", this.dynamoConfig.common.arnPrefix, this.dynamoConfig.service.id)},
        {id: "myS3DataBucket", stringValue: this.dynamoConfig.common.data},
        {id: "myS3InfrastructureBucket", stringValue: this.dynamoConfig.common.infrastructure}

    ]


};


