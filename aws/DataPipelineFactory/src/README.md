# transferToS3FromArbitraryURL

 * install node-lambda
 
 https://www.npmjs.com/package/node-lambda
 
 ```
 npm install -g node-lambda
 ``` 

 * initial setup
 ```
 node-lambda setup
 ```

 * package
 
 ```
  node-lambda package -e production 
 ```
 
 * deploy
 
 ```
  aws lambda update-function-code --function-name transferToS3FromArbitraryURL --zip-file fileb://./build/transferToS3FromArbitraryURL-production.zip
 ```
 