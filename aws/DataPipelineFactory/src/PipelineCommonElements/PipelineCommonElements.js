

module.exports = PipelineCommonElements;


function PipelineCommonElements() {};

PipelineCommonElements.prototype.getPipelineHeader = function () {

    
    return [

        {
            id: "Default",
            name: "Default",
            fields : [
                {key: "type", stringValue: "Default"},
                {key: "failureAndRerunMode",  stringValue: "CASCADE"},
                {key: "resourceRole",  stringValue: "DataPipelineDefaultResourceRole"},
                {key: "role",  stringValue: "DataPipelineDefaultRole"},
                {key: "pipelineLogUri",  stringValue: "#{myPipelineLogUri}"},
                {key: "scheduleType",  stringValue: "cron"},
                {key: "schedule", refValue : "Schedule"},
                {key: "lateAfterTimeout", stringValue : "59 minutes"},
                {key: "onLateAction", refValue : "Terminate"},
                {key: "maxActiveInstances", stringValue : "1"},

                {key: "workerGroup",     stringValue: "#{myWorkerGroup}"},

                {key: "database",   refValue: "RedshiftCluster"},
                {key: "onFail",     refValue: "SNSFailureAlert"}



            ]
        },
        {
            id: "Terminate",
            name: "Terminate",
            fields: [{key: "type", stringValue: "Terminate"}]
        },
        {
            id: "SNSFailureAlert",
            name: "SNSFailureAlert",
            fields: [
                {key: "type", stringValue: "SnsAlarm"},
                {key: "subject", stringValue: "DataPipelineMonitor"},
                {key: "topicArn", stringValue: "#{myARNTopic}"},
                {key: "message", stringValue: "Pipeline Object Failed: #{node}, #{node.@pipelineId}, #{node.@error}"}
            ]
        },
        {
            id: "RedshiftCluster",
            name: "RedshiftCluster",
            fields : [
                {key: "type", stringValue: "RedshiftDatabase"},
                {key: "connectionString", stringValue: "#{myRedshiftJdbcConnectStr}"},
                {key: "*password", stringValue: "#{myRedshiftPassword}"},
                {key: "username", stringValue: "#{myRedshiftUsername}"}

            ]
        },
        {
            id: "RDSSlaveDatabase",
            name: "RDSSlaveDatabase",
            fields : [
                {key: "type", stringValue: "JdbcDatabase"},
                {key: "jdbcProperties", stringValue: "allowMultiQueries=true"},
                {key: "jdbcDriverClass", stringValue: "com.mysql.jdbc.Driver"},
                {key: "connectionString", stringValue: "#{myRDSJdbcConnectStr}?zeroDateTimeBehavior=convertToNull"},
                {key: "*password", stringValue: "#{myRDSPassword}"},
                {key: "username", stringValue: "#{myRDSUsername}"}
            ]
        },
        {id: "Schedule",
            name: "Schedule",
            fields: [
                {key: "type", stringValue: "Schedule"},
                {key: "period", stringValue: "#{mySchedulePeriod}"},
                {key: "startAt", stringValue: "FIRST_ACTIVATION_DATE_TIME"}
            ]
        }

    ]
};


PipelineCommonElements.prototype.getPipelineParametersObjects = function() {

    return [
        {id: "myPipelineLogUri", attributes: [{key: "type", stringValue:"String"}]},
        {id: "myRedshiftJdbcConnectStr", attributes: [{key: "type", stringValue:"String"}]},
        {id: "myRedshiftPassword", attributes: [{key: "type", stringValue:"String"}]},
        {id: "myRedshiftUsername", attributes: [{key: "type", stringValue:"String"}]},

        {id: "mySchedulePeriod", attributes: [{key: "type", stringValue:"String"}]},
        {id: "myRDSJdbcConnectStr", attributes: [{key: "type", stringValue:"String"}]},
        {id: "myRDSPassword", attributes: [{key: "type", stringValue:"String"}]},
        {id: "myRDSUsername", attributes: [{key: "type", stringValue:"String"}]},

        {id: "myWorkerGroup", attributes: [{key: "type", stringValue:"String"}]},
        {id: "myARNTopic", attributes: [{key: "type", stringValue:"String"}]},
        {id: "myS3DataBucket", attributes: [{key: "type", stringValue:"String"}]},
        {id: "myS3InfrastructureBucket", attributes: [{key: "type", stringValue:"String"}]}
        
    ]
    
}