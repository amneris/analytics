module.exports = {

    /*
    params: {
        name: 'RedshiftLoaderService',
        uniqueId: 'RedshiftLoaderService-' + new Date().getTime(),
        description: 'RedshiftLoaderService',
        tags: [
            {key: 'Service', value: 'RedshiftLoader'},
            {key: 'Area', value: 'Analytics'}
        ]
    },

*/
    dynamoSettings: {

        RequestItems: {
            "AnalyticsConfig": {
                Keys: [
                    {"id": {S: "RedshiftConnection"}},
                    {"id": {S: "NotificationService"}},
                    //{"id": {S: "TaskRunner"}},
                    {"id": {S: "S3Buckets"}},
                    {"id": {S: "RDSConnection"}}
                ]
            }


        }
    }
}
    

 