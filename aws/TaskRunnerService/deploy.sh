#!/usr/bin/env bash

aws s3 cp ./definition/templates s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/TaskRunnerService/definition/templates --recursive
aws s3 cp s3://infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/TaskRunnerService/definition/parameters/params.json ./definition/parameters/params.json


aws cloudformation create-stack --stack-name analytics-TaskRunnerService \
  --region eu-west-1 \
  --template-url https://s3-eu-west-1.amazonaws.com/infrastructure.${ENV_ANALYTICS_DEPLOY_BUCKET}/TaskRunnerService/definition/templates/ec2instance.template \
  --parameters file://./definition/parameters/params.json \
  --capabilities CAPABILITY_NAMED_IAM






